//=============================================================================
// resource file path
// 2019/02/18
// 21:37:18
//=============================================================================
#pragma once

//=============================================================================
// filename extension [.png]
//=============================================================================

#ifndef RESOURCE_Burst01_png
#define RESOURCE_Burst01_png "resource/Effekseer/Texture/Burst01.png"
#endif // !RESOURCE_Burst01_png
#ifndef RESOURCE_Cloud01_png
#define RESOURCE_Cloud01_png "resource/Effekseer/Texture/Cloud01.png"
#endif // !RESOURCE_Cloud01_png
#ifndef RESOURCE_Flame01_png
#define RESOURCE_Flame01_png "resource/Effekseer/Texture/Flame01.png"
#endif // !RESOURCE_Flame01_png
#ifndef RESOURCE_LaserMain01_png
#define RESOURCE_LaserMain01_png "resource/Effekseer/Texture/LaserMain01.png"
#endif // !RESOURCE_LaserMain01_png
#ifndef RESOURCE_LaserMain02_png
#define RESOURCE_LaserMain02_png "resource/Effekseer/Texture/LaserMain02.png"
#endif // !RESOURCE_LaserMain02_png
#ifndef RESOURCE_Particle01_png
#define RESOURCE_Particle01_png "resource/Effekseer/Texture/Particle01.png"
#endif // !RESOURCE_Particle01_png
#ifndef RESOURCE_Particle02_png
#define RESOURCE_Particle02_png "resource/Effekseer/Texture/Particle02.png"
#endif // !RESOURCE_Particle02_png
#ifndef RESOURCE_Particle03_png
#define RESOURCE_Particle03_png "resource/Effekseer/Texture/Particle03.png"
#endif // !RESOURCE_Particle03_png
#ifndef RESOURCE_Particle04_bokashistrong_hard_png
#define RESOURCE_Particle04_bokashistrong_hard_png "resource/Effekseer/Texture/Particle04_bokashistrong_hard.png"
#endif // !RESOURCE_Particle04_bokashistrong_hard_png
#ifndef RESOURCE_Particle04_bokashistrong_soft_png
#define RESOURCE_Particle04_bokashistrong_soft_png "resource/Effekseer/Texture/Particle04_bokashistrong_soft.png"
#endif // !RESOURCE_Particle04_bokashistrong_soft_png
#ifndef RESOURCE_Particle04_bokashi_hard_png
#define RESOURCE_Particle04_bokashi_hard_png "resource/Effekseer/Texture/Particle04_bokashi_hard.png"
#endif // !RESOURCE_Particle04_bokashi_hard_png
#ifndef RESOURCE_Particle04_bokashi_soft_png
#define RESOURCE_Particle04_bokashi_soft_png "resource/Effekseer/Texture/Particle04_bokashi_soft.png"
#endif // !RESOURCE_Particle04_bokashi_soft_png
#ifndef RESOURCE_Particle04_clear_hard_png
#define RESOURCE_Particle04_clear_hard_png "resource/Effekseer/Texture/Particle04_clear_hard.png"
#endif // !RESOURCE_Particle04_clear_hard_png
#ifndef RESOURCE_Particle04_clear_soft_png
#define RESOURCE_Particle04_clear_soft_png "resource/Effekseer/Texture/Particle04_clear_soft.png"
#endif // !RESOURCE_Particle04_clear_soft_png
#ifndef RESOURCE_Ring01_png
#define RESOURCE_Ring01_png "resource/Effekseer/Texture/Ring01.png"
#endif // !RESOURCE_Ring01_png
#ifndef RESOURCE_Splash01_png
#define RESOURCE_Splash01_png "resource/Effekseer/Texture/Splash01.png"
#endif // !RESOURCE_Splash01_png
#ifndef RESOURCE_SwordLine01_png
#define RESOURCE_SwordLine01_png "resource/Effekseer/Texture/SwordLine01.png"
#endif // !RESOURCE_SwordLine01_png
#ifndef RESOURCE_Thunder01_png
#define RESOURCE_Thunder01_png "resource/Effekseer/Texture/Thunder01.png"
#endif // !RESOURCE_Thunder01_png
#ifndef RESOURCE_Wind01_png
#define RESOURCE_Wind01_png "resource/Effekseer/Texture/Wind01.png"
#endif // !RESOURCE_Wind01_png
#ifndef RESOURCE_wind02_png
#define RESOURCE_wind02_png "resource/Effekseer/Texture/wind02.png"
#endif // !RESOURCE_wind02_png

//=============================================================================
// filename extension [.DDS]
//=============================================================================

#ifndef RESOURCE_LoadTex_DDS
#define RESOURCE_LoadTex_DDS "resource/texture/Load/LoadTex.DDS"
#endif // !RESOURCE_LoadTex_DDS
#ifndef RESOURCE_NowLoading_DDS
#define RESOURCE_NowLoading_DDS "resource/texture/Load/NowLoading.DDS"
#endif // !RESOURCE_NowLoading_DDS

//=============================================================================
// filename extension [.x]
//=============================================================================

#ifndef RESOURCE_Box_x
#define RESOURCE_Box_x "resource/Model/Box.x"
#endif // !RESOURCE_Box_x

//=============================================================================
// filename extension [.bmp]
//=============================================================================

#ifndef RESOURCE_画像表示枠サイズ_bmp
#define RESOURCE_画像表示枠サイズ_bmp "画像表示枠サイズ.bmp"
#endif // !RESOURCE_画像表示枠サイズ_bmp

//=============================================================================
// filename extension [.fx]
//=============================================================================

#ifndef RESOURCE_PerPixelLighting_fx
#define RESOURCE_PerPixelLighting_fx "resource/Shader/PerPixelLighting.fx"
#endif // !RESOURCE_PerPixelLighting_fx
#ifndef RESOURCE_Reflect_fx
#define RESOURCE_Reflect_fx "resource/Shader/Reflect.fx"
#endif // !RESOURCE_Reflect_fx
#ifndef RESOURCE_Shadow_fx
#define RESOURCE_Shadow_fx "resource/Shader/Shadow.fx"
#endif // !RESOURCE_Shadow_fx
#ifndef RESOURCE_Sprite2D_fx
#define RESOURCE_Sprite2D_fx "resource/Shader/Sprite2D.fx"
#endif // !RESOURCE_Sprite2D_fx
#ifndef RESOURCE_ToonShader_fx
#define RESOURCE_ToonShader_fx "resource/Shader/ToonShader.fx"
#endif // !RESOURCE_ToonShader_fx
#ifndef RESOURCE_Wave_fx
#define RESOURCE_Wave_fx "resource/Shader/Wave.fx"
#endif // !RESOURCE_Wave_fx

//=============================================================================
// filename extension [.efk]
//=============================================================================

#ifndef RESOURCE_b_square_efk
#define RESOURCE_b_square_efk "resource/Effekseer/b_square.efk"
#endif // !RESOURCE_b_square_efk
#ifndef RESOURCE_distortion_efk
#define RESOURCE_distortion_efk "resource/Effekseer/distortion.efk"
#endif // !RESOURCE_distortion_efk
#ifndef RESOURCE_g_square_efk
#define RESOURCE_g_square_efk "resource/Effekseer/g_square.efk"
#endif // !RESOURCE_g_square_efk
#ifndef RESOURCE_r_square_efk
#define RESOURCE_r_square_efk "resource/Effekseer/r_square.efk"
#endif // !RESOURCE_r_square_efk

//=============================================================================
// filename extension [.ttc]
//=============================================================================

#ifndef RESOURCE_meiryo_ttc
#define RESOURCE_meiryo_ttc "resource/font/meiryo/meiryo.ttc"
#endif // !RESOURCE_meiryo_ttc
#ifndef RESOURCE_meiryob_ttc
#define RESOURCE_meiryob_ttc "resource/font/meiryo/meiryob.ttc"
#endif // !RESOURCE_meiryob_ttc
