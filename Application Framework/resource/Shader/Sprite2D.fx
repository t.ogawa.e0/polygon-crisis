//==========================================================================
// 2D描画の基礎シェーダー[Sprite2D.fx]
// author: tatsuya ogawa
//==========================================================================

//==========================================================================
// グローバル変数
//==========================================================================
float4x4 Projection; // プロジェクション変換行列
float4x4 World; // ワールド行列
texture Texture; // テクスチャ
float uvLeft; // 左UV
float uvTop; // 上UV
float uvWidth; // 右UV
float uvHeight; // 下UV
float4 Color; // 色
bool TexFlag; // テクスチャのありなし false:なし true:あり

//==========================================================================
// サンプラー
//==========================================================================
sampler Sampler = sampler_state
{
    texture = <Texture>;
};

//==========================================================================
// 入力用構造体
//==========================================================================
struct vsInput
{
    float3 pos : POSITION;
    float2 tex : TEXCOORD0;
};

//==========================================================================
// 出力用構造体
//==========================================================================
struct vsOutput
{
    float4 pos : POSITION;
    float2 tex : TEXCOORD0;
};

//==========================================================================
// 頂点シェーダ
//==========================================================================
void main(in vsInput input, out vsOutput output)
{
    output = (vsOutput) 0;
   
    output.pos = mul(float4(input.pos, 1.0f), World);
    output.pos = mul(output.pos, Projection);
    output.tex = input.tex * float2(uvWidth, uvHeight) + float2(uvLeft, uvTop);
}

//==========================================================================
// ピクセルシェーダ
//==========================================================================
void ps2DMain(in vsOutput input, out float4 color : COLOR0)
{
    if (TexFlag)
    {
        color = tex2D(Sampler, input.tex);
        color *= Color;
    }
    else
    {
        color = Color;
    }
}

//==========================================================================
// メインシェーダーのパッケージ
//==========================================================================
technique MainShader2D
{
    pass P0
    {
        // 使用するシェーダーを渡す
        VertexShader = compile vs_3_0 main();
        PixelShader = compile ps_3_0 ps2DMain();
        
        // Effect States
        AlphaBlendEnable = true;
        SrcBlend = SRCALPHA;
        DestBlend = INVSRCALPHA;
    }
}
