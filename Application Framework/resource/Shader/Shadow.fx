//==========================================================================
// 影 [Shadow.fx]
// author: tatsuya ogawa
//==========================================================================

//==========================================================================
// グローバル変数
//==========================================================================
float4x4 World; // ワールド変換行列
float4x4 CameraView; // カメラ変換行列
float4x4 CameraProjection; // プロジェクション変換行列
float4x4 LightView; // 光源から見た変換行列
float4x4 LightProjection; //  光源のプロジェクション変換行列

float3 LightDir; // 平行光源の方向

float3 LightDiffuse; // 光源Diffuse
float3 LightAmbient; // 光源Ambient

float3 MaterialDiffuse; // マテリアルのDiffusec
float3 MaterialAmbient; // マテリアルのAmbient

float Bias = 0.0f; // バイアス

texture Texture; // テクスチャ

sampler Sampler = sampler_state
{
    texture = <Texture>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
    AddressW = Clamp;
};

struct Output
{
    float4 pos : POSITION;
    float4 color : COLOR0;
    float4 shadowmap : TEXCOORD0;
    float4 color2 : TEXCOORD1; //w=影の濃さ
};

//==========================================================================
// 頂点シェーダ
//==========================================================================
Output main(
in float3 pos : POSITION,
in float3 normal : NORMAL)
{
    Output Out;

    float4 p = mul(float4(pos, 1.0f), World);
    Out.pos = mul(mul(p, CameraView), CameraProjection);

    float3 n = mul(normal, (float3x3) World);
    n = normalize(n);

    float k = max(dot(LightDir.xyz, n), 0.0f);
    float4 color = k * LightDiffuse * MaterialDiffuse;
    color += LightAmbient * MaterialAmbient;
    Out.color = color;
    
    //影の濃さ（適当に3乗してみた）
    Out.color2.w = k * k * k * 0.6f;
	
    p = mul(float4(pos, 1.0f), World);
    p = mul(mul(p, LightView), LightProjection);
    Out.shadowmap = p;

    return Out;
}

//==========================================================================
// ピクセルシェーダ
//==========================================================================
float4 psShadow(Output input) : COLOR
{
    float w = 1.0f / input.shadowmap.w;
    float2 tex;
    tex.x = (1.0f + input.shadowmap.x * w) * 0.5f;
    tex.y = (1.0f - input.shadowmap.y * w) * 0.5f;
    float z = tex2D(Sampler, tex).x;

    float color = input.color;
    if (input.shadowmap.z * w > z + Bias)
        color = color * (1.0f - input.color2.w); //影の濃さを掛ける

    return color;
}

//==========================================================================
// シェーダーのパッケージ
//==========================================================================
technique Shadow
{
    pass P0
    {
        // 使用するシェーダーを渡す
        VertexShader = compile vs_3_0 main();
        PixelShader = compile ps_3_0 psShadow();
        
        // Effect States
        AlphaBlendEnable = true;
        SrcBlend = SRCALPHA;
        DestBlend = INVSRCALPHA;
    }
}
