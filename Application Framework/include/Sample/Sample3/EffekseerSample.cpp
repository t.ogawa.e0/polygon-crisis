//==========================================================================
// EffekseerSample [EffekseerSample.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EffekseerSample.h"
#include "common/DefaultSystem.h"
#include "Editor/common/EditorCamera.h"

namespace Sample3
{
    struct MyStruct
    {
        const EFK_CHAR* pass;
        const char* name;
    };

    MyStruct data_list[]{
		{(const EFK_CHAR*)L"resource/Effekseer/r_square.efk", "r_square"},
        {(const EFK_CHAR*)L"resource/Effekseer/g_square.efk", "g_square"},
		{(const EFK_CHAR*)L"resource/Effekseer/b_square.efk", "b_square"},
		{(const EFK_CHAR*)L"resource/Effekseer/distortion.efk", "distortion"},
    };

	EffekseerSample::EffekseerSample() : ObjectManager("EffekseerSample")
    {
        m_obj = nullptr;
    }

	EffekseerSample::~EffekseerSample()
    {
    }
    void EffekseerSample::Init()
    {
        auto *pEffekseer = AddComponent<mslib::MsEffekseer::MsEffekseer>(GetDevice()->GetD3DDevice(), 2000);

        // 投影行列を設定
        pEffekseer->GetRenderer()->SetProjectionMatrix(
            ::Effekseer::Matrix44().PerspectiveFovLH(D3DXToRadian(60), (float)this->GetDevice()->GetWindowsSize().x / (float)GetDevice()->GetWindowsSize().y, 0.1f, 1000.0f));

        for (int i = 0; i < (int)((sizeof(data_list) / sizeof(MyStruct))); i++)
        {
            pEffekseer->EffectLoad(data_list[i].pass, data_list[i].name);
        }

        for (int i = 0; i < (int)((sizeof(data_list) / sizeof(MyStruct))); i++)
        {
            auto obj = Effekseer()->Create();
            obj->SetEffekseerData(GetEffekseerLoader()->Load(data_list[i].pass, data_list[i].name));
            obj->SetComponentName(data_list[i].name + mslib::text(" [%p]", obj));
        }
    }
    void EffekseerSample::Update()
    {
        auto * pCamera = GetParent()->GetComponent<Editor::EditorCamera>()->GetCamera();
        auto *pEffekseer = GetComponent<mslib::MsEffekseer::MsEffekseer>();

        auto vEye = pCamera->GetLook1().eye + pCamera->GetLook2().eye;
        auto vAt = pCamera->GetLook1().at + pCamera->GetLook2().at;
        auto vUp = pCamera->GetLook1().up;

        // カメラ行列を設定
        pEffekseer->GetRenderer()->SetCameraMatrix(
            ::Effekseer::Matrix44().LookAtLH(Effekseer::Vector3D(vEye.x, vEye.y, vEye.z), ::Effekseer::Vector3D(vAt.x, vAt.y, vAt.z), ::Effekseer::Vector3D(vUp.x, vUp.y, vUp.z)));

        if (m_obj == nullptr)return;

        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_A))
        {
            m_obj->AddPosition(-0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_D))
        {
            m_obj->AddPosition(0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_W))
        {
            m_obj->AddPosition(0, 0, 0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_S))
        {
            m_obj->AddPosition(0, 0, -0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Q))
        {
            m_obj->AddRotation(-0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_E))
        {
            m_obj->AddRotation(0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_R))
        {
            m_obj->AddRotation(0, -0.05f, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_F))
        {
            m_obj->AddRotation(0, 0.05f, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Z))
        {
            m_obj->AddRotation(0, 0, 0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_C))
        {
            m_obj->AddRotation(0, 0, -0.05f);
        }
    }
    void EffekseerSample::Debug()
    {
        static bool bField = false;

        auto *pEffekseer = GetComponent<mslib::MsEffekseer::MsEffekseer>();

        ImGui()->Checkbox("Effekseer", bField);
        if (bField)
        {
            ImGui()->NewWindow("Effekseer", bField, mslib::DefaultSystem::SystemWindowFlags);
            ImGui()->Separator();
            ImGui()->Text("Effekseer");
            ImGui()->Separator();
            for (auto &itr : pEffekseer->GetEffectList())
            {
                if (ImGui()->NewMenu(itr.GetEffectName()))
                {
                    if (ImGui()->MenuItem("Play"))
                    {
                        // エフェクトの再生
                        m_handle = pEffekseer->GetManager()->Play(itr.GetEffect(), 0, 0, 0);
                        pEffekseer->SetPlayData(m_handle, itr.GetEffect(), itr.GetEffectName());
                    }
                    if (ImGui()->MenuItem("Stop"))
                    {
                        pEffekseer->GetManager()->StopRoot(itr.GetEffect());
                        pEffekseer->DestroyHandle(itr.GetEffect());
                        m_handle--;
                    }
                    ImGui()->EndMenu();
                }
            }

            for (int i = 0; i < Effekseer()->Size(); i++)
            {
                auto obj = Effekseer()->Get(i);
                if (ImGui()->NewMenu(obj->GetEffekseerData().Tag() + mslib::text(" [%p]", obj)))
                {
                    if (ImGui()->MenuItem("Play"))
                    {
                        // エフェクトの再生
                        obj->Play();
                        m_obj = obj;
                    }
                    if (ImGui()->MenuItem("Stop"))
                    {
                        obj->Stop();
                        m_obj = nullptr;
                    }
                    ImGui()->EndMenu();
                }
            }
            ImGui()->EndWindow();
        }
    }
}