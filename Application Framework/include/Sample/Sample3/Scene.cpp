//==========================================================================
// Effekseer�V�[�� [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "config.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "EffekseerSample.h"

namespace Sample3
{
    Scene::Scene() : BaseScene("Sample3", SceneLevel::__Sample3__)
    {
        AddComponent<Editor::EditorCamera>();
        AddComponent<Editor::EditorGrid>();
        AddComponent<EffekseerSample>();
    }

    Scene::~Scene()
    {
    }
}