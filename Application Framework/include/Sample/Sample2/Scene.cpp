//==========================================================================
// コリジョンのチェック [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "CheckCollider.h"
#include "config.h"

namespace Sample2
{
    Scene::Scene() : BaseScene("Sample2", SceneLevel::__Sample2__)
    {
        AddComponent<Editor::EditorCamera>();
        AddComponent<Editor::EditorGrid>();
        AddComponent<CheckCollider>();
    }

    Scene::~Scene()
    {
    }
}