//==========================================================================
// メイン関数 [main.cpp]
// author: tatuya ogawa
//==========================================================================
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>
#define _GLIBCXX_DEBUG
#include "dxlib.h"
#include "GameWindow.h"
#include "SystemWindow.h"

//==========================================================================
//	メイン関数
int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    // CRTメモリリーク箇所検出
    //   _CrtSetBreakAlloc(35556);
    
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    std::unique_ptr<mslib::component::Component> gameObject(new mslib::component::Component);
    std::unique_ptr<mslib::int2> pAspect(new mslib::int2(16, 9));

    gameObject->SetComponentName("GameObject");

    // マネージャーの親コンポーネントを指定します
    mslib::manager::Manager::SetManagerComponent(gameObject->GetParent());

    auto *pAsp = gameObject->AddComponent<mslib::AspectRatio>();

    // 指定した数のウィンドウサイズの生成
    for (int i = 0; i < 10; i++)
    {
        if (3 < i)
        {
            // 指定したアスペクト比かどうかのセット&登録
            pAsp->Search((*pAspect * 12)*(i + 1), *pAspect);
        }
    }

#if defined(_DEBUG) || defined(DEBUG)
    auto * pGamewin = gameObject->AddComponent<CGameWindow>("Game Window", "Game Window");

    // ゲームウィンドウ
    return pGamewin->Window(hInstance, pAsp->Get(3).size, true, nCmdShow);
#else
    auto * pSyswin = gameObject->AddComponent<CSystemWindow>("Config", "Config");

    // 生成されたウィンドウサイズの情報の入力
    for (int i = 0; i < pAsp->Size(); i++)
    {
        pSyswin->SetAspectRatio(pAsp->Get(i));
    }

    // システムウィンドウ
    if (pSyswin->Window(hInstance, nCmdShow))
    {
        bool bWinMode = pSyswin->GetWindowMode();

        *pAspect = pSyswin->GetWindowSize();

        gameObject->DestroyComponent<CSystemWindow>();

        auto * pGamewin = gameObject->AddComponent<CGameWindow>("Game Window", "Game Window");

        // ゲームウィンドウ
        return pGamewin->Window(hInstance, *pAspect, bWinMode, nCmdShow);
    }
#endif
    return 0;
}
