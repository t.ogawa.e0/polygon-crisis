//==========================================================================
// スクリーン[Screen.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "Load\LoadScene.h"

//==========================================================================
//
// class  : CScreen
// Content: スクリーン
//
//==========================================================================
class CScreen : private mslib::SetRender, private mslib::SetSampler, public mslib::component::Component
{
public:
    CScreen();
	~CScreen();
	// 初期化
	bool Init(HINSTANCE hInstance, HWND hWnd);
	// 解放
	void Uninit(void);
	// 更新
	bool Update(void);
	// 描画
	void Draw(void);
	// シーン変更キー
    static void ScreenChange(mslib::scene_manager::BaseScene * scene);
private:
	// スクリーンの切り替え
    void Change(mslib::scene_manager::BaseScene * scene);
	// 最初の初期化データの破棄
	void startdatarelease(void);
	// スクリーンの初期化
	bool initializer(void);
	// フェード
	void fade(void);

    // 描画モード
    void Fillmode(LPDIRECT3DDEVICE9 pDevice);

    // 描画モード
    void EndFillmode(LPDIRECT3DDEVICE9 pDevice);

    // フィルタリング
    void Fittering(LPDIRECT3DDEVICE9 pDevice);

    // デバッグ用シーンchange
    void DebugSceneChange(void);

    // SystemWindow
    void ImGuiSystemMenuBar(void);

    // NextWindow
    void ImGuiNextWindow(void);
private:
    LoadScene * m_LoadScene; // ロード画面
    mslib::scene_manager::SceneManager * m_scene; // スクリーンセット
    mslib::MsImGui m_ImGui;
    bool m_start; // 初期化フラグ
    bool m_change; // 切り替えフラグ
    int m_count; // カウンタ
    int m_end_count; // カウンタ
    bool m_initializer; // 初期化フラグ

    static mslib::scene_manager::BaseScene * m_next_scene; // シーン

#if defined(_MSLIB_DEBUG)
    mslib::boollist m_system; // システム
#endif
};
