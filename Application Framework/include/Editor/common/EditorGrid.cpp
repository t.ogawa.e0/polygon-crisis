//==========================================================================
// グリッド [EditorGrid.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EditorGrid.h"

namespace Editor
{
    EditorGrid::EditorGrid() : ObjectManager("EditorGrid")
    {
    }

    EditorGrid::~EditorGrid()
    {
    }

    /**
    @brief 初期化
    */
    void EditorGrid::Init()
    {
        Grid_()->Init(30);
    }

    /**
    @brief 更新
    */
    void EditorGrid::Update()
    {
    }

    /**
    @brief デバッグ
    */
    void EditorGrid::Debug()
    {
    }
}