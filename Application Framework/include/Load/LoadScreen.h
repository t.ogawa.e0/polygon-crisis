//==========================================================================
// ロードスクリーン[LoadScreen.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CLoadScreen
// Content: ロードスクリーン
//
//==========================================================================
class CLoadScreen : public mslib::ObjectManager
{
private:
    // パラメータ
    class CParam
    {
    public:
        CParam()
        {
            m_a = 0;
            m_Change = false;
        }
        ~CParam() {}
    public:
        int m_a; // α
        bool m_Change; // change
    };
public:
    CLoadScreen() :ObjectManager("CLoadScreen") {
        m_activity = true;
    }
    ~CLoadScreen() {}

    // 初期化
    void Init(void)override;
    // 更新
    void Update(void)override;
    // デバッグ
    void Debug(void)override;
    // アクティビティ
    void SetActivity(bool activity);
private:
    // αチェンジ
    void Change(int Speed);
private:
    CParam m_paramload; // パラメータ
    DWORD m_NewTime = 0, m_OldTime = 0; //時間格納
    bool m_activity; // アクティビティ
};
