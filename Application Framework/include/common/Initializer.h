//==========================================================================
// Initializer [Initializer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include "mslib.hpp"

_MSLIB_BEGIN

namespace initializer
{
    class Initializer
    {
    public:
        Initializer();
        ~Initializer();

        /**
        @brief 全て初期化
        */
        static void InitAll();
    protected:
        /**
        @brief 継承初期化
        */
        virtual void Init();
    protected:
        static std::list<Initializer*> m_initializer_list; // 初期化リスト
    };
}

_MSLIB_END