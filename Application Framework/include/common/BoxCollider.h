//==========================================================================
// 立方体のプリミティブコライダー [BoxCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include "mslib.hpp"
#include "Component.h"
#include "Transform.h"
#include "Collider.h"

_MSLIB_BEGIN

namespace collider
{
    //==========================================================================
    //
    // class  : BoxCollider
    // Content: 立方体のプリミティブコライダー
    //
    //==========================================================================
    class BoxCollider : public Collider, public Target<BoxCollider>
    {
    private:
        class BoxSprite {
        public:
            BoxSprite();
            BoxSprite(transform::Transform * v1, transform::Transform * v2, transform::Transform * v3, transform::Transform * v4);
            ~BoxSprite();

            bool Update(Collider * target);
        private:
            /**
            @brief 判定処理
            */
            bool Judgment(
                const D3DXVECTOR3 & target,
                const D3DXVECTOR3 & v01,
                const D3DXVECTOR3 & v12,
                const D3DXVECTOR3 & v20,
                const D3DXVECTOR3 & v0p,
                const D3DXVECTOR3 & v1p,
                const D3DXVECTOR3 & v2p,
                const D3DXVECTOR3 & vpos);
        public:
            std::vector<transform::Transform*> m_vertex; // 頂点
            bool m_upward_collision; // 上方向の判定
            bool m_under_collision; // 下方向の判定
        };
    public:
        BoxCollider();
        ~BoxCollider();

        /**
        @brief 初期化
        */
        void Init()override;

        /**
        @brief 更新
        */
        void Update()override;

        /**
        @brief 描画
        */
        void Draw(LPDIRECT3DDEVICE9 device)override;

        /**
        @brief コリジョンデータの複製
        */
        void Copy(const BoxCollider * in);
    protected:
        std::vector<BoxSprite> m_sprite; // 面
    };
}

_MSLIB_END