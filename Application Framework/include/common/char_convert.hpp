//==========================================================================
// Character code conversion [char_convert.hpp]
// author: tatsuya ogawa 
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <codecvt> 
#include <cstdlib> 
#include <iomanip> 
#include <locale> 
#include <string> 
#include <system_error> 
#include <vector> 
#include <Windows.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

namespace char_convert {
    /**
    @brief [C] string -> wstring
    @param src [in] string
    @return wstring
    */
    std::wstring multi_to_wide_capi(std::string const& src);

    /**
    @brief [Windows API] string -> wstring
    @param src [in] string
    @return wstring
    */
    std::wstring multi_to_wide_winapi(std::string const& src);

    /**
    @brief [C/C++] UTF-8 -> Shift_JIS
    @param src [in] wstring
    @return string
    */
    std::string wide_to_multi_capi(std::wstring const& src);

    /**
    @brief [Windows API] UTF-8 -> Shift_JIS
    @param src [in] wstring
    @return string
    */
    std::string wide_to_multi_winapi(std::wstring const& src);

    /**
    @brief [C++] wstring -> string
    @param src [in] wstring
    @return string
    */
    std::string wide_to_utf8_cppapi(std::wstring const& src);

    /**
    @brief [Windows API] wstring -> string
    @param src [in] wstring
    @return string
    */
    std::string wide_to_utf8_winapi(std::wstring const& src);

    /**
    @brief [C++] string -> wstring
    @param src [in] string
    @return wstring
    */
    std::wstring utf8_to_wide_cppapi(std::string const& src);

    /**
    @brief [Windows API] string -> wstring
    @param src [in] string
    @return wstring
    */
    std::wstring utf8_to_wide_winapi(std::string const& src);

    /**
    @brief [C/C++] Shift_JIS -> UTF-8
    @param src [in] string
    @return string
    */
    std::string multi_to_utf8_cppapi(std::string const& src);

    /**
    @brief [Windows API] Shift_JIS -> UTF-8
    @param src [in] string
    @return string
    */
    std::string multi_to_utf8_winapi(std::string const& src);

    /**
    @brief [C/C++] UTF-8 -> Shift_JIS
    @param src [in] string
    @return string
    */
    std::string utf8_to_multi_cppapi(std::string const& src);

    /**
    @brief [Windows API] UTF-8 -> Shift_JIS
    @param src [in] string
    @return string
    */
    std::string utf8_to_multi_winapi(std::string const& src);
}

_MSLIB_END