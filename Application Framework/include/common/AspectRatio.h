//==========================================================================
// アスペクト比[AspectRatio.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <vector>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "Component.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : AspectRatio
// Content: アスペクト比
//
//==========================================================================
class AspectRatio : public component::Component
{
private:
    // コピー禁止 (C++11)
    AspectRatio(const AspectRatio &) = delete;
    AspectRatio &operator=(const AspectRatio &) = delete;
    AspectRatio &operator=(AspectRatio&&) = delete;
public:
    struct List {
        int2 size; // ウィンドウサイズ
        int2 asp; // アスペクト比
    };
public:
    AspectRatio() {
        SetComponentName("AspectRatio");
    }
    ~AspectRatio() {
        m_AspectRatio.clear();
        m_AspectRatioALL.clear();
    }

    /**
    @brief アスペクト比チェック
    @param win [in] {x,y}ウィンドウサイズ
    @param asps [in] {x,y}指定アスペクト比
    @return 指定のアスペクト比ならtrueが返ります
    */
    bool Search(const int2 & win, const int2 & asps) {
        int n = 0;
        int2 size(win);
        int2 asp;
        List list;

        if ((n = GCD(size)) > 0) {
            asp.x = size.x / n;
            asp.y = size.y / n;
        }

        // データの登録
        list.size = int2(win);
        list.asp = asp;
        m_AspectRatioALL.emplace_back(list);

        // 指定するアスペクト比ならそのウィンドウサイズの登録
        if (asps.x == asp.x&&asps.y == asp.y) {
            m_AspectRatio.emplace_back(list);
            return true;
        }
        return false;
    }

    /**
    @brief 指定したアスペクト比と一致したデータ数の取得
    @return 一致したデータ数
    */
    int Size(void) {
        return m_AspectRatio.size();
    }

    /**
    @brief 処理したデータ数の取得
    @return 処理したデータ数
    */
    int AllSize(void) {
        return m_AspectRatioALL.size();
    }

    /**
    @brief 指定したアスペクト比のデータのみ取得
    @param label アクセスID
    @return 指定したアスペクト比
    */
    List & Get(int label) {
        return m_AspectRatio[label];
    }

    /**
    @brief 処理したデータの結果を取得
    @param label アクセスID
    @return 処理したデータの結果
    */
    List & AllDataGet(int label) {
        return m_AspectRatioALL[label];
    }
private:
    /**
    @brief 最大公約数を求める
    @param size ウィンドウサイズ
    @return 公約数
    */
    int GCD(int2 size) {
        int tmp = 0;

        // xよりもyの値が大きければ，tmp変数を使って入れ替える
        if (size.x < size.y) {
            tmp = size.x;
            size.x = size.y;
            size.y = tmp;
        }

        // yが自然数でなければ-1を返す
        if (size.y <= 0) {
            return -1;
        }

        // xをyで割った余りが0であれば，それが最大公約数
        if (size.x % size.y == 0) {
            return size.y;
        }

        // 再帰関数（上のif文に引っかかるまでGCDを繰り返す）
        return GCD(int2(size.y, size.x % size.y));
    }
private:
    std::vector<List> m_AspectRatio; // ヒットしたデータの格納
    std::vector<List> m_AspectRatioALL; // データの格納
};

_MSLIB_END