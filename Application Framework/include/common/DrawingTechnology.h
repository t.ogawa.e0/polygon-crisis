//==========================================================================
// 描画のテクニック [DrawingTechnology.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "mslib.hpp"

_MSLIB_BEGIN

namespace drawing_technology
{
    class DrawingTechnology
    {
    public:
        DrawingTechnology();
        virtual ~DrawingTechnology();

        /**
        @brief 加算合成
        @param key [in] 操作
        */
        void TechnologyAddSynthesis(bool key);
    protected:
        /**
        @brief 半透明処理
        @param pDevice [in] デバイス
        */
        void Add(LPDIRECT3DDEVICE9 device);

        /**
        @brief アルファテスト
        @param pDevice [in] デバイス
        @param Power [in] パワー
        */
        void AlpharefStart(LPDIRECT3DDEVICE9 device, int Power);

        /**
        @brief アルファテスト_終わり
        @param pDevice [in] デバイス
        */
        void AlpharefEnd(LPDIRECT3DDEVICE9 device);

        /**
        @brief 加算合成
        @param pDevice [in] デバイス
        */
        void Sub(LPDIRECT3DDEVICE9 device);

        /**
        @brief Zバッファを描画するか否か
        @param pDevice [in] デバイス
        */
        void ZwriteenableStart(LPDIRECT3DDEVICE9 device);

        /**
        @brief Zバッファを描画するか否か_終わり
        @param pDevice [in] デバイス
        */
        void ZwriteenableEnd(LPDIRECT3DDEVICE9 device);
    protected:
        bool m_TechnologyAdd;
    };
}

_MSLIB_END