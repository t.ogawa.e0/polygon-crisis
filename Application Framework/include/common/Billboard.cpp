//==========================================================================
// ビルボード [Billboard.h]
// author: tatsuya ogawa
//==========================================================================
#include "Billboard.h"
#include "mslib_struct.h"

_MSLIB_BEGIN

namespace billboard
{
    constexpr float VCRCT = 0.5f;
    constexpr int NumDfaltIndex = 6;

    //==========================================================================
    //
    // class  : BillboardData
    // Content: ビルボードデータ
    //
    //==========================================================================
    BillboardData::BillboardData()
    {
        CreatePtr = nullptr;
    }

    BillboardData::~BillboardData()
    {
        if (GetRef() == 0)
        {
            asset.Release();
            CreatePtr = nullptr;
        }
    }

    //==========================================================================
    //
    // class  : BillboardReference
    // Content: 参照用
    //
    //==========================================================================
    BillboardReference::BillboardReference()
    {
        m_data = nullptr;
    }

    BillboardReference::~BillboardReference()
    {
        Release();
    }

    void BillboardReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->CreatePtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->CreatePtr->Delete(m_data);
        m_data = nullptr;
    }

    //==========================================================================
    //
    // class  : CreateBillboard
    // Content: ビルボード生成
    //
    //==========================================================================
    CreateBillboard::CreateBillboard()
    {
        SetComponentName("CreateBillboard");
        m_BillboardData = nullptr;
        m_device = nullptr;
        m_hwnd = nullptr;
    }

    CreateBillboard::CreateBillboard(LPDIRECT3DDEVICE9 device, HWND hWnd)
    {
        SetComponentName("CreateBillboard");
        m_BillboardData = nullptr;
        m_device = device;
        m_hwnd = hWnd;
    }

    CreateBillboard::~CreateBillboard()
    {
        AllDestroyComponent();
        if (m_BillboardData != nullptr)
        {
            m_BillboardData->asset.Release();
            delete m_BillboardData;
            m_BillboardData = nullptr;
        }
    }

    //==========================================================================
    /**
    @brief ビルボードを生成する。
    */
    BillboardReference CreateBillboard::Create()
    {
        if (m_BillboardData == nullptr)
        {
            VERTEX_4* pseudo = nullptr;
            WORD* index = nullptr;

            m_BillboardData = new BillboardData;

            CreateVertexBuffer(m_device, m_hwnd, sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &m_BillboardData->asset.VertexBuffer, nullptr);
            CreateIndexBuffer(m_device, m_hwnd, sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_BillboardData->asset.IndexBuffer, nullptr);

            m_BillboardData->asset.VertexBuffer->Lock(0, 0, (void**)&pseudo, D3DLOCK_DISCARD);
            CreateVertex(pseudo);
            m_BillboardData->asset.VertexBuffer->Unlock();

            m_BillboardData->asset.IndexBuffer->Lock(0, 0, (void**)&index, D3DLOCK_DISCARD);
            CreateIndex(index, 1);
            m_BillboardData->asset.IndexBuffer->Unlock();

            m_BillboardData->CreatePtr = this;
        }
        return m_BillboardData;
    }

    //==========================================================================
    /**
    @brief ビルボードを破棄する。
    @param data [in] データ
    */
    void CreateBillboard::Delete(BillboardData * data)
    {
        if (data == nullptr)return;
        if (data->CreatePtr != this)return;
        if (data != m_BillboardData)return;
        delete data;
        data = nullptr;
        m_BillboardData = nullptr;
    }

    //==========================================================================
    /**
    @brief バーテックスの生成
    @param out [out] 受け取り
    @param NumRectangle [in] 数
    */
    void CreateBillboard::CreateVertex(VERTEX_4 * out)
    {
        floatUV uv = floatUV(0.0f, 0.0f, 1.0f, 1.0f); // UV
        VERTEX_4 vVertex[] =
        {
            // 手前
            { D3DXVECTOR3(-VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(uv.u0,uv.v0) }, // 左上
            { D3DXVECTOR3(VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(uv.u1,uv.v0) }, // 右上
            { D3DXVECTOR3(VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(uv.u1,uv.v1) }, // 右下
            { D3DXVECTOR3(-VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(uv.u0,uv.v1) }, // 左下
        };
        for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
        {
            out[i] = vVertex[i];
        }
    }

    //==========================================================================
    /**
    @brief Indexの生成
    @param out [out] 受け取り
    @param NumRectangle [in] 数
    */
    void CreateBillboard::CreateIndex(WORD * Output, int NumRectangle)
    {
        for (int i = 0, s = 0, ncount = 0; i < NumDfaltIndex*NumRectangle; i++, s++, ncount++)
        {
            switch (ncount)
            {
            case 3:
                s -= 3;
                break;
            case 4:
                s += 1;
                break;
            case 6:
                ncount = 0;
                break;
            default:
                break;
            }
            Output[i] = (WORD)s;
        }
    }

    //==========================================================================
    //
    // class  : SetBillboard
    // Content: ビルボード登録クラス
    //
    //==========================================================================
    SetBillboard::SetBillboard() {}
    SetBillboard::~SetBillboard() {}

    //==========================================================================
    /**
    @brief ビルボードの登録
    @param data [in] ビルボード
    */
    void SetBillboard::SetBillboardData(const BillboardReference & data)
    {
        m_BillboardData = data;
    }

    //==========================================================================
    /**
    @brief ビルボードの取得
    */
    BillboardReference & SetBillboard::GetBillboardData()
    {
        return m_BillboardData;
    }
}
_MSLIB_END