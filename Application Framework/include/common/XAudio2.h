//==========================================================================
// サウンド[XAudio2.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <iomanip>
#include <string>
#include <XAudio2.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "Component.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

namespace xaudio2
{
    //==========================================================================
    //
    // class  : XAudio2Device
    // Content: サウンドのデバイス
    //
    //==========================================================================
    class XAudio2Device : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        XAudio2Device(const XAudio2Device &) = delete;
        XAudio2Device &operator=(const XAudio2Device &) = delete;
        XAudio2Device &operator=(XAudio2Device&&) = delete;
    public:
        XAudio2Device();
        ~XAudio2Device();

        /**
        @brief 初期化
        @param hWnd [in] インスタンスハンドル
        @return Component Object Model defines, and macros
        */
        HRESULT Init(HWND hWnd);

        /**
        @brief 解放
        */
        void Release(void);

        /**
        @brief インスタンスハンドルの取得
        @return インスタンスハンドル
        */
        static HWND GetWnd(void);

        /**
        @brief Audio2の取得
        @return Audio2
        */
        static IXAudio2 *GetAudio2(void);
    private:
        /**
        @brief ボイスデータの破棄
        */
        void DestroyVoice(void);
    private:
        IXAudio2MasteringVoice * m_pMasteringVoice; // マスターボイス
        static IXAudio2 *m_pXAudio2; // XAudio2オブジェクトへのインターフェイス
        static HWND m_hWnd; // ウィンドハンドル
    };

    //==========================================================================
    //
    // class  : XAudio2Param
    // Content: データ
    //
    //==========================================================================
    class XAudio2Param
    {
    private:
        // コピー禁止 (C++11)
        XAudio2Param(const XAudio2Param &) = delete;
        XAudio2Param &operator=(const XAudio2Param &) = delete;
        XAudio2Param &operator=(XAudio2Param&&) = delete;
    public:
        XAudio2Param();
        ~XAudio2Param();
    private:
        /**
        @brief ボイスデータの破棄
        */
        void Release(void);
    public:
        IXAudio2SourceVoice *m_pSourceVoice; // サウンド
        BYTE *m_pDataAudio; // オーディオデーター
        DWORD m_SizeAudio; // オーディオデータサイズ
        int m_CntLoop; // ループカウント
        float m_Volume; // ボリューム
    };

    //==========================================================================
    //
    // class  : XAudio2SoundLabel
    // Content: サウンドのリンク
    //
    //==========================================================================
    class XAudio2SoundLabel
    {
    public:
        XAudio2SoundLabel();
        XAudio2SoundLabel(const std::string & name, int loop, float vol);
        ~XAudio2SoundLabel();
    public:
        std::string m_strName; // ファイル名
        int m_CntLoop; // ループカウント -1=ループ 0 一回
        float m_Volume; // ボリューム 最大=1.0f 無音=0.0f
    };

    //==========================================================================
    //
    // class  : XAudio2
    // Content: サウンド
    //
    //==========================================================================
    class XAudio2 : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        XAudio2(const XAudio2 &) = delete;
        XAudio2 &operator=(const XAudio2 &) = delete;
        XAudio2 &operator=(XAudio2&&) = delete;
    public:
        XAudio2();
        ~XAudio2();

        /**
        @brief 初期化
        @param Input [in] サウンドのデータ
        @param OutLabel [in] 一つ前のデータlabel
        @return Component Object Model defines, and macros
        */
        HRESULT Init(const XAudio2SoundLabel & Input, int & OutLabel);

        /**
        @brief 初期化
        @param Input [in] サウンドのデータ
        @return Component Object Model defines, and macros
        */
        HRESULT Init(const XAudio2SoundLabel & Input);

        /**
        @brief 解放
        */
        void Release(void);

        /**
        @brief 特定のサウンドの破棄
        @param Input [in] データID
        */
        void PinpointRelease(int label);

        /**
        @brief 再生
        @param Input [in] データID
        @return Component Object Model defines, and macros
        */
        HRESULT Play(int label);

        /**
        @brief 特定のサウンド停止
        @param Input [in] データID
        */
        void Stop(int label);

        /**
        @brief 格納してある全てのサウンド停止
        */
        void Stop(void);

        /**
        @brief 特定のサウンドのボリューム設定
        @param label [in] データID
        @param volume [in] 0.0f〜1.0f
        */
        void SetVolume(int label, float volume);

        /**
        @brief 全てのサウンドのボリューム設定
        @param volume [in] 0.0f〜1.0f
        */
        void SetVolume(float volume);

        /**
        @brief 全てのサウンドのボリューム自動設定
        */
        void SetVolume(void);

        /**
        @brief 特定のサウンドのボリューム設定
        @param pSound [in] サウンドデータ
        @param volume [in] 0.0f〜1.0f
        */
        void SetVolume(XAudio2Param * pSound, float volume);

        /**
        @brief 特定のサウンドのボリュームの取得
        @param label [in] データID
        */
        const float GetVolume(int label);
    private:
        /**
        @brief 特定のサウンド停止
        @param pSound [in] データ
        */
        void Stop(XAudio2Param * pSound);

        /**
        @brief チャンクのチェック
        @param hFile [in]
        @param format [in]
        @param pChunkSize [in]
        @param pChunkDataPosition [in]
        */
        HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition);

        /**
        @brief チャンクデータの読み込み
        @param hFile [in]
        @param pBuffer [in]
        @param dwBuffersize [in]
        @param dwBufferoffset [in]
        */
        HRESULT ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset);
    private:
        wrapper::vector<XAudio2Param> m_Sound; // サウンドケース
    };
}

_MSLIB_END