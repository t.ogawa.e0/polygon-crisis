//==========================================================================
// �����̃R���C�_�[ [DistanceCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <vector>
#include "mslib.hpp"
#include "Component.h"
#include "Transform.h"
#include "Collider.h"

_MSLIB_BEGIN

namespace collider
{
    //==========================================================================
    //
    // class  : DistanceCollider
    // Content: �����̃R���C�_�[
    //
    //==========================================================================
    class DistanceCollider : public Collider
    {
    protected:
        class D3DXMATRIX_BOOL
        {
        public:
            D3DXMATRIX_BOOL() {
                D3DXMatrixIdentity(&mat);
                hit = false;
            }
            D3DXMATRIX_BOOL(const D3DXMATRIX & mat_, bool hit_) {
                mat = mat_;
                hit = hit_;
            }
            ~D3DXMATRIX_BOOL() {}
        public:
            D3DXMATRIX mat;
            bool hit;
        };
    public:
        DistanceCollider();
        ~DistanceCollider();

        /**
        @brief ������
        */
        void Init()override;

        /**
        @brief �X�V
        */
        void Update()override;

        /**
        @brief �`��
        */
        void Draw(LPDIRECT3DDEVICE9 device)override;

        /**
        @brief ���苗��
        */
        void DeterminationDistance(float distance);
    protected:
        transform::Transform * m_distance; // �����𑪂邽�߂̃I�u�W�F�N�g
        std::list<D3DXMATRIX_BOOL> m_myself_matrix; // �s��
        std::list<D3DXMATRIX_BOOL> m_distance_matrix; // �s��
    };
}

_MSLIB_END