//==========================================================================
// Xモデル[XModel.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "XModel.h"

_MSLIB_BEGIN

namespace xmodel
{
    //==========================================================================
    //
    // class  : XModelData
    // Content: Xモデルデータ
    //
    //==========================================================================
    XModelData::XModelData()
    {
        num = (DWORD)0; // マテリアル数
        createID = (int64_t)0; // 生成ID
        LoaderPtr = nullptr; // ローダーのポインタ
    }
    XModelData::~XModelData()
    {
        if (GetRef() == 0)
        {
            if (asset != nullptr)
            {
                asset->Release();
                asset = nullptr;
            }
            MatD3D.clear();
            texture.clear();
            LoaderPtr = nullptr;
            tag.c_str();
        }
    }

    //==========================================================================
    //
    // class  : XModelReference
    // Content: 参照用
    //
    //==========================================================================
    XModelReference::XModelReference()
    {
        m_data = nullptr;
    }

    XModelReference::~XModelReference()
    {
        Release();
    }

    void XModelReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->LoaderPtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->LoaderPtr->Unload(m_data);
        m_data = nullptr;
    }

    const DWORD & XModelReference::NumMesh()
    {
        return m_data->num;
    }

    const std::string & XModelReference::Tag()
    {
        return m_data->tag;
    }

    const std::vector<D3DMATERIAL9> & XModelReference::Material()
    {
        return m_data->MatD3D;
    }

    std::vector<texture::TextureReference> & XModelReference::Texture()
    {
        return m_data->texture;
    }

    std::vector<D3DMATERIAL9>& XModelReference::MatD3D()
    {
        return m_data->MatD3D;
    }

    //==========================================================================
    //
    // class  : XModelLoader
    // Content: Xモデル読み込み破棄クラス
    //
    //==========================================================================
    XModelLoader::XModelLoader()
    {
        m_device = nullptr;
        m_hwnd = nullptr;
        m_TextureLoader = nullptr;
        createIDCount = (int64_t)0;
        SetComponentName("XModelLoader");
    }
    XModelLoader::XModelLoader(LPDIRECT3DDEVICE9 device, HWND hWnd, texture::TextureLoader * TextureLoader)
    {
        m_device = device;
        m_hwnd = hWnd;
        m_TextureLoader = TextureLoader;
        createIDCount = (int64_t)0;
        SetComponentName("XModelLoader");
    }
    XModelLoader::~XModelLoader()
    {
        AllDestroyComponent();
        for (auto &itr : m_data)
        {
            if (itr.second.asset != nullptr)
            {
                itr.second.asset->Release();
                itr.second.asset = nullptr;
            }
        }
        m_data.clear();
    }

    //==========================================================================
    /**
    @brief Xモデルを読み込む。
    @param path [in] 読み込みパス
    @return Xモデルのポインタ
    */
    XModelReference XModelLoader::Load(const std::string & path)
    {
        auto itr = m_data.find(path);
        // 存在しない場合は読み込み処理を開始する
        if (itr == m_data.end())
        {
            // 読み取り用のデータを生成
            auto &data = m_data[path];

            // 読み込み
            Load(path, &data);
        }
        // データが存在する
        else if (itr != m_data.end())
        {
            // メッシュが存在しない
            if (itr->second.asset == nullptr)
            {
                // 読み込み
                Load(path, &itr->second);
            }
        }

        return &m_data[path];
    }

    //==========================================================================
    /**
    @brief Xモデルを破棄する。
    @param data [in] データ
    */
    void XModelLoader::Unload(XModelData * data)
    {
        if (data == nullptr)return;
        if (data->LoaderPtr != this)return;
        auto itr = m_data.find(data->tag);
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }

    //==========================================================================
    /**
    @brief Xモデルの読み込み機能の本体
    @param path [in] 読み込みパス
    @param Out [out] 受け取り先
    */
    void XModelLoader::Load(const std::string & path, XModelData * Out)
    {
        LPD3DXBUFFER pAdjacency = nullptr; // 隣接情報
        LPD3DXMESH pTempMesh = nullptr; // テンプレートメッシュ
        LPD3DXBUFFER pMaterialBuffer = nullptr; // マテリアルバッファ
        D3DVERTEXELEMENT9 Elements[MAX_FVF_DECL_SIZE];
        HRESULT hr = (HRESULT)0;
        std::string strFilePass; // ファイルパス

        // モデルの読み込み
        if (FAILED(read(path, Out, &pAdjacency, &pMaterialBuffer)))
        {
            ErrorMessage("Xデータが読み込めませんでした。\n %s", path);
        }

        // 最適化
        hr = optimisation(Out, pAdjacency);
        if (FAILED(hr))
        {
            ErrorMessage("Xデータの最適化に失敗しました。\n %s", path);
        }

        // 頂点の宣言
        hr = declaration(Out, Elements);
        if (FAILED(hr))
        {
            ErrorMessage("頂点の宣言に失敗しました。\n %s", path);
        }

        // 複製
        hr = replication(Out, Elements, &pTempMesh);
        if (FAILED(hr))
        {
            ErrorMessage("複製に失敗しました。\n %s", path);
        }

        // ファイルパスの生成
        CreateTexturePass(path, strFilePass);

        // テクスチャの読み込み
        LoadTexture(Out, strFilePass, (LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer());

        if (pMaterialBuffer != nullptr)
        {
            pMaterialBuffer->Release();
            pMaterialBuffer = nullptr;
        }
        if (Out->asset != nullptr)
        {
            Out->asset->Release();
            Out->asset = nullptr;
        }
        if (pAdjacency != nullptr)
        {
            pAdjacency->Release();
            pAdjacency = nullptr;
        }
        Out->asset = pTempMesh;
        for (int i = 0; i < MAX_FVF_DECL_SIZE; i++)
        {
            Out->elements[i] = Elements[i];
        }
        createIDCount++; // 生成カウンタを回す
        Out->createID = createIDCount;
        Out->LoaderPtr = this;
        Out->tag = path;
    }

    //==========================================================================
    /**
    @brief 読み込み
    @param path [in] 読み込みパス
    @param Out [out] 受け取り先
    @param pAdjacency [in] ID3DXBuffer*
    @param pMaterialBuffer [in] ID3DXBuffer*
    @param pDevice [in] デバイス
    @return Component Object Model defines, and macros
    */
    HRESULT XModelLoader::read(const std::string & path, XModelData * Out, LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer)
    {
        return D3DXLoadMeshFromX(path.c_str(), D3DXMESH_SYSTEMMEM, m_device, pAdjacency, pMaterialBuffer, nullptr, &Out->num, &Out->asset);
    }

    //==========================================================================
    /**
    @brief 最適化
    @param Out [out] 受け取り先
    @param pAdjacency [in] ID3DXBuffer*
    @return Component Object Model defines, and macros
    */
    HRESULT XModelLoader::optimisation(XModelData * Out, LPD3DXBUFFER pAdjacency)
    {
        return Out->asset->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)pAdjacency->GetBufferPointer(), nullptr, nullptr, nullptr);
    }

    //==========================================================================
    /**
    @brief 頂点の宣言
    @param Out [out] 受け取り先
    @param pElements [in] D3DVERTEXELEMENT9
    @return Component Object Model defines, and macros
    */
    HRESULT XModelLoader::declaration(XModelData * Out, D3DVERTEXELEMENT9 *pElements)
    {
        return Out->asset->GetDeclaration(pElements);
    }

    //==========================================================================
    /**
    @brief 複製
    @param Out [out] 受け取り先
    @param pElements [in] D3DVERTEXELEMENT9
    @param pTempMesh [in] ID3DXMesh
    @param pDevice [in] デバイス
    @return Component Object Model defines, and macros
    */
    HRESULT XModelLoader::replication(XModelData * Out, D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh)
    {
        return Out->asset->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY, pElements, m_device, pTempMesh);
    }

    //==========================================================================
    /**
    @brief ファイルパスの生成
    @param In [in] 読み込み対象
    @param Out [out] 読み込み対象へのファイルパス受け取り
    */
    void XModelLoader::CreateTexturePass(const std::string & In, std::string & Out)
    {
        Out = In;

        for (auto itr = --Out.end(); itr != Out.begin(); --itr)
        {
            if (itr == Out.begin())
            {
                break;
            }
            if ((strcmp(&(*itr), "/") == 0) || (strcmp(&(*itr), "\0") == 0))
            {
                break;
            }
            Out.erase(itr);
        }
    }

    //==========================================================================
    /**
    @brief テクスチャの読み込み
    @param path [in] 読み込みパス
    @param filepass [in] 読み込み対象へのファイルパス
    @param Out [out] 受け取り先
    @param Input [in] マテリアル
    @return ANSI (Multi-byte Character) types
    */
    void XModelLoader::LoadTexture(XModelData * Out, const std::string & filepass, const LPD3DXMATERIAL Input)
    {
        // 格納領域の予約
        Out->texture.clear();
        Out->texture.reserve(Out->num);
        Out->MatD3D.reserve(Out->num);

        // マテリアルの数だけ
        for (int i = 0; i < (int)Out->num; i++)
        {
            D3DXMATERIAL * pmat = &Input[i];
            Out->MatD3D.push_back(pmat->MatD3D);

            // 使用しているテクスチャがあれば読み込む
            if (pmat->pTextureFilename != nullptr &&lstrlen(pmat->pTextureFilename) > 0)
            {
                Out->texture.emplace_back();
                auto itr = --Out->texture.end();
                (*itr) = m_TextureLoader->Load(filepass + pmat->pTextureFilename);
            }
            else
            {
                Out->texture.emplace_back();
                auto itr = --Out->texture.end();
                (*itr) = m_TextureLoader->Load("");
            }
        }
    }

    //==========================================================================
    /**
    @brief エラーメッセージの表示
    @param text [in] メッセージ内容
    @param path [in] エラーが出たもの
    */
    void XModelLoader::ErrorMessage(const std::string & text, const std::string & path)
    {
        size_t size = snprintf(nullptr, 0, text.c_str(), path.c_str()) + 1; // Extra space for '\0'
        std::unique_ptr<char[]> buf(new char[size]);
        snprintf(buf.get(), size, text.c_str(), path.c_str());
        MessageBox(m_hwnd, std::string(buf.get(), buf.get() + size - 1).c_str(), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
    }

    //==========================================================================
    //
    // class  : SetXModel
    // Content: Xモデル登録クラス
    //
    //==========================================================================
    SetXModel::SetXModel() {}
    SetXModel::~SetXModel() {}

    //==========================================================================
    /**
    @brief モデルデータの登録
    @param data [in] モデルデータ
    */
    void SetXModel::SetXModelData(const XModelReference & data)
    {
        m_XModelData = data;
    }

    //==========================================================================
    /**
    @brief モデルデータの取得
    */
    XModelReference & SetXModel::GetXModelData()
    {
        return m_XModelData;
    }
}

_MSLIB_END