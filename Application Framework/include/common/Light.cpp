//==========================================================================
// ライト[Light.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Light.h"

_MSLIB_BEGIN

Light::Light(LPDIRECT3DDEVICE9 pDevice)
{
    SetComponentName("Light");
    m_aLight = D3DLIGHT9();
    m_Device = pDevice;
}

Light::Light(const Light & light)
{
    m_Device = nullptr;
    *this = light;
    SetComponentName("Light");
}

Light::~Light()
{
}

//==========================================================================
// 初期化
/**
@brief 初期化
@param aVecDir [in] 光のベクトルを入れるところ
*/
void Light::Init(const D3DXVECTOR3 & aVecDir)
{
	// ライトの設定
	ZeroMemory(&m_aLight, sizeof(m_aLight));
	m_aLight.Type = D3DLIGHT_DIRECTIONAL; // ディレクショナルライト
	D3DXVec3Normalize((D3DXVECTOR3*)&m_aLight.Direction, &aVecDir); // 正規化
	m_aLight.Diffuse.r = 1.0f; // 色
	m_aLight.Diffuse.g = 1.0f; // 色
	m_aLight.Diffuse.b = 1.0f; // 色
	m_aLight.Diffuse.a = 1.0f; // 色
	m_aLight.Ambient.r = 0.3f; // アンビエントライト
	m_aLight.Ambient.g = 0.3f; // アンビエントライト
	m_aLight.Ambient.b = 0.3f; // アンビエントライト
	m_aLight.Ambient.a = 0.3f; // アンビエントライト

    m_aLight.Position.y = 10;

	// ライトの設定
    m_Device->SetLight(0, &m_aLight); // ライトの種類
    m_Device->LightEnable(0, TRUE); // 使用の許可

	// グローバルアンビエントの設定
    m_Device->SetRenderState(D3DRS_AMBIENT, D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.f));
}

Light & Light::operator=(const Light & timer)
{
    m_aLight = timer.m_aLight;
    m_Device = timer.m_Device;
    return *this;
}

_MSLIB_END