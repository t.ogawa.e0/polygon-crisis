//==========================================================================
// 描画のテクニック [DrawingTechnology.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DrawingTechnology.h"

_MSLIB_BEGIN

namespace drawing_technology
{
    DrawingTechnology::DrawingTechnology()
    {
        m_TechnologyAdd = false;
    }
    DrawingTechnology::~DrawingTechnology()
    {
    }

    //==========================================================================
    /**
    @brief 加算合成
    @param key [in] 操作
    */
    void DrawingTechnology::TechnologyAddSynthesis(bool key)
    {
        m_TechnologyAdd = key;
    }

    //==========================================================================
    /**
    @brief 半透明処理
    @param pDevice [in] デバイス
    */
    void DrawingTechnology::Add(LPDIRECT3DDEVICE9 device)
    {
        // 半加算合成
        device->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); //アルファブレンディングの有効化
        device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD); //ブレンディングオプション加算
        device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA); //SRCの設定
        device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	//DESTの設定
        device->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); //アルファテストの無効化
        device->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_TRUE);
    }

    //==========================================================================
    /**
    @brief アルファテスト
    @param pDevice [in] デバイス
    @param Power [in] パワー
    */
    void DrawingTechnology::AlpharefStart(LPDIRECT3DDEVICE9 device, int Power)
    {
        device->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_TRUE); //アルファテストの有効化
        device->SetRenderState(D3DRS_ALPHAREF, Power); //アルファ参照値
        device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL); //アルファテスト合格基準
    }

    //==========================================================================
    /**
    @brief アルファテスト_終わり
    @param pDevice [in] デバイス
    */
    void DrawingTechnology::AlpharefEnd(LPDIRECT3DDEVICE9 device)
    {
        device->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); // αテスト
    }

    //==========================================================================
    /**
    @brief 加算合成
    @param pDevice [in] デバイス
    */
    void DrawingTechnology::Sub(LPDIRECT3DDEVICE9 device)
    {
        // 全加算合成
        device->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); //アルファブレンディングの有効化
        device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD); //ブレンディングオプション加算
        device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	//SRCの設定
        device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE); //DESTの設定
        device->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); //アルファテストの無効化
    }

    //==========================================================================
    /**
    @brief Zバッファを描画するか否か
    @param pDevice [in] デバイス
    */
    void DrawingTechnology::ZwriteenableStart(LPDIRECT3DDEVICE9 device)
    {
        device->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_FALSE);
    }

    //==========================================================================
    /**
    @brief Zバッファを描画するか否か_終わり
    @param pDevice [in] デバイス
    */
    void DrawingTechnology::ZwriteenableEnd(LPDIRECT3DDEVICE9 device)
    {
        device->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_TRUE);
    }
}

_MSLIB_END