//==========================================================================
// エフェクト [Effect.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Effect.h"

_MSLIB_BEGIN

namespace effect
{
    //==========================================================================
    //
    // class  : EffectData
    // Content: エフェクトデータ
    //
    //==========================================================================
    EffectData::EffectData()
    {
        asset = nullptr;
        createID = (int64_t)0;
        LoaderPtr = nullptr;
    }
    EffectData::~EffectData()
    {
        if (GetRef() == 0)
        {
            if (asset != nullptr)
            {
                asset->Release();
                asset = nullptr;
            }
            tag.clear();
            LoaderPtr = nullptr;
            createID = 0;
        }
    }

    //==========================================================================
    //
    // class  : EffectReference
    // Content: 参照用
    //
    //==========================================================================
    EffectReference::EffectReference()
    {
        m_data = nullptr;
    }

    EffectReference::~EffectReference()
    {
        Release();
    }
    void EffectReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->LoaderPtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->LoaderPtr->Unload(m_data);
        m_data = nullptr;
    }

    const std::string & EffectReference::Tag()
    {
        return m_data->tag;
    }

    //==========================================================================
    //
    // class  : EffectLoader
    // Content: エフェクト読み込み破棄クラス
    //
    //==========================================================================
    EffectLoader::EffectLoader()
    {
        m_device = nullptr;
        createIDCount = (int64_t)0;
        SetComponentName("EffectLoader");
    }
    EffectLoader::EffectLoader(LPDIRECT3DDEVICE9 device)
    {
        m_device = device;
        createIDCount = (int64_t)0;
        SetComponentName("EffectLoader");
    }
    EffectLoader::~EffectLoader()
    {
        AllDestroyComponent();
        for (auto &itr : m_data)
        {
            if (itr.second.asset != nullptr)
            {
                itr.second.asset->Release();
                itr.second.asset = nullptr;
            }
        }
        m_data.clear();
    }
    EffectReference EffectLoader::Load(const std::string & path)
    {
        auto itr = m_data.find(path);

        // 存在しない場合は読み込み処理を開始する
        if (itr == m_data.end())
        {
            // 読み取り用のデータを生成
            auto * data = &m_data[path];
            LPD3DXBUFFER error = 0;

            if (FAILED(D3DXCreateEffectFromFile(m_device, path.c_str(), 0, 0, 0, 0, &data->asset, &error)))
            {
                OutputDebugStringA((const char*)error->GetBufferPointer());
                return nullptr;
            }

            createIDCount++;
            data->LoaderPtr = this;
            data->createID = createIDCount;
            data->tag = path;
        }

        return &m_data[path];
    }
    void EffectLoader::Unload(EffectData * data)
    {
        if (data == nullptr)return;
        if (data->LoaderPtr != this)return;
        auto itr = m_data.find(data->tag);
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }
    void EffectLoader::Unload(EffectData & data)
    {
        if (data.LoaderPtr != this)return;
        auto itr = m_data.find(data.tag);
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }

    //==========================================================================
    //
    // class  : SetEffect
    // Content: エフェクト登録
    //
    //==========================================================================
    SetEffect::SetEffect()
    {
        m_EffectData = nullptr;
    }
    SetEffect::~SetEffect()
    {
    }
    void SetEffect::SetEffectData(const EffectReference & effect)
    {
        m_EffectData = effect;
    }
    EffectReference & SetEffect::GetEffectData()
    {
        return m_EffectData;
    }

}

_MSLIB_END