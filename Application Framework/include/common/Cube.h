//==========================================================================
// キューブ[Cube.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "mslib.hpp"
#include "mslib_struct.h"
#include "Component.h"
#include "CreateBuffer.h"
#include "DX9_Vertex3D.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace cube
{
    class CreateCube;

    struct CubeInfo
    {
        int MaxIndex = 0; // インデックス数 
        int NumVertex = 0; // 頂点数
        int NumTriangle = 0; // 三角形の数
    };

    //==========================================================================
    //
    // class  : CubeData
    // Content: キューブデータ
    //
    //==========================================================================
    class CubeData : public ireference::ReferenceData<create_buffer::Buffer>
    {
    public:
        CubeData();
        ~CubeData();
    public:
        int Direction1; // 一行の画像数
        int Direction2; // 一列の画像数
        int Pattern; // 面の数
        CubeInfo info; // キューブの情報
        floatTexvec texsize; // テクスチャのサイズ
        floatTexvec texcutsize; // テクスチャの切り取りサイズ
        CreateCube * CreatePtr; // 生成元のポインタ
    };

    //==========================================================================
    //
    // class  : CubeReference
    // Content: 参照用
    //
    //==========================================================================
    class CubeReference : public ireference::ReferenceOperator<CubeData>
    {
    public:
        CubeReference();
        ~CubeReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        const CubeInfo & Info();
    };

    //==========================================================================
    //
    // class  : CreateCube
    // Content: キューブ生成機能
    //
    //==========================================================================
    class CreateCube : public component::Component , public create_buffer::CreateBuffer, private DX9_VERTEX_3D
    {
    private:
        // コピー禁止 (C++11)
        CreateCube(const CreateCube &) = delete;
        CreateCube &operator=(const CreateCube &) = delete;
        CreateCube &operator=(CreateCube&&) = delete;
    public:
        CreateCube();
        CreateCube(LPDIRECT3DDEVICE9 device, HWND hWnd);
        ~CreateCube();

        /**
        @brief キューブを生成する。
        */
        CubeReference Create();

        /**
        @brief キューブを破棄する。
        @param data [in] データ
        */
        void Delete(CubeData * data);
    private:
        /**
        @brief キューブ生成
        @param Output [out] 頂点情報
        @param _this [in] UV座標
        */
        void CreateVertex(VERTEX_4 * Output, const floatUV * _this);

        /**
        @brief Indexの生成
        @param Output [in/out] 受け取り
        @param NumRectangle [in] 数
        */
        void CreateIndex(WORD * Output, int NumRectangle);

        /**
        @brief UV生成
        @param Input [in] 面情報
        @param Output [in] UV情報
        @param nNum [in] 現在の面
        */
        void CreateUV(const CubeData * Input, floatUV * Output, const int nNum);
    private:
        CubeData * m_CubeData; // キューブデータ
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
    };

    //==========================================================================
    //
    // class  : SetCube
    // Content: キューブ登録クラス
    //
    //==========================================================================
    class SetCube
    {
    public:
        SetCube();
        virtual ~SetCube();

        /**
        @brief キューブの登録
        @param data [in] キューブ
        */
        void SetCubeData(const CubeReference & data);

        /**
        @brief キューブの取得
        */
        CubeReference & GetCubeData();
    protected:
        CubeReference m_CubeData; // キューブデータ
    };
}

_MSLIB_END