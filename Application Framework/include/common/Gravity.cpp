//==========================================================================
// �d�� [Gravity.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Gravity.h"
#include "Transform.h"

_MSLIB_BEGIN

namespace gravity
{
    Gravity::Gravity()
    {
        SetComponentName("Gravity");
        m_power = 0.0f;
        m_flag = false;
    }
    Gravity::~Gravity()
    {
    }
    void Gravity::Update()
    {
        auto * target = GetWarrantyParent<transform::Transform>();
        if (target == nullptr)return;

        auto position = *target->GetPosition();

        m_flag = true;
        if (m_position.y == position.y) {
            m_flag = false;
            m_power = 0.0f;
        }

        m_position = position;

        if (-0.5f < m_power)
        {
            m_power -= 0.015f;
        }

        position.y += m_power;
        target->SetPosition(position);
    }
    bool Gravity::Trigger()
    {
        return m_flag;
    }
}

_MSLIB_END