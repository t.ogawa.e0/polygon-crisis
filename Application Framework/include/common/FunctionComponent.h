//==========================================================================
// FunctionComponent [FunctionComponent.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include "mslib.hpp"
#include "Component.h"
#include "Activity.h"

_MSLIB_BEGIN

namespace function
{
    //==========================================================================
    //
    // class  : FunctionComponent
    // Content: 機能コンポーネント一括更新
    //
    //==========================================================================
    class FunctionComponent : public component::Component, public activity::Activity
    {
    public:
        FunctionComponent();
        virtual ~FunctionComponent();

        /**
        @brief 更新
        */
        static void UpdateAll();
    protected:
        virtual void Update() = 0;
    private:
        static std::list<FunctionComponent*> m_list;
    };
}

_MSLIB_END