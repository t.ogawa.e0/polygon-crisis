//==========================================================================
// デバイス[Device.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <memory> 
#include <vector>
#include "mslib.hpp"
#include "mslib_struct.h"
#include "Component.h"

_MSLIB_BEGIN

namespace device
{
    //==========================================================================
    //
    // class  : Device
    // Content: デバイス 
    //
    //==========================================================================
    class Device : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        Device(const Device &) = delete;
        Device &operator=(const Device &) = delete;
        Device &operator=(Device&&) = delete;
    public:
        Device();
        ~Device();

        /**
        @brief 初期化
        @return 失敗時に true が返ります
        */
        void Init();

        /**
        @brief 解放
        */
        void Release();

        /**
        @brief ウィンドウモードの生成
        @param size [in] ウィンドウサイズ
        @param Mode [in] ウィンドウモード true でフルスクリーン
        */
        bool CreateWindowMode(const int2 & size, bool Mode);

        /**
        @brief デバイスの生成
        @return 失敗時に true が返ります
        */
        bool CreateDevice();

        /**
        @brief デバイスの習得
        @return デバイス
        */
        LPDIRECT3DDEVICE9 GetD3DDevice() const;

        /**
        @brief デバイスの習得
        @return デバイス
        */
        D3DPRESENT_PARAMETERS & Getd3dpp() ;

        /**
        @brief ウィンドウサイズの習得
        @return ウィンドウサイズ
        */
        const int2 & GetWindowsSize() const;

        /**
        @brief ウィンドウハンドルの入力
        @param hWnd [in] ウィンドウハンドル
        */
        void SetHwnd(HWND hWnd);

        /**
        @brief ウィンドウハンドルの取得
        @return ウィンドウハンドル
        */
        HWND GetHwnd() const;

        /**
        @brief 推奨ウィンドウサイズとの誤差の取得
        @return 誤差割合が返ります
        */
        float ScreenBuffScale(void) const;

        /**
        @brief 描画開始
        @return 描画可能な際に true が返ります
        */
        bool DrawBegin(void);

        /**
        @brief 描画終了
        @param SourceRect [in]
        @param DestRect [in]
        @param hDestWindowOverride [in]
        @param DirtyRegion [in]
        @return Component Object Model defines, and macros
        */
        HRESULT STDMETHODCALLTYPE DrawEnd(const RECT* SourceRect, const RECT* DestRect, HWND hDestWindowOverride, const RGNDATA* DirtyRegion);
        
        /**
        @brief 描画終了
        @return Component Object Model defines, and macros
        */
        HRESULT STDMETHODCALLTYPE DrawEnd(void);

        /**
        @brief エラーメッセージ
        @param text [in] テキスト入力
        @param args [in] 出力文字
        */
        template <typename ... Args>
        void ErrorMessage(const char * text, const Args & ... args);
    private:
        LPDIRECT3D9 m_lpd3d; //ダイレクト3Dインターフェース
        LPDIRECT3DDEVICE9 m_lpd3ddevice;//ダイレクト3Dデバイス
        D3DPRESENT_PARAMETERS m_d3dpp; // パラメーター
        D3DDISPLAYMODE m_d3dpm; // ディスプレイモード
        HWND m_hwnd; // ウィンドウハンドル
        std::vector<D3DDISPLAYMODE> m_d3dspMode; // ディスプレイモード管理
        int2 m_WindowsSize; // ウィンドウサイズ
        int m_adapter; // ディスプレイアダプタ番号
        float m_ratio; // 推奨サイズとの描画サイズの誤差
    };

    //==========================================================================
    /**
    @brief エラーメッセージ
    @param text [in] テキスト入力
    @param args [in] 出力文字
    */
    template<typename ...Args>
    inline void Device::ErrorMessage(const char * text, const Args & ... args)
    {
        size_t size = snprintf(nullptr, 0, text, args ...) + 1; // Extra space for '\0'
        std::unique_ptr<char[]> buf(new char[size]);
        snprintf(buf.get(), size, text, args ...);

        MessageBox(m_hwnd, std::string(buf.get(), buf.get() + size - 1).c_str(), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
    }
}

_MSLIB_END