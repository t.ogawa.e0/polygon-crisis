//==========================================================================
// ダイレクトインプットのマウス[DinputMouse.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DinputMouse.h"

_MSLIB_BEGIN

namespace dinput_mouse
{
    Speed::Speed()
    {
        m_lX = (LONG)0;
        m_lY = (LONG)0;
        m_lZ = (LONG)0;
    }

    Speed::~Speed()
    {
    }

    Mouse::Mouse()
    {
        SetComponentName("Mouse");
    }

    Mouse::~Mouse()
    {
    }

    //==========================================================================
    /**
    @brief 初期化
    @param hInstance [in] インスタンスハンドル
    @param hWnd [in] ウィンドウハンドル
    @return 失敗時 true が返ります
    */
    bool Mouse::Init(HINSTANCE hInstance, HWND hWnd)
    {
        m_hWnd = hWnd;
        m_mousePos.x = m_mousePos.y = (LONG)0;

        if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_DInput, nullptr)))
        {
            MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
            return true;
        }

        if (FAILED(m_DInput->CreateDevice(GUID_SysMouse, &m_DIDevice, nullptr)))
        {
            MessageBox(hWnd, "マウスがありませんでした。", "警告", MB_OK);
            return true;
        }

        if (m_DIDevice != nullptr)
        {
            // マウス用のデータ・フォーマットを設定
            if (FAILED(m_DIDevice->SetDataFormat(&c_dfDIMouse2)))
            {
                MessageBox(hWnd, "マウスのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
                return true;
            }

            // 協調モードを設定（フォアグラウンド＆非排他モード）
            if (FAILED(m_DIDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
            {
                MessageBox(hWnd, "マウスの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
                return true;
            }

            if (FAILED(m_DIDevice->EnumObjects(EnumAxesCallback, hWnd, DIDFT_AXIS)))
            {
                MessageBox(hWnd, "プロパティを設定できません", "警告", MB_OK);
                return true;
            }

            if (FAILED(m_DIDevice->Poll()))
            {
                while (m_DIDevice->Acquire() == DIERR_INPUTLOST)
                {
                    m_DIDevice->Acquire();
                }
            }
        }

        return false;
    }

    //==========================================================================
    // 軸のコールバック
    BOOL CALLBACK Mouse::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext)
    {
        DIPROPDWORD diprop;

        pContext;
        diprop.diph.dwSize = sizeof(DIPROPDWORD);
        diprop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
        diprop.diph.dwHow = DIPH_DEVICE;
        diprop.diph.dwObj = pdidoi->dwType;
        diprop.dwData = DIPROPAXISMODE_REL; // 相対値モードで設定（絶対値はDIPROPAXISMODE_ABS）    

        return DIENUM_CONTINUE;
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void Mouse::Update(void)
    {
        DIMOUSESTATE2 aState;

        GetCursorPos(&m_mousePos);
        ScreenToClient(m_hWnd, &m_mousePos);

        if (m_DIDevice == nullptr)
        {
            return;
        }

        // デバイスからデータを取得
        if (SUCCEEDED(m_DIDevice->GetDeviceState(sizeof(aState), &aState)))
        {
            for (int i = 0; i < (int)sizeof(DIMOUSESTATE2::rgbButtons); i++)
            {
                // トリガー・リリース情報を生成
                m_StateTrigger[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
                m_StateRelease[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & m_State.rgbButtons[i];

                // リピート情報を生成
                if (aState.rgbButtons[i])
                {
                    if (m_StateRepeatCnt[i] < 20)
                    {
                        m_StateRepeatCnt[i]++;
                        if (m_StateRepeatCnt[i] == 1 || m_StateRepeatCnt[i] >= 20)
                        {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                            m_StateRepeat[i] = aState.rgbButtons[i];
                        }
                        else
                        {
                            m_StateRepeat[i] = 0;
                        }
                    }
                }
                else
                {
                    m_StateRepeatCnt[i] = 0;
                    m_StateRepeat[i] = 0;
                }
                // プレス情報を保存
                m_State.rgbButtons[i] = aState.rgbButtons[i];
            }
            m_State.lX = aState.lX;
            m_State.lY = aState.lY;
            m_State.lZ = aState.lZ;
        }
        else
        {
            // アクセス権を取得
            m_DIDevice->Acquire();
        }
    }

    //==========================================================================
    /**
    @brief Button Cast(int)
    @param cast [in] キャスト対象
    @return キャストされた値が返ります
    */
    Button Mouse::KeyCast(int cast)
    {
        return (Button)cast;
    }

    //==========================================================================
    /**
    @brief プレス
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Mouse::Press(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_State.rgbButtons[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief トリガー
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Mouse::Trigger(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateTrigger[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief リピート
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Mouse::Repeat(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateRelease[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief リリース
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Mouse::Release(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateRelease[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief マウスの速度
    @return マウスの移動速度が返ります
    */
    Speed Mouse::speed(void)
    {
        Speed _speed;

        _speed.m_lX = m_State.lX;
        _speed.m_lY = m_State.lY;
        _speed.m_lZ = m_State.lZ;

        return _speed;
    }

    //==========================================================================
    /**
    @brief カーソルの座標
    @return 画面内座標が返ります
    */
    POINT Mouse::WIN32Cursor(void)
    {
        return m_mousePos;
    }

    //==========================================================================
    /**
    @brief 左クリック
    @return 入力されている場合値が返ります
    */
    SHORT Mouse::WIN32LeftClick(void)
    {
        return GetAsyncKeyState(VK_LBUTTON);
    }

    //==========================================================================
    /**
    @brief 右クリック
    @return 入力されている場合値が返ります
    */
    SHORT Mouse::WIN32RightClick(void)
    {
        return GetAsyncKeyState(VK_RBUTTON);
    }

    //==========================================================================
    /**
    @brief マウスホイールのホールド判定
    @return 入力されている場合値が返ります
    */
    SHORT Mouse::WIN32WheelHold(void)
    {
        return GetAsyncKeyState(VK_MBUTTON);
    }
}
_MSLIB_END