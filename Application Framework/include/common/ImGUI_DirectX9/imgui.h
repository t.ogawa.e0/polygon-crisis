// dear imgui, v1.60 WIP
// (headers)

// ドキュメントについては、imgui.cppファイルを参照してください。
// imgui_demo.cppのImGui::ShowDemoWindow()を呼び出してデモコードを呼び出します。
// imgui.cppの 'Programmer guide'を読み、コードベースでImGuiを設定する方法についての注意を参照してください。
// 最新版を入手 https://github.com/ocornut/imgui

#pragma once

// ユーザ編集可能な設定ファイル（imconfig.hを編集するか、自分のファイル名にIMGUI_USER_CONFIGを定義する）
#ifdef IMGUI_USER_CONFIG
#include IMGUI_USER_CONFIG
#endif
#if !defined(IMGUI_DISABLE_INCLUDE_IMCONFIG_H) || defined(IMGUI_INCLUDE_IMCONFIG_H)
#include "imconfig.h"
#endif

#include <float.h>          // FLT_MAX
#include <stdarg.h>         // va_list
#include <stddef.h>         // ptrdiff_t, NULL
#include <string.h>         // memset, memmove, memcpy, strlen, strchr, strcpy, strcmp

#define IMGUI_VERSION       "1.60 WIP"

// WindowsのDLLの場合、すべてのAPIシンボル宣言の属性を定義します。
#ifndef IMGUI_API
#define IMGUI_API
#endif

// アサーションハンドラを定義します。
#ifndef IM_ASSERT
#include <assert.h>
#define IM_ASSERT(_EXPR)    assert(_EXPR)
#endif

// ヘルパー
// 一部のコンパイラは、printf形式の警告をユーザー関数に適用することをサポートしています。
#if defined(__clang__) || defined(__GNUC__)
#define IM_FMTARGS(FMT)             __attribute__((format(printf, FMT, FMT+1)))
#define IM_FMTLIST(FMT)             __attribute__((format(printf, FMT, 0)))
#else
#define IM_FMTARGS(FMT)
#define IM_FMTLIST(FMT)
#endif
#define IM_ARRAYSIZE(_ARR)          ((int)(sizeof(_ARR)/sizeof(*_ARR)))
#define IM_OFFSETOF(_TYPE,_MEMBER)  ((size_t)&(((_TYPE*)0)->_MEMBER))       // _TYPE内の_MEMBERのオフセット。 現代のC ++でoffsetof()として標準化されています。

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wold-style-cast"
#endif

// フォワード宣言
struct ImDrawChannel;               // ImDrawList::ChannelsSplit()が使用する描画コマンドを順不同で出力するための一時記憶域。
struct ImDrawCmd;                   // 親ImDrawList内の単一の描画コマンド（一般に1 GPU描画呼び出しにマップされます）
struct ImDrawData;                  // フレームのレンダリングに必要なすべての描画コマンドリスト
struct ImDrawList;                  // 単一の描画コマンドリスト（一般に1つのウィンドウに1つ）
struct ImDrawListSharedData;        // 複数の描画リスト間で共有されるデータ（通常、親ImGuiコンテキストが所有しますが、自分で作成することもできます）
struct ImDrawVert;                  // 1つの頂点（デフォルトで20バイト、IMGUI_OVERRIDE_DRAWVERT_STRUCT_LAYOUTのレイアウトをオーバーライド）
struct ImFont;                      // 親ImFontAtlas内の単一フォントのランタイムデータ
struct ImFontAtlas;                 // 複数のフォントのランタイムデータ、複数のフォントを1つのテクスチャに焼く、TTF / OTFフォントローダ
struct ImFontConfig;                // フォントを追加したり、フォントをマージする際の構成データ
struct ImColor;                     // ヘルパー関数は、u32またはfloat4のいずれかに変換できる色を作成します。
struct ImGuiIO;                     // アプリケーションとImGui間の主な構成とI/O
struct ImGuiOnceUponAFrame;         // IMGUI_ONCE_UPON_A_FRAMEマクロで使用されるフレームのコードを1回だけ実行するための簡単なヘルパー
struct ImGuiStorage;                // シンプルなカスタムキー値の保存
struct ImGuiStyle;                  // スタイリング/カラーのランタイムデータ
struct ImGuiTextFilter;             // テキストフィルタを解析して適用します。 フォーマット "aaaaa[,bbbb][,ccccc]"
struct ImGuiTextBuffer;             // テキストのログ/蓄積のためのテキストバッファ
struct ImGuiTextEditCallbackData;   // カスタムImGuiTextEditCallbackを使用している場合のImGui::InputText()の共有状態（希少/高度使用）
struct ImGuiSizeCallbackData;       // カスタムImGuiSizeCallbackを使用するときのカスタム方法でのウィンドウサイズの制約に使用される構造（希少/高度な使用）
struct ImGuiListClipper;            // アイテムの大きなリストを手動でクリップするヘルパー
struct ImGuiPayload;                // ドラッグアンドドロップ操作のユーザーデータペイロード
struct ImGuiContext;                // ImGui context (opaque)

// typedefsと列挙型（互換性とこのファイルの先頭を汚染しないようにintとして宣言されています）
typedef unsigned int ImU32;         // 32ビットの符号なし整数（通常、パックされた色を格納するために使用されます）
typedef unsigned int ImGuiID;       // ウィジェットで使用される一意のID（通常は文字列のスタックからハッシュされます）
typedef unsigned short ImWchar;     // キーボード入力/表示の文字
typedef void* ImTextureID;          // ユーザーデータを使ってテクスチャを識別します（これはあなたが望むものです）imgui.cppのImTextureIDについてのFAQを読んでください。
typedef int ImGuiCol;               // enum: スタイリングのカラー識別子         // enum ImGuiCol_
typedef int ImGuiCond;              // enum: Set *()の条件                    // enum ImGuiCond_
typedef int ImGuiKey;               // enum: キー識別子（ImGui-side enum）      // enum ImGuiKey_
typedef int ImGuiNavInput;          // enum: ナビゲーションの入力識別子         // enum ImGuiNavInput_
typedef int ImGuiMouseCursor;       // enum: マウスカーソル識別子               // enum ImGuiMouseCursor_
typedef int ImGuiStyleVar;          // enum: スタイリングのための可変識別子     // enum ImGuiStyleVar_
typedef int ImDrawCornerFlags;      // flags: for ImDrawList::AddRect*() etc.   // enum ImDrawCornerFlags_
typedef int ImDrawListFlags;        // flags: for ImDrawList                    // enum ImDrawListFlags_
typedef int ImGuiColorEditFlags;    // flags: for ColorEdit*(), ColorPicker*()  // enum ImGuiColorEditFlags_
typedef int ImGuiColumnsFlags;      // flags: for *Columns*()                   // enum ImGuiColumnsFlags_
typedef int ImGuiDragDropFlags;     // flags: for *DragDrop*()                  // enum ImGuiDragDropFlags_
typedef int ImGuiComboFlags;        // flags: for BeginCombo()                  // enum ImGuiComboFlags_
typedef int ImGuiFocusedFlags;      // flags: for IsWindowFocused()             // enum ImGuiFocusedFlags_
typedef int ImGuiHoveredFlags;      // flags: for IsItemHovered() etc.          // enum ImGuiHoveredFlags_
typedef int ImGuiInputTextFlags;    // flags: for InputText*()                  // enum ImGuiInputTextFlags_
typedef int ImGuiNavFlags;          // flags: for io.NavFlags                   // enum ImGuiNavFlags_
typedef int ImGuiSelectableFlags;   // flags: for Selectable()                  // enum ImGuiSelectableFlags_
typedef int ImGuiTreeNodeFlags;     // flags: for TreeNode*(),CollapsingHeader()// enum ImGuiTreeNodeFlags_
typedef int ImGuiWindowFlags;       // flags: for Begin*()                      // enum ImGuiWindowFlags_
typedef int (*ImGuiTextEditCallback)(ImGuiTextEditCallbackData *data);
typedef void (*ImGuiSizeCallback)(ImGuiSizeCallbackData* data);
#if defined(_MSC_VER) && !defined(__clang__)
typedef unsigned __int64 ImU64;     // 64-bit unsigned integer
#else
typedef unsigned long long ImU64;   // 64-bit unsigned integer
#endif 

// その他のヘルパーはファイルの最後にあります:
// class ImVector<>                 // 軽量std::vectorのようなクラス。
// IMGUI_ONCE_UPON_A_FRAME          // フレームごとに一度だけコードブロックを実行する（複数回実行されるディープネストされたコード内のUIを作成するのに便利）

struct ImVec2
{
    float x, y;
    ImVec2() { x = y = 0.0f; }
    ImVec2(float _x, float _y) { x = _x; y = _y; }
    float operator[] (size_t idx) const { IM_ASSERT(idx == 0 || idx == 1); return *(&x + idx); }    // この[]演算子はほとんど使用しないので、アサーションは問題ありません。
#ifdef IM_VEC2_CLASS_EXTRA          // imconfig.hにコンストラクタと暗黙のキャスト演算子を定義して、数式の型とImVec2を逆に変換します。
    IM_VEC2_CLASS_EXTRA
#endif
};

struct ImVec4
{
    float x, y, z, w;
    ImVec4() { x = y = z = w = 0.0f; }
    ImVec4(float _x, float _y, float _z, float _w) { x = _x; y = _y; z = _z; w = _w; }
#ifdef IM_VEC4_CLASS_EXTRA          // imconfig.hにコンストラクタと暗黙的なキャスト演算子を定義して、数式の型とImVec4を逆に変換します。
    IM_VEC4_CLASS_EXTRA
#endif
};

// ImGuiエンドユーザAPI
// namespaceで、ユーザーが別のファイルに別の関数を追加できるようにする（例：Vectorや一般的な型のValue()ヘルパーなど）
namespace ImGui
{
    // コンテキストの作成とアクセス、複数のコンテキストを使用する場合は、モジュール間でコンテキストを共有します（DLLなど）。
    // デフォルトでは、すべてのコンテキストが同じImFontAtlasを共有します。 別のフォントアトラスが必要な場合は、それらをnew()してGetIO()。ImguiコンテキストのFonts変数を上書きすることができます。
    // これらの関数はすべて現在のコンテキストに依存しません。 
    IMGUI_API ImGuiContext* CreateContext(ImFontAtlas* shared_font_atlas = NULL);
    IMGUI_API void          DestroyContext(ImGuiContext* ctx = NULL);   // NULL = Destroy current context
    IMGUI_API ImGuiContext* GetCurrentContext();
    IMGUI_API void          SetCurrentContext(ImGuiContext* ctx);

    // メイン
    IMGUI_API ImGuiIO&      GetIO();
    IMGUI_API ImGuiStyle&   GetStyle();
    IMGUI_API ImDrawData*   GetDrawData();                              // io.RenderDrawListsFn()関数に渡される値と同じです。 Render()の後とNewFrame()への次の呼び出しまで有効です。
    IMGUI_API void          NewFrame();                                 // 新しいImGuiフレームを開始するときは、Render()/EndFrame()までこのコマンドを実行することができます。
    IMGUI_API void          Render();                                   // ImGuiフレームを終了し、描画データを確定してから、設定されている場合はio.RenderDrawListsFn()関数を呼び出します。
    IMGUI_API void          EndFrame();                                 // 私はImGuiフレームを完成させます。 Render()によって自動的に呼び出されるため、直接呼び出す必要はありません。 レンダリングを実行する必要がない場合、EndFrame()は呼び出すことができますが、CPUを無駄にします。 レンダリングする必要がない場合は、imguiウィンドウを作成しないでください。

    // デモ、デバッグ、情報
    IMGUI_API void          ShowDemoWindow(bool* p_open = NULL);        // デモ/テストウィンドウ（以前はShowTestWindowと呼ばれていました）を作成します。 ほとんどのImGuiの機能を示しています。 ライブラリについて学ぶためにこれを呼んでください！ あなたのアプリケーションでいつでも利用できるようにしてください！
    IMGUI_API void          ShowMetricsWindow(bool* p_open = NULL);     // メトリックウィンドウを作成します。 ImGuiの内部を表示する：描画コマンド（個々の描画呼び出しと頂点付き）、ウィンドウリスト、基本的な内部状態など
    IMGUI_API void          ShowStyleEditor(ImGuiStyle* ref = NULL);    // スタイルエディタブロックを追加します（ウィンドウではありません）。 比較するImGuiStyle構造体を渡して元に戻して保存することができます（それ以外の場合はデフォルトスタイルを使用します）
    IMGUI_API bool          ShowStyleSelector(const char* label);
    IMGUI_API void          ShowFontSelector(const char* label);
    IMGUI_API void          ShowUserGuide();                            // 基本的なヘルプ/情報ブロック（ウィンドウではない）を追加する：ImGuiをエンドユーザ（マウス/キーボードコントロール）として操作する方法。
    IMGUI_API const char*   GetVersion();

    // Window
    IMGUI_API bool          Begin(const char* name, bool* p_open = NULL, ImGuiWindowFlags flags = 0);   // ウィンドウをスタックにプッシュして追加を開始します。 詳細は.cppを参照してください。 ウィンドウが折りたたまれているときにfalseを返すので（コード内で早めに行うことができます）、End()を常に呼び出す必要があります。 'bool * p_open'は右上にウィジェットを作成してウィンドウを閉じます（boolをfalseに設定します）。
    IMGUI_API void          End();                                                              // Begin()がfalseを返す場合でも常に呼び出されます（折りたたまれたウィンドウを示します）！ 現在のウィンドウに追加を終了し、ウィンドウスタックからポップします。
    IMGUI_API bool          BeginChild(const char* str_id, const ImVec2& size = ImVec2(0,0), bool border = false, ImGuiWindowFlags flags = 0);  // スクロール領域を開始します。 size == 0.0f：残りのウィンドウサイズ、サイズ<0.0f：残りのウィンドウサイズからabs（サイズ）を引いた値を使用します。 サイズ> 0.0f：固定サイズ。 各軸は異なるモードを使用することができる。 ImVec2（0,400）。
    IMGUI_API bool          BeginChild(ImGuiID id, const ImVec2& size = ImVec2(0,0), bool border = false, ImGuiWindowFlags flags = 0);          // "
    IMGUI_API void          EndChild();                                                         // BeginChild()がfalseを返す場合でも常に呼び出されます（子ウィンドウが折りたたまれているかクリッピングされていることを示します）
    IMGUI_API ImVec2        GetContentRegionMax();                                              // 現在のコンテンツ境界（通常、スクロールを含むウィンドウ境界、または現在の列境界）、ウィンドウ座標
    IMGUI_API ImVec2        GetContentRegionAvail();                                            // == GetContentRegionMax() - GetCursorPos()
    IMGUI_API float         GetContentRegionAvailWidth();                                       //
    IMGUI_API ImVec2        GetWindowContentRegionMin();                                        // content boundaries min (roughly (0,0)-Scroll), ウィンドウ座標
    IMGUI_API ImVec2        GetWindowContentRegionMax();                                        // content boundaries max (roughly (0,0)+Size-Scroll) Sizeは、SetNextWindowContentSize()を使用してウィンドウ座標でオーバーライドすることができます
    IMGUI_API float         GetWindowContentRegionWidth();                                      //
    IMGUI_API ImDrawList*   GetWindowDrawList();                                                // 独自の描画プリミティブを追加する場合は、描画コマンドリストを取得する
    IMGUI_API ImVec2        GetWindowPos();                                                     // 画面スペース内の現在のウィンドウ位置を取得します（DrawList APIを使用して独自の描画を行いたい場合に便利です）
    IMGUI_API ImVec2        GetWindowSize();                                                    // 現在のウィンドウサイズを取得する
    IMGUI_API float         GetWindowWidth();
    IMGUI_API float         GetWindowHeight();
    IMGUI_API bool          IsWindowCollapsed();
    IMGUI_API bool          IsWindowAppearing();
    IMGUI_API void          SetWindowFontScale(float scale);                                    // ウィンドウごとのフォントサイズ。 すべてのウィンドウを拡大/縮小する場合は、TO.Font GlobalScaleを調整します。

    IMGUI_API void          SetNextWindowPos(const ImVec2& pos, ImGuiCond cond = 0, const ImVec2& pivot = ImVec2(0,0)); // 次のウィンドウ位置を設定します。 Begin()の前に呼び出します。 ピボット=（0.5f、0.5f）を使用して、与えられた点を中心にします。
    IMGUI_API void          SetNextWindowSize(const ImVec2& size, ImGuiCond cond = 0);          // 次のウィンドウサイズを設定します。 この軸にオートフィットを強制するには、0.0fに軸を設定します。 Begin()の前に呼び出す
    IMGUI_API void          SetNextWindowSizeConstraints(const ImVec2& size_min, const ImVec2& size_max, ImGuiSizeCallback custom_callback = NULL, void* custom_callback_data = NULL); // 次のウィンドウサイズ制限を設定します。 X / Y軸のいずれかで-1、-1を使用して現在のサイズを維持します。 コールバックを使用して、プログラム上の制約を軽く適用します。
    IMGUI_API void          SetNextWindowContentSize(const ImVec2& size);                       // 次のウィンドウのコンテンツサイズを設定する（スクロールバーの範囲を強制する）。 ウィンドウ装飾（タイトルバー、メニューバーなど）は含まれません。 0.0fに軸を設定して自動にします。 Begin()の前に呼び出す
    IMGUI_API void          SetNextWindowCollapsed(bool collapsed, ImGuiCond cond = 0);         // 次のウィンドウの折り畳み状態を設定します。 Begin()の前に呼び出す
    IMGUI_API void          SetNextWindowFocus();                                               // 次のウィンドウをフォーカス/最前面に設定します。 Begin()の前に呼び出す
    IMGUI_API void          SetNextWindowBgAlpha(float alpha);                                  // 次のウィンドウの背景色アルファを設定します。 ImGuiCol_WindowBg / ChildBg / PopupBgを簡単に変更するヘルパー。
    IMGUI_API void          SetWindowPos(const ImVec2& pos, ImGuiCond cond = 0);                // （推奨しません）現在のウィンドウ位置を設定する - Begin() End()内で呼び出します。 SetNextWindowPos()を使用することをお勧めします。これは、引き裂きや副作用が発生する可能性があるためです。
    IMGUI_API void          SetWindowSize(const ImVec2& size, ImGuiCond cond = 0);              // （推奨しません）現在のウィンドウサイズを設定する - Begin() End()内で呼び出します。 オートフィットを強制するには、ImVec2（0,0）に設定します。 SetNextWindowSize()を使用することをお勧めします。これは、引き裂きや副作用が発生する可能性があるためです。    
    IMGUI_API void          SetWindowCollapsed(bool collapsed, ImGuiCond cond = 0);             // （推奨しません）現在のウィンドウの折り畳み状態を設定します。 SetNextWindowCollapsed()を使用することをお勧めします。
    IMGUI_API void          SetWindowFocus();                                                   // （推奨しません）現在のウィンドウをフォーカス/最前面に設定します。 SetNextWindowFocus()を使用することをお勧めします。
    IMGUI_API void          SetWindowPos(const char* name, const ImVec2& pos, ImGuiCond cond = 0);      // 名前付きウィンドウの位置を設定します。
    IMGUI_API void          SetWindowSize(const char* name, const ImVec2& size, ImGuiCond cond = 0);    // 名前付きウィンドウサイズを設定します。 この軸にオートフィットを強制するには、0.0fに軸を設定します。
    IMGUI_API void          SetWindowCollapsed(const char* name, bool collapsed, ImGuiCond cond = 0);   // 名前付きウィンドウの折り畳み状態を設定する
    IMGUI_API void          SetWindowFocus(const char* name);                                           // 指定されたウィンドウをフォーカス/最前面に設定します。 フォーカスを削除するにはNULLを使用します。

    IMGUI_API float         GetScrollX();                                                       // スクロール量を取得する [0..GetScrollMaxX()]
    IMGUI_API float         GetScrollY();                                                       // スクロール量を取得する [0..GetScrollMaxY()]
    IMGUI_API float         GetScrollMaxX();                                                    // 最大のスクロール量を得る ~~ ContentSize.X - WindowSize.X
    IMGUI_API float         GetScrollMaxY();                                                    // 最大のスクロール量を得る ~~ ContentSize.Y - WindowSize.Y
    IMGUI_API void          SetScrollX(float scroll_x);                                         // スクロール量を設定する [0..GetScrollMaxX()]
    IMGUI_API void          SetScrollY(float scroll_y);                                         // スクロール量を設定する [0..GetScrollMaxY()]
    IMGUI_API void          SetScrollHere(float center_y_ratio = 0.5f);                         // スクロール量を調整して、現在のカーソル位置を表示させます。 center_y_ratio = 0.0：top、0.5：center、1.0：bottom。 「デフォルト/現在のアイテム」を表示するために使用する場合は、代わりにSetItemDefaultFocus()を使用することを検討してください。
    IMGUI_API void          SetScrollFromPosY(float pos_y, float center_y_ratio = 0.5f);        // 指定した位置を有効にするためにスクロール量を調整します。 GetCursorPos()またはGetCursorStartPos()+ offsetを使用して有効な位置を取得します。
    IMGUI_API void          SetStateStorage(ImGuiStorage* tree);                                // ツリー状態のストレージを自分のものに置き換えます（自分で操作したい場合は、通常はそのサブセクションをクリアします）
    IMGUI_API ImGuiStorage* GetStateStorage();

    // パラメータスタック（共有）
    IMGUI_API void          PushFont(ImFont* font);                                             // ショートカットとしてNULLを使用してデフォルトフォントをプッシュする
    IMGUI_API void          PopFont();
    IMGUI_API void          PushStyleColor(ImGuiCol idx, ImU32 col);
    IMGUI_API void          PushStyleColor(ImGuiCol idx, const ImVec4& col);
    IMGUI_API void          PopStyleColor(int count = 1);
    IMGUI_API void          PushStyleVar(ImGuiStyleVar idx, float val);
    IMGUI_API void          PushStyleVar(ImGuiStyleVar idx, const ImVec2& val);
    IMGUI_API void          PopStyleVar(int count = 1);
    IMGUI_API const ImVec4& GetStyleColorVec4(ImGuiCol idx);                                    // ImGuiStyle構造体に格納されているスタイルカラーを取得します。 PushStyleColor()にフィードバックし、GetColorU32()を使用してスタイルカラー+アルファを取得します。
    IMGUI_API ImFont*       GetFont();                                                          // 現在のフォントを取得する
    IMGUI_API float         GetFontSize();                                                      // 現在のフォントが適用されている現在のフォントサイズ（=ピクセル単位の高さ）を取得する
    IMGUI_API ImVec2        GetFontTexUvWhitePixel();                                           // ImDrawList APIを使用してカスタムシェイプを描画する際に便利なUV座標の取得
    IMGUI_API ImU32         GetColorU32(ImGuiCol idx, float alpha_mul = 1.0f);                  // 指定されたスタイルカラーをスタイルアルファを適用し、オプションのアルファアルファ
    IMGUI_API ImU32         GetColorU32(const ImVec4& col);                                     // 指定された色をスタイルアルファを適用して取得する
    IMGUI_API ImU32         GetColorU32(ImU32 col);                                             // 指定された色をスタイルアルファを適用して取得する

    // パラメータスタック（現在のウィンドウ）
    IMGUI_API void          PushItemWidth(float item_width);                                    // 共通アイテムのアイテムの幅+ラベルの場合、ピクセル。 0.0f =ウィンドウの幅の2/3までデフォルト、> 0.0f：ピクセルの幅、<0.0fウィンドウの右側にxxピクセルを配置する（-1.0fは常に右側に幅を揃える）
    IMGUI_API void          PopItemWidth();
    IMGUI_API float         CalcItemWidth();                                                    // プッシュされた設定のアイテムの幅と現在のカーソル位置
    IMGUI_API void          PushTextWrapPos(float wrap_pos_x = 0.0f);                           // Text *()コマンドのワードラップ。 <0.0f：ラッピングなし。 0.0f：ウィンドウ（またはカラム）の終わりまでラップします。 > 0.0f：ウィンドウローカルスペースの 'wrap_pos_x'の位置でラップする
    IMGUI_API void          PopTextWrapPos();
    IMGUI_API void          PushAllowKeyboardFocus(bool allow_keyboard_focus);                  // デフォルトで有効になっているTAB / Shift-Tabを使ってフォーカスを合わせることができますが、特定のウィジェットでは無効にすることができます
    IMGUI_API void          PopAllowKeyboardFocus();
    IMGUI_API void          PushButtonRepeat(bool repeat);                                      // 'repeat'モードでは、Button *()関数は（io.KeyRepeatDelay / io.KeyRepeatRate設定を使用して）typematicな方法で繰り返しtrueを返します。 Button()の後にIsItemActive()を呼び出すと、ボタンが現在のフレームに保持されているかどうかを知ることができます。
    IMGUI_API void          PopButtonRepeat();

    // カーソル/レイアウト
    IMGUI_API void          Separator();                                                        // セパレータ、一般に水平である。 メニューバーの内側または水平レイアウトモードでは、これは垂直セパレータになります。
    IMGUI_API void          SameLine(float pos_x = 0.0f, float spacing_w = -1.0f);              // ウィジェットまたはグループ間を呼び出してそれらを水平にレイアウトする
    IMGUI_API void          NewLine();                                                          // SameLine()を元に戻す
    IMGUI_API void          Spacing();                                                          // 垂直間隔を追加する
    IMGUI_API void          Dummy(const ImVec2& size);                                          // 指定されたサイズのダミーアイテムを追加する
    IMGUI_API void          Indent(float indent_w = 0.0f);                                      // コンテンツの位置をstyle.IndentSpacingまたは！= 0の場合はindent_wで右側に移動します。
    IMGUI_API void          Unindent(float indent_w = 0.0f);                                    // コンテンツの位置をstyle.IndentSpacingまたは！= 0の場合はindent_wで左に戻します。
    IMGUI_API void          BeginGroup();                                                       // （グループ全体でIsItemHovered()やSameLine()などのレイアウトプリミティブを使用できるように、水平開始位置+グループの境界ボックスを1つのアイテムにキャプチャします）
    IMGUI_API void          EndGroup();
    IMGUI_API ImVec2        GetCursorPos();                                                     // カーソルの位置はウィンドウの位置に相対的です
    IMGUI_API float         GetCursorPosX();                                                    // "
    IMGUI_API float         GetCursorPosY();                                                    // "
    IMGUI_API void          SetCursorPos(const ImVec2& local_pos);                              // "
    IMGUI_API void          SetCursorPosX(float x);                                             // "
    IMGUI_API void          SetCursorPosY(float y);                                             // "
    IMGUI_API ImVec2        GetCursorStartPos();                                                // 初期カーソル位置
    IMGUI_API ImVec2        GetCursorScreenPos();                                               // カーソル位置を絶対座標[0..io.DisplaySize]（ImDrawList APIを使用する場合に便利）
    IMGUI_API void          SetCursorScreenPos(const ImVec2& pos);                              // 絶対スクリーン座標のカーソル位置[0..io.DisplaySize]
    IMGUI_API void          AlignTextToFramePadding();                                          // 次回のテキストをFramePadding.yに垂直方向に整列/下げます。これにより、今後のウィジェットにアライメントされます（通常のウィジェットの前に行頭にテキストがある場合に呼び出されます）
    IMGUI_API float         GetTextLineHeight();                                                // ~ フォントサイズ
    IMGUI_API float         GetTextLineHeightWithSpacing();                                     // ~ FontSize + style.ItemSpacing.y（連続する2行のテキスト間の距離）
    IMGUI_API float         GetFrameHeight();                                                   // ~ FontSize + style.FramePadding.y * 2
    IMGUI_API float         GetFrameHeightWithSpacing();                                        // ~ FontSize + style.FramePadding.y * 2 + style.ItemSpacing.y （フレームウィジェットの2つの連続した行の間のピクセル単位の距離）

    // 列
    // SameLine（pos_x）を単純な列に使用することもできます。 列APIはまだ進行中であり、むしろ欠けています。
    IMGUI_API void          Columns(int count = 1, const char* id = NULL, bool border = true);
    IMGUI_API void          NextColumn();                                                       // 次の列は、現在の行が終了した場合は現在の行または次の行にデフォルト設定されます
    IMGUI_API int           GetColumnIndex();                                                   // 現在の列インデックスを取得する
    IMGUI_API float         GetColumnWidth(int column_index = -1);                              // 列幅を取得します（ピクセル単位）。 現在の列を使用するには-1を渡す
    IMGUI_API void          SetColumnWidth(int column_index, float width);                      // 列幅をピクセル単位で設定します。 現在の列を使用するには-1を渡す
    IMGUI_API float         GetColumnOffset(int column_index = -1);                             // 列の行の位置を取得します（コンテンツ領域の左側からピクセル単位）。 現在の列を使用する場合は-1を渡し、それ以外の場合は0..GetColumnsCount()を含めます。 列0は通常0.0fです
    IMGUI_API void          SetColumnOffset(int column_index, float offset_x);                  // 列の行の位置を設定します（コンテンツ領域の左側からピクセル単位）。 現在の列を使用するには-1を渡す
    IMGUI_API int           GetColumnsCount();

    // IDスコープ
    // ループ内にウィジェットを作成する場合は、一意の識別子（オブジェクトポインタ、ループインデックスなど）をプッシュしたいと思う可能性が高いため、ImGuiで区別することができます。
    // ウィジェットラベル内の "## foobar"構文を使用して、それらを区別することもできます。 詳細については、FAQの「ラベル/ IDの使用に関するプライマー」をお読みください。
    IMGUI_API void          PushID(const char* str_id);                                         // 識別子をIDスタックにプッシュします。 IDはスタック全体のハッシュです！
    IMGUI_API void          PushID(const char* str_id_begin, const char* str_id_end);
    IMGUI_API void          PushID(const void* ptr_id);
    IMGUI_API void          PushID(int int_id);
    IMGUI_API void          PopID();
    IMGUI_API ImGuiID       GetID(const char* str_id);                                          // 一意のID（IDスタック全体のハッシュ+与えられたパラメータ）を計算します。 例えば あなたがImGuiStorageにあなた自身を照会したいのであれば
    IMGUI_API ImGuiID       GetID(const char* str_id_begin, const char* str_id_end);
    IMGUI_API ImGuiID       GetID(const void* ptr_id);

    // ウィジェット：テキスト
    IMGUI_API void          TextUnformatted(const char* text, const char* text_end = NULL);               // フォーマットせずに生のテキスト。 テキスト（ "％s"、テキスト）とほぼ同じですが、A） 'text_end'が指定されている場合はNULL終了文字列を必要とせず、B）高速ですが、 テキストのメモリコピーが行われず、バッファサイズ制限もなく
    IMGUI_API void          Text(const char* fmt, ...)                                     IM_FMTARGS(1); // 単純な書式付きテキスト
    IMGUI_API void          TextV(const char* fmt, va_list args)                           IM_FMTLIST(1);
    IMGUI_API void          TextColored(const ImVec4& col, const char* fmt, ...)           IM_FMTARGS(2); // ショートカット for PushStyleColor(ImGuiCol_Text, col); Text(fmt, ...); PopStyleColor();
    IMGUI_API void          TextColoredV(const ImVec4& col, const char* fmt, va_list args) IM_FMTLIST(2);
    IMGUI_API void          TextDisabled(const char* fmt, ...)                             IM_FMTARGS(1); // ショートカット for PushStyleColor(ImGuiCol_Text, style.Colors[ImGuiCol_TextDisabled]); Text(fmt, ...); PopStyleColor();
    IMGUI_API void          TextDisabledV(const char* fmt, va_list args)                   IM_FMTLIST(1);
    IMGUI_API void          TextWrapped(const char* fmt, ...)                              IM_FMTARGS(1); // ショートカット for PushTextWrapPos(0.0f); Text(fmt, ...); PopTextWrapPos();. ウィンドウの幅を拡張する他のウィジェットがない場合は、自動サイズ変更ウィンドウでは機能しません.YoyはSetNextWindowSize()を使用してサイズを設定する必要があります。
    IMGUI_API void          TextWrappedV(const char* fmt, va_list args)                    IM_FMTLIST(1);
    IMGUI_API void          LabelText(const char* label, const char* fmt, ...)             IM_FMTARGS(2); // 表示テキスト+ラベルは値+ラベルウィジェットと同じ方法で整列されます
    IMGUI_API void          LabelTextV(const char* label, const char* fmt, va_list args)   IM_FMTLIST(2);
    IMGUI_API void          BulletText(const char* fmt, ...)                               IM_FMTARGS(1); // ショートカット for Bullet()+Text()
    IMGUI_API void          BulletTextV(const char* fmt, va_list args)                     IM_FMTLIST(1);
    IMGUI_API void          Bullet();                                                                     // 小さな円を描き、同じ行にカーソルを置きます。 GetTreeNodeToLabelSpacing()、TreeNode()と同じ距離でカーソルの位置を進める

    // ウィジェット：メイン
    IMGUI_API bool          Button(const char* label, const ImVec2& size = ImVec2(0,0));            // ボタン
    IMGUI_API bool          SmallButton(const char* label);                                         // ボタンにFramePadding =（0,0）を指定すると、テキスト内に簡単に埋め込むことができます
    IMGUI_API bool          InvisibleButton(const char* str_id, const ImVec2& size);                // ビジュアルのないボタンの動作、パブリックAPI（IsItemActive、IsItemHoveredなど）を使用してカスタムビヘイビアを構築する場合に便利です。
    IMGUI_API void          Image(ImTextureID user_texture_id, const ImVec2& size, const ImVec2& uv0 = ImVec2(0,0), const ImVec2& uv1 = ImVec2(1,1), const ImVec4& tint_col = ImVec4(1,1,1,1), const ImVec4& border_col = ImVec4(0,0,0,0));
    IMGUI_API bool          ImageButton(ImTextureID user_texture_id, const ImVec2& size, const ImVec2& uv0 = ImVec2(0,0),  const ImVec2& uv1 = ImVec2(1,1), int frame_padding = -1, const ImVec4& bg_col = ImVec4(0,0,0,0), const ImVec4& tint_col = ImVec4(1,1,1,1));    // <0 frame_paddingは、デフォルトのフレーム埋め込み設定を使用します。 パディングなしの場合は0
    IMGUI_API bool          Checkbox(const char* label, bool* v);
    IMGUI_API bool          CheckboxFlags(const char* label, unsigned int* flags, unsigned int flags_value);
    IMGUI_API bool          RadioButton(const char* label, bool active);
    IMGUI_API bool          RadioButton(const char* label, int* v, int v_button);
    IMGUI_API void          PlotLines(const char* label, const float* values, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0,0), int stride = sizeof(float));
    IMGUI_API void          PlotLines(const char* label, float (*values_getter)(void* data, int idx), void* data, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0,0));
    IMGUI_API void          PlotHistogram(const char* label, const float* values, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0,0), int stride = sizeof(float));
    IMGUI_API void          PlotHistogram(const char* label, float (*values_getter)(void* data, int idx), void* data, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0,0));
    IMGUI_API void          ProgressBar(float fraction, const ImVec2& size_arg = ImVec2(-1,0), const char* overlay = NULL);

    // ウィジェット：コンボボックス
    // 新しいBeginCombo()/ EndCombo()APIを使用すると、コンテンツや選択状態を自由に管理できます。
    // 古いCombo()APIはBeginCombo()/EndCombo()のヘルパーであり、利便性のために利用できます。
    IMGUI_API bool          BeginCombo(const char* label, const char* preview_value, ImGuiComboFlags flags = 0);
    IMGUI_API void          EndCombo(); // BeginCombo()がtrueを返す場合にのみ、EndCombo()を呼び出してください！
    IMGUI_API bool          Combo(const char* label, int* current_item, const char* const items[], int items_count, int popup_max_height_in_items = -1);
    IMGUI_API bool          Combo(const char* label, int* current_item, const char* items_separated_by_zeros, int popup_max_height_in_items = -1);      // Separate items with \0 within a string, end item-list with \0\0. e.g. "One\0Two\0Three\0"
    IMGUI_API bool          Combo(const char* label, int* current_item, bool(*items_getter)(void* data, int idx, const char** out_text), void* data, int items_count, int popup_max_height_in_items = -1);

    // ウィジェット：ドラッグ（先端：ctrl +ドラッグボックスをクリックしてキーボードで入力する。手動入力値はクランプされず、オフ境界に行くことができる）
    // すべての関数のすべてのFloat2 / Float3 / Float4 / Int2 / Int3 / Int4バージョンでは、 'float v [X]'関数の引数は 'float * v'と同じであることに注意してください。 アクセス可能であると予想される要素の数 最初の要素のアドレスを連続したセットから渡すことができます。 &myvector.x
    // 速度はマウスの動きのピクセル単位です（v_speed = 0.2f：マウスを5ピクセル移動して値を1増やす必要があります）。 ゲームパッド/キーボードナビゲーションの場合、最低速度はMax（v_speed、minimum_step_at_given_precision）です。
    IMGUI_API bool          DragFloat(const char* label, float* v, float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", float power = 1.0f);     // v_min> = v_maxの場合、バインドされていません
    IMGUI_API bool          DragFloat2(const char* label, float v[2], float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          DragFloat3(const char* label, float v[3], float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          DragFloat4(const char* label, float v[4], float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          DragFloatRange2(const char* label, float* v_current_min, float* v_current_max, float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", const char* display_format_max = NULL, float power = 1.0f);
    IMGUI_API bool          DragInt(const char* label, int* v, float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");                                       // v_min> = v_maxの場合、バインドされていません
    IMGUI_API bool          DragInt2(const char* label, int v[2], float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");
    IMGUI_API bool          DragInt3(const char* label, int v[3], float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");
    IMGUI_API bool          DragInt4(const char* label, int v[4], float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");
    IMGUI_API bool          DragIntRange2(const char* label, int* v_current_min, int* v_current_max, float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f", const char* display_format_max = NULL);

    // ウィジェット：キーボードによる入力
    IMGUI_API bool          InputText(const char* label, char* buf, size_t buf_size, ImGuiInputTextFlags flags = 0, ImGuiTextEditCallback callback = NULL, void* user_data = NULL);
    IMGUI_API bool          InputTextMultiline(const char* label, char* buf, size_t buf_size, const ImVec2& size = ImVec2(0,0), ImGuiInputTextFlags flags = 0, ImGuiTextEditCallback callback = NULL, void* user_data = NULL);
    IMGUI_API bool          InputFloat(const char* label, float* v, float step = 0.0f, float step_fast = 0.0f, int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputFloat2(const char* label, float v[2], int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputFloat3(const char* label, float v[3], int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputFloat4(const char* label, float v[4], int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputInt(const char* label, int* v, int step = 1, int step_fast = 100, ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputInt2(const char* label, int v[2], ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputInt3(const char* label, int v[3], ImGuiInputTextFlags extra_flags = 0);
    IMGUI_API bool          InputInt4(const char* label, int v[4], ImGuiInputTextFlags extra_flags = 0);

    // ウィジェット：スライダ（ヒント：ctrl +スライダをクリックしてキーボードで入力します。手動入力値はクランプされず、オフ境界に入ることがあります）
    IMGUI_API bool          SliderFloat(const char* label, float* v, float v_min, float v_max, const char* display_format = "%.3f", float power = 1.0f);     // display_formatを調整して、スライダ内のラベルや単位表示の接頭辞または接尾辞で値を飾ります。 対数スライダにはpower！= 1.0を使用する
    IMGUI_API bool          SliderFloat2(const char* label, float v[2], float v_min, float v_max, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          SliderFloat3(const char* label, float v[3], float v_min, float v_max, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          SliderFloat4(const char* label, float v[4], float v_min, float v_max, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          SliderAngle(const char* label, float* v_rad, float v_degrees_min = -360.0f, float v_degrees_max = +360.0f);
    IMGUI_API bool          SliderInt(const char* label, int* v, int v_min, int v_max, const char* display_format = "%.0f");
    IMGUI_API bool          SliderInt2(const char* label, int v[2], int v_min, int v_max, const char* display_format = "%.0f");
    IMGUI_API bool          SliderInt3(const char* label, int v[3], int v_min, int v_max, const char* display_format = "%.0f");
    IMGUI_API bool          SliderInt4(const char* label, int v[4], int v_min, int v_max, const char* display_format = "%.0f");
    IMGUI_API bool          VSliderFloat(const char* label, const ImVec2& size, float* v, float v_min, float v_max, const char* display_format = "%.3f", float power = 1.0f);
    IMGUI_API bool          VSliderInt(const char* label, const ImVec2& size, int* v, int v_min, int v_max, const char* display_format = "%.0f");

    // ウィジェット：カラーエディタ/ピッカー（ヒント：ColorEdit *の機能には、ピッカーを開くために左クリックして右クリックしてオプションメニューを開くことができる、ちょっとした色のプレビュースクエアがあります）
    // 'float v [X]'関数の引数は 'float * v'と同じであることに注意してください。配列の構文は、アクセス可能と思われる要素の数を記録する単なる方法です。 連続する構造体のうち、最初のfloat要素のアドレスを渡すことができます。 &myvector.x
    IMGUI_API bool          ColorEdit3(const char* label, float col[3], ImGuiColorEditFlags flags = 0);
    IMGUI_API bool          ColorEdit4(const char* label, float col[4], ImGuiColorEditFlags flags = 0);
    IMGUI_API bool          ColorPicker3(const char* label, float col[3], ImGuiColorEditFlags flags = 0);
    IMGUI_API bool          ColorPicker4(const char* label, float col[4], ImGuiColorEditFlags flags = 0, const float* ref_col = NULL);
    IMGUI_API bool          ColorButton(const char* desc_id, const ImVec4& col, ImGuiColorEditFlags flags = 0, ImVec2 size = ImVec2(0,0));  // 色付きの正方形/ボタンを表示し、詳細を表示するにはホバーし、押されたら真を返します。
    IMGUI_API void          SetColorEditOptions(ImGuiColorEditFlags flags);                         // 既定の形式、ピッカーの種類などを選択する場合は、現在のオプションを（通常はアプリケーションの起動時に）初期化します。呼び出しに_NoOptionsフラグを渡さない限り、ユーザーは多くの設定を変更できます。

    // ウィジェット：ツリー
    IMGUI_API bool          TreeNode(const char* label);                                            // 'true'を返すと、ノードは開いており、ツリーidはidスタックにプッシュされます。 ユーザーはTreePop()を呼び出す責任があります。
    IMGUI_API bool          TreeNode(const char* str_id, const char* fmt, ...) IM_FMTARGS(2);       // なぜIDを使用するのかについてのFAQを読んでください。 任意のテキストをTreeNode()と同じレベルに揃えるには、Bullet()を使用できます。
    IMGUI_API bool          TreeNode(const void* ptr_id, const char* fmt, ...) IM_FMTARGS(2);       // "
    IMGUI_API bool          TreeNodeV(const char* str_id, const char* fmt, va_list args) IM_FMTLIST(2);
    IMGUI_API bool          TreeNodeV(const void* ptr_id, const char* fmt, va_list args) IM_FMTLIST(2);
    IMGUI_API bool          TreeNodeEx(const char* label, ImGuiTreeNodeFlags flags = 0);
    IMGUI_API bool          TreeNodeEx(const char* str_id, ImGuiTreeNodeFlags flags, const char* fmt, ...) IM_FMTARGS(3);
    IMGUI_API bool          TreeNodeEx(const void* ptr_id, ImGuiTreeNodeFlags flags, const char* fmt, ...) IM_FMTARGS(3);
    IMGUI_API bool          TreeNodeExV(const char* str_id, ImGuiTreeNodeFlags flags, const char* fmt, va_list args) IM_FMTLIST(3);
    IMGUI_API bool          TreeNodeExV(const void* ptr_id, ImGuiTreeNodeFlags flags, const char* fmt, va_list args) IM_FMTLIST(3);
    IMGUI_API void          TreePush(const char* str_id);                                           // ~ Indent()+ PushId()。 trueを返すときにすでにTreeNode()によって呼び出されていますが、レイアウト目的でPush / Popを自分で呼び出すことができます
    IMGUI_API void          TreePush(const void* ptr_id = NULL);                                    // "
    IMGUI_API void          TreePop();                                                              // ~ Unindent()+PopId()
    IMGUI_API void          TreeAdvanceToLabelPos();                                                // GetTreeNodeToLabelSpacing()によってカーソルxの位置を進める
    IMGUI_API float         GetTreeNodeToLabelSpacing();                                            // 通常のunframed TreeNodeの場合、TreeNode *()またはBullet()==（g.FontSize + style.FramePadding.x * 2）を使用するときのラベルの前の水平距離
    IMGUI_API void          SetNextTreeNodeOpen(bool is_open, ImGuiCond cond = 0);                  // 次のTreeNode / CollapsingHeaderオープン状態を設定します。
    IMGUI_API bool          CollapsingHeader(const char* label, ImGuiTreeNodeFlags flags = 0);      // 'true'を返す場合、ヘッダーは開いています。 インデントしたり、IDスタックを押したりしません。 ユーザーはTreePop()を呼び出す必要はありません。
    IMGUI_API bool          CollapsingHeader(const char* label, bool* p_open, ImGuiTreeNodeFlags flags = 0); // 'p_open'がNULLでなければ、ヘッダの右上に追加の小さな閉じるボタンを表示します

    // ウィジェット：選択可能/リスト
    IMGUI_API bool          Selectable(const char* label, bool selected = false, ImGuiSelectableFlags flags = 0, const ImVec2& size = ImVec2(0,0));  // "bool selected"は選択状態を保持します（読み取り専用）。 Selectable()がクリックされた場合はtrueを返し、選択状態を変更できます。 size.x == 0.0：残りの幅を使用する、size.x> 0.0：幅を指定する。 size.y == 0.0：ラベルの高さを使用、size.y> 0.0：高さを指定
    IMGUI_API bool          Selectable(const char* label, bool* p_selected, ImGuiSelectableFlags flags = 0, const ImVec2& size = ImVec2(0,0));       // "bool * p_selected"は便利なヘルパーとして選択状態（読み書き可能）を指します。
    IMGUI_API bool          ListBox(const char* label, int* current_item, const char* const items[], int items_count, int height_in_items = -1);
    IMGUI_API bool          ListBox(const char* label, int* current_item, bool (*items_getter)(void* data, int idx, const char** out_text), void* data, int items_count, int height_in_items = -1);
    IMGUI_API bool          ListBoxHeader(const char* label, const ImVec2& size = ImVec2(0,0));     // ListBox()がカスタムデータまたはインタラクションを再実装する場合に使用します。 後でListBoxFooter()を呼び出すようにしてください。
    IMGUI_API bool          ListBoxHeader(const char* label, int items_count, int height_in_items = -1); // "
    IMGUI_API void          ListBoxFooter();                                                        // スクロール領域を終了する

    // ウィジェット：Value()ヘルパー。 "name：value"形式で単一の値を出力する（ヒント：あなたの型を扱うためにコード内でもっと自由に宣言する、ImGui名前空間に関数を追加することができる）
    IMGUI_API void          Value(const char* prefix, bool b);
    IMGUI_API void          Value(const char* prefix, int v);
    IMGUI_API void          Value(const char* prefix, unsigned int v);
    IMGUI_API void          Value(const char* prefix, float v, const char* float_format = NULL);

    // ツールチップ
    IMGUI_API void          SetTooltip(const char* fmt, ...) IM_FMTARGS(1);                     // マウスカーソルの下にテキストツールチップを設定します。通常、ImGuI::IsItemHovered()で使用します。 以前のSetTooltip()呼び出しをオーバーライドします。
    IMGUI_API void          SetTooltipV(const char* fmt, va_list args) IM_FMTLIST(1);
    IMGUI_API void          BeginTooltip();                                                     // ツールチップウィンドウを開始/追加します。 フル装備のツールチップ（あらゆる種類のコンテンツを含む）を作成します。
    IMGUI_API void          EndTooltip();

    // メニュー
    IMGUI_API bool          BeginMainMenuBar();                                                 // 作成し、フルスクリーンのメニューバーに追加します。
    IMGUI_API bool          BeginMainMenuBar(const ImVec2 & v);                                                 // 作成し、フルスクリーンのメニューバーに追加します。
    IMGUI_API void          EndMainMenuBar();                                                   // BeginMainMenuBar()がtrueを返す場合にのみ、EndMainMenuBar()を呼び出します。
    IMGUI_API bool          BeginMenuBar();                                                     // 現在のウィンドウのメニューバーに追加します（親ウィンドウにImGuiWindowFlags_MenuBarフラグが設定されている必要があります）。
    IMGUI_API void          EndMenuBar();                                                       // BeginMenuBar()がtrueを返す場合にのみEndMenuBar()を呼び出します。
    IMGUI_API bool          BeginMenu(const char* label, bool enabled = true);                  // サブメニュー項目を作成します。 trueを返す場合は、EndMenu()を呼び出します。
    IMGUI_API void          EndMenu();                                                          // BeginMenu()がtrueを返す場合にのみ、EndBegin()を呼び出してください！
    IMGUI_API bool          MenuItem(const char* label, const char* shortcut = NULL, bool selected = false, bool enabled = true);  // 起動時にtrueを返します。 ショートカットは便宜上表示されますが、現時点ではImGuiで処理されません
    IMGUI_API bool          MenuItem(const char* label, const char* shortcut, bool* p_selected, bool enabled = true);              // 起動時に真を返す+ p_selected！= NULLの場合はトグル（* p_selected）

    // ポップアップ
    IMGUI_API void          OpenPopup(const char* str_id);                                      // ポップアップを開いた状態で呼び出す（すべてのフレームを呼び出さないでください）。 ユーザーが外部をクリックするか、BeginPopup()/ EndPopup()ブロック内でCloseCurrentPopup()が呼び出されたときにポップアップが閉じられます。 デフォルトでは、Selectable()/ MenuItem()はCloseCurrentPopup()を呼び出しています。 ポップアップ識別子は現在のIDスタックとの相対的なものです（したがって、OpenPopupとBeginPopupは同じレベルにする必要があります）。
    IMGUI_API bool          BeginPopup(const char* str_id, ImGuiWindowFlags flags = 0);                                             // ポップアップが開いている場合はtrueを返し、出力を開始することができます。 BeginPopup()がtrueを返す場合にのみ、EndPopup()を呼び出します。
    IMGUI_API bool          BeginPopupContextItem(const char* str_id = NULL, int mouse_button = 1);                                 // 最後のアイテムをクリックするとヘルパが開き、ポップアップを開始します。 前の項目にidがあった場合にのみ、NULLのstr_idを渡すことができます。 Text()などの非インタラクティブな項目でそれを使用する場合は、ここで明示的なIDを渡す必要があります。 .cppのコメントを読む！
    IMGUI_API bool          BeginPopupContextWindow(const char* str_id = NULL, int mouse_button = 1, bool also_over_items = true);  // 現在のウィンドウでクリックするとポップアップを開き、ヘルパーを起動します。
    IMGUI_API bool          BeginPopupContextVoid(const char* str_id = NULL, int mouse_button = 1);                                 // void（imguiウィンドウがないところ）をクリックすると、ヘルパーが開いてポップアップを開始します。
    IMGUI_API bool          BeginPopupModal(const char* name, bool* p_open = NULL, ImGuiWindowFlags flags = 0);                     // モーダルダイアログ（タイトルバーを持つ通常のウィンドウ、モーダルウィンドウの後ろのやりとりをブロックする、外部をクリックしてモーダルウィンドウを閉じることはできません）
    IMGUI_API void          EndPopup();                                                                                             // BeginPopupXXX()がtrueを返す場合にのみ、EndPopup()を呼び出します。
    IMGUI_API bool          OpenPopupOnItemClick(const char* str_id = NULL, int mouse_button = 1);                                  // 最後の項目をクリックしたときにポップアップを開くヘルパー。 開いたときにtrueを返します。
    IMGUI_API bool          IsPopupOpen(const char* str_id);                                    // ポップアップが開いている場合はtrueを返します。
    IMGUI_API void          CloseCurrentPopup();                                                // 開始したポップアップを閉じます。 MenuItemまたはSelectableをクリックすると、現在のポップアップが自動的に閉じます。

    // ロギング/キャプチャ：インターフェイスからのすべてのテキスト出力は、tty / file / clipboardにキャプチャされます。 デフォルトでは、ログ中にツリーノードが自動的に開きます。
    IMGUI_API void          LogToTTY(int max_depth = -1);                                       // ttyへのロギングを開始する
    IMGUI_API void          LogToFile(int max_depth = -1, const char* filename = NULL);         // ファイルへのロギングを開始する
    IMGUI_API void          LogToClipboard(int max_depth = -1);                                 // OSクリップボードへのロギングを開始する
    IMGUI_API void          LogFinish();                                                        // ロギングを停止する（ファイルを閉じるなど）
    IMGUI_API void          LogButtons();                                                       // tty / file / clipboardへのロギングのためのボタンを表示するヘルパー
    IMGUI_API void          LogText(const char* fmt, ...) IM_FMTARGS(1);                        // テキストデータをそのままログに渡す（表示せずに）

    // ドラッグアンドドロップ
    // [BETA API]デモコードがありません。 APIが進化するかもしれない。
    IMGUI_API bool          BeginDragDropSource(ImGuiDragDropFlags flags = 0, int mouse_button = 0);                // 現在の項目がアクティブなときに呼び出されます。 これがtrueを返す場合は、SetDragDropPayload()+ EndDragDropSource()を呼び出すことができます。
    IMGUI_API bool          SetDragDropPayload(const char* type, const void* data, size_t size, ImGuiCond cond = 0);// typeは最大12文字のユーザー定義文字列です。 '_'で始まる文字列は、貴重なimgui内部型のために予約されています。 データはimguiによってコピーされ、保持されます。
    IMGUI_API void          EndDragDropSource();                                                                    // BeginDragDropSource()がtrueを返す場合にのみ、EndDragDropSource()を呼び出します。
    IMGUI_API bool          BeginDragDropTarget();                                                                  // アイテムを受け取ったアイテムを送信した後に呼び出します。 trueを返す場合は、AcceptDragDropPayload()+ EndDragDropTarget()を呼び出すことができます。
    IMGUI_API const ImGuiPayload* AcceptDragDropPayload(const char* type, ImGuiDragDropFlags flags = 0);            // 指定された型の内容を受け入れます。 ImGuiDragDropFlags_AcceptBeforeDeliveryが設定されている場合、マウスボタンが離される前にペイロードを覗くことができます。
    IMGUI_API void          EndDragDropTarget();                                                                    // BeginDragDropTarget()がtrueを返す場合にのみ、EndDragDropTarget()を呼び出します。

    // クリッピング
    IMGUI_API void          PushClipRect(const ImVec2& clip_rect_min, const ImVec2& clip_rect_max, bool intersect_with_current_clip_rect);
    IMGUI_API void          PopClipRect();

    // スタイル
    IMGUI_API void          StyleColorsClassic(ImGuiStyle* dst = NULL);
    IMGUI_API void          StyleColorsDark(ImGuiStyle* dst = NULL);
    IMGUI_API void          StyleColorsLight(ImGuiStyle* dst = NULL);

    // フォーカス、アクティブ化
    // （可能であれば "If（IsWindowAppearing()）SetScrollHere()"よりも "SetItemDefaultFocus()"を使用して、ナビゲーション分岐がマージされたときにコードをより順方向互換にすることを推奨します）
    IMGUI_API void          SetItemDefaultFocus();                                              // 最後の項目をウィンドウのデフォルトフォーカス項目にする。 "if（IsWindowAppearing()）SetScrollHere()"の代わりに "default item"を使用してください。
    IMGUI_API void          SetKeyboardFocusHere(int offset = 0);                               // フォーカスキーボードを次のウィジェットに追加します。 複数のコンポーネントウィジェットのサブコンポーネントにアクセスするには、正のオフセットを使用します。 前のウィジェットにアクセスするには-1を使用します。

    // ユーティリティー
    IMGUI_API bool          IsItemHovered(ImGuiHoveredFlags flags = 0);                         // 最後に見つけたアイテムですか？ （そして、使用可能、ポップアップなどでブロックされていない）。 その他のオプションについては、ImGuiHoveredFlagsを参照してください。
    IMGUI_API bool          IsItemActive();                                                     // 最後のアイテムはアクティブですか？ （例えば、ボタンが押されている、テキストフィールドが編集中 - 相互作用しないアイテムは常にfalseを返す）
    IMGUI_API bool          IsItemFocused();                                                    // キーボード/ゲームパッドナビゲーションのための最後の項目ですか？
    IMGUI_API bool          IsItemClicked(int mouse_button = 0);                                // 最後にクリックされた項目ですか？ （ボタン/ノードをクリックしたなど）
    IMGUI_API bool          IsItemVisible();                                                    // 最後の項目が表示されていますか？ （切り抜き/スクロールによる視覚障害ではありません）
    IMGUI_API bool          IsAnyItemHovered();
    IMGUI_API bool          IsAnyItemActive();
    IMGUI_API bool          IsAnyItemFocused();
    IMGUI_API ImVec2        GetItemRectMin();                                                   // 最後のアイテムの境界矩形を画面空間に取得する
    IMGUI_API ImVec2        GetItemRectMax();                                                   // "
    IMGUI_API ImVec2        GetItemRectSize();                                                  // 画面空間の最後のアイテムのサイズを取得する
    IMGUI_API void          SetItemAllowOverlap();                                              // 最後の項目を次の項目と重複させることができます。 使用されていない領域をキャッチするために不可視のボタンや選択可能なものなどで便利なことがあります。
    IMGUI_API bool          IsWindowFocused(ImGuiFocusedFlags flags = 0);                       // 現在のウィンドウにフォーカスしていますか？ またはフラグに応じてそのルート/子になります。 オプションのフラグを参照してください。
    IMGUI_API bool          IsWindowHovered(ImGuiHoveredFlags flags = 0);                       // 現在のウィンドウがホバリングされています（通常、ポップアップ/モーダルでブロックされません）。 オプションのフラグを参照してください。
    IMGUI_API bool          IsRectVisible(const ImVec2& size);                                  // 指定されたサイズの矩形がカーソル位置から始まっているかどうかをテストします。クリップされません。
    IMGUI_API bool          IsRectVisible(const ImVec2& rect_min, const ImVec2& rect_max);      // 矩形（画面空間内）が可視かどうかテストします。 ユーザの側で粗いクリッピングを実行する。
    IMGUI_API float         GetTime();
    IMGUI_API int           GetFrameCount();
    IMGUI_API ImDrawList*   GetOverlayDrawList();                                               // この描画リストは最後にレンダリングされたもので、オーバーレイの図形やテキストを素早く描画するのに便利です
    IMGUI_API ImDrawListSharedData* GetDrawListSharedData();
    IMGUI_API const char*   GetStyleColorName(ImGuiCol idx);
    IMGUI_API ImVec2        CalcTextSize(const char* text, const char* text_end = NULL, bool hide_text_after_double_hash = false, float wrap_width = -1.0f);
    IMGUI_API void          CalcListClipping(int items_count, float items_height, int* out_items_display_start, int* out_items_display_end);    // 均等に大きさのあるアイテムの大きなリストのための粗いクリッピングを計算します。 可能であれば、ImGuiListClipperの上位ヘルパーを使用することをお勧めします。

    IMGUI_API bool          BeginChildFrame(ImGuiID id, const ImVec2& size, ImGuiWindowFlags flags = 0); // 通常のウィジェットフレームのように見える子ウィンドウ/スクロール領域を作成するヘルパー
    IMGUI_API void          EndChildFrame();                                                    // BeginChildFrame()の戻り値（折りたたまれたウィンドウまたはクリップされたウィンドウを示す）に関係なく、常にEndChildFrame()を呼び出します。

    IMGUI_API ImVec4        ColorConvertU32ToFloat4(ImU32 in);
    IMGUI_API ImU32         ColorConvertFloat4ToU32(const ImVec4& in);
    IMGUI_API void          ColorConvertRGBtoHSV(float r, float g, float b, float& out_h, float& out_s, float& out_v);
    IMGUI_API void          ColorConvertHSVtoRGB(float h, float s, float v, float& out_r, float& out_g, float& out_b);

    // 入力
    IMGUI_API int           GetKeyIndex(ImGuiKey imgui_key);                                    // ImGuiKey_ *の値をユーザーのキーインデックスにマップします。 == io.KeyMap [key]
    IMGUI_API bool          IsKeyDown(int user_key_index);                                      // キーが開催されています。 == io.KeysDown [user_key_index]。 imguiはio.KeyDown []の各エントリの意味を知らないことに注意してください。 あなたのバックエンド/エンジンがそれらをKeyDown []に保存する方法に応じて、独自のインデックス/列挙型を使用してください！
    IMGUI_API bool          IsKeyPressed(int user_key_index, bool repeat = true);               // キーが押された（ダウンからダウンへ）。 repeat = trueの場合、io.KeyRepeatDelay / KeyRepeatRateを使用します。
    IMGUI_API bool          IsKeyReleased(int user_key_index);                                  // キーがリリースされました（ダウンからダウンへ）
    IMGUI_API int           GetKeyPressedAmount(int key_index, float repeat_delay, float rate); // 提供された繰り返しレート/遅延を使用します。 カウントを返します。ほとんどの場合0または1ですが、RepeatRateがDeltaTime> RepeatRate
    IMGUI_API bool          IsMouseDown(int button);                                            // マウスボタンが押されている
    IMGUI_API bool          IsMouseClicked(int button, bool repeat = false);                    // マウスボタンをクリックしたか（！ダウンからダウン）
    IMGUI_API bool          IsMouseDoubleClicked(int button);                                   // マウスボタンをダブルクリックしました。 ダブルクリックはIsMouseClicked()でfalseを返します。 io.MouseDoubleClickTimeを使用します。
    IMGUI_API bool          IsMouseReleased(int button);                                        // マウスボタンが放されました（ダウンからダウンへ）
    IMGUI_API bool          IsMouseDragging(int button = 0, float lock_threshold = -1.0f);      // マウスドラッグです。 lock_threshold <-1.0fがio.MouseDraggingThresholdを使用する場合
    IMGUI_API bool          IsMouseHoveringRect(const ImVec2& r_min, const ImVec2& r_max, bool clip = true);  // マウスがマウスの上に置かれています（スクリーンスペースで）。 現在のクリッピング設定でクリップされます。 フォーカス/ウィンドウ順序の考慮の無視/ポップアップによってブロックされます。
    IMGUI_API bool          IsMousePosValid(const ImVec2* mouse_pos = NULL);                    //
    IMGUI_API ImVec2        GetMousePos();                                                      // ImGui::GetIO()へのショートカット。ユーザーが提供するMousePos。他の呼び出しと一貫性がある
    IMGUI_API ImVec2        GetMousePosOnOpeningCurrentPopup();                                 // 私たちはBeginPopup()を持ってポップアップを開く時にマウスの位置のバックアップを取得
    IMGUI_API ImVec2        GetMouseDragDelta(int button = 0, float lock_threshold = -1.0f);    // クリックしてからドラッグします。 lock_threshold <-1.0fがio.MouseDraggingThresholdを使用する場合
    IMGUI_API void          ResetMouseDragDelta(int button = 0);                                //
    IMGUI_API ImGuiMouseCursor GetMouseCursor();                                                // ImGui::NewFrame()でリセットしたいカーソルタイプを取得します。これはフレーム中に更新されます。 Render()の前に有効です。 io.MouseDrawCursorを設定してソフトウェアレンダリングを使用する場合、ImGuiはそれらをレンダリングします
    IMGUI_API void          SetMouseCursor(ImGuiMouseCursor type);                              // 希望のカーソルタイプを設定する
    IMGUI_API void          CaptureKeyboardFromApp(bool capture = true);                        // 手動でio.WantCaptureKeyboardフラグを次のフレームにオーバーライドします（このフラグはアプリケーションハンドルのために完全に残されています）。 例えば あなたのウィジェットがホバリングされているときに強制的にキャプチャキーボード。
    IMGUI_API void          CaptureMouseFromApp(bool capture = true);                           // 手動でio.WantCaptureMouseフラグを次のフレームにオーバーライドします（このフラグは、アプリケーションハンドルのために完全に残されています）。

    // ヘルパーは、メモリアロケータとクリップボード関数にアクセスする機能を持っています。
    IMGUI_API void          SetAllocatorFunctions(void* (*alloc_func)(size_t sz, void* user_data), void(*free_func)(void* ptr, void* user_data), void* user_data = NULL);
    IMGUI_API void*         MemAlloc(size_t size);
    IMGUI_API void          MemFree(void* ptr);
    IMGUI_API const char*   GetClipboardText();
    IMGUI_API void          SetClipboardText(const char* text);

} // namespace ImGui

// Flags for ImGui::Begin()
enum ImGuiWindowFlags_
{
    ImGuiWindowFlags_NoTitleBar             = 1 << 0,   // タイトルバーを無効にする
    ImGuiWindowFlags_NoResize               = 1 << 1,   // 右下のグリップでユーザーのサイズ変更を無効にする
    ImGuiWindowFlags_NoMove                 = 1 << 2,   // ユーザーがウィンドウを移動するのを無効にする
    ImGuiWindowFlags_NoScrollbar            = 1 << 3,   // スクロールバーを無効にする（ウィンドウはマウスまたはプログラムでスクロールできる）
    ImGuiWindowFlags_NoScrollWithMouse      = 1 << 4,   // マウスホイールで垂直方向にスクロールするユーザーを無効にします。子ウィンドウでは、NoScrollbarも設定されていない限り、マウスホイールは親に転送されます。
    ImGuiWindowFlags_NoCollapse             = 1 << 5,   // ユーザがダブルクリックして折り畳むことを無効にする
    ImGuiWindowFlags_AlwaysAutoResize       = 1 << 6,   // すべてのウィンドウをすべてのフレームのコンテンツにサイズ変更する
    //ImGuiWindowFlags_ShowBorders          = 1 << 7,   // ウィンドウとアイテムの周りに境界を表示する（OBSOLETE！境界線を有効にするには、例えばstyle.FrameBorderSize = 1.0fを使用します）。
    ImGuiWindowFlags_NoSavedSettings        = 1 << 8,   // 設定を.iniファイルにロード/保存しない
    ImGuiWindowFlags_NoInputs               = 1 << 9,   // マウスやキーボード入力の捕捉を無効にし、パススルーでホバリングテストを行います。
    ImGuiWindowFlags_MenuBar                = 1 << 10,  // メニューバーがあります
    ImGuiWindowFlags_HorizontalScrollbar    = 1 << 11,  // 水平スクロールバーが表示されるようにします（デフォルトではオフ）。 SetNextWindowContentSize（ImVec2（width、0.0f））を使用できます。 Begin()を呼び出して幅を指定します。 imgui_demoのコードを「水平スクロール」セクションで読み込みます。
    ImGuiWindowFlags_NoFocusOnAppearing     = 1 << 12,  // 非表示状態から可視状態に遷移するときにフォーカスを無効にする
    ImGuiWindowFlags_NoBringToFrontOnFocus  = 1 << 13,  // フォーカスを取るときにウィンドウを前面に移動しないようにします（例えば、クリックするか、フォーカスをプログラム的に与えるなど）
    ImGuiWindowFlags_AlwaysVerticalScrollbar= 1 << 14,  // 常に垂直スクロールバーを表示する（ContentSize.y <Size.yの場合でも）
    ImGuiWindowFlags_AlwaysHorizontalScrollbar=1<< 15,  // 常に水平スクロールバーを表示する（ContentSize.x <Size.xの場合でも）
    ImGuiWindowFlags_AlwaysUseWindowPadding = 1 << 16,  // 境界線を持たない子ウィンドウにstyle.WindowPaddingを使用するようにしました。（ボーダーなしの子ウィンドウではデフォルトで無視されます。
    ImGuiWindowFlags_ResizeFromAnySide      = 1 << 17,  // （WIP）任意の角と罫線からのサイズ変更を有効にします。バックエンドは、imguiによって設定された異なるio.MouseCursorの値を尊重する必要があります。
    ImGuiWindowFlags_NoNavInputs            = 1 << 18,  // ウィンドウ内にゲームパッド/キーボードナビゲーションがありません
    ImGuiWindowFlags_NoNavFocus             = 1 << 19,  // ゲームパッド/キーボードナビゲーションを使用してこのウィンドウにフォーカスしない（例：CTRL + TABでスキップ）
    ImGuiWindowFlags_NoNav                  = ImGuiWindowFlags_NoNavInputs | ImGuiWindowFlags_NoNavFocus,

    // [Internal]
    ImGuiWindowFlags_NavFlattened           = 1 << 23,  // （WIP）ゲームパッド/キーボードナビゲーションがこのボーダーの枠線を越えて移動できるようにします（スクロールしていない子供だけに使用します）。
    ImGuiWindowFlags_ChildWindow            = 1 << 24,  // BeginChild()による内部使用については、使用しないでください 
    ImGuiWindowFlags_Tooltip                = 1 << 25,  // BeginTooltip()による内部使用については、使用しないでください 
    ImGuiWindowFlags_Popup                  = 1 << 26,  // BeginPopup()による内部使用については、使用しないでください
    ImGuiWindowFlags_Modal                  = 1 << 27,  // BeginPopupModal()による内部使用については、使用しないでください
    ImGuiWindowFlags_ChildMenu              = 1 << 28   // BeginMenu()による内部使用については、使用しないでください
};

// Flags for ImGui::InputText()
enum ImGuiInputTextFlags_
{
    ImGuiInputTextFlags_CharsDecimal        = 1 << 0,   // Allow 0123456789.+-*/
    ImGuiInputTextFlags_CharsHexadecimal    = 1 << 1,   // Allow 0123456789ABCDEFabcdef
    ImGuiInputTextFlags_CharsUppercase      = 1 << 2,   // a..zをA..Zに変換する
    ImGuiInputTextFlags_CharsNoBlank        = 1 << 3,   // 空白、タブを除外する
    ImGuiInputTextFlags_AutoSelectAll       = 1 << 4,   // 最初にマウスをフォーカスしたときにテキスト全体を選択する
    ImGuiInputTextFlags_EnterReturnsTrue    = 1 << 5,   // Enterキーが押されたときに（値が変更されたときとは対照的に）「true」を返します。
    ImGuiInputTextFlags_CallbackCompletion  = 1 << 6,   // TABを押すとユーザー関数を呼び出します（完了処理のために）
    ImGuiInputTextFlags_CallbackHistory     = 1 << 7,   // 上向き/下向きの矢印を押してユーザ機能を呼び出す（履歴処理用）
    ImGuiInputTextFlags_CallbackAlways      = 1 << 8,   // 毎回ユーザー関数を呼び出します。ユーザコードは、カーソル位置を照会し、テキストバッファを変更することができる。
    ImGuiInputTextFlags_CallbackCharFilter  = 1 << 9,   // 文字をフィルタリングするユーザ関数を呼び出します。 data-> EventCharを変更して入力を置き換えたり、1を返して文字を破棄したりします。
    ImGuiInputTextFlags_AllowTabInput       = 1 << 10,  // TABを押すと、テキストフィールドに '\ t'文字が入力されます
    ImGuiInputTextFlags_CtrlEnterForNewLine = 1 << 11,  // 複数行モードでは、Enterキーでフォーカスを外し、Ctrl + Enterで新しい行を追加します（デフォルトは逆：Ctrl + Enterで非表示、Enterで行を追加）。
    ImGuiInputTextFlags_NoHorizontalScroll  = 1 << 12,  // カーソルを水平方向に移動しない
    ImGuiInputTextFlags_AlwaysInsertMode    = 1 << 13,  // 挿入モード
    ImGuiInputTextFlags_ReadOnly            = 1 << 14,  // 読み取り専用モード
    ImGuiInputTextFlags_Password            = 1 << 15,  // パスワードモードでは、すべての文字を「*」として表示します。
    ImGuiInputTextFlags_NoUndoRedo          = 1 << 16,  // 元に戻す/やり直しを無効にします。独自の取り消し/やり直しスタックを提供したい場合は、入力テキストがアクティブな間にテキストデータを所有することに注意してください。 ClearActiveID()を呼び出します。
    // [内部]
    ImGuiInputTextFlags_Multiline           = 1 << 20   // InputTextMultiline()による内部使用
};

// Flags for ImGui::TreeNodeEx(), ImGui::CollapsingHeader*()
enum ImGuiTreeNodeFlags_
{
    ImGuiTreeNodeFlags_Selected             = 1 << 0,   // 選択して描画
    ImGuiTreeNodeFlags_Framed               = 1 << 1,   // フルカラーのフレーム（CollapsingHeaderなど）
    ImGuiTreeNodeFlags_AllowItemOverlap     = 1 << 2,   // 後続のウィジェットがこのウィジェットをオーバーラップできるようにテストを打つ
    ImGuiTreeNodeFlags_NoTreePushOnOpen     = 1 << 3,   // 開いているときにTreePush()を実行しないでください（例えば、CollapsingHeaderの場合）=余分なインデントがない、またはIDスタックを押していない
    ImGuiTreeNodeFlags_NoAutoOpenOnLog      = 1 << 4,   // ロギングがアクティブなときにノードを自動的かつ一時的に開かないでください（デフォルトでロギングは自動的にツリーノードを開きます）
    ImGuiTreeNodeFlags_DefaultOpen          = 1 << 5,   // 既定のノードを開く
    ImGuiTreeNodeFlags_OpenOnDoubleClick    = 1 << 6,   // ノードを開くにはダブルクリックが必要です
    ImGuiTreeNodeFlags_OpenOnArrow          = 1 << 7,   // 矢印部分をクリックしたときにのみ開きます。 ImGuiTreeNodeFlags_OpenOnDoubleClickも設定されている場合は、矢印を1回クリックするか、すべてのボックスをダブルクリックして開きます。
    ImGuiTreeNodeFlags_Leaf                 = 1 << 8,   // 折りたたみなし、矢印なし（リーフノードの利便性として使用） 
    ImGuiTreeNodeFlags_Bullet               = 1 << 9,   // 矢印の代わりに箇条書きを表示する
    ImGuiTreeNodeFlags_FramePadding         = 1 << 10,  // テキストベースラインを通常のウィジェットの高さに垂直に揃えるには、フレーム枠なしのテキストノードの場合でもFramePaddingを使用します。 AlignTextToFramePadding()を呼び出すのと同じです。
    //ImGuITreeNodeFlags_SpanAllAvailWidth  = 1 << 11,  // FIXME: TODO: 枠なしの場合でもヒットボックスを水平に拡張
    //ImGuiTreeNodeFlags_NoScrollOnOpen     = 1 << 12,  // FIXME: TODO: ノードが開いていて内容が表示されない場合、TreePop()で自動スクロールを無効にする
    ImGuiTreeNodeFlags_NavCloseFromChild    = 1 << 13,  // (WIP) Nav: 任意の子（TreeNodeとTreePopの間でサブミットされたアイテム）に着目すると、このTreeNode()を閉じることができます。
    ImGuiTreeNodeFlags_CollapsingHeader     = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_NoAutoOpenOnLog

    // 廃止された名前（削除されます）
#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
    , ImGuiTreeNodeFlags_AllowOverlapMode = ImGuiTreeNodeFlags_AllowItemOverlap
#endif
};

// Flags for ImGui::Selectable()
enum ImGuiSelectableFlags_
{
    ImGuiSelectableFlags_DontClosePopups    = 1 << 0,   // クリックすると親のポップアップウィンドウが閉じない
    ImGuiSelectableFlags_SpanAllColumns     = 1 << 1,   // 選択可能なフレームはすべての列にまたがることができます（テキストは現在の列に収まります）
    ImGuiSelectableFlags_AllowDoubleClick   = 1 << 2    // ダブルクリックでプレスイベントを生成する
};

// Flags for ImGui::BeginCombo()
enum ImGuiComboFlags_
{
    ImGuiComboFlags_PopupAlignLeft          = 1 << 0,   // 既定では、ポップアップを左に揃えます
    ImGuiComboFlags_HeightSmall             = 1 << 1,   // 最大4つの項目が表示されます。 ヒント：コンボポップアップを特定のサイズにする場合は、BeginCombo()を呼び出す前にSetNextWindowSizeConstraints()を使用できます。
    ImGuiComboFlags_HeightRegular           = 1 << 2,   // 最大8つのアイテムが表示されます（デフォルト）
    ImGuiComboFlags_HeightLarge             = 1 << 3,   // 最大20個のアイテムが表示されます
    ImGuiComboFlags_HeightLargest           = 1 << 4,   // 可能な限り多くの適合項目
    ImGuiComboFlags_HeightMask_             = ImGuiComboFlags_HeightSmall | ImGuiComboFlags_HeightRegular | ImGuiComboFlags_HeightLarge | ImGuiComboFlags_HeightLargest
};

// Flags for ImGui::IsWindowFocused()
enum ImGuiFocusedFlags_
{
    ImGuiFocusedFlags_ChildWindows                  = 1 << 0,   // IsWindowFocused(): ウィンドウの子にフォーカスがある場合はtrueを返します。
    ImGuiFocusedFlags_RootWindow                    = 1 << 1,   // IsWindowFocused(): ルートウィンドウからのテスト（現在の階層の一番上の親）
    ImGuiFocusedFlags_AnyWindow                     = 1 << 2,   // IsWindowFocused(): ウィンドウがフォーカスされている場合はtrueを返します。
    ImGuiFocusedFlags_RootAndChildWindows           = ImGuiFocusedFlags_RootWindow | ImGuiFocusedFlags_ChildWindows
};

// Flags for ImGui::IsItemHovered(), ImGui::IsWindowHovered()
enum ImGuiHoveredFlags_
{
    ImGuiHoveredFlags_Default                       = 0,        // 他のウィンドウで遮られていないアイテム/ウィンドウ上で直接アクティブであるか、アクティブなポップアップやそれらの下のモーダルブロックイン入力によって妨げられていない場合はtrueを返します。
    ImGuiHoveredFlags_ChildWindows                  = 1 << 0,   // IsWindowHovered() only: ウィンドウの子どもがどれくらい隠れているかを返す
    ImGuiHoveredFlags_RootWindow                    = 1 << 1,   // IsWindowHovered() only: ルートウィンドウからのテスト（現在の階層の一番上の親）
    ImGuiHoveredFlags_AnyWindow                     = 1 << 2,   // IsWindowHovered() only: いずれかのウィンドウが表示されている場合はtrueを返します。
    ImGuiHoveredFlags_AllowWhenBlockedByPopup       = 1 << 3,   // ポップアップウィンドウがこの項目またはウィンドウへのアクセスを通常ブロックしている場合でもtrueを返します
    //ImGuiHoveredFlags_AllowWhenBlockedByModal     = 1 << 4,   // モーダルポップアップウィンドウが通常このアイテム/ウィンドウへのアクセスをブロックしている場合でもtrueを返します。 FIXME-TODO：まだ利用できません。
    ImGuiHoveredFlags_AllowWhenBlockedByActiveItem  = 1 << 5,   // アクティブなアイテムがこのアイテム/ウィンドウへのアクセスをブロックしている場合でもtrueを返します。 ドラッグアンドドロップのパターンに便利です。
    ImGuiHoveredFlags_AllowWhenOverlapped           = 1 << 6,   // 位置が別のウィンドウで重なっていても真を返す
    ImGuiHoveredFlags_RectOnly                      = ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_AllowWhenBlockedByActiveItem | ImGuiHoveredFlags_AllowWhenOverlapped,
    ImGuiHoveredFlags_RootAndChildWindows           = ImGuiHoveredFlags_RootWindow | ImGuiHoveredFlags_ChildWindows
};

// Flags for ImGui::BeginDragDropSource(), ImGui::AcceptDragDropPayload()
enum ImGuiDragDropFlags_
{
    // BeginDragDropSource() flags
    ImGuiDragDropFlags_SourceNoPreviewTooltip       = 1 << 0,       // By default, a successful call to BeginDragDropSource opens a tooltip so you can display a preview or description of the source contents. This flag disable this behavior.
    ImGuiDragDropFlags_SourceNoDisableHover         = 1 << 1,       //デフォルトでは、ドラッグするとIsItemHovered()がtrueを返すようにデータがクリアされ、以降のユーザーコードがツールチップを送信しないようにします。このフラグは、この動作を無効にして、ソースアイテムでIsItemHovered()を呼び出せるようにします。
    ImGuiDragDropFlags_SourceNoHoldToOpenOthers     = 1 << 2,       //ツリーノードを開き、ソースアイテムをドラッグしている間にそれらを保持することでヘッダーを折りたたむことを可能にするビヘイビアを無効にします。
    ImGuiDragDropFlags_SourceAllowNullID            = 1 << 3,       //固有の識別子を持たないText()、Image()などのアイテムを、ウィンドウ相対位置に基づいて一時識別子を作成することによって、ドラッグソースとして使用できるようにします。これはimguiの親愛なる生態系内では非常に珍しいので、明示しました。
    ImGuiDragDropFlags_SourceExtern                 = 1 << 4,       //外部ソース（imguiの外部から）は、現在のアイテム/ウィンドウ情報を読み込もうとしません。常に真実を返します。同時にアクティブにできるExternソースは1つだけです。
    // AcceptDragDropPayload()フラグ
    ImGuiDragDropFlags_AcceptBeforeDelivery         = 1 << 10,      // AcceptDragDropPayload()は、マウスボタンが離される前でもtrueを返します。 IsDelivery()を呼び出して、ペイロードを配信する必要があるかどうかをテストできます。
    ImGuiDragDropFlags_AcceptNoDrawDefaultRect      = 1 << 11,      //ターゲットにカーソルを置いたときに、デフォルトの強調表示矩形を描画しません。
    ImGuiDragDropFlags_AcceptPeekOnly = ImGuiDragDropFlags_AcceptBeforeDelivery | ImGuiDragDropFlags_AcceptNoDrawDefaultRect //先読みし、配信前にペイロードを検査するため。
};

// 標準のドラッグ＆ドロップペイロードタイプ。 12文字の長い文字列を使用して、独自のペイロードタイプを定義することができます。 '_'で始まるタイプは、Dear ImGuiによって定義されます。
#define IMGUI_PAYLOAD_TYPE_COLOR_3F     "_COL3F"    // float[3]     // アルファなしの色の標準タイプ。 ユーザーコードがこのタイプを使用することがあります。 
#define IMGUI_PAYLOAD_TYPE_COLOR_4F     "_COL4F"    // float[4]     // 色の標準タイプ。 ユーザーコードがこのタイプを使用することがあります。

// ユーザーは、ImGuiIO.KeysDown [512]配列にインデックスを持つImGuiIO.KeyMap []配列を埋め込みます。
enum ImGuiKey_
{
    ImGuiKey_Tab,
    ImGuiKey_LeftArrow,
    ImGuiKey_RightArrow,
    ImGuiKey_UpArrow,
    ImGuiKey_DownArrow,
    ImGuiKey_PageUp,
    ImGuiKey_PageDown,
    ImGuiKey_Home,
    ImGuiKey_End,
    ImGuiKey_Insert,
    ImGuiKey_Delete,
    ImGuiKey_Backspace,
    ImGuiKey_Space,
    ImGuiKey_Enter,
    ImGuiKey_Escape,
    ImGuiKey_A,         // for text edit CTRL+A: select all
    ImGuiKey_C,         // for text edit CTRL+C: copy
    ImGuiKey_V,         // for text edit CTRL+V: paste
    ImGuiKey_X,         // for text edit CTRL+X: cut
    ImGuiKey_Y,         // for text edit CTRL+Y: redo
    ImGuiKey_Z,         // for text edit CTRL+Z: undo
    ImGuiKey_COUNT
};

// [BETA] ゲームパッド/キーボードの方向ナビゲーション
// Keyboard: io.NavFlags | = ImGuiNavFlags_EnableKeyboardを有効に設定します。 NewFrame()は、あなたのio.KeyDown [] + io.KeyMap []配列に基づいてio.NavInputs []を自動的に埋めます。
// Gamepad:  io.NavFlags | = ImGuiNavFlags_EnableGamepadを有効に設定します。 NewFrame()を呼び出す前にio.NavInputs []フィールドを埋めてください。 io.NavInputs []はEndFrame()によってクリアされることに注意してください。
// 詳細については、imgui.cppの説明をお読みください。
enum ImGuiNavInput_
{
    // ゲームパッドマッピング
    ImGuiNavInput_Activate,      // activate / open / toggle / tweak value       // e.g. Circle (PS4), A (Xbox), B (Switch), Space (Keyboard)
    ImGuiNavInput_Cancel,        // cancel / close / exit                        // e.g. Cross  (PS4), B (Xbox), A (Switch), Escape (Keyboard)
    ImGuiNavInput_Input,         // text input / on-screen keyboard              // e.g. Triang.(PS4), Y (Xbox), X (Switch), Return (Keyboard)
    ImGuiNavInput_Menu,          // tap: toggle menu / hold: focus, move, resize // e.g. Square (PS4), X (Xbox), Y (Switch), Alt (Keyboard)
    ImGuiNavInput_DpadLeft,      // move / tweak / resize window (w/ PadMenu)    // e.g. D-pad Left/Right/Up/Down (Gamepads), Arrow keys (Keyboard)
    ImGuiNavInput_DpadRight,     // 
    ImGuiNavInput_DpadUp,        // 
    ImGuiNavInput_DpadDown,      // 
    ImGuiNavInput_LStickLeft,    // scroll / move window (w/ PadMenu)            // e.g. Left Analog Stick Left/Right/Up/Down
    ImGuiNavInput_LStickRight,   // 
    ImGuiNavInput_LStickUp,      // 
    ImGuiNavInput_LStickDown,    // 
    ImGuiNavInput_FocusPrev,     // next window (w/ PadMenu)                     // e.g. L1 or L2 (PS4), LB or LT (Xbox), L or ZL (Switch)
    ImGuiNavInput_FocusNext,     // prev window (w/ PadMenu)                     // e.g. R1 or R2 (PS4), RB or RT (Xbox), R or ZL (Switch) 
    ImGuiNavInput_TweakSlow,     // slower tweaks                                // e.g. L1 or L2 (PS4), LB or LT (Xbox), L or ZL (Switch)
    ImGuiNavInput_TweakFast,     // faster tweaks                                // e.g. R1 or R2 (PS4), RB or RT (Xbox), R or ZL (Switch)

    // [Internal] 直接使用しないでください！ これは内部的に使用され、キーボードとゲームパッドの入力を差別化するための動作を区別するために使用されます。
    // 対応するゲームパッドマッピング（CTRL + TABなど）がないキーボードの動作は、io.NavInputs []の代わりにio.KeyDown []から直接読み取っている可能性があります。
    ImGuiNavInput_KeyMenu_,      // toggle menu                                  // = io.KeyAlt
    ImGuiNavInput_KeyLeft_,      // move left                                    // = Arrow keys
    ImGuiNavInput_KeyRight_,     // move right
    ImGuiNavInput_KeyUp_,        // move up
    ImGuiNavInput_KeyDown_,      // move down
    ImGuiNavInput_COUNT,
    ImGuiNavInput_InternalStart_ = ImGuiNavInput_KeyMenu_
};

// [BETA] ゲームパッド/キーボードのナビゲーションオプション
enum ImGuiNavFlags_
{
    ImGuiNavFlags_EnableKeyboard    = 1 << 0,   // マスターキーボードナビゲーションイネーブルフラグ。 NewFrame()はio.KeyDown []に基づいてio.NavInputs []を自動的に埋めます。
    ImGuiNavFlags_EnableGamepad     = 1 << 1,   // マスターゲームパッドのナビゲーション可能フラグ。 これは主にimguiバックエンドにio.NavInputs []を書き込むよう指示するものです。
    ImGuiNavFlags_MoveMouse         = 1 << 2,   // マウスカーソルの移動を許可するナビゲーションを要求します。 仮想マウスを動かすことが厄介なTV /コンソールシステムに役立つかもしれません。 io.MousePosを更新し、io.WantMoveMouse = trueに設定します。 有効になっている場合は、io.WantMoveMouseリクエストをバインドする必要があります。そうしないと、マウスが前後にジャンプしているようにImGuiが反応します。
    ImGuiNavFlags_NoCaptureKeyboard = 1 << 3    // io.NavActiveが設定された状態でio.WantCaptureKeyboardフラグを設定しないでください。
};

// PushStyleColor() PopStyleColor()の列挙体
enum ImGuiCol_
{
    ImGuiCol_Text,
    ImGuiCol_TextDisabled,
    ImGuiCol_WindowBg,              // 通常のウィンドウの背景
    ImGuiCol_ChildBg,               // 子ウィンドウの背景
    ImGuiCol_PopupBg,               // ポップアップ、メニュー、ツールチップウインドウの背景
    ImGuiCol_Border,
    ImGuiCol_BorderShadow,
    ImGuiCol_FrameBg,               // チェックボックス、ラジオボタン、プロット、スライダ、テキスト入力の背景
    ImGuiCol_FrameBgHovered,
    ImGuiCol_FrameBgActive,
    ImGuiCol_TitleBg,
    ImGuiCol_TitleBgActive,
    ImGuiCol_TitleBgCollapsed,
    ImGuiCol_MenuBarBg,
    ImGuiCol_ScrollbarBg,
    ImGuiCol_ScrollbarGrab,
    ImGuiCol_ScrollbarGrabHovered,
    ImGuiCol_ScrollbarGrabActive,
    ImGuiCol_CheckMark,
    ImGuiCol_SliderGrab,
    ImGuiCol_SliderGrabActive,
    ImGuiCol_Button,
    ImGuiCol_ButtonHovered,
    ImGuiCol_ButtonActive,
    ImGuiCol_Header,
    ImGuiCol_HeaderHovered,
    ImGuiCol_HeaderActive,
    ImGuiCol_Separator,
    ImGuiCol_SeparatorHovered,
    ImGuiCol_SeparatorActive,
    ImGuiCol_ResizeGrip,
    ImGuiCol_ResizeGripHovered,
    ImGuiCol_ResizeGripActive,
    ImGuiCol_CloseButton,
    ImGuiCol_CloseButtonHovered,
    ImGuiCol_CloseButtonActive,
    ImGuiCol_PlotLines,
    ImGuiCol_PlotLinesHovered,
    ImGuiCol_PlotHistogram,
    ImGuiCol_PlotHistogramHovered,
    ImGuiCol_TextSelectedBg,
    ImGuiCol_ModalWindowDarkening,  // モーダルウィンドウがアクティブなときに画面全体を暗くする
    ImGuiCol_DragDropTarget,
    ImGuiCol_NavHighlight,          // ゲームパッド/キーボード：現在強調表示されている項目 
    ImGuiCol_NavWindowingHighlight, // ゲームパッド/キーボード：NavMenuを保持して、ウィンドウのフォーカス/移動/サイズ変更を行う
    ImGuiCol_COUNT

    // 廃止された名前（削除されます）
#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
    //, ImGuiCol_ComboBg = ImGuiCol_PopupBg // ComboBgはPopupBgとマージされているため、リダイレクトは正確ではありません。
    , ImGuiCol_ChildWindowBg = ImGuiCol_ChildBg, ImGuiCol_Column = ImGuiCol_Separator, ImGuiCol_ColumnHovered = ImGuiCol_SeparatorHovered, ImGuiCol_ColumnActive = ImGuiCol_SeparatorActive
#endif
};

// ImguiStyle構造体を一時的に変更するためのPushStyleVar()/ PopStyleVar()の列挙体。
// NB：enumはImGuiStyleのフィールドを参照するだけで、UIコード内でプッシュ/ポップされることに意味があります。 初期化中は、ImGuiStyleを直接ポークしてください。
// NB：この列挙を変更する場合は、それに応じて関連する内部テーブルGStyleVarInfo []を更新する必要があります。 これは、enum値をメンバoffset / typeにリンクするところです。
enum ImGuiStyleVar_
{
    // Enum name ......................// ImGuiStyle構造のメンバ（説明はImGuiStyleを参照してください）
    ImGuiStyleVar_Alpha,               // float     アルファ
    ImGuiStyleVar_WindowPadding,       // ImVec2 WindowPadding
    ImGuiStyleVar_WindowRounding,      // WindowRoundingを浮動させる
    ImGuiStyleVar_WindowBorderSize,    // WindowBorderSizeを浮動させる
    ImGuiStyleVar_WindowMinSize,       // ImVec2 WindowMinSize
    ImGuiStyleVar_WindowTitleAlign,    // ImVec2 WindowTitleAlign
    ImGuiStyleVar_ChildRounding,       // float ChildRounding
    ImGuiStyleVar_ChildBorderSize,     // float ChildBorderSize
    ImGuiStyleVar_PopupRounding,       // float PopupRounding
    ImGuiStyleVar_PopupBorderSize,     // float PopupBorderSize
    ImGuiStyleVar_FramePadding,        // ImVec2 FramePadding
    ImGuiStyleVar_FrameRounding,       // float FrameRounding
    ImGuiStyleVar_FrameBorderSize,     // float FrameBorderSize
    ImGuiStyleVar_ItemSpacing,         // ImVec2 ItemSpacing
    ImGuiStyleVar_ItemInnerSpacing,    // ImVec2 ItemInnerSpacing
    ImGuiStyleVar_IndentSpacing,       // IndentSpacingを浮動させる
    ImGuiStyleVar_ScrollbarSize,       // float ScrollbarSize
    ImGuiStyleVar_ScrollbarRounding,   // float ScrollbarRounding
    ImGuiStyleVar_GrabMinSize,         // float GrabMinSize
    ImGuiStyleVar_GrabRounding,        // float GrabRounding
    ImGuiStyleVar_ButtonTextAlign,     // ImVec2 ButtonTextAlign
    ImGuiStyleVar_Count_

    // 廃止された名前（削除されます）
#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
    , ImGuiStyleVar_ChildWindowRounding = ImGuiStyleVar_ChildRounding
#endif
};

// Enumeration for ColorEdit3() / ColorEdit4() / ColorPicker3() / ColorPicker4() / ColorButton()
enum ImGuiColorEditFlags_
{
    ImGuiColorEditFlags_NoAlpha         = 1 << 1,   //              // ColorEdit、ColorPicker、ColorButton：Alphaコンポーネントを無視する（入力ポインタから3つのコンポーネントを読み込む）。
    ImGuiColorEditFlags_NoPicker        = 1 << 2,   //              // ColorEdit：色付きの四角をクリックするとピッカーを無効にします。
    ImGuiColorEditFlags_NoOptions       = 1 << 3,   //              // ColorEdit：入力/小プレビューを右クリックすると、トグルオプションメニューが無効になります。
    ImGuiColorEditFlags_NoSmallPreview  = 1 << 4,   //              // ColorEdit、ColorPicker：入力の隣に色付きの四角形プレビューを無効にします。 （例えば、入力のみを表示する）
    ImGuiColorEditFlags_NoInputs        = 1 << 5,   //              // ColorEdit、ColorPicker：入力スライダ/テキストウィジェットを無効にします（小さなプレビュー色の四角形のみを表示するなど）。
    ImGuiColorEditFlags_NoTooltip       = 1 << 6,   //              // ColorEdit、ColorPicker、ColorButton：プレビューを表示するときにツールチップを無効にします。
    ImGuiColorEditFlags_NoLabel         = 1 << 7,   //              // ColorEdit、ColorPicker：インラインテキストラベルの表示を無効にします（ラベルはツールチップとピッカーに転送されます）。
    ImGuiColorEditFlags_NoSidePreview   = 1 << 8,   //              // ColorPicker：ピッカーの右側でより大きなカラープレビューを無効にし、代わりに小さい色の四角形のプレビューを使用します。
    // User Options（ウィジェットを右クリックしてそれらの一部を変更します）。 SetColorEditOptions()を使用してアプリケーションのデフォルトを設定できます。基本的な考え方は、起動時にユーザーがSetColorEditOptions()を選択または呼び出せるように、ほとんどの呼び出しでオーバーライドしたくないということです。
    ImGuiColorEditFlags_AlphaBar        = 1 << 9,   //              // ColorEdit、ColorPicker：ピッカーの垂直アルファベット/グラデーションを表示します。
    ImGuiColorEditFlags_AlphaPreview    = 1 << 10,  //              // ColorEdit、ColorPicker、ColorButton：不透明ではなく、チェッカーボード上に透明な色としてプレビューを表示します。
    ImGuiColorEditFlags_AlphaPreviewHalf = 1 << 11, //              // ColorEdit、ColorPicker、ColorButton：不透明ではなく、半透明/半透明を表示します。
    ImGuiColorEditFlags_HDR             = 1 << 12,  //              //（WIP）ColorEdit：現在、RGBA版では0.0f..1.0fの制限を無効にしています（注：ImGuiColorEditFlags_Floatフラグも使用します）。
    ImGuiColorEditFlags_RGB             = 1 << 13,  //              [入力] // ColorEdit：RGB / HSV / HEXから1つを選択します。 ColorPicker：RGB / HSV / HEXを使用して任意の組み合わせを選択します。
    ImGuiColorEditFlags_HSV             = 1 << 14,  //              [入力] // "
    ImGuiColorEditFlags_HEX             = 1 << 15,  //              [入力] // "
    ImGuiColorEditFlags_Uint8           = 1 << 16,  //              [DataType] // ColorEdit、ColorPicker、ColorButton：_display_の値は、0..255にフォーマットされています。
    ImGuiColorEditFlags_Float           = 1 << 17,  //              [DataType] // ColorEdit、ColorPicker、ColorButton：0.0f..1.0fとしてフォーマットされた_display_の値は、0..255の整数の代わりに浮動小数点になります。整数による値のラウンドトリップはありません。
    ImGuiColorEditFlags_PickerHueBar    = 1 << 18,  //              [PickerMode] // ColorPicker：色相のバー、Sat / Valueの四角形。
    ImGuiColorEditFlags_PickerHueWheel  = 1 << 19,  //              [PickerMode] // ColorPicker：色相のホイール、Sat / Valueの三角形。    // Internals/Masks
    ImGuiColorEditFlags__InputsMask     = ImGuiColorEditFlags_RGB|ImGuiColorEditFlags_HSV|ImGuiColorEditFlags_HEX,
    ImGuiColorEditFlags__DataTypeMask   = ImGuiColorEditFlags_Uint8|ImGuiColorEditFlags_Float,
    ImGuiColorEditFlags__PickerMask     = ImGuiColorEditFlags_PickerHueWheel|ImGuiColorEditFlags_PickerHueBar,
    ImGuiColorEditFlags__OptionsDefault = ImGuiColorEditFlags_Uint8|ImGuiColorEditFlags_RGB|ImGuiColorEditFlags_PickerHueBar    // SetColorEditOptions()を使用してアプリケーションのデフォルトを変更する
};

// Enumeration for GetMouseCursor()
enum ImGuiMouseCursor_
{
    ImGuiMouseCursor_None = -1,
    ImGuiMouseCursor_Arrow = 0,
    ImGuiMouseCursor_TextInput,         // InputTextなどでマウスを動かすと
    ImGuiMouseCursor_Move,              //未使用
    ImGuiMouseCursor_ResizeNS,          //水平ボーダー上にマウスを移動するとき
    ImGuiMouseCursor_ResizeEW,          //縦の枠線または列の上にカーソルを置いたとき
    ImGuiMouseCursor_ResizeNESW,        //ウィンドウの左下隅にカーソルを移動すると
    ImGuiMouseCursor_ResizeNWSE,        //ウィンドウの右下隅にカーソルを移動すると    
    ImGuiMouseCursor_Count_
};

// ImGui::SetWindow ***()、SetNextWindow ***()、SetNextTreeNode ***()関数の条件
//これらの関数は0をImGuiCond_Alwaysへのショートカットとして扱います。 ユーザーの視点から見ると、これを列挙型として使用します（複数の値をフラグに組み合わせないでください）。
enum ImGuiCond_
{
    ImGuiCond_Always        = 1 << 0,   // 変数を設定する
    ImGuiCond_Once          = 1 << 1,   // 実行時セッションごとに変数を1回設定する（成功した最初の呼び出しのみ）
    ImGuiCond_FirstUseEver  = 1 << 2,   // ウィンドウに保存されたデータがない場合は変数を設定します（.iniファイルに存在しない場合）
    ImGuiCond_Appearing     = 1 << 3    // ウィンドウが非表示/非アクティブ（または初めて）になった後に表示される場合は、変数を設定します。

    // 廃止された名前（削除されます）
#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
    , ImGuiSetCond_Always = ImGuiCond_Always, ImGuiSetCond_Once = ImGuiCond_Once, ImGuiSetCond_FirstUseEver = ImGuiCond_FirstUseEver, ImGuiSetCond_Appearing = ImGuiCond_Appearing
#endif
};

// You may modify the ImGui::GetStyle() main instance during initialization and before NewFrame().
// During the frame, prefer using ImGui::PushStyleVar(ImGuiStyleVar_XXXX)/PopStyleVar() to alter the main style values, and ImGui::PushStyleColor(ImGuiCol_XXX)/PopStyleColor() for colors.
struct ImGuiStyle
{
    float       Alpha;                      // Global alpha applies to everything in ImGui.
    ImVec2      WindowPadding;              // Padding within a window.
    float       WindowRounding;             // Radius of window corners rounding. Set to 0.0f to have rectangular windows.
    float       WindowBorderSize;           // Thickness of border around windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
    ImVec2      WindowMinSize;              // Minimum window size. This is a global setting. If you want to constraint individual windows, use SetNextWindowSizeConstraints().
    ImVec2      WindowTitleAlign;           // Alignment for title bar text. Defaults to (0.0f,0.5f) for left-aligned,vertically centered.
    float       ChildRounding;              // Radius of child window corners rounding. Set to 0.0f to have rectangular windows.
    float       ChildBorderSize;            // Thickness of border around child windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
    float       PopupRounding;              // Radius of popup window corners rounding.
    float       PopupBorderSize;            // Thickness of border around popup windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
    ImVec2      FramePadding;               // Padding within a framed rectangle (used by most widgets).
    float       FrameRounding;              // Radius of frame corners rounding. Set to 0.0f to have rectangular frame (used by most widgets).
    float       FrameBorderSize;            // Thickness of border around frames. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
    ImVec2      ItemSpacing;                // Horizontal and vertical spacing between widgets/lines.
    ImVec2      ItemInnerSpacing;           // Horizontal and vertical spacing between within elements of a composed widget (e.g. a slider and its label).
    ImVec2      TouchExtraPadding;          // Expand reactive bounding box for touch-based system where touch position is not accurate enough. Unfortunately we don't sort widgets so priority on overlap will always be given to the first widget. So don't grow this too much!
    float       IndentSpacing;              // Horizontal indentation when e.g. entering a tree node. Generally == (FontSize + FramePadding.x*2).
    float       ColumnsMinSpacing;          // Minimum horizontal spacing between two columns.
    float       ScrollbarSize;              // Width of the vertical scrollbar, Height of the horizontal scrollbar.
    float       ScrollbarRounding;          // Radius of grab corners for scrollbar.
    float       GrabMinSize;                // Minimum width/height of a grab box for slider/scrollbar.
    float       GrabRounding;               // Radius of grabs corners rounding. Set to 0.0f to have rectangular slider grabs.
    ImVec2      ButtonTextAlign;            // Alignment of button text when button is larger than text. Defaults to (0.5f,0.5f) for horizontally+vertically centered.
    ImVec2      DisplayWindowPadding;       // Window positions are clamped to be visible within the display area by at least this amount. Only covers regular windows.
    ImVec2      DisplaySafeAreaPadding;     // If you cannot see the edge of your screen (e.g. on a TV) increase the safe area padding. Covers popups/tooltips as well regular windows.
    float       MouseCursorScale;           // Scale software rendered mouse cursor (when io.MouseDrawCursor is enabled). May be removed later.
    bool        AntiAliasedLines;           // Enable anti-aliasing on lines/borders. Disable if you are really tight on CPU/GPU.
    bool        AntiAliasedFill;            // Enable anti-aliasing on filled shapes (rounded rectangles, circles, etc.)
    float       CurveTessellationTol;       // Tessellation tolerance when using PathBezierCurveTo() without a specific number of segments. Decrease for highly tessellated curves (higher quality, more polygons), increase to reduce quality.
    ImVec4      Colors[ImGuiCol_COUNT];

    IMGUI_API ImGuiStyle();
    IMGUI_API void ScaleAllSizes(float scale_factor);
};

// This is where your app communicate with ImGui. Access via ImGui::GetIO().
// Read 'Programmer guide' section in .cpp file for general usage.
struct ImGuiIO
{
    //------------------------------------------------------------------
    // Settings (fill once)                 // Default value:
    //------------------------------------------------------------------

    ImVec2        DisplaySize;              // <unset>              // Display size, in pixels. For clamping windows positions.
    float         DeltaTime;                // = 1.0f/60.0f         // Time elapsed since last frame, in seconds.
    float         IniSavingRate;            // = 5.0f               // Maximum time between saving positions/sizes to .ini file, in seconds.
    const char*   IniFilename;              // = "imgui.ini"        // Path to .ini file. NULL to disable .ini saving.
    const char*   LogFilename;              // = "imgui_log.txt"    // Path to .log file (default parameter to ImGui::LogToFile when no file is specified).
    ImGuiNavFlags NavFlags;                 // = 0                  // See ImGuiNavFlags_. Gamepad/keyboard navigation options.
    float         MouseDoubleClickTime;     // = 0.30f              // Time for a double-click, in seconds.
    float         MouseDoubleClickMaxDist;  // = 6.0f               // Distance threshold to stay in to validate a double-click, in pixels.
    float         MouseDragThreshold;       // = 6.0f               // Distance threshold before considering we are dragging.
    int           KeyMap[ImGuiKey_COUNT];   // <unset>              // Map of indices into the KeysDown[512] entries array which represent your "native" keyboard state.
    float         KeyRepeatDelay;           // = 0.250f             // When holding a key/button, time before it starts repeating, in seconds (for buttons in Repeat mode, etc.).
    float         KeyRepeatRate;            // = 0.050f             // When holding a key/button, rate at which it repeats, in seconds.
    void*         UserData;                 // = NULL               // Store your own data for retrieval by callbacks.

    ImFontAtlas*  Fonts;                    // <auto>               // Load and assemble one or more fonts into a single tightly packed texture. Output to Fonts array.
    float         FontGlobalScale;          // = 1.0f               // Global scale all fonts
    bool          FontAllowUserScaling;     // = false              // Allow user scaling text of individual window with CTRL+Wheel.
    ImFont*       FontDefault;              // = NULL               // Font to use on NewFrame(). Use NULL to uses Fonts->Fonts[0].
    ImVec2        DisplayFramebufferScale;  // = (1.0f,1.0f)        // For retina display or other situations where window coordinates are different from framebuffer coordinates. User storage only, presently not used by ImGui.
    ImVec2        DisplayVisibleMin;        // <unset> (0.0f,0.0f)  // If you use DisplaySize as a virtual space larger than your screen, set DisplayVisibleMin/Max to the visible area.
    ImVec2        DisplayVisibleMax;        // <unset> (0.0f,0.0f)  // If the values are the same, we defaults to Min=(0.0f) and Max=DisplaySize

    // Advanced/subtle behaviors
    bool          OptMacOSXBehaviors;       // = defined(__APPLE__) // OS X style: Text editing cursor movement using Alt instead of Ctrl, Shortcuts using Cmd/Super instead of Ctrl, Line/Text Start and End using Cmd+Arrows instead of Home/End, Double click selects by word instead of selecting whole text, Multi-selection in lists uses Cmd/Super instead of Ctrl
    bool          OptCursorBlink;           // = true               // Enable blinking cursor, for users who consider it annoying.

    //------------------------------------------------------------------
    // Settings (User Functions)
    //------------------------------------------------------------------

    // Rendering function, will be called in Render().
    // Alternatively you can keep this to NULL and call GetDrawData() after Render() to get the same pointer.
    // See example applications if you are unsure of how to implement this.
    void        (*RenderDrawListsFn)(ImDrawData* data);

    // Optional: access OS clipboard
    // (default to use native Win32 clipboard on Windows, otherwise uses a private clipboard. Override to access OS clipboard on other architectures)
    const char* (*GetClipboardTextFn)(void* user_data);
    void        (*SetClipboardTextFn)(void* user_data, const char* text);
    void*       ClipboardUserData;

    // Optional: notify OS Input Method Editor of the screen position of your cursor for text input position (e.g. when using Japanese/Chinese IME in Windows)
    // (default to use native imm32 api on Windows)
    void        (*ImeSetInputScreenPosFn)(int x, int y);
    void*       ImeWindowHandle;            // (Windows) Set this to your HWND to get automatic IME cursor positioning.

    //------------------------------------------------------------------
    // Input - Fill before calling NewFrame()
    //------------------------------------------------------------------

    ImVec2      MousePos;                       // Mouse position, in pixels. Set to ImVec2(-FLT_MAX,-FLT_MAX) if mouse is unavailable (on another screen, etc.)
    bool        MouseDown[5];                   // Mouse buttons: left, right, middle + extras. ImGui itself mostly only uses left button (BeginPopupContext** are using right button). Others buttons allows us to track if the mouse is being used by your application + available to user as a convenience via IsMouse** API.
    float       MouseWheel;                     // Mouse wheel: 1 unit scrolls about 5 lines text. 
    float       MouseWheelH;                    // Mouse wheel (Horizontal). Most users don't have a mouse with an horizontal wheel, may not be filled by all back ends.
    bool        MouseDrawCursor;                // Request ImGui to draw a mouse cursor for you (if you are on a platform without a mouse cursor).
    bool        KeyCtrl;                        // Keyboard modifier pressed: Control
    bool        KeyShift;                       // Keyboard modifier pressed: Shift
    bool        KeyAlt;                         // Keyboard modifier pressed: Alt
    bool        KeySuper;                       // Keyboard modifier pressed: Cmd/Super/Windows
    bool        KeysDown[512];                  // Keyboard keys that are pressed (ideally left in the "native" order your engine has access to keyboard keys, so you can use your own defines/enums for keys).
    ImWchar     InputCharacters[16+1];          // List of characters input (translated by user from keypress+keyboard state). Fill using AddInputCharacter() helper.
    float       NavInputs[ImGuiNavInput_COUNT]; // Gamepad inputs (keyboard keys will be auto-mapped and be written here by ImGui::NewFrame)

    // Functions
    IMGUI_API void AddInputCharacter(ImWchar c);                        // Add new character into InputCharacters[]
    IMGUI_API void AddInputCharactersUTF8(const char* utf8_chars);      // Add new characters into InputCharacters[] from an UTF-8 string
    inline void    ClearInputCharacters() { InputCharacters[0] = 0; }   // Clear the text input buffer manually

    //------------------------------------------------------------------
    // Output - Retrieve after calling NewFrame()
    //------------------------------------------------------------------

    bool        WantCaptureMouse;           // When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application. This is set by ImGui when it wants to use your mouse (e.g. unclicked mouse is hovering a window, or a widget is active). 
    bool        WantCaptureKeyboard;        // When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application. This is set by ImGui when it wants to use your keyboard inputs.
    bool        WantTextInput;              // Mobile/console: when io.WantTextInput is true, you may display an on-screen keyboard. This is set by ImGui when it wants textual keyboard input to happen (e.g. when a InputText widget is active).
    bool        WantMoveMouse;              // MousePos has been altered, back-end should reposition mouse on next frame. Set only when ImGuiNavFlags_MoveMouse flag is enabled in io.NavFlags.
    bool        NavActive;                  // Directional navigation is currently allowed (will handle ImGuiKey_NavXXX events) = a window is focused and it doesn't use the ImGuiWindowFlags_NoNavInputs flag.
    bool        NavVisible;                 // Directional navigation is visible and allowed (will handle ImGuiKey_NavXXX events).
    float       Framerate;                  // Application framerate estimation, in frame per second. Solely for convenience. Rolling average estimation based on IO.DeltaTime over 120 frames
    int         MetricsRenderVertices;      // Vertices output during last call to Render()
    int         MetricsRenderIndices;       // Indices output during last call to Render() = number of triangles * 3
    int         MetricsActiveWindows;       // Number of visible root windows (exclude child windows)
    ImVec2      MouseDelta;                 // Mouse delta. Note that this is zero if either current or previous position are invalid (-FLT_MAX,-FLT_MAX), so a disappearing/reappearing mouse won't have a huge delta.

    //------------------------------------------------------------------
    // [Internal] ImGui will maintain those fields. Forward compatibility not guaranteed!
    //------------------------------------------------------------------

    ImVec2      MousePosPrev;               // Previous mouse position temporary storage (nb: not for public use, set to MousePos in NewFrame())
    ImVec2      MouseClickedPos[5];         // Position at time of clicking
    float       MouseClickedTime[5];        // Time of last click (used to figure out double-click)
    bool        MouseClicked[5];            // Mouse button went from !Down to Down
    bool        MouseDoubleClicked[5];      // Has mouse button been double-clicked?
    bool        MouseReleased[5];           // Mouse button went from Down to !Down
    bool        MouseDownOwned[5];          // Track if button was clicked inside a window. We don't request mouse capture from the application if click started outside ImGui bounds.
    float       MouseDownDuration[5];       // Duration the mouse button has been down (0.0f == just clicked)
    float       MouseDownDurationPrev[5];   // Previous time the mouse button has been down
    ImVec2      MouseDragMaxDistanceAbs[5]; // Maximum distance, absolute, on each axis, of how much mouse has traveled from the clicking point
    float       MouseDragMaxDistanceSqr[5]; // Squared maximum distance of how much mouse has traveled from the clicking point
    float       KeysDownDuration[512];      // Duration the keyboard key has been down (0.0f == just pressed)
    float       KeysDownDurationPrev[512];  // Previous duration the key has been down
    float       NavInputsDownDuration[ImGuiNavInput_COUNT];
    float       NavInputsDownDurationPrev[ImGuiNavInput_COUNT];

    IMGUI_API   ImGuiIO();
};

//-----------------------------------------------------------------------------
// Obsolete functions (Will be removed! Read 'API BREAKING CHANGES' section in imgui.cpp for details)
//-----------------------------------------------------------------------------

#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
namespace ImGui
{
    // OBSOLETED in 1.60 (from Dec 2017)
    static inline bool  IsAnyWindowFocused()                  { return IsWindowFocused(ImGuiFocusedFlags_AnyWindow); }
    static inline bool  IsAnyWindowHovered()                  { return IsWindowHovered(ImGuiHoveredFlags_AnyWindow); }
    static inline ImVec2 CalcItemRectClosestPoint(const ImVec2& pos, bool on_edge = false, float outward = 0.f) { (void)on_edge; (void)outward; IM_ASSERT(0); return pos; }
    // OBSOLETED in 1.53 (between Oct 2017 and Dec 2017)
    static inline void  ShowTestWindow()                      { return ShowDemoWindow(); }
    static inline bool  IsRootWindowFocused()                 { return IsWindowFocused(ImGuiFocusedFlags_RootWindow); }
    static inline bool  IsRootWindowOrAnyChildFocused()       { return IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows); }
    static inline void  SetNextWindowContentWidth(float w)    { SetNextWindowContentSize(ImVec2(w, 0.0f)); }
    static inline float GetItemsLineHeightWithSpacing()       { return GetFrameHeightWithSpacing(); }
    // OBSOLETED in 1.52 (between Aug 2017 and Oct 2017)
    bool                Begin(const char* name, bool* p_open, const ImVec2& size_on_first_use, float bg_alpha_override = -1.0f, ImGuiWindowFlags flags = 0); // Use SetNextWindowSize(size, ImGuiCond_FirstUseEver) + SetNextWindowBgAlpha() instead.
    static inline bool  IsRootWindowOrAnyChildHovered()       { return IsItemHovered(ImGuiHoveredFlags_RootAndChildWindows); }
    static inline void  AlignFirstTextHeightToWidgets()       { AlignTextToFramePadding(); }
    static inline void  SetNextWindowPosCenter(ImGuiCond c=0) { ImGuiIO& io = GetIO(); SetNextWindowPos(ImVec2(io.DisplaySize.x * 0.5f, io.DisplaySize.y * 0.5f), c, ImVec2(0.5f, 0.5f)); }
    // OBSOLETED in 1.51 (between Jun 2017 and Aug 2017)
    static inline bool  IsItemHoveredRect()                   { return IsItemHovered(ImGuiHoveredFlags_RectOnly); }
    static inline bool  IsPosHoveringAnyWindow(const ImVec2&) { IM_ASSERT(0); return false; } // This was misleading and partly broken. You probably want to use the ImGui::GetIO().WantCaptureMouse flag instead.
    static inline bool  IsMouseHoveringAnyWindow()            { return IsWindowHovered(ImGuiHoveredFlags_AnyWindow); }
    static inline bool  IsMouseHoveringWindow()               { return IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByPopup | ImGuiHoveredFlags_AllowWhenBlockedByActiveItem); }
    // OBSOLETED IN 1.49 (between Apr 2016 and May 2016)
    static inline bool  CollapsingHeader(const char* label, const char* str_id, bool framed = true, bool default_open = false) { (void)str_id; (void)framed; ImGuiTreeNodeFlags default_open_flags = 1 << 5; return CollapsingHeader(label, (default_open ? default_open_flags : 0)); }
}
#endif

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

// Lightweight std::vector<> like class to avoid dragging dependencies (also: windows implementation of STL with debug enabled is absurdly slow, so let's bypass it so our code runs fast in debug).
// Our implementation does NOT call C++ constructors/destructors. This is intentional and we do not require it. Do not use this class as a straight std::vector replacement in your code!
template<typename T>
class ImVector
{
public:
    int                         Size;
    int                         Capacity;
    T*                          Data;

    typedef T                   value_type;
    typedef value_type*         iterator;
    typedef const value_type*   const_iterator;

    inline ImVector()           { Size = Capacity = 0; Data = NULL; }
    inline ~ImVector()          { if (Data) ImGui::MemFree(Data); }

    inline bool                 empty() const                   { return Size == 0; }
    inline int                  size() const                    { return Size; }
    inline int                  capacity() const                { return Capacity; }

    inline value_type&          operator[](int i)               { IM_ASSERT(i < Size); return Data[i]; }
    inline const value_type&    operator[](int i) const         { IM_ASSERT(i < Size); return Data[i]; }

    inline void                 clear()                         { if (Data) { Size = Capacity = 0; ImGui::MemFree(Data); Data = NULL; } }
    inline iterator             begin()                         { return Data; }
    inline const_iterator       begin() const                   { return Data; }
    inline iterator             end()                           { return Data + Size; }
    inline const_iterator       end() const                     { return Data + Size; }
    inline value_type&          front()                         { IM_ASSERT(Size > 0); return Data[0]; }
    inline const value_type&    front() const                   { IM_ASSERT(Size > 0); return Data[0]; }
    inline value_type&          back()                          { IM_ASSERT(Size > 0); return Data[Size - 1]; }
    inline const value_type&    back() const                    { IM_ASSERT(Size > 0); return Data[Size - 1]; }
    inline void                 swap(ImVector<T>& rhs)          { int rhs_size = rhs.Size; rhs.Size = Size; Size = rhs_size; int rhs_cap = rhs.Capacity; rhs.Capacity = Capacity; Capacity = rhs_cap; value_type* rhs_data = rhs.Data; rhs.Data = Data; Data = rhs_data; }

    inline int                  _grow_capacity(int sz) const    { int new_capacity = Capacity ? (Capacity + Capacity/2) : 8; return new_capacity > sz ? new_capacity : sz; }

    inline void                 resize(int new_size)            { if (new_size > Capacity) reserve(_grow_capacity(new_size)); Size = new_size; }
    inline void                 resize(int new_size, const T& v){ if (new_size > Capacity) reserve(_grow_capacity(new_size)); if (new_size > Size) for (int n = Size; n < new_size; n++) Data[n] = v; Size = new_size; }
    inline void                 reserve(int new_capacity)
    {
        if (new_capacity <= Capacity) 
            return;
        T* new_data = (value_type*)ImGui::MemAlloc((size_t)new_capacity * sizeof(T));
        if (Data)
            memcpy(new_data, Data, (size_t)Size * sizeof(T));
        ImGui::MemFree(Data);
        Data = new_data;
        Capacity = new_capacity;
    }

    // NB: &v cannot be pointing inside the ImVector Data itself! e.g. v.push_back(v[10]) is forbidden.
    inline void                 push_back(const value_type& v)  { if (Size == Capacity) reserve(_grow_capacity(Size + 1)); Data[Size++] = v; }
    inline void                 pop_back()                      { IM_ASSERT(Size > 0); Size--; }
    inline void                 push_front(const value_type& v) { if (Size == 0) push_back(v); else insert(Data, v); }

    inline iterator             erase(const_iterator it)        { IM_ASSERT(it >= Data && it < Data+Size); const ptrdiff_t off = it - Data; memmove(Data + off, Data + off + 1, ((size_t)Size - (size_t)off - 1) * sizeof(value_type)); Size--; return Data + off; }
    inline iterator             insert(const_iterator it, const value_type& v)  { IM_ASSERT(it >= Data && it <= Data+Size); const ptrdiff_t off = it - Data; if (Size == Capacity) reserve(_grow_capacity(Size + 1)); if (off < (int)Size) memmove(Data + off + 1, Data + off, ((size_t)Size - (size_t)off) * sizeof(value_type)); Data[off] = v; Size++; return Data + off; }
    inline bool                 contains(const value_type& v) const             { const T* data = Data;  const T* data_end = Data + Size; while (data < data_end) if (*data++ == v) return true; return false; }
};

// Helper: execute a block of code at maximum once a frame. Convenient if you want to quickly create an UI within deep-nested code that runs multiple times every frame.
// Usage:
//   static ImGuiOnceUponAFrame oaf;
//   if (oaf)
//       ImGui::Text("This will be called only once per frame");
struct ImGuiOnceUponAFrame
{
    ImGuiOnceUponAFrame() { RefFrame = -1; }
    mutable int RefFrame;
    operator bool() const { int current_frame = ImGui::GetFrameCount(); if (RefFrame == current_frame) return false; RefFrame = current_frame; return true; }
};

// Helper macro for ImGuiOnceUponAFrame. Attention: The macro expands into 2 statement so make sure you don't use it within e.g. an if() statement without curly braces.
#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS    // Will obsolete
#define IMGUI_ONCE_UPON_A_FRAME     static ImGuiOnceUponAFrame imgui_oaf; if (imgui_oaf)
#endif

// Helper: Parse and apply text filters. In format "aaaaa[,bbbb][,ccccc]"
struct ImGuiTextFilter
{
    struct TextRange
    {
        const char* b;
        const char* e;

        TextRange() { b = e = NULL; }
        TextRange(const char* _b, const char* _e) { b = _b; e = _e; }
        const char* begin() const { return b; }
        const char* end() const { return e; }
        bool empty() const { return b == e; }
        char front() const { return *b; }
        static bool is_blank(char c) { return c == ' ' || c == '\t'; }
        void trim_blanks() { while (b < e && is_blank(*b)) b++; while (e > b && is_blank(*(e-1))) e--; }
        IMGUI_API void split(char separator, ImVector<TextRange>& out);
    };

    char                InputBuf[256];
    ImVector<TextRange> Filters;
    int                 CountGrep;

    IMGUI_API           ImGuiTextFilter(const char* default_filter = "");
    IMGUI_API bool      Draw(const char* label = "Filter (inc,-exc)", float width = 0.0f);    // Helper calling InputText+Build
    IMGUI_API bool      PassFilter(const char* text, const char* text_end = NULL) const;
    IMGUI_API void      Build();
    void                Clear() { InputBuf[0] = 0; Build(); }
    bool                IsActive() const { return !Filters.empty(); }
};

// Helper: Text buffer for logging/accumulating text
struct ImGuiTextBuffer
{
    ImVector<char>      Buf;

    ImGuiTextBuffer()   { Buf.push_back(0); }
    inline char         operator[](int i) { return Buf.Data[i]; }
    const char*         begin() const { return &Buf.front(); }
    const char*         end() const { return &Buf.back(); }      // Buf is zero-terminated, so end() will point on the zero-terminator
    int                 size() const { return Buf.Size - 1; }
    bool                empty() { return Buf.Size <= 1; }
    void                clear() { Buf.clear(); Buf.push_back(0); }
    void                reserve(int capacity) { Buf.reserve(capacity); }
    const char*         c_str() const { return Buf.Data; }
    IMGUI_API void      appendf(const char* fmt, ...) IM_FMTARGS(2);
    IMGUI_API void      appendfv(const char* fmt, va_list args) IM_FMTLIST(2);
};

// Helper: Simple Key->value storage
// Typically you don't have to worry about this since a storage is held within each Window.
// We use it to e.g. store collapse state for a tree (Int 0/1), store color edit options. 
// This is optimized for efficient reading (dichotomy into a contiguous buffer), rare writing (typically tied to user interactions)
// You can use it as custom user storage for temporary values. Declare your own storage if, for example:
// - You want to manipulate the open/close state of a particular sub-tree in your interface (tree node uses Int 0/1 to store their state).
// - You want to store custom debug data easily without adding or editing structures in your code (probably not efficient, but convenient)
// Types are NOT stored, so it is up to you to make sure your Key don't collide with different types.
struct ImGuiStorage
{
    struct Pair
    {
        ImGuiID key;
        union { int val_i; float val_f; void* val_p; };
        Pair(ImGuiID _key, int _val_i)   { key = _key; val_i = _val_i; }
        Pair(ImGuiID _key, float _val_f) { key = _key; val_f = _val_f; }
        Pair(ImGuiID _key, void* _val_p) { key = _key; val_p = _val_p; }
    };
    ImVector<Pair>      Data;

    // - Get***() functions find pair, never add/allocate. Pairs are sorted so a query is O(log N)
    // - Set***() functions find pair, insertion on demand if missing.
    // - Sorted insertion is costly, paid once. A typical frame shouldn't need to insert any new pair.
    void                Clear() { Data.clear(); }
    IMGUI_API int       GetInt(ImGuiID key, int default_val = 0) const;
    IMGUI_API void      SetInt(ImGuiID key, int val);
    IMGUI_API bool      GetBool(ImGuiID key, bool default_val = false) const;
    IMGUI_API void      SetBool(ImGuiID key, bool val);
    IMGUI_API float     GetFloat(ImGuiID key, float default_val = 0.0f) const;
    IMGUI_API void      SetFloat(ImGuiID key, float val);
    IMGUI_API void*     GetVoidPtr(ImGuiID key) const; // default_val is NULL
    IMGUI_API void      SetVoidPtr(ImGuiID key, void* val);

    // - Get***Ref() functions finds pair, insert on demand if missing, return pointer. Useful if you intend to do Get+Set.
    // - References are only valid until a new value is added to the storage. Calling a Set***() function or a Get***Ref() function invalidates the pointer.
    // - A typical use case where this is convenient for quick hacking (e.g. add storage during a live Edit&Continue session if you can't modify existing struct)
    //      float* pvar = ImGui::GetFloatRef(key); ImGui::SliderFloat("var", pvar, 0, 100.0f); some_var += *pvar;
    IMGUI_API int*      GetIntRef(ImGuiID key, int default_val = 0);
    IMGUI_API bool*     GetBoolRef(ImGuiID key, bool default_val = false);
    IMGUI_API float*    GetFloatRef(ImGuiID key, float default_val = 0.0f);
    IMGUI_API void**    GetVoidPtrRef(ImGuiID key, void* default_val = NULL);

    // Use on your own storage if you know only integer are being stored (open/close all tree nodes)
    IMGUI_API void      SetAllInt(int val);

    // For quicker full rebuild of a storage (instead of an incremental one), you may add all your contents and then sort once.
    IMGUI_API void      BuildSortByKey();
};

// Shared state of InputText(), passed to callback when a ImGuiInputTextFlags_Callback* flag is used and the corresponding callback is triggered.
struct ImGuiTextEditCallbackData
{
    ImGuiInputTextFlags EventFlag;      // One of ImGuiInputTextFlags_Callback* // Read-only
    ImGuiInputTextFlags Flags;          // What user passed to InputText()      // Read-only
    void*               UserData;       // What user passed to InputText()      // Read-only
    bool                ReadOnly;       // Read-only mode                       // Read-only

    // CharFilter event:
    ImWchar             EventChar;      // Character input                      // Read-write (replace character or set to zero)

    // Completion,History,Always events:
    // If you modify the buffer contents make sure you update 'BufTextLen' and set 'BufDirty' to true.
    ImGuiKey            EventKey;       // Key pressed (Up/Down/TAB)            // Read-only
    char*               Buf;            // Current text buffer                  // Read-write (pointed data only, can't replace the actual pointer)
    int                 BufTextLen;     // Current text length in bytes         // Read-write
    int                 BufSize;        // Maximum text length in bytes         // Read-only
    bool                BufDirty;       // Set if you modify Buf/BufTextLen!!   // Write
    int                 CursorPos;      //                                      // Read-write
    int                 SelectionStart; //                                      // Read-write (== to SelectionEnd when no selection)
    int                 SelectionEnd;   //                                      // Read-write

    // NB: Helper functions for text manipulation. Calling those function loses selection.
    IMGUI_API void    DeleteChars(int pos, int bytes_count);
    IMGUI_API void    InsertChars(int pos, const char* text, const char* text_end = NULL);
    bool              HasSelection() const { return SelectionStart != SelectionEnd; }
};

// Resizing callback data to apply custom constraint. As enabled by SetNextWindowSizeConstraints(). Callback is called during the next Begin().
// NB: For basic min/max size constraint on each axis you don't need to use the callback! The SetNextWindowSizeConstraints() parameters are enough.
struct ImGuiSizeCallbackData
{
    void*   UserData;       // Read-only.   What user passed to SetNextWindowSizeConstraints()
    ImVec2  Pos;            // Read-only.   Window position, for reference.
    ImVec2  CurrentSize;    // Read-only.   Current window size.
    ImVec2  DesiredSize;    // Read-write.  Desired size, based on user's mouse position. Write to this field to restrain resizing.
};

// Data payload for Drag and Drop operations
struct ImGuiPayload
{
    // Members
    const void*     Data;               // Data (copied and owned by dear imgui)
    int             DataSize;           // Data size

    // [Internal]
    ImGuiID         SourceId;           // Source item id
    ImGuiID         SourceParentId;     // Source parent id (if available)
    int             DataFrameCount;     // Data timestamp
    char            DataType[12 + 1];   // Data type tag (short user-supplied string, 12 characters max)
    bool            Preview;            // Set when AcceptDragDropPayload() was called and mouse has been hovering the target item (nb: handle overlapping drag targets)
    bool            Delivery;           // Set when AcceptDragDropPayload() was called and mouse button is released over the target item.

    ImGuiPayload()  { Clear(); }
    void Clear()    { SourceId = SourceParentId = 0; Data = NULL; DataSize = 0; memset(DataType, 0, sizeof(DataType)); DataFrameCount = -1; Preview = Delivery = false; }
    bool IsDataType(const char* type) const { return DataFrameCount != -1 && strcmp(type, DataType) == 0; }
    bool IsPreview() const                  { return Preview; }
    bool IsDelivery() const                 { return Delivery; }
};

// Helpers macros to generate 32-bits encoded colors
#ifdef IMGUI_USE_BGRA_PACKED_COLOR
#define IM_COL32_R_SHIFT    16
#define IM_COL32_G_SHIFT    8
#define IM_COL32_B_SHIFT    0
#define IM_COL32_A_SHIFT    24
#define IM_COL32_A_MASK     0xFF000000
#else
#define IM_COL32_R_SHIFT    0
#define IM_COL32_G_SHIFT    8
#define IM_COL32_B_SHIFT    16
#define IM_COL32_A_SHIFT    24
#define IM_COL32_A_MASK     0xFF000000
#endif
#define IM_COL32(R,G,B,A)    (((ImU32)(A)<<IM_COL32_A_SHIFT) | ((ImU32)(B)<<IM_COL32_B_SHIFT) | ((ImU32)(G)<<IM_COL32_G_SHIFT) | ((ImU32)(R)<<IM_COL32_R_SHIFT))
#define IM_COL32_WHITE       IM_COL32(255,255,255,255)  // Opaque white = 0xFFFFFFFF
#define IM_COL32_BLACK       IM_COL32(0,0,0,255)        // Opaque black
#define IM_COL32_BLACK_TRANS IM_COL32(0,0,0,0)          // Transparent black = 0x00000000

// ImColor() helper to implicity converts colors to either ImU32 (packed 4x1 byte) or ImVec4 (4x1 float)
// Prefer using IM_COL32() macros if you want a guaranteed compile-time ImU32 for usage with ImDrawList API.
// **Avoid storing ImColor! Store either u32 of ImVec4. This is not a full-featured color class. MAY OBSOLETE.
// **None of the ImGui API are using ImColor directly but you can use it as a convenience to pass colors in either ImU32 or ImVec4 formats. Explicitly cast to ImU32 or ImVec4 if needed.
struct ImColor
{
    ImVec4              Value;

    ImColor()                                                       { Value.x = Value.y = Value.z = Value.w = 0.0f; }
    ImColor(int r, int g, int b, int a = 255)                       { float sc = 1.0f/255.0f; Value.x = (float)r * sc; Value.y = (float)g * sc; Value.z = (float)b * sc; Value.w = (float)a * sc; }
    ImColor(ImU32 rgba)                                             { float sc = 1.0f/255.0f; Value.x = (float)((rgba>>IM_COL32_R_SHIFT)&0xFF) * sc; Value.y = (float)((rgba>>IM_COL32_G_SHIFT)&0xFF) * sc; Value.z = (float)((rgba>>IM_COL32_B_SHIFT)&0xFF) * sc; Value.w = (float)((rgba>>IM_COL32_A_SHIFT)&0xFF) * sc; }
    ImColor(float r, float g, float b, float a = 1.0f)              { Value.x = r; Value.y = g; Value.z = b; Value.w = a; }
    ImColor(const ImVec4& col)                                      { Value = col; }
    inline operator ImU32() const                                   { return ImGui::ColorConvertFloat4ToU32(Value); }
    inline operator ImVec4() const                                  { return Value; }

    // FIXME-OBSOLETE: May need to obsolete/cleanup those helpers.
    inline void    SetHSV(float h, float s, float v, float a = 1.0f){ ImGui::ColorConvertHSVtoRGB(h, s, v, Value.x, Value.y, Value.z); Value.w = a; }
    static ImColor HSV(float h, float s, float v, float a = 1.0f)   { float r,g,b; ImGui::ColorConvertHSVtoRGB(h, s, v, r, g, b); return ImColor(r,g,b,a); }
};

// Helper: Manually clip large list of items.
// If you are submitting lots of evenly spaced items and you have a random access to the list, you can perform coarse clipping based on visibility to save yourself from processing those items at all.
// The clipper calculates the range of visible items and advance the cursor to compensate for the non-visible items we have skipped. 
// ImGui already clip items based on their bounds but it needs to measure text size to do so. Coarse clipping before submission makes this cost and your own data fetching/submission cost null.
// Usage:
//     ImGuiListClipper clipper(1000);  // we have 1000 elements, evenly spaced.
//     while (clipper.Step())
//         for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++)
//             ImGui::Text("line number %d", i);
// - Step 0: the clipper let you process the first element, regardless of it being visible or not, so we can measure the element height (step skipped if we passed a known height as second arg to constructor).
// - Step 1: the clipper infer height from first element, calculate the actual range of elements to display, and position the cursor before the first element.
// - (Step 2: dummy step only required if an explicit items_height was passed to constructor or Begin() and user call Step(). Does nothing and switch to Step 3.)
// - Step 3: the clipper validate that we have reached the expected Y position (corresponding to element DisplayEnd), advance the cursor to the end of the list and then returns 'false' to end the loop.
struct ImGuiListClipper
{
    float   StartPosY;
    float   ItemsHeight;
    int     ItemsCount, StepNo, DisplayStart, DisplayEnd;

    // items_count:  Use -1 to ignore (you can call Begin later). Use INT_MAX if you don't know how many items you have (in which case the cursor won't be advanced in the final step).
    // items_height: Use -1.0f to be calculated automatically on first step. Otherwise pass in the distance between your items, typically GetTextLineHeightWithSpacing() or GetFrameHeightWithSpacing().
    // If you don't specify an items_height, you NEED to call Step(). If you specify items_height you may call the old Begin()/End() api directly, but prefer calling Step().
    ImGuiListClipper(int items_count = -1, float items_height = -1.0f)  { Begin(items_count, items_height); } // NB: Begin() initialize every fields (as we allow user to call Begin/End multiple times on a same instance if they want).
    ~ImGuiListClipper()                                                 { IM_ASSERT(ItemsCount == -1); }      // Assert if user forgot to call End() or Step() until false.

    IMGUI_API bool Step();                                              // Call until it returns false. The DisplayStart/DisplayEnd fields will be set and you can process/draw those items.
    IMGUI_API void Begin(int items_count, float items_height = -1.0f);  // Automatically called by constructor if you passed 'items_count' or by Step() in Step 1.
    IMGUI_API void End();                                               // Automatically called on the last call of Step() that returns false.
};

//-----------------------------------------------------------------------------
// Draw List
// Hold a series of drawing commands. The user provides a renderer for ImDrawData which essentially contains an array of ImDrawList.
//-----------------------------------------------------------------------------

// Draw callbacks for advanced uses.
// NB- You most likely do NOT need to use draw callbacks just to create your own widget or customized UI rendering (you can poke into the draw list for that)
// Draw callback may be useful for example, A) Change your GPU render state, B) render a complex 3D scene inside a UI element (without an intermediate texture/render target), etc.
// The expected behavior from your rendering function is 'if (cmd.UserCallback != NULL) cmd.UserCallback(parent_list, cmd); else RenderTriangles()'
typedef void (*ImDrawCallback)(const ImDrawList* parent_list, const ImDrawCmd* cmd);

// Typically, 1 command = 1 GPU draw call (unless command is a callback)
struct ImDrawCmd
{
    unsigned int    ElemCount;              // Number of indices (multiple of 3) to be rendered as triangles. Vertices are stored in the callee ImDrawList's vtx_buffer[] array, indices in idx_buffer[].
    ImVec4          ClipRect;               // Clipping rectangle (x1, y1, x2, y2)
    ImTextureID     TextureId;              // User-provided texture ID. Set by user in ImfontAtlas::SetTexID() for fonts or passed to Image*() functions. Ignore if never using images or multiple fonts atlas.
    ImDrawCallback  UserCallback;           // If != NULL, call the function instead of rendering the vertices. clip_rect and texture_id will be set normally.
    void*           UserCallbackData;       // The draw callback code can access this.

    ImDrawCmd() { ElemCount = 0; ClipRect.x = ClipRect.y = ClipRect.z = ClipRect.w = 0.0f; TextureId = NULL; UserCallback = NULL; UserCallbackData = NULL; }
};

// Vertex index (override with '#define ImDrawIdx unsigned int' inside in imconfig.h)
#ifndef ImDrawIdx
typedef unsigned short ImDrawIdx;
#endif

// Vertex layout
#ifndef IMGUI_OVERRIDE_DRAWVERT_STRUCT_LAYOUT
struct ImDrawVert
{
    ImVec2  pos;
    ImVec2  uv;
    ImU32   col;
};
#else
// You can override the vertex format layout by defining IMGUI_OVERRIDE_DRAWVERT_STRUCT_LAYOUT in imconfig.h
// The code expect ImVec2 pos (8 bytes), ImVec2 uv (8 bytes), ImU32 col (4 bytes), but you can re-order them or add other fields as needed to simplify integration in your engine.
// The type has to be described within the macro (you can either declare the struct or use a typedef)
// NOTE: IMGUI DOESN'T CLEAR THE STRUCTURE AND DOESN'T CALL A CONSTRUCTOR SO ANY CUSTOM FIELD WILL BE UNINITIALIZED. IF YOU ADD EXTRA FIELDS (SUCH AS A 'Z' COORDINATES) YOU WILL NEED TO CLEAR THEM DURING RENDER OR TO IGNORE THEM. 
IMGUI_OVERRIDE_DRAWVERT_STRUCT_LAYOUT;
#endif

// Draw channels are used by the Columns API to "split" the render list into different channels while building, so items of each column can be batched together.
// You can also use them to simulate drawing layers and submit primitives in a different order than how they will be rendered.
struct ImDrawChannel
{
    ImVector<ImDrawCmd>     CmdBuffer;
    ImVector<ImDrawIdx>     IdxBuffer;
};

enum ImDrawCornerFlags_
{
    ImDrawCornerFlags_TopLeft   = 1 << 0, // 0x1
    ImDrawCornerFlags_TopRight  = 1 << 1, // 0x2
    ImDrawCornerFlags_BotLeft   = 1 << 2, // 0x4
    ImDrawCornerFlags_BotRight  = 1 << 3, // 0x8
    ImDrawCornerFlags_Top       = ImDrawCornerFlags_TopLeft | ImDrawCornerFlags_TopRight,   // 0x3
    ImDrawCornerFlags_Bot       = ImDrawCornerFlags_BotLeft | ImDrawCornerFlags_BotRight,   // 0xC
    ImDrawCornerFlags_Left      = ImDrawCornerFlags_TopLeft | ImDrawCornerFlags_BotLeft,    // 0x5
    ImDrawCornerFlags_Right     = ImDrawCornerFlags_TopRight | ImDrawCornerFlags_BotRight,  // 0xA
    ImDrawCornerFlags_All       = 0xF     // In your function calls you may use ~0 (= all bits sets) instead of ImDrawCornerFlags_All, as a convenience
};

enum ImDrawListFlags_
{
    ImDrawListFlags_AntiAliasedLines = 1 << 0,
    ImDrawListFlags_AntiAliasedFill  = 1 << 1
};

// Draw command list
// This is the low-level list of polygons that ImGui functions are filling. At the end of the frame, all command lists are passed to your ImGuiIO::RenderDrawListFn function for rendering.
// Each ImGui window contains its own ImDrawList. You can use ImGui::GetWindowDrawList() to access the current window draw list and draw custom primitives.
// You can interleave normal ImGui:: calls and adding primitives to the current draw list.
// All positions are generally in pixel coordinates (top-left at (0,0), bottom-right at io.DisplaySize), however you are totally free to apply whatever transformation matrix to want to the data (if you apply such transformation you'll want to apply it to ClipRect as well)
// Important: Primitives are always added to the list and not culled (culling is done at higher-level by ImGui:: functions), if you use this API a lot consider coarse culling your drawn objects.
struct ImDrawList
{
    // This is what you have to render
    ImVector<ImDrawCmd>     CmdBuffer;          // Draw commands. Typically 1 command = 1 GPU draw call, unless the command is a callback.
    ImVector<ImDrawIdx>     IdxBuffer;          // Index buffer. Each command consume ImDrawCmd::ElemCount of those
    ImVector<ImDrawVert>    VtxBuffer;          // Vertex buffer.

    // [Internal, used while building lists]
    ImDrawListFlags         Flags;              // Flags, you may poke into these to adjust anti-aliasing settings per-primitive.
    const ImDrawListSharedData* _Data;          // Pointer to shared draw data (you can use ImGui::GetDrawListSharedData() to get the one from current ImGui context)
    const char*             _OwnerName;         // Pointer to owner window's name for debugging
    unsigned int            _VtxCurrentIdx;     // [Internal] == VtxBuffer.Size
    ImDrawVert*             _VtxWritePtr;       // [Internal] point within VtxBuffer.Data after each add command (to avoid using the ImVector<> operators too much)
    ImDrawIdx*              _IdxWritePtr;       // [Internal] point within IdxBuffer.Data after each add command (to avoid using the ImVector<> operators too much)
    ImVector<ImVec4>        _ClipRectStack;     // [Internal]
    ImVector<ImTextureID>   _TextureIdStack;    // [Internal]
    ImVector<ImVec2>        _Path;              // [Internal] current path building
    int                     _ChannelsCurrent;   // [Internal] current channel number (0)
    int                     _ChannelsCount;     // [Internal] number of active channels (1+)
    ImVector<ImDrawChannel> _Channels;          // [Internal] draw channels for columns API (not resized down so _ChannelsCount may be smaller than _Channels.Size)

    // If you want to create ImDrawList instances, pass them ImGui::GetDrawListSharedData() or create and use your own ImDrawListSharedData (so you can use ImDrawList without ImGui)
    ImDrawList(const ImDrawListSharedData* shared_data) { _Data = shared_data; _OwnerName = NULL; Clear(); }
    ~ImDrawList() { ClearFreeMemory(); }
    IMGUI_API void  PushClipRect(ImVec2 clip_rect_min, ImVec2 clip_rect_max, bool intersect_with_current_clip_rect = false);  // Render-level scissoring. This is passed down to your render function but not used for CPU-side coarse clipping. Prefer using higher-level ImGui::PushClipRect() to affect logic (hit-testing and widget culling)
    IMGUI_API void  PushClipRectFullScreen();
    IMGUI_API void  PopClipRect();
    IMGUI_API void  PushTextureID(const ImTextureID& texture_id);
    IMGUI_API void  PopTextureID();
    inline ImVec2   GetClipRectMin() const { const ImVec4& cr = _ClipRectStack.back(); return ImVec2(cr.x, cr.y); }
    inline ImVec2   GetClipRectMax() const { const ImVec4& cr = _ClipRectStack.back(); return ImVec2(cr.z, cr.w); }

    // Primitives
    IMGUI_API void  AddLine(const ImVec2& a, const ImVec2& b, ImU32 col, float thickness = 1.0f);
    IMGUI_API void  AddRect(const ImVec2& a, const ImVec2& b, ImU32 col, float rounding = 0.0f, int rounding_corners_flags = ImDrawCornerFlags_All, float thickness = 1.0f);   // a: upper-left, b: lower-right, rounding_corners_flags: 4-bits corresponding to which corner to round
    IMGUI_API void  AddRectFilled(const ImVec2& a, const ImVec2& b, ImU32 col, float rounding = 0.0f, int rounding_corners_flags = ImDrawCornerFlags_All);                     // a: upper-left, b: lower-right
    IMGUI_API void  AddRectFilledMultiColor(const ImVec2& a, const ImVec2& b, ImU32 col_upr_left, ImU32 col_upr_right, ImU32 col_bot_right, ImU32 col_bot_left);
    IMGUI_API void  AddQuad(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, ImU32 col, float thickness = 1.0f);
    IMGUI_API void  AddQuadFilled(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, ImU32 col);
    IMGUI_API void  AddTriangle(const ImVec2& a, const ImVec2& b, const ImVec2& c, ImU32 col, float thickness = 1.0f);
    IMGUI_API void  AddTriangleFilled(const ImVec2& a, const ImVec2& b, const ImVec2& c, ImU32 col);
    IMGUI_API void  AddCircle(const ImVec2& centre, float radius, ImU32 col, int num_segments = 12, float thickness = 1.0f);
    IMGUI_API void  AddCircleFilled(const ImVec2& centre, float radius, ImU32 col, int num_segments = 12);
    IMGUI_API void  AddText(const ImVec2& pos, ImU32 col, const char* text_begin, const char* text_end = NULL);
    IMGUI_API void  AddText(const ImFont* font, float font_size, const ImVec2& pos, ImU32 col, const char* text_begin, const char* text_end = NULL, float wrap_width = 0.0f, const ImVec4* cpu_fine_clip_rect = NULL);
    IMGUI_API void  AddImage(ImTextureID user_texture_id, const ImVec2& a, const ImVec2& b, const ImVec2& uv_a = ImVec2(0,0), const ImVec2& uv_b = ImVec2(1,1), ImU32 col = 0xFFFFFFFF);
    IMGUI_API void  AddImageQuad(ImTextureID user_texture_id, const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, const ImVec2& uv_a = ImVec2(0,0), const ImVec2& uv_b = ImVec2(1,0), const ImVec2& uv_c = ImVec2(1,1), const ImVec2& uv_d = ImVec2(0,1), ImU32 col = 0xFFFFFFFF);
    IMGUI_API void  AddImageRounded(ImTextureID user_texture_id, const ImVec2& a, const ImVec2& b, const ImVec2& uv_a, const ImVec2& uv_b, ImU32 col, float rounding, int rounding_corners = ImDrawCornerFlags_All);
    IMGUI_API void  AddPolyline(const ImVec2* points, const int num_points, ImU32 col, bool closed, float thickness);
    IMGUI_API void  AddConvexPolyFilled(const ImVec2* points, const int num_points, ImU32 col);
    IMGUI_API void  AddBezierCurve(const ImVec2& pos0, const ImVec2& cp0, const ImVec2& cp1, const ImVec2& pos1, ImU32 col, float thickness, int num_segments = 0);

    // Stateful path API, add points then finish with PathFill() or PathStroke()
    inline    void  PathClear()                                                 { _Path.resize(0); }
    inline    void  PathLineTo(const ImVec2& pos)                               { _Path.push_back(pos); }
    inline    void  PathLineToMergeDuplicate(const ImVec2& pos)                 { if (_Path.Size == 0 || memcmp(&_Path[_Path.Size-1], &pos, 8) != 0) _Path.push_back(pos); }
    inline    void  PathFillConvex(ImU32 col)                                   { AddConvexPolyFilled(_Path.Data, _Path.Size, col); PathClear(); }
    inline    void  PathStroke(ImU32 col, bool closed, float thickness = 1.0f)  { AddPolyline(_Path.Data, _Path.Size, col, closed, thickness); PathClear(); }
    IMGUI_API void  PathArcTo(const ImVec2& centre, float radius, float a_min, float a_max, int num_segments = 10);
    IMGUI_API void  PathArcToFast(const ImVec2& centre, float radius, int a_min_of_12, int a_max_of_12);                                // Use precomputed angles for a 12 steps circle
    IMGUI_API void  PathBezierCurveTo(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, int num_segments = 0);
    IMGUI_API void  PathRect(const ImVec2& rect_min, const ImVec2& rect_max, float rounding = 0.0f, int rounding_corners_flags = ImDrawCornerFlags_All);

    // Channels
    // - Use to simulate layers. By switching channels to can render out-of-order (e.g. submit foreground primitives before background primitives)
    // - Use to minimize draw calls (e.g. if going back-and-forth between multiple non-overlapping clipping rectangles, prefer to append into separate channels then merge at the end)
    IMGUI_API void  ChannelsSplit(int channels_count);
    IMGUI_API void  ChannelsMerge();
    IMGUI_API void  ChannelsSetCurrent(int channel_index);

    // Advanced
    IMGUI_API void  AddCallback(ImDrawCallback callback, void* callback_data);  // Your rendering function must check for 'UserCallback' in ImDrawCmd and call the function instead of rendering triangles.
    IMGUI_API void  AddDrawCmd();                                               // This is useful if you need to forcefully create a new draw call (to allow for dependent rendering / blending). Otherwise primitives are merged into the same draw-call as much as possible

    // Internal helpers
    // NB: all primitives needs to be reserved via PrimReserve() beforehand!
    IMGUI_API void  Clear();
    IMGUI_API void  ClearFreeMemory();
    IMGUI_API void  PrimReserve(int idx_count, int vtx_count);
    IMGUI_API void  PrimRect(const ImVec2& a, const ImVec2& b, ImU32 col);      // Axis aligned rectangle (composed of two triangles)
    IMGUI_API void  PrimRectUV(const ImVec2& a, const ImVec2& b, const ImVec2& uv_a, const ImVec2& uv_b, ImU32 col);
    IMGUI_API void  PrimQuadUV(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, const ImVec2& uv_a, const ImVec2& uv_b, const ImVec2& uv_c, const ImVec2& uv_d, ImU32 col);
    inline    void  PrimWriteVtx(const ImVec2& pos, const ImVec2& uv, ImU32 col){ _VtxWritePtr->pos = pos; _VtxWritePtr->uv = uv; _VtxWritePtr->col = col; _VtxWritePtr++; _VtxCurrentIdx++; }
    inline    void  PrimWriteIdx(ImDrawIdx idx)                                 { *_IdxWritePtr = idx; _IdxWritePtr++; }
    inline    void  PrimVtx(const ImVec2& pos, const ImVec2& uv, ImU32 col)     { PrimWriteIdx((ImDrawIdx)_VtxCurrentIdx); PrimWriteVtx(pos, uv, col); }
    IMGUI_API void  UpdateClipRect();
    IMGUI_API void  UpdateTextureID();
};

// All draw data to render an ImGui frame
struct ImDrawData
{
    bool            Valid;                  // Only valid after Render() is called and before the next NewFrame() is called.
    ImDrawList**    CmdLists;
    int             CmdListsCount;
    int             TotalVtxCount;          // For convenience, sum of all cmd_lists vtx_buffer.Size
    int             TotalIdxCount;          // For convenience, sum of all cmd_lists idx_buffer.Size

    // Functions
    ImDrawData() { Clear(); }
    void Clear() { Valid = false; CmdLists = NULL; CmdListsCount = TotalVtxCount = TotalIdxCount = 0; } // Draw lists are owned by the ImGuiContext and only pointed to here.
    IMGUI_API void DeIndexAllBuffers();               // For backward compatibility or convenience: convert all buffers from indexed to de-indexed, in case you cannot render indexed. Note: this is slow and most likely a waste of resources. Always prefer indexed rendering!
    IMGUI_API void ScaleClipRects(const ImVec2& sc);  // Helper to scale the ClipRect field of each ImDrawCmd. Use if your final output buffer is at a different scale than ImGui expects, or if there is a difference between your window resolution and framebuffer resolution.
};

struct ImFontConfig
{
    void*           FontData;                   //          // TTF/OTF data
    int             FontDataSize;               //          // TTF/OTF data size
    bool            FontDataOwnedByAtlas;       // true     // TTF/OTF data ownership taken by the container ImFontAtlas (will delete memory itself).
    int             FontNo;                     // 0        // Index of font within TTF/OTF file
    float           SizePixels;                 //          // Size in pixels for rasterizer.
    int             OversampleH, OversampleV;   // 3, 1     // Rasterize at higher quality for sub-pixel positioning. We don't use sub-pixel positions on the Y axis.
    bool            PixelSnapH;                 // false    // Align every glyph to pixel boundary. Useful e.g. if you are merging a non-pixel aligned font with the default font. If enabled, you can set OversampleH/V to 1.
    ImVec2          GlyphExtraSpacing;          // 0, 0     // Extra spacing (in pixels) between glyphs. Only X axis is supported for now.
    ImVec2          GlyphOffset;                // 0, 0     // Offset all glyphs from this font input.
    const ImWchar*  GlyphRanges;                // NULL     // Pointer to a user-provided list of Unicode range (2 value per range, values are inclusive, zero-terminated list). THE ARRAY DATA NEEDS TO PERSIST AS LONG AS THE FONT IS ALIVE.
    bool            MergeMode;                  // false    // Merge into previous ImFont, so you can combine multiple inputs font into one ImFont (e.g. ASCII font + icons + Japanese glyphs). You may want to use GlyphOffset.y when merge font of different heights.
    unsigned int    RasterizerFlags;            // 0x00     // Settings for custom font rasterizer (e.g. ImGuiFreeType). Leave as zero if you aren't using one.
    float           RasterizerMultiply;         // 1.0f     // Brighten (>1.0f) or darken (<1.0f) font output. Brightening small fonts may be a good workaround to make them more readable.

    // [Internal]
    char            Name[32];                               // Name (strictly to ease debugging)
    ImFont*         DstFont;

    IMGUI_API ImFontConfig();
};

struct ImFontGlyph
{
    ImWchar         Codepoint;          // 0x0000..0xFFFF
    float           AdvanceX;           // Distance to next character (= data from font + ImFontConfig::GlyphExtraSpacing.x baked in)
    float           X0, Y0, X1, Y1;     // Glyph corners
    float           U0, V0, U1, V1;     // Texture coordinates
};

// Load and rasterize multiple TTF/OTF fonts into a same texture.
// Sharing a texture for multiple fonts allows us to reduce the number of draw calls during rendering.
// We also add custom graphic data into the texture that serves for ImGui.
//  1. (Optional) Call AddFont*** functions. If you don't call any, the default font will be loaded for you.
//  2. Call GetTexDataAsAlpha8() or GetTexDataAsRGBA32() to build and retrieve pixels data.
//  3. Upload the pixels data into a texture within your graphics system.
//  4. Call SetTexID(my_tex_id); and pass the pointer/identifier to your texture. This value will be passed back to you during rendering to identify the texture.
// IMPORTANT: If you pass a 'glyph_ranges' array to AddFont*** functions, you need to make sure that your array persist up until the ImFont is build (when calling GetTextData*** or Build()). We only copy the pointer, not the data.
struct ImFontAtlas
{
    IMGUI_API ImFontAtlas();
    IMGUI_API ~ImFontAtlas();
    IMGUI_API ImFont*           AddFont(const ImFontConfig* font_cfg);
    IMGUI_API ImFont*           AddFontDefault(const ImFontConfig* font_cfg = NULL);
    IMGUI_API ImFont*           AddFontFromFileTTF(const char* filename, float size_pixels, const ImFontConfig* font_cfg = NULL, const ImWchar* glyph_ranges = NULL);
    IMGUI_API ImFont*           AddFontFromMemoryTTF(void* font_data, int font_size, float size_pixels, const ImFontConfig* font_cfg = NULL, const ImWchar* glyph_ranges = NULL); // Note: Transfer ownership of 'ttf_data' to ImFontAtlas! Will be deleted after Build(). Set font_cfg->FontDataOwnedByAtlas to false to keep ownership.
    IMGUI_API ImFont*           AddFontFromMemoryCompressedTTF(const void* compressed_font_data, int compressed_font_size, float size_pixels, const ImFontConfig* font_cfg = NULL, const ImWchar* glyph_ranges = NULL); // 'compressed_font_data' still owned by caller. Compress with binary_to_compressed_c.cpp.
    IMGUI_API ImFont*           AddFontFromMemoryCompressedBase85TTF(const char* compressed_font_data_base85, float size_pixels, const ImFontConfig* font_cfg = NULL, const ImWchar* glyph_ranges = NULL);              // 'compressed_font_data_base85' still owned by caller. Compress with binary_to_compressed_c.cpp with -base85 parameter.
    IMGUI_API void              ClearTexData();             // Clear the CPU-side texture data. Saves RAM once the texture has been copied to graphics memory.
    IMGUI_API void              ClearInputData();           // Clear the input TTF data (inc sizes, glyph ranges)
    IMGUI_API void              ClearFonts();               // Clear the ImGui-side font data (glyphs storage, UV coordinates)
    IMGUI_API void              Clear();                    // Clear all

    // Build atlas, retrieve pixel data.
    // User is in charge of copying the pixels into graphics memory (e.g. create a texture with your engine). Then store your texture handle with SetTexID().
    // RGBA32 format is provided for convenience and compatibility, but note that unless you use CustomRect to draw color data, the RGB pixels emitted from Fonts will all be white (~75% of waste). 
    // Pitch = Width * BytesPerPixels
    IMGUI_API bool              Build();                    // Build pixels data. This is called automatically for you by the GetTexData*** functions.
    IMGUI_API void              GetTexDataAsAlpha8(unsigned char** out_pixels, int* out_width, int* out_height, int* out_bytes_per_pixel = NULL);  // 1 byte per-pixel
    IMGUI_API void              GetTexDataAsRGBA32(unsigned char** out_pixels, int* out_width, int* out_height, int* out_bytes_per_pixel = NULL);  // 4 bytes-per-pixel
    void                        SetTexID(ImTextureID id)    { TexID = id; }

    //-------------------------------------------
    // Glyph Ranges
    //-------------------------------------------

    // Helpers to retrieve list of common Unicode ranges (2 value per range, values are inclusive, zero-terminated list)
    // NB: Make sure that your string are UTF-8 and NOT in your local code page. In C++11, you can create UTF-8 string literal using the u8"Hello world" syntax. See FAQ for details.
    IMGUI_API const ImWchar*    GetGlyphRangesDefault();    // Basic Latin, Extended Latin
    IMGUI_API const ImWchar*    GetGlyphRangesKorean();     // Default + Korean characters
    IMGUI_API const ImWchar*    GetGlyphRangesJapanese();   // Default + Hiragana, Katakana, Half-Width, Selection of 1946 Ideographs
    IMGUI_API const ImWchar*    GetGlyphRangesChinese();    // Default + Japanese + full set of about 21000 CJK Unified Ideographs
    IMGUI_API const ImWchar*    GetGlyphRangesCyrillic();   // Default + about 400 Cyrillic characters
    IMGUI_API const ImWchar*    GetGlyphRangesThai();       // Default + Thai characters

    // Helpers to build glyph ranges from text data. Feed your application strings/characters to it then call BuildRanges().
    struct GlyphRangesBuilder
    {
        ImVector<unsigned char> UsedChars;  // Store 1-bit per Unicode code point (0=unused, 1=used)
        GlyphRangesBuilder()                { UsedChars.resize(0x10000 / 8); memset(UsedChars.Data, 0, 0x10000 / 8); }
        bool           GetBit(int n)        { return (UsedChars[n >> 3] & (1 << (n & 7))) != 0; }
        void           SetBit(int n)        { UsedChars[n >> 3] |= 1 << (n & 7); }  // Set bit 'c' in the array
        void           AddChar(ImWchar c)   { SetBit(c); }                          // Add character
        IMGUI_API void AddText(const char* text, const char* text_end = NULL);      // Add string (each character of the UTF-8 string are added)
        IMGUI_API void AddRanges(const ImWchar* ranges);                            // Add ranges, e.g. builder.AddRanges(ImFontAtlas::GetGlyphRangesDefault) to force add all of ASCII/Latin+Ext
        IMGUI_API void BuildRanges(ImVector<ImWchar>* out_ranges);                  // Output new ranges
    };

    //-------------------------------------------
    // Custom Rectangles/Glyphs API
    //-------------------------------------------

    // You can request arbitrary rectangles to be packed into the atlas, for your own purposes. After calling Build(), you can query the rectangle position and render your pixels.
    // You can also request your rectangles to be mapped as font glyph (given a font + Unicode point), so you can render e.g. custom colorful icons and use them as regular glyphs.
    struct CustomRect
    {
        unsigned int    ID;             // Input    // User ID. Use <0x10000 to map into a font glyph, >=0x10000 for other/internal/custom texture data.
        unsigned short  Width, Height;  // Input    // Desired rectangle dimension
        unsigned short  X, Y;           // Output   // Packed position in Atlas
        float           GlyphAdvanceX;  // Input    // For custom font glyphs only (ID<0x10000): glyph xadvance
        ImVec2          GlyphOffset;    // Input    // For custom font glyphs only (ID<0x10000): glyph display offset
        ImFont*         Font;           // Input    // For custom font glyphs only (ID<0x10000): target font
        CustomRect()            { ID = 0xFFFFFFFF; Width = Height = 0; X = Y = 0xFFFF; GlyphAdvanceX = 0.0f; GlyphOffset = ImVec2(0,0); Font = NULL; }
        bool IsPacked() const   { return X != 0xFFFF; }
    };

    IMGUI_API int       AddCustomRectRegular(unsigned int id, int width, int height);                                                                   // Id needs to be >= 0x10000. Id >= 0x80000000 are reserved for ImGui and ImDrawList
    IMGUI_API int       AddCustomRectFontGlyph(ImFont* font, ImWchar id, int width, int height, float advance_x, const ImVec2& offset = ImVec2(0,0));   // Id needs to be < 0x10000 to register a rectangle to map into a specific font.
    const CustomRect*   GetCustomRectByIndex(int index) const { if (index < 0) return NULL; return &CustomRects[index]; }

    // Internals
    IMGUI_API void      CalcCustomRectUV(const CustomRect* rect, ImVec2* out_uv_min, ImVec2* out_uv_max);
    IMGUI_API bool      GetMouseCursorTexData(ImGuiMouseCursor cursor, ImVec2* out_offset, ImVec2* out_size, ImVec2 out_uv_border[2], ImVec2 out_uv_fill[2]);

    //-------------------------------------------
    // Members
    //-------------------------------------------

    ImTextureID                 TexID;              // User data to refer to the texture once it has been uploaded to user's graphic systems. It is passed back to you during rendering via the ImDrawCmd structure.
    int                         TexDesiredWidth;    // Texture width desired by user before Build(). Must be a power-of-two. If have many glyphs your graphics API have texture size restrictions you may want to increase texture width to decrease height.
    int                         TexGlyphPadding;    // Padding between glyphs within texture in pixels. Defaults to 1.

    // [Internal]
    // NB: Access texture data via GetTexData*() calls! Which will setup a default font for you.
    unsigned char*              TexPixelsAlpha8;    // 1 component per pixel, each component is unsigned 8-bit. Total size = TexWidth * TexHeight
    unsigned int*               TexPixelsRGBA32;    // 4 component per pixel, each component is unsigned 8-bit. Total size = TexWidth * TexHeight * 4
    int                         TexWidth;           // Texture width calculated during Build().
    int                         TexHeight;          // Texture height calculated during Build().
    ImVec2                      TexUvScale;         // = (1.0f/TexWidth, 1.0f/TexHeight)
    ImVec2                      TexUvWhitePixel;    // Texture coordinates to a white pixel
    ImVector<ImFont*>           Fonts;              // Hold all the fonts returned by AddFont*. Fonts[0] is the default font upon calling ImGui::NewFrame(), use ImGui::PushFont()/PopFont() to change the current font.
    ImVector<CustomRect>        CustomRects;        // Rectangles for packing custom texture data into the atlas.
    ImVector<ImFontConfig>      ConfigData;         // Internal data
    int                         CustomRectIds[1];   // Identifiers of custom texture rectangle used by ImFontAtlas/ImDrawList
};

// Font runtime data and rendering
// ImFontAtlas automatically loads a default embedded font for you when you call GetTexDataAsAlpha8() or GetTexDataAsRGBA32().
struct ImFont
{
    // Members: Hot ~62/78 bytes
    float                       FontSize;           // <user set>   // Height of characters, set during loading (don't change after loading)
    float                       Scale;              // = 1.f        // Base font scale, multiplied by the per-window font scale which you can adjust with SetFontScale()
    ImVec2                      DisplayOffset;      // = (0.f,1.f)  // Offset font rendering by xx pixels
    ImVector<ImFontGlyph>       Glyphs;             //              // All glyphs.
    ImVector<float>             IndexAdvanceX;      //              // Sparse. Glyphs->AdvanceX in a directly indexable way (more cache-friendly, for CalcTextSize functions which are often bottleneck in large UI).
    ImVector<unsigned short>    IndexLookup;        //              // Sparse. Index glyphs by Unicode code-point.
    const ImFontGlyph*          FallbackGlyph;      // == FindGlyph(FontFallbackChar)
    float                       FallbackAdvanceX;   // == FallbackGlyph->AdvanceX
    ImWchar                     FallbackChar;       // = '?'        // Replacement glyph if one isn't found. Only set via SetFallbackChar()

    // Members: Cold ~18/26 bytes
    short                       ConfigDataCount;    // ~ 1          // Number of ImFontConfig involved in creating this font. Bigger than 1 when merging multiple font sources into one ImFont.
    ImFontConfig*               ConfigData;         //              // Pointer within ContainerAtlas->ConfigData
    ImFontAtlas*                ContainerAtlas;     //              // What we has been loaded into
    float                       Ascent, Descent;    //              // Ascent: distance from top to bottom of e.g. 'A' [0..FontSize]
    int                         MetricsTotalSurface;//              // Total surface in pixels to get an idea of the font rasterization/texture cost (not exact, we approximate the cost of padding between glyphs)

    // Methods
    IMGUI_API ImFont();
    IMGUI_API ~ImFont();
    IMGUI_API void              ClearOutputData();
    IMGUI_API void              BuildLookupTable();
    IMGUI_API const ImFontGlyph*FindGlyph(ImWchar c) const;
    IMGUI_API void              SetFallbackChar(ImWchar c);
    float                       GetCharAdvance(ImWchar c) const     { return ((int)c < IndexAdvanceX.Size) ? IndexAdvanceX[(int)c] : FallbackAdvanceX; }
    bool                        IsLoaded() const                    { return ContainerAtlas != NULL; }
    const char*                 GetDebugName() const                { return ConfigData ? ConfigData->Name : "<unknown>"; }

    // 'max_width' stops rendering after a certain width (could be turned into a 2d size). FLT_MAX to disable.
    // 'wrap_width' enable automatic word-wrapping across multiple lines to fit into given width. 0.0f to disable.
    IMGUI_API ImVec2            CalcTextSizeA(float size, float max_width, float wrap_width, const char* text_begin, const char* text_end = NULL, const char** remaining = NULL) const; // utf8
    IMGUI_API const char*       CalcWordWrapPositionA(float scale, const char* text, const char* text_end, float wrap_width) const;
    IMGUI_API void              RenderChar(ImDrawList* draw_list, float size, ImVec2 pos, ImU32 col, unsigned short c) const;
    IMGUI_API void              RenderText(ImDrawList* draw_list, float size, ImVec2 pos, ImU32 col, const ImVec4& clip_rect, const char* text_begin, const char* text_end, float wrap_width = 0.0f, bool cpu_fine_clip = false) const;

    // [Internal]
    IMGUI_API void              GrowIndex(int new_size);
    IMGUI_API void              AddGlyph(ImWchar c, float x0, float y0, float x1, float y1, float u0, float v0, float u1, float v1, float advance_x);
    IMGUI_API void              AddRemapChar(ImWchar dst, ImWchar src, bool overwrite_dst = true); // Makes 'dst' character/glyph points to 'src' character/glyph. Currently needs to be called AFTER fonts have been built.

#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
    typedef ImFontGlyph Glyph; // OBSOLETE 1.52+
#endif
};

#if defined(__clang__)
#pragma clang diagnostic pop
#endif

// Include imgui_user.h at the end of imgui.h (convenient for user to only explicitly include vanilla imgui.h)
#ifdef IMGUI_INCLUDE_IMGUI_USER_H
#include "imgui_user.h"
#endif
