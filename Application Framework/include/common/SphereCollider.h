//==========================================================================
// 球体形状の衝突プリミティブ [SphereCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include "mslib.hpp"
#include "Collider.h"

_MSLIB_BEGIN

namespace collider
{
    //==========================================================================
    //
    // class  : SphereCollider
    // Content: 球体形状の衝突プリミティブ
    //
    //==========================================================================
    class SphereCollider : public Collider, public Target<SphereCollider>
    {
    public:
        SphereCollider();
        ~SphereCollider();

        /**
        @brief 初期化
        */
        void Init()override;

        /**
        @brief 更新
        */
        void Update()override;

        /**
        @brief 描画
        */
        void Draw(LPDIRECT3DDEVICE9 device)override;

        /**
        @brief 現在のコリダーのサイズ
        */
        float GetSize();

        /**
        @brief コリダーのサイズ変更
        */
        void SetSize(float scale);
    protected:
        float m_scale; // 判定範囲
    };
}

_MSLIB_END