//==========================================================================
// サウンド[XAudio2.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "XAudio2.h"

_MSLIB_BEGIN
namespace xaudio2
{
    //==========================================================================
    // 実体
    //==========================================================================
    IXAudio2 *XAudio2Device::m_pXAudio2 = nullptr; // XAudio2オブジェクトへのインターフェイス
    HWND XAudio2Device::m_hWnd = nullptr; // ウィンドハンドル

    XAudio2Device::XAudio2Device()
    {
        SetComponentName("XAudio2Device");
        m_pMasteringVoice = nullptr;
    }

    XAudio2Device::~XAudio2Device()
    {
        Release();
    }

    //==========================================================================
    /**
    @brief 初期化
    @param hWnd [in] インスタンスハンドル
    @return Component Object Model defines, and macros
    */
    HRESULT XAudio2Device::Init(HWND hWnd)
    {
        m_pXAudio2 = nullptr;
        m_pMasteringVoice = nullptr;
        m_hWnd = hWnd;

        // COMライブラリの初期化
        if (FAILED(CoInitializeEx(nullptr, COINIT_MULTITHREADED)))
        {
            MessageBox(hWnd, "初期化に失敗しました", "error", MB_ICONWARNING);

            // COMライブラリの終了処理
            CoUninitialize();

            return E_FAIL;
        }

        // XAudio2オブジェクトの作成
        if (FAILED(XAudio2Create(&m_pXAudio2, 0)))
        {
            MessageBox(hWnd, "XAudio2オブジェクトの失敗しました", "error", MB_ICONWARNING);

            // COMライブラリの終了処理
            CoUninitialize();

            return E_FAIL;
        }

        // マスターボイスの生成
        if (FAILED(m_pXAudio2->CreateMasteringVoice(&m_pMasteringVoice)))
        {
            MessageBox(hWnd, "マスターボイスの生成に失敗しました", "error", MB_ICONWARNING);

            // XAudio2オブジェクトの開放
            if (m_pXAudio2 != nullptr)
            {
                m_pXAudio2->Release();
                m_pXAudio2 = nullptr;
            }

            // COMライブラリの終了処理
            CoUninitialize();

            return E_FAIL;
        }

        return S_OK;
    }

    //==========================================================================
    /**
    @brief 解放
    */
    void XAudio2Device::Release(void)
    {
        AllDestroyComponent();
        // マスターボイスの破棄
        DestroyVoice();

        // XAudio2オブジェクトの開放
        if (m_pXAudio2 != nullptr)
        {
            m_pXAudio2->Release();
            m_pXAudio2 = nullptr;
        }

        // COMライブラリの終了処理
        CoUninitialize();
    }

    //==========================================================================
    /**
    @brief インスタンスハンドルの取得
    @return インスタンスハンドル
    */
    HWND XAudio2Device::GetWnd(void)
    {
        return m_hWnd;
    }

    //==========================================================================
    /**
    @brief Audio2の取得
    @return Audio2
    */
    IXAudio2 * XAudio2Device::GetAudio2(void)
    {
        return m_pXAudio2;
    }

    //==========================================================================
    /**
    @brief ボイスデータの破棄
    */
    void XAudio2Device::DestroyVoice(void)
    {
        if (m_pMasteringVoice != nullptr)
        {
            m_pMasteringVoice->DestroyVoice();
            m_pMasteringVoice = nullptr;
        }
    }

    XAudio2::XAudio2()
    {
        SetComponentName("XAudio2");
    }

    XAudio2::~XAudio2()
    {
        Release();
    }

    //==========================================================================
    /**
    @brief 初期化
    @param Input [in] サウンドのデータ
    @param OutLabel [in] 一つ前のデータlabel
    @return Component Object Model defines, and macros
    */
    HRESULT XAudio2::Init(const XAudio2SoundLabel & Input, int & OutLabel)
    {
        HANDLE hFile;
        DWORD dwChunkSize = 0;
        DWORD dwChunkPosition = 0;
        DWORD dwFiletype;
        WAVEFORMATEXTENSIBLE wfx;
        XAUDIO2_BUFFER buffer;
        auto phWnd = XAudio2Device::GetWnd();
        auto * pXAudio2 = XAudio2Device::GetAudio2();

        // インスタンスの生成
        auto * pSound = m_Sound.Create();

        // バッファのクリア
        memset(&wfx, 0, sizeof(WAVEFORMATEXTENSIBLE));
        memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));

        // サウンドデータファイルの生成
        hFile = CreateFile(Input.m_strName.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
        if (hFile == INVALID_HANDLE_VALUE)
        {
            MessageBox(phWnd, "サウンドデータファイルの生成に失敗(1)", "警告", MB_ICONWARNING);
            return HRESULT_FROM_WIN32(GetLastError());
        }

        // ファイルポインタを先頭に移動
        if (SetFilePointer(hFile, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
        {
            MessageBox(phWnd, "サウンドデータファイルの生成に失敗(2)", "警告", MB_ICONWARNING);
            return HRESULT_FROM_WIN32(GetLastError());
        }

        // WAVEファイルのチェック
        if (FAILED(CheckChunk(hFile, 'FFIR', &dwChunkSize, &dwChunkPosition)))
        {
            MessageBox(phWnd, "WAVEファイルのチェックに失敗(1)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        if (FAILED(ReadChunkData(hFile, &dwFiletype, sizeof(DWORD), dwChunkPosition)))
        {
            MessageBox(phWnd, "WAVEファイルのチェックに失敗(2)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        if (dwFiletype != 'EVAW')
        {
            MessageBox(phWnd, "WAVEファイルのチェックに失敗(3)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        // フォーマットチェック
        if (FAILED(CheckChunk(hFile, ' tmf', &dwChunkSize, &dwChunkPosition)))
        {
            MessageBox(phWnd, "フォーマットチェックに失敗(1)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        if (FAILED(ReadChunkData(hFile, &wfx, dwChunkSize, dwChunkPosition)))
        {
            MessageBox(phWnd, "フォーマットチェックに失敗(2)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        // オーディオデータ読み込み
        if (FAILED(CheckChunk(hFile, 'atad', &pSound->m_SizeAudio, &dwChunkPosition)))
        {
            MessageBox(phWnd, "オーディオデータ読み込みに失敗(1)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        // メモリ確保
        pSound->m_pDataAudio = new BYTE[pSound->m_SizeAudio];

        if (FAILED(ReadChunkData(hFile, pSound->m_pDataAudio, pSound->m_SizeAudio, dwChunkPosition)))
        {
            MessageBox(phWnd, "オーディオデータ読み込みに失敗(2)", "警告", MB_ICONWARNING);
            return S_FALSE;
        }

        // ソースボイスの生成
        if (FAILED(pXAudio2->CreateSourceVoice(&pSound->m_pSourceVoice, &(wfx.Format))))
        {
            MessageBox(phWnd, "ソースボイスの生成に失敗！", "警告！", MB_ICONWARNING);
            return S_FALSE;
        }

        //// バッファの値設定
        //memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
        //buffer.AudioBytes = pSound->m_SizeAudio;
        //buffer.pAudioData = pSound->m_pDataAudio;
        //buffer.Flags = XAUDIO2_END_OF_STREAM;
        //buffer.LoopCount = Input.m_CntLoop;

        // カウント記憶
        pSound->m_CntLoop = Input.m_CntLoop;

        // ボリューム記憶
        pSound->m_Volume = Input.m_Volume;

        SetVolume(pSound, Input.m_Volume);

        //// オーディオバッファの登録
        //pSound->m_pSourceVoice->SubmitSourceBuffer(&buffer);

        // ラベルの登録
        OutLabel = (int)m_Sound.Size() - 1;

        return S_OK;
    }

    //==========================================================================
    /**
    @brief 初期化
    @param Input [in] サウンドのデータ
    @return Component Object Model defines, and macros
    */
    HRESULT XAudio2::Init(const XAudio2SoundLabel & Input)
    {
        int OutLabel = 0;
        return Init(Input, OutLabel);
    }

    //==========================================================================
    /**
    @brief 解放
    */
    void XAudio2::Release(void)
    {
        // 登録済みデータの破棄
        m_Sound.Release();
    }

    //==========================================================================
    /**
    @brief 特定のサウンドの破棄
    @param Input [in] データID
    */
    void XAudio2::PinpointRelease(int label)
    {
        m_Sound.Release(m_Sound.Get(label));
    }

    //==========================================================================
    /**
    @brief 再生
    @param Input [in] データID
    @return Component Object Model defines, and macros
    */
    HRESULT XAudio2::Play(int label)
    {
        XAUDIO2_BUFFER buffer;
        auto *pSound = m_Sound.Get(label);

        if (pSound != nullptr)
        {
            // バッファの値設定
            memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
            buffer.AudioBytes = pSound->m_SizeAudio;
            buffer.pAudioData = pSound->m_pDataAudio;
            buffer.Flags = XAUDIO2_END_OF_STREAM;
            buffer.LoopCount = pSound->m_CntLoop;

            Stop(pSound);

            pSound->m_pSourceVoice->SetVolume(pSound->m_Volume);

            // オーディオバッファの登録
            pSound->m_pSourceVoice->SubmitSourceBuffer(&buffer);

            // 再生
            pSound->m_pSourceVoice->Start(0);
        }

        return S_OK;
    }

    //==========================================================================
    /**
    @brief 特定のサウンド停止
    @param Input [in] データID
    */
    void XAudio2::Stop(int label)
    {
        Stop(m_Sound.Get(label));
    }

    //==========================================================================
    /**
    @brief 特定のサウンド停止
    @param pSound [in] データ
    */
    void XAudio2::Stop(XAudio2Param * pSound)
    {
        if (pSound != nullptr)
        {
            XAUDIO2_VOICE_STATE xa2state;

            // 状態取得
            pSound->m_pSourceVoice->GetState(&xa2state);
            if (xa2state.BuffersQueued != 0)
            {// 再生中
             // 一時停止
                pSound->m_pSourceVoice->Stop(0);

                // オーディオバッファの削除
                pSound->m_pSourceVoice->FlushSourceBuffers();
            }
        }
    }

    //==========================================================================
    /**
    @brief 格納してある全てのサウンド停止
    */
    void XAudio2::Stop(void)
    {
        for (int i = 0; i < m_Sound.Size(); i++)
        {
            Stop(m_Sound.Get(i));
        }
    }

    //==========================================================================
    /**
    @brief 特定のサウンドのボリューム設定
    @param label [in] データID
    @param volume [in] 0.0f〜1.0f
    */
    void XAudio2::SetVolume(int label, float volume)
    {
        auto * par = m_Sound.Get(label);

        if (par != nullptr)
        {
            par->m_Volume = volume;
            par->m_pSourceVoice->SetVolume(par->m_Volume);
        }
    }

    //==========================================================================
    /**
    @brief 全てのサウンドのボリューム設定
    @param volume [in] 0.0f〜1.0f
    */
    void XAudio2::SetVolume(float volume)
    {
        for (int i = 0; i < m_Sound.Size(); i++)
        {
            auto * par = m_Sound.Get(i);

            if (par != nullptr)
            {
                par->m_Volume = volume;
                par->m_pSourceVoice->SetVolume(par->m_Volume);
            }
        }
    }

    //==========================================================================
    /**
    @brief 全てのサウンドのボリューム自動設定
    */
    void XAudio2::SetVolume(void)
    {
        for (int i = 0; i < m_Sound.Size(); i++)
        {
            auto * par = m_Sound.Get(i);

            if (par != nullptr)
            {
                if (par->m_CntLoop == 0)
                {
                    par->m_Volume = 1.0f;
                    par->m_pSourceVoice->SetVolume(par->m_Volume);
                }
                else if (par->m_CntLoop == -1)
                {
                    par->m_Volume = 0.5f;
                    par->m_pSourceVoice->SetVolume(par->m_Volume);
                }
            }
        }
    }

    //==========================================================================
    /**
    @brief 特定のサウンドのボリューム設定
    @param pSound [in] サウンドデータ
    @param volume [in] 0.0f〜1.0f
    */
    void XAudio2::SetVolume(XAudio2Param * pSound, float volume)
    {
        if (pSound != nullptr)
        {
            pSound->m_Volume = volume;
            pSound->m_pSourceVoice->SetVolume(volume);
        }
    }

    //==========================================================================
    /**
    @brief 特定のサウンドのボリュームの取得
    @param label [in] データID
    */
    const float XAudio2::GetVolume(int label)
    {
        auto * par = m_Sound.Get(label);
        float Volume = 0.0;

        if (par != nullptr)
        {
            par->m_pSourceVoice->GetVolume(&Volume);
        }
        return Volume;
    }

    XAudio2Param::XAudio2Param()
    {
        m_pSourceVoice = nullptr;
        m_pDataAudio = nullptr;
        m_SizeAudio = (DWORD)0;
        m_CntLoop = 0;
        m_Volume = 1.0f;
    }

    XAudio2Param::~XAudio2Param()
    {
        Release();
    }

    //==========================================================================
    /**
    @brief ボイスデータの破棄
    */
    void XAudio2Param::Release(void)
    {
        XAUDIO2_VOICE_STATE xa2state;

        // サウンドが再生中の場合
        if (m_pSourceVoice != nullptr)
        {
            m_pSourceVoice->GetState(&xa2state);
            if (xa2state.BuffersQueued != 0)
            {// 再生中
             // 一時停止
                m_pSourceVoice->Stop(0);

                // オーディオバッファの削除
                m_pSourceVoice->FlushSourceBuffers();
            }

            // ソースボイスの破棄
            m_pSourceVoice->DestroyVoice();
            m_pSourceVoice = nullptr;

            // オーディオデータの開放
            if (m_pDataAudio != nullptr)
            {
                delete[]m_pDataAudio;
                m_pDataAudio = nullptr;
            }
        }
    }

    //==========================================================================
    /**
    @brief チャンクのチェック
    @param hFile [in]
    @param format [in]
    @param pChunkSize [in]
    @param pChunkDataPosition [in]
    */
    HRESULT XAudio2::CheckChunk(HANDLE hFile, DWORD format, DWORD * pChunkSize, DWORD * pChunkDataPosition)
    {
        HRESULT hr = S_OK;
        DWORD dwRead;
        DWORD dwChunkType;
        DWORD dwChunkDataSize;
        DWORD dwRIFFDataSize = 0;
        DWORD dwFileType;
        DWORD dwBytesRead = 0;
        DWORD dwOffset = 0;

        // ファイルポインタを先頭に移動
        if (SetFilePointer(hFile, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
        {
            return HRESULT_FROM_WIN32(GetLastError());
        }

        while (hr == S_OK)
        {
            // チャンクの読み込み
            if (ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, nullptr) == 0)
            {
                hr = HRESULT_FROM_WIN32(GetLastError());
            }

            // チャンクデータの読み込み
            if (ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, nullptr) == 0)
            {
                hr = HRESULT_FROM_WIN32(GetLastError());
            }

            switch (dwChunkType)
            {
            case 'FFIR':
                dwRIFFDataSize = dwChunkDataSize;
                dwChunkDataSize = 4;

                // ファイルタイプの読み込み
                if (ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, nullptr) == 0)
                {
                    hr = HRESULT_FROM_WIN32(GetLastError());
                }
                break;
            default:
                // ファイルポインタをチャンクデータ分移動
                if (SetFilePointer(hFile, dwChunkDataSize, nullptr, FILE_CURRENT) == INVALID_SET_FILE_POINTER)
                {
                    return HRESULT_FROM_WIN32(GetLastError());
                }
            }

            dwOffset += sizeof(DWORD) * 2;
            if (dwChunkType == format)
            {
                *pChunkSize = dwChunkDataSize;
                *pChunkDataPosition = dwOffset;

                return S_OK;
            }

            dwOffset += dwChunkDataSize;
            if (dwBytesRead >= dwRIFFDataSize)
            {
                return S_FALSE;
            }
        }

        return S_OK;
    }

    //==========================================================================
    /**
    @brief チャンクデータの読み込み
    @param hFile [in]
    @param pBuffer [in]
    @param dwBuffersize [in]
    @param dwBufferoffset [in]
    */
    HRESULT XAudio2::ReadChunkData(HANDLE hFile, void * pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset)
    {
        DWORD dwRead;

        // ファイルポインタを指定位置まで移動
        if (SetFilePointer(hFile, dwBufferoffset, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
        {
            return HRESULT_FROM_WIN32(GetLastError());
        }

        // データの読み込み
        if (ReadFile(hFile, pBuffer, dwBuffersize, &dwRead, nullptr) == 0)
        {
            return HRESULT_FROM_WIN32(GetLastError());
        }

        return S_OK;
    }

    XAudio2SoundLabel::XAudio2SoundLabel()
    {
        m_strName = "";
        m_CntLoop = 0;
        m_Volume = 1.0f;
    }

    XAudio2SoundLabel::XAudio2SoundLabel(const std::string & name, int loop, float vol)
    {
        m_strName = name;
        m_CntLoop = loop;
        m_Volume = vol;
    }

    XAudio2SoundLabel::~XAudio2SoundLabel()
    {
        m_strName.clear();
    }
}
_MSLIB_END