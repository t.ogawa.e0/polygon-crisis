//==========================================================================
// Activity [Activity.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Activity.h"
#include <algorithm>

_MSLIB_BEGIN

namespace activity
{
    std::list<Activity*> Activity::m_AllActivity; // �S�Ă�Activity

    Activity::Activity()
    {
        m_Activity = true;
    }
    Activity::Activity(const Activity & obj)
    {
        *this = obj;
    }
    Activity::~Activity()
    {
        DestroyActivity();
    }

    bool Activity::GetActivity() const
    {
        return m_Activity;
    }

    void Activity::SetActivity(bool activity)
    {
        m_Activity = activity;
    }
    void Activity::AddActivity()
    {
        auto itr = std::find(m_AllActivity.begin(), m_AllActivity.end(), this);
        if (itr != m_AllActivity.end())return;
        m_AllActivity.push_back(this);
    }
    void Activity::DestroyActivity()
    {
        auto itr = std::find(m_AllActivity.begin(), m_AllActivity.end(), this);
        if (itr == m_AllActivity.end())return;
        m_AllActivity.erase(itr);
    }
    std::list<Activity*> & Activity::GetAllActivity()
    {
        return m_AllActivity;
    }
    Activity & Activity::operator=(const Activity & obj)
    {
        m_Activity = obj.m_Activity;
        return *this;
    }
}

_MSLIB_END