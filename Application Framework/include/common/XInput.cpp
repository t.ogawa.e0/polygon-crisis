//==========================================================================
// Xインプット[XInput.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "XInput.h"

_MSLIB_BEGIN
namespace xinput
{
    XInputBool4::XInputBool4()
    {
        m_up = m_down = m_left = m_right = false;
    }

    XInputBool4::~XInputBool4()
    {
    }

    XInput::XInput()
    {
        SetComponentName("XInput");
        m_state = nullptr;
        m_stateOld = nullptr;
        m_statedat = nullptr;
        m_trigger = nullptr;
        m_numdata = 0;
    }

    XInput::~XInput()
    {
        Release();
    }

    //==========================================================================
    /**
    @brief 初期化
    @param Num [in] 取るコントローラーの数
    @return 失敗時 true
    */
    bool XInput::Init(int Num)
    {
        DWORD dwResult;
        if (m_state != nullptr)
        {
            delete[]m_state;
            m_state = nullptr;
        }
        if (m_stateOld != nullptr)
        {
            delete[]m_stateOld;
            m_stateOld = nullptr;
        }
        if (m_statedat != nullptr)
        {
            delete[]m_statedat;
            m_statedat = nullptr;
        }
        if (m_trigger != nullptr)
        {
            delete[]m_trigger;
            m_trigger = nullptr;
        }

        // 登録数の代入
        m_numdata = Num;
        m_statedat = new bool[m_numdata];
        m_state = new XINPUT_STATE[m_numdata];
        m_stateOld = new XINPUT_STATE[m_numdata];
        m_trigger = new XInputAnalogTrigger[m_numdata];

        for (int i = 0; i < m_numdata; i++)
        {
            // 中身をきれいにする
            ZeroMemory(&m_state[i], sizeof(XINPUT_STATE));

            // コントローラーの情報の取得
            dwResult = XInputGetState((DWORD)i, &m_state[i]);
            if (dwResult != ERROR_SUCCESS)
            {
                m_statedat[i] = false;
                break;
            }
            else
            {
                m_statedat[i] = true;
            }
        }

        return false;
    }

    //==========================================================================
    /**
    @brief 解放
    */
    void XInput::Release(void)
    {
        if (m_state != nullptr)
        {
            delete[]m_state;
            m_state = nullptr;
        }
        if (m_stateOld != nullptr)
        {
            delete[]m_stateOld;
            m_stateOld = nullptr;
        }
        if (m_statedat != nullptr)
        {
            delete[]m_statedat;
            m_statedat = nullptr;
        }
        if (m_trigger != nullptr)
        {
            delete[]m_trigger;
            m_trigger = nullptr;
        }
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void XInput::Update(void)
    {
        for (int i = 0; i < m_numdata; i++)
        {
            // アクセス速度の向上
            auto * pnew = &m_state[i];
            auto * pold = &m_stateOld[i];
            auto * panatri = &m_trigger[i];

            // コントローラ情報の複製
            *pold = *pnew;

            // コントローラーの情報の取得
            XInputGetState(i, pnew);

            // 左スティックのデッドゾーン
            if ((pnew->Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && pnew->Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) &&
                (pnew->Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && pnew->Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
            {
                pnew->Gamepad.sThumbLX = 0;
                pnew->Gamepad.sThumbLY = 0;
            }

            // 右スティックのデッドゾーン
            if ((pnew->Gamepad.sThumbRX < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && pnew->Gamepad.sThumbRX > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) &&
                (pnew->Gamepad.sThumbRY < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && pnew->Gamepad.sThumbRY > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE))
            {
                pnew->Gamepad.sThumbRX = 0;
                pnew->Gamepad.sThumbRY = 0;
            }

            // アナログスティックトリガー用更新
            panatri->update();

            // 左アナログのトリガー処理
            if (pnew->Gamepad.sThumbLY > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
                pnew->Gamepad.sThumbLY < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
                pnew->Gamepad.sThumbLX < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
                pnew->Gamepad.sThumbLX > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000)
            {
                panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbLY), &panatri->m_Ltrigger.m_up);
                panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbLY, -(1.41421356f / 2.0f)), &panatri->m_Ltrigger.m_down);
                panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbLX, -(1.41421356f / 2.0f)), &panatri->m_Ltrigger.m_left);
                panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbLX), &panatri->m_Ltrigger.m_right);
            }
            else
            {
                panatri->m_Ltrigger = XInputBool4();
            }

            // 右アナログのトリガー処理
            if (pnew->Gamepad.sThumbRY > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
                pnew->Gamepad.sThumbRY < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
                pnew->Gamepad.sThumbRX < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
                pnew->Gamepad.sThumbRX > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000)
            {
                panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbRY), &panatri->m_Rtrigger.m_up);
                panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbRY, -(1.41421356f / 2.0f)), &panatri->m_Rtrigger.m_down);
                panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbRX, -(1.41421356f / 2.0f)), &panatri->m_Rtrigger.m_left);
                panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbRX), &panatri->m_Rtrigger.m_right);
            }
            else
            {
                panatri->m_Rtrigger = XInputBool4();
            }
        }
    }

    //==========================================================================
    /**
    @brief プレス
    @param button [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::Press(XInputButton button, int index)
    {
        return (m_state[index].Gamepad.wButtons & (WORD)button) ? true : false;
    }

    //==========================================================================
    /**
    @brief トリガー
    @param button [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::Trigger(XInputButton button, int index)
    {
        return KeyTrigger((bool)((m_state[index].Gamepad.wButtons & (WORD)button) ? true : false), (bool)((m_stateOld[index].Gamepad.wButtons & (WORD)button) ? true : false));
    }

    //==========================================================================
    /**
    @brief リリース
    @param button [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::Release(XInputButton button, int index)
    {
        return KeyRelease((m_state[index].Gamepad.wButtons & (WORD)button) ? true : false, (m_stateOld[index].Gamepad.wButtons & (WORD)button) ? true : false);
    }

    //==========================================================================
    /**
    @brief 左アナログスティック
    @param index [in] 処理コントローラー
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool XInput::AnalogL(int index, D3DXVECTOR3 & Out)
    {
        auto * pstate = &m_state[index];

        Out = D3DXVECTOR3(pstate->Gamepad.sThumbLX, 0.0f, pstate->Gamepad.sThumbLY);
        D3DXVec3Normalize(&Out, &Out);

        return AnalogL(index);
    }

    //==========================================================================
    /**
    @brief 左アナログスティック
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::AnalogL(int index)
    {
        auto * pstate = &m_state[index];

        if (pstate->Gamepad.sThumbLY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
            pstate->Gamepad.sThumbLY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
            pstate->Gamepad.sThumbLX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
            pstate->Gamepad.sThumbLX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
        {
            return true;
        }

        return false;
    }

    //==========================================================================
    /**
    @brief 右アナログスティック
    @param index [in] 処理コントローラー
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool XInput::AnalogR(int index, D3DXVECTOR3 & Out)
    {
        auto * pstate = &m_state[index];

        Out = D3DXVECTOR3(pstate->Gamepad.sThumbRX, 0.0f, pstate->Gamepad.sThumbRY);
        D3DXVec3Normalize(&Out, &Out);

        return AnalogR(index);
    }

    //==========================================================================
    /**
    @brief 右アナログスティック
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::AnalogR(int index)
    {
        auto * pstate = &m_state[index];

        if (pstate->Gamepad.sThumbRY > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
            pstate->Gamepad.sThumbRY < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
            pstate->Gamepad.sThumbRX < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
            pstate->Gamepad.sThumbRX > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
        {
            return true;
        }

        return false;
    }

    //==========================================================================
    /**
    @brief 左アナログスティックのトリガー
    @param key [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::AnalogLTrigger(XInputAnalog key, int index)
    {
        switch (key)
        {
        case XInputAnalog::UP:
            return KeyTrigger(m_trigger[index].m_Ltrigger.m_up, m_trigger[index].m_LtriggerOld.m_up);
            break;
        case XInputAnalog::DOWN:
            return KeyTrigger(m_trigger[index].m_Ltrigger.m_down, m_trigger[index].m_LtriggerOld.m_down);
            break;
        case XInputAnalog::LEFT:
            return KeyTrigger(m_trigger[index].m_Ltrigger.m_left, m_trigger[index].m_LtriggerOld.m_left);
            break;
        case XInputAnalog::RIGHT:
            return KeyTrigger(m_trigger[index].m_Ltrigger.m_right, m_trigger[index].m_LtriggerOld.m_right);
            break;
        default:
            break;
        }
        return false;
    }

    //==========================================================================
    /**
    @brief 左アナログスティックのトリガー
    @param index [in] 処理コントローラー
    @param key [in] ボタンID
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool XInput::AnalogLTrigger(XInputAnalog key, int index, D3DXVECTOR3 * Out)
    {
        auto * pstate = &m_state[index];

        *Out = D3DXVECTOR3(pstate->Gamepad.sThumbLX, 0.0f, pstate->Gamepad.sThumbLY);
        D3DXVec3Normalize(Out, Out);

        return AnalogLTrigger(key, index);
    }

    //==========================================================================
    /**
    @brief 右アナログスティックのトリガー
    @param index [in] 処理コントローラー
    @param key [in] ボタンID
    @return 押しているとき true
    */
    bool XInput::AnalogRTrigger(XInputAnalog key, int index)
    {
        switch (key)
        {
        case XInputAnalog::UP:
            return KeyTrigger(m_trigger[index].m_Rtrigger.m_up, m_trigger[index].m_RtriggerOld.m_up);
            break;
        case XInputAnalog::DOWN:
            return KeyTrigger(m_trigger[index].m_Rtrigger.m_down, m_trigger[index].m_RtriggerOld.m_down);
            break;
        case XInputAnalog::LEFT:
            return KeyTrigger(m_trigger[index].m_Rtrigger.m_left, m_trigger[index].m_RtriggerOld.m_left);
            break;
        case XInputAnalog::RIGHT:
            return KeyTrigger(m_trigger[index].m_Rtrigger.m_right, m_trigger[index].m_RtriggerOld.m_right);
            break;
        default:
            break;
        }
        return false;
    }

    //==========================================================================
    /**
    @brief 右アナログスティックのトリガー
    @param index [in] 処理コントローラー
    @param key [in] ボタンID
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool XInput::AnalogRTrigger(XInputAnalog key, int index, D3DXVECTOR3 * Out)
    {
        auto * pstate = &m_state[index];

        *Out = D3DXVECTOR3(pstate->Gamepad.sThumbRX, 0.0f, pstate->Gamepad.sThumbRY);
        D3DXVec3Normalize(Out, Out);

        return AnalogRTrigger(key, index);
    }

    //==========================================================================
    /**
    @brief 左トリガーボタン
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::LT(int index)
    {
        return (m_state[index].Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) ? true : false;
    }

    //==========================================================================
    /**
    @brief 右トリガーボタン
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool XInput::RT(int index)
    {
        return (m_state[index].Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) ? true : false;
    }

    //==========================================================================
    /**
    @brief コントローラーの存在の確認
    @param index [in] 処理コントローラー
    @return 存在する場合 true
    */
    bool XInput::Check(int index)
    {
        return m_statedat[index];
    }

    //==========================================================================
    /**
    @brief コントローラーの現在の状態を取得
    @param index [in] 処理コントローラー
    @return インスタンス
    */
    XINPUT_STATE * XInput::GetState(int index)
    {
        return &m_state[index];
    }

    //==========================================================================
    /**
    @brief サイズ
    @return コントローラー数
    */
    int XInput::Size(void)
    {
        return m_numdata;
    }

    //==========================================================================
    /**
    @brief コントローラのキートリガー
    @param bNew [in] 新しい判定キー
    @param bOld [in] 古い判定キー
    @return 結果
    */
    bool XInput::KeyTrigger(bool bNew, bool bOld)
    {
        return (bOld ^ bNew) & bNew;
    }

    //==========================================================================
    /**
    @brief コントローラのキーリリース
    @param bNew [in] 新しい判定キー
    @param bOld [in] 古い判定キー
    @return 結果
    */
    bool XInput::KeyRelease(bool bNew, bool bOld)
    {
        return bOld ^ bNew & bOld;
    }

    XInputAnalogTrigger::XInputAnalogTrigger()
    {
        m_Ltrigger = XInputBool4();
        m_LtriggerOld = XInputBool4();
        m_Rtrigger = XInputBool4();
        m_RtriggerOld = XInputBool4();
    }

    XInputAnalogTrigger::~XInputAnalogTrigger()
    {
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void XInputAnalogTrigger::update(void)
    {
        m_LtriggerOld = m_Ltrigger;
        m_RtriggerOld = m_Rtrigger;
    }

    //==========================================================================
    /**
    @brief 切り替え
    @param input [in] 判定キー
    @param Out [in/out] 対象
    */
    void XInputAnalogTrigger::ifset(bool input, bool * Out)
    {
        (*Out) = ((input == true) ? true : false);
    }

    //==========================================================================
    /**
    @brief 比較 in1 < in2
    @param in1 [in] 対象1
    @param in2 [in/out] 対象2
    @return in2 が大きい場合 true
    */
    bool XInputAnalogTrigger::ifbool(float in1, float in2)
    {
        return (in1 < in2) ? true : false;
    }
}
_MSLIB_END