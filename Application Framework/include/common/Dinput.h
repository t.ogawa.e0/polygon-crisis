//==========================================================================
// ダイレクインプット[Dinput.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include<dinput.h>
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")
#include <list>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "Component.h"

_MSLIB_BEGIN

namespace dinput
{
    //==========================================================================
    //
    // class  : DirectInputManager
    // Content: DirectInputManager
    //
    //==========================================================================
    class Dinput
    {
    private:
        // コピー禁止 (C++11)
        Dinput(const Dinput &) = delete;
        Dinput &operator=(const Dinput &) = delete;
        Dinput &operator=(Dinput&&) = delete;
    protected:
        Dinput();
        virtual ~Dinput();
        /**
        @brief 更新
        */
        virtual void Update() = 0;
    public:
        /**
        @brief 登録済みデバイスの更新
        */
        static void UpdateAll();
        /**
        @brief インプットリスト
        */
        static std::list<Dinput*> & GetInputList();
    private:
        static std::list<Dinput*> m_DirectInput; // デバイスの格納
    };

    //==========================================================================
    //
    // class  : Input
    // Content: Input
    //
    //==========================================================================
    class Input : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        Input(const Input &) = delete;
        Input &operator=(const Input &) = delete;
        Input &operator=(Input&&) = delete;
    public:
        Input();
        virtual ~Input();
    private:
        /**
        @brief 解放
        */
        void Release();
    protected:
        LPDIRECTINPUT8 m_DInput; // DirectInputオブジェクトへのポインタ
        LPDIRECTINPUTDEVICE8 m_DIDevice; // 入力デバイス(キーボード)へのポインタ
    };
}

_MSLIB_END