//==========================================================================
// 平面のプリミティブコライダー [PlaneCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlaneCollider.h"

_MSLIB_BEGIN

namespace collider
{
    constexpr const char * __vertex_c__ = "vertex";
    constexpr float VCRCT = 0.5f;
    constexpr const int __number_of_vertices__ = 4; // 頂点数
    const D3DXVECTOR3 __vertex_coordinates__[__number_of_vertices__] =
    {
        D3DXVECTOR3(-VCRCT, 0,VCRCT),// 0
        D3DXVECTOR3(VCRCT, 0,VCRCT), // 1
        D3DXVECTOR3(VCRCT,0,-VCRCT), // 2
        D3DXVECTOR3(-VCRCT,0,-VCRCT),// 3
    };
    struct VectorDrawData
    {
        D3DXVECTOR3 pos = D3DXVECTOR3(0, 0, 0); // 座標変換が必要
        D3DCOLOR color = D3DCOLOR_RGBA(255, 255, 255, 255); // ポリゴンの色
    };

    PlaneCollider::PlaneCollider()
    {
        SetComponentName("PlaneCollider");
        m_transform.reserve(__number_of_vertices__ + 1);
        m_vertex.reserve(__number_of_vertices__);
        m_upward = false;
        m_under = true;
        m_position_input_mode = true;
    }

    PlaneCollider::~PlaneCollider()
    {
    }

    void PlaneCollider::Init()
    {
        auto target = GetWarrantyParent<transform::Transform>();
        if (target == nullptr)return;

        auto tar = target->AddComponent<transform::Transform>();
        tar->SetComponentName(GetComponentName() + "_Transform");
        m_transform.push_back(tar);

        for (int i = 0; i < __number_of_vertices__; i++)
        {
            auto obj = tar->AddComponent<transform::Transform>();
            obj->SetComponentName(__vertex_c__ + std::to_string(i));
            obj->SetPosition(__vertex_coordinates__[i]);
            m_transform.push_back(obj);
            m_vertex.push_back(obj);
        }
    }

    void PlaneCollider::Update()
    {
        if (!((int)m_vertex.size() == __number_of_vertices__))return;
        if (!m_upward && !m_under)return;

        for (auto & itr : m_target_collider)
        {
            D3DXVECTOR3 v01, v12, v20, v0p, v1p, v2p;
            if (!itr->GetUpdateFlag())continue;

            auto target_itr = itr->GetWarrantyParent<transform::Transform>();
            if (target_itr == nullptr)continue;
            auto target_mat = target_itr->GetWorldMatrix();
            auto mat1 = m_vertex[0]->GetWorldMatrix();// 左上
            auto mat2 = m_vertex[1]->GetWorldMatrix();// 右上
            auto mat3 = m_vertex[2]->GetWorldMatrix();// 右下
            auto mat4 = m_vertex[3]->GetWorldMatrix();// 左下

            auto target_position = D3DXVECTOR3(target_mat->_41, target_mat->_42, target_mat->_43);
            auto v0 = D3DXVECTOR3(mat1->_41, mat1->_42, mat1->_43); // 左上
            auto v1 = D3DXVECTOR3(mat2->_41, mat2->_42, mat2->_43); // 右上
            auto v2 = D3DXVECTOR3(mat3->_41, mat3->_42, mat3->_43); // 右下
            auto v3 = D3DXVECTOR3(mat4->_41, mat4->_42, mat4->_43); // 左下

            //==========================================================================
            // 頂点0,1,3の三角形の表面と裏面の処理
            //==========================================================================

            // 1 - 0
            v01 = v1 - v0;
            // 3 - 1
            v12 = v3 - v1;
            // 0 - 3
            v20 = v0 - v3;
            // 0
            v0p = target_position - v0;
            // 1
            v1p = target_position - v1;
            // 3
            v2p = target_position - v3;

            if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1, target_itr)) 
            {
                itr->SetTrigger(true);
                continue;
            }

            // 裏面より上にいるか

            // 0
            v0p = v0 - target_position;
            // 1
            v1p = v1 - target_position;
            // 3
            v2p = v3 - target_position;

            if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1, target_itr))
            {
                itr->SetTrigger(true);
                continue;
            }

            //==========================================================================
            // 頂点1,2,3の三角形の表面と裏面の処理
            //==========================================================================

            // 表面

            // 2 - 1
            v01 = v2 - v1;
            // 3 - 2
            v12 = v3 - v2;
            // 1 - 3
            v20 = v1 - v3;
            // 1
            v0p = target_position - v1;
            // 2
            v1p = target_position - v2;
            // 3
            v2p = target_position - v3;

            if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1, target_itr))
            {
                itr->SetTrigger(true);
                continue;
            }

            // 裏面

            // 0
            v0p = v1 - target_position;
            // 1
            v1p = v2 - target_position;
            // 3
            v2p = v3 - target_position;

            if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1, target_itr))
            {
                itr->SetTrigger(true);
                continue;
            }
        }
    }

    void PlaneCollider::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 描画を行うかのチェック
        auto target = GetWarrantyParent<transform::Transform>();
        if (target == nullptr)return;
        if (m_transform.size() == 0)return;

        int size = __number_of_vertices__ + 1;
        auto *pVector = new VectorDrawData[size];

        // 座標渡し
        for (int i = 0; i < (int)m_vertex.size(); i++)
        {
            pVector[i].pos = *m_vertex[i]->GetPosition();
        }

        pVector[__number_of_vertices__].pos = *m_vertex[0]->GetPosition();

        // 当たり判定が出たときだけ色を変える
        if ((m_collision))
            for (int i = 0; i < size; i++)
                pVector[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

        device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);
        device->SetTransform(D3DTS_WORLD, m_transform[0]->GetWorldMatrix());
        device->SetTexture(0, nullptr);
        device->DrawPrimitiveUP(D3DPT_LINESTRIP, __number_of_vertices__, pVector, sizeof(VectorDrawData));

        delete[] pVector;
    }
    void PlaneCollider::UpCollider(bool flag)
    {
        m_upward = flag;
    }
    void PlaneCollider::UnderCollider(bool flag)
    {
        m_under = flag;
    }
    void PlaneCollider::SetPositionInputMode(bool flag)
    {
        m_position_input_mode = flag;
    }
    bool PlaneCollider::Judgment(
        const D3DXVECTOR3 & target,
        const D3DXVECTOR3 & v01,
        const D3DXVECTOR3 & v12,
        const D3DXVECTOR3 & v20,
        const D3DXVECTOR3 & v0p,
        const D3DXVECTOR3 & v1p,
        const D3DXVECTOR3 & v2p,
        const D3DXVECTOR3 & vpos,
        transform::Transform * obj)
    {
        auto c0 = v01.x*v0p.z - v01.z*v0p.x;
        auto c1 = v12.x*v1p.z - v12.z*v1p.x;
        auto c2 = v20.x*v2p.z - v20.z*v2p.x;

        if (c0 <= 0.0f && c1 <= 0.0f && c2 <= 0.0f)
        {
            D3DXVECTOR3 N;
            auto hitposition = target;

            D3DXVec3Cross(&N, &v01, &v12);
            hitposition.y = vpos.y - (N.x * (target.x - vpos.x) + N.z * (target.z - vpos.z)) / N.y;

            if (m_under&&target.y < hitposition.y)
            {
                m_collision = true;
                if (m_position_input_mode)
                {
                    obj->SetPosition(hitposition);
                    obj->UnlockUpdate();
                }
                return true;
            }
            if (m_upward&&hitposition.y < target.y)
            {
                m_collision = true;
                if (m_position_input_mode)
                {
                    obj->SetPosition(hitposition);
                    obj->UnlockUpdate();
                }
                return true;
            }
        }
        return false;
    }
}
_MSLIB_END