//==========================================================================
// エフェクト [Effect.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "mslib.hpp"
#include "IReference.h"

_MSLIB_BEGIN

namespace effect
{
    class EffectLoader;

    //==========================================================================
    //
    // class  : EffectData
    // Content: エフェクトデータ
    //
    //==========================================================================
    class EffectData : public ireference::ReferenceData<LPD3DXEFFECT>
    {
    public:
        EffectData();
        ~EffectData();
    public:
        int64_t createID; // 生成ID
        EffectLoader * LoaderPtr; // ローダーのポインタ
        std::string tag; // タグ
    };

    //==========================================================================
    //
    // class  : EffectReference
    // Content: 参照用
    //
    //==========================================================================
    class EffectReference : public ireference::ReferenceOperator<EffectData>
    {
    public:
        EffectReference();
        ~EffectReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        const std::string & Tag();
    };

    //==========================================================================
    //
    // class  : EffectLoader
    // Content: エフェクト読み込み破棄クラス
    //
    //==========================================================================
    class EffectLoader : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        EffectLoader(const EffectLoader &) = delete;
        EffectLoader &operator=(const EffectLoader &) = delete;
        EffectLoader &operator=(EffectLoader&&) = delete;
    public:
        EffectLoader();
        EffectLoader(LPDIRECT3DDEVICE9 device);
        ~EffectLoader();

        /**
        @brief エフェクトを読み込む。
        @param path [in] 読み込みパス
        @return エフェクトのポインタ
        */
        EffectReference Load(const std::string & path);

        /**
        @brief エフェクトを破棄する。
        @param data [in] エフェクト
        */
        void Unload(EffectData * data);

        /**
        @brief エフェクトを破棄する。
        @param data [in] エフェクト
        */
        void Unload(EffectData & data);
    private:
        LPDIRECT3DDEVICE9 m_device; // デバイス
        int64_t createIDCount; // カウンタ
        std::unordered_map<std::string, EffectData> m_data; // データ
    };

    //==========================================================================
    //
    // class  : SetEffect
    // Content: エフェクト登録
    //
    //==========================================================================
    class SetEffect
    {
    public:
        SetEffect();
        virtual ~SetEffect();

        /**
        @brief エフェクトデータの登録
        @param EffectData [in] データ
        */
        void SetEffectData(const EffectReference & effect);

        /**
        @brief エフェクトデータの取得
        @return エフェクトのポインタ
        */
        EffectReference & GetEffectData();
    protected:
        EffectReference m_EffectData;
    };
}

_MSLIB_END