//==========================================================================
// DefaultSystem [DefaultSystem.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib include
//==========================================================================
#include "mslib.hpp"
#include "Sprite.h"
#include "MsImGui.h"
#include "Texture.h"
#include "Component.h"
#include "Transform.h"
#include "MsImGui.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
//
// デフォルトの機能
//
//==========================================================================
namespace DefaultSystem
{
    // スタック
    namespace stack
    {
        class Stack
        {
        public:
            Stack() {}
            ~Stack() {}

            static std::ofstream & BeginOfstream();
            static void EndOfstream();

            static std::ifstream & BeginIfstream();
            static void EndIfstream();
        private:
            static std::list<std::ofstream> ofstream_stack;
            static std::list<std::ifstream> ifstream_stack;
        };
    }
}

//==========================================================================
//
// デフォルトの機能
//
//==========================================================================
namespace DefaultSystem
{
    //==========================================================================
    // テクスチャリソース表示
    //==========================================================================
    void ResourceManager(mslib::texture::TextureLoader * loader);

    //==========================================================================
    // ノード探索
    //==========================================================================
    void NodeSearch(component::Component * child);

    //==========================================================================
    // UIエディタ
    //==========================================================================
    void UIEditor(sprite::Sprite * obj);

    //==========================================================================
    // UIエディタ(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void UIEditor(wrapper::vector<_Ty>* obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                UIEditor(dynamic_cast<sprite::Sprite*>(obj->Get(i)));
    }

    //==========================================================================
    // UIエディタ(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void UIEditor(wrapper::vector_component<_Ty>* obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                UIEditor(dynamic_cast<sprite::Sprite*>(obj->Get(i)));
    }

    //==========================================================================
    // 管理されているオブジェクト表示
    //==========================================================================
    void ObjectData(component::Component * this_);

    //==========================================================================
    // 管理されているオブジェクト表示(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void ObjectData(wrapper::vector<_Ty>* this_)
    {
        if (this_ != nullptr)
            for (int i = 0; i < this_->Size(); i++)
                ObjectData(dynamic_cast<component::Component*>(this_->Get(i)));
    }

    //==========================================================================
    // 管理されているオブジェクト表示(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void ObjectData(wrapper::vector_component<_Ty>* this_)
    {
        if (this_ != nullptr)
            for (int i = 0; i < this_->Size(); i++)
                ObjectData(dynamic_cast<component::Component*>(this_->Get(i)));
    }

    //==========================================================================
    // 保存機能
    //==========================================================================
    void SaverTemplate(std::ofstream & ofstream, const std::string & manager_object_name, transform::Transform * obj, int data_id);

    //==========================================================================
    // 保存機能(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void SaverTemplate(std::ofstream & ofstream, const std::string & manager_object_name, wrapper::vector<_Ty> * obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                SaverTemplate(ofstream, manager_object_name, dynamic_cast<transform::Transform*>(obj->Get(i)), i);
    }

    //==========================================================================
    // 保存機能(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void SaverTemplate(std::ofstream & ofstream, const std::string & manager_object_name, wrapper::vector_component<_Ty> * obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                SaverTemplate(ofstream, manager_object_name, dynamic_cast<transform::Transform*>(obj->Get(i)), i);
    }

    //==========================================================================
    // 読み取り機能
    //==========================================================================
    void LoaderTemplate(std::ifstream & ifstream, const std::string & manager_object_name, transform::Transform * obj, int data_id);

    //==========================================================================
    // 読み取り機能(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void LoaderTemplate(std::ifstream & ifstream, const std::string & manager_object_name, wrapper::vector<_Ty> * obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                LoaderTemplate(ifstream, manager_object_name, dynamic_cast<transform::Transform*>(obj->Get(i)), i);
    }

    //==========================================================================
    // 読み取り機能(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void LoaderTemplate(std::ifstream & ifstream, const std::string & manager_object_name, wrapper::vector_component<_Ty> * obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                LoaderTemplate(ifstream, manager_object_name, dynamic_cast<transform::Transform*>(obj->Get(i)), i);
    }

    //==========================================================================
    // バイナリ書き込み開始
    //========================================================================== 
    std::ofstream & BeginFstreamWritingBinary(const std::string & filename);

    //==========================================================================
    // バイナリ書き込み終了
    //==========================================================================
    void EndFstreamWritingBinary();

    //==========================================================================
    // バイナリ読み取り開始
    //==========================================================================
    std::ifstream & BeginFstreamReadingBinary(const std::string & filename);

    //==========================================================================
    // バイナリ読み取り終了
    //==========================================================================
    void EndFstreamReadingBinary();

    //==========================================================================
    // アクセスキーの登録
    //==========================================================================
    void SetAccessKey(std::ofstream & ofstream, const std::string & manager_object_name, const std::string & class_name, const std::string & component_name, int id);

    //==========================================================================
    // transformを操作する処理(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void TransformOperation(wrapper::vector<_Ty>* obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                TransformOperation(dynamic_cast<transform::Transform*>(obj->Get(i)));
    }

    //==========================================================================
    // transformを操作する処理(テンプレート)
    //==========================================================================
    template<typename _Ty>
    void TransformOperation(wrapper::vector_component<_Ty>* obj)
    {
        if (obj != nullptr)
            for (int i = 0; i < obj->Size(); i++)
                TransformOperation(dynamic_cast<transform::Transform*>(obj->Get(i)));
    }

    //==========================================================================
    // transformを操作する処理
    //==========================================================================
    void TransformOperation(transform::Transform*obj);
    
    //==========================================================================
    // 表示用ウィンドウUIのフラグテンプレート
    //==========================================================================
    constexpr ImGuiWindowFlags SystemWindowFlags =
        ImGuiWindowFlags_NoTitleBar |
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_AlwaysAutoResize |
        ImGuiWindowFlags_NoSavedSettings |
        ImGuiWindowFlags_NoCollapse;

}

_MSLIB_END