//==========================================================================
// テキスト表示[Text.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : Text
// Content: テキストクラス 
//
//==========================================================================
class Text : public component::Component
{
private:
    // コピー禁止 (C++11)
    Text(const Text &) = delete;
    Text &operator=(const Text &) = delete;
    Text &operator=(Text&&) = delete;
public:
    Text(LPDIRECT3DDEVICE9 pDevice);
    ~Text();

    /**
    @brief 初期化 フォントの種類はデフォルトで Terminal になってます。
    @param w [in] 画面の横幅
    @param h [in] 画面の高さ
    */
    void Init(int w, int h);

    /**
    @brief 初期化 フォントの種類はデフォルトで Terminal になってます。
    @param w [in] 画面の横幅
    @param h [in] 画面の高さ
    @param font [in] フォント
    */
	void Init(int w, int h, const char * font);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 色
    @param r [in] 色
    @param g [in] 色
    @param b [in] 色
    @param a [in] 色
    */
    void color(int r, int g, int b, int a);

    /**
    @brief 色
    @param input [in] {r,g,b,a}
    */
    void color(const intColor & input);

    /**
    @brief 座標
    @param x [in] X座標
    @param y [in] Y座標
    */
    void pos(int x, int y);

    /**
    @brief テキスト表示
    @param input [in] 表示内容
    */
	void text(const char* input, ...);

    /**
    @brief テキストの始まりに必ずセットしよう。
    */
    void textnew(void);

    /**
    @brief テキストの終わりに必ずセットしよう。
    */
    void textend(void);

    /**
    @brief 文字のサイズ
    @param input [in] フォントサイズ
    */
    void size(int input);
private:
    LPDIRECT3DDEVICE9 m_Device; // デバイス
    intColor m_color; // 色
    intTexvec m_pos; // 座標
	LPD3DXFONT m_font; // フォント
	int m_fontheight; // 文字の高さ
	int m_fontheight_master; // 文字の高さ
	bool m_Lock; // 描画のロック
};

_MSLIB_END