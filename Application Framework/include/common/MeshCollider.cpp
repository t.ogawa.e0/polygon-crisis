//==========================================================================
// メッシュとプリミティブの間の衝突判定 [MeshCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "MeshCollider.h"

_MSLIB_BEGIN

namespace collider
{
    constexpr const char * __vertex_c__ = "vertex";
    MeshCollider::MeshCollider()
    {
        SetComponentName("MeshCollider");
    }

    MeshCollider::~MeshCollider()
    {
    }

    void MeshCollider::Init()
    {
    }

    void MeshCollider::Update()
    {
        auto * target = GetWarrantyParent<transform::Transform>();

        if (target == nullptr || !m_MeshField.Existence())return;

        m_hitposition = D3DXVECTOR3(0, -1000000000.0f, 0);
        m_collision = false;

        auto * area = Search();

        if (area != nullptr)
        {
            auto & vector = m_MeshField.FieldData();
            auto & Position = *target->GetPosition();

            // エリア内を探索
            for (int iZ = area->Get(0)->z; iZ <= area->Get(3)->z - 1; iZ++)
            {
                // 三角形片面
                // 左下
                for (int iX = area->Get(0)->x; iX <= area->Get(3)->x - 1; iX++)
                {
                    // 1 - 0
                    auto v01 = vector.Get(iZ)->Get(iX)->vertex.pos - vector.Get(iZ + 1)->Get(iX)->vertex.pos;
                    // 3 - 1
                    auto v12 = vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos - vector.Get(iZ)->Get(iX)->vertex.pos;
                    // 0 - 3
                    auto v20 = vector.Get(iZ + 1)->Get(iX)->vertex.pos - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;
                    // 0
                    auto v0p = Position - vector.Get(iZ + 1)->Get(iX)->vertex.pos;
                    // 1
                    auto v1p = Position - vector.Get(iZ)->Get(iX)->vertex.pos;
                    // 3
                    auto v2p = Position - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;

                    float c0 = v01.x*v0p.z - v01.z*v0p.x;
                    float c1 = v12.x*v1p.z - v12.z*v1p.x;
                    float c2 = v20.x*v2p.z - v20.z*v2p.x;

                    if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
                    {
                        auto N = D3DXVECTOR3(0, 0, 0);
                        auto &vpos = vector.Get(iZ)->Get(iX)->vertex.pos;

                        D3DXVec3Cross(&N, &v01, &v12);
                        m_hitposition = Position;
                        m_hitposition.y = vpos.y - (N.x * (Position.x - vpos.x) + N.z * (Position.z - vpos.z)) / N.y;

                        if (Position.y < m_hitposition.y) 
                        {
                            target->SetPosition(m_hitposition);
                            m_collision = true;
                        }

                        return;
                    }
                }

                // 三角形片面
                // 右上
                for (int iX = area->Get(0)->x; iX <= area->Get(3)->x - 1; iX++)
                {
                    // 2 - 1
                    auto v01 = vector.Get(iZ)->Get(iX + 1)->vertex.pos - vector.Get(iZ)->Get(iX)->vertex.pos;
                    // 3 - 2
                    auto v12 = vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos - vector.Get(iZ)->Get(iX + 1)->vertex.pos;
                    // 1 - 3
                    auto v20 = vector.Get(iZ)->Get(iX)->vertex.pos - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;
                    // 1
                    auto v0p = Position - vector.Get(iZ)->Get(iX)->vertex.pos;
                    // 2
                    auto v1p = Position - vector.Get(iZ)->Get(iX + 1)->vertex.pos;
                    // 3
                    auto v2p = Position - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;

                    float c0 = v01.x*v0p.z - v01.z*v0p.x;
                    float c1 = v12.x*v1p.z - v12.z*v1p.x;
                    float c2 = v20.x*v2p.z - v20.z*v2p.x;

                    if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
                    {
                        auto N = D3DXVECTOR3(0, 0, 0);
                        auto vpos = vector.Get(iZ)->Get(iX)->vertex.pos;

                        D3DXVec3Cross(&N, &v01, &v12);
                        m_hitposition = Position;
                        m_hitposition.y = vpos.y - (N.x * (Position.x - vpos.x) + N.z * (Position.z - vpos.z)) / N.y;

                        if (Position.y < m_hitposition.y) 
                        {
                            target->SetPosition(m_hitposition);
                            m_collision = true;
                        }

                        return;
                    }
                }
            }
        }
    }

    void MeshCollider::Draw(LPDIRECT3DDEVICE9 device)
    {
        device;
    }

    void MeshCollider::SetFieldData(const meshfield::MeshFieldReference & data)
    {
        m_MeshField = data;
    }

    const D3DXVECTOR3 & MeshCollider::GetHitPosition()const
    {
        return m_hitposition;
    }

    wrapper::vector<meshfield::FieldID>* MeshCollider::Search()
    {
        D3DXVECTOR3 v01, v12, v20, v0p, v1p, v2p;
        float c0, c1, c2;
        // 探索空間の検出
        auto * pArrayID = m_MeshField.ArrayID().Get(m_hitID);
        auto & vector = m_MeshField.FieldData();
        auto * trans = (transform::Transform*)GetParent();
        auto & Position = *trans->GetPosition();
        if (pArrayID != nullptr)
        {
            auto &pdata = *pArrayID;
            //==========================================================================
            // エリア三角形 左下
            v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
            c0 = c1 = c2 = 0.0f;

            // 左上 - 左下
            v01 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
            // 右下 - 左上
            v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 左下 - 右下
            v20 = vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
            // 左下
            v0p = Position - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
            // 左上
            v1p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 右下
            v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

            c0 = v01.x*v0p.z - v01.z*v0p.x;
            c1 = v12.x*v1p.z - v12.z*v1p.x;
            c2 = v20.x*v2p.z - v20.z*v2p.x;

            if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
            {
                return &pdata;
            }

            //==========================================================================
            // エリア三角形 右上
            v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
            c0 = c1 = c2 = 0.0f;

            // 右上 - 左上
            v01 = vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 右下 - 右上
            v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
            // 左上 - 右下
            v20 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
            // 左上
            v0p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 右上
            v1p = Position - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
            // 右下
            v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

            c0 = v01.x*v0p.z - v01.z*v0p.x;
            c1 = v12.x*v1p.z - v12.z*v1p.x;
            c2 = v20.x*v2p.z - v20.z*v2p.x;

            if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
            {
                return &pdata;
            }
        }

        // 探索空間の検出
        for (auto &itr : m_MeshField.GroupID())
        {
            pArrayID = m_MeshField.ArrayID().Get(itr);
            if (pArrayID != nullptr)
            {
                auto &pdata = *pArrayID;

                //==========================================================================
                // エリア三角形 左下
                v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
                c0 = c1 = c2 = 0.0f;

                // 左上 - 左下
                v01 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
                // 右下 - 左上
                v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
                // 左下 - 右下
                v20 = vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
                // 左下
                v0p = Position - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
                // 左上
                v1p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
                // 右下
                v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

                c0 = v01.x*v0p.z - v01.z*v0p.x;
                c1 = v12.x*v1p.z - v12.z*v1p.x;
                c2 = v20.x*v2p.z - v20.z*v2p.x;

                if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
                {
                    m_hitID = itr;
                    return &pdata;
                }

                //==========================================================================
                // エリア三角形 右上
                v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
                c0 = c1 = c2 = 0.0f;

                // 右上 - 左上
                v01 = vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
                // 右下 - 右上
                v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
                // 左上 - 右下
                v20 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
                // 左上
                v0p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
                // 右上
                v1p = Position - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
                // 右下
                v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

                c0 = v01.x*v0p.z - v01.z*v0p.x;
                c1 = v12.x*v1p.z - v12.z*v1p.x;
                c2 = v20.x*v2p.z - v20.z*v2p.x;

                if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
                {
                    m_hitID = itr;
                    return &pdata;
                }
            }
        }
        return nullptr;
    }
}

_MSLIB_END