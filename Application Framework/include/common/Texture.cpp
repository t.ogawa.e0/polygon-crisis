//==========================================================================
// テクスチャ[Texture.xpp]
// author: tatsuya ogawa
//==========================================================================
#include "Texture.h"

_MSLIB_BEGIN

namespace texture
{
    constexpr char * NotTexture = "Not Texture";

    //==========================================================================
    //
    // class  : TextureData
    // Content: テクスチャデータ
    //
    //==========================================================================
    TextureData::TextureData()
    {
        ratio = 1.0f;
        WindowsSize = int2();
        info = D3DXIMAGE_INFO();
        asset = nullptr;
        createID = (int64_t)0;
        LoaderPtr = nullptr;
    }
    TextureData::~TextureData()
    {
        if (GetRef() == 0)
        {
            if (asset != nullptr)
            {
                asset->Release();
                asset = nullptr;
            }
            LoaderPtr = nullptr;
            tag.c_str();
        }
    }

    //==========================================================================
    //
    // class  : TextureReference
    // Content: 参照用
    //
    //==========================================================================
    TextureReference::TextureReference()
    {
        m_data = nullptr;
    }

    TextureReference::~TextureReference()
    {
        Release();
    }

    void TextureReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->LoaderPtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->LoaderPtr->Unload(m_data);
        m_data = nullptr;
    }

    const D3DXIMAGE_INFO & TextureReference::Info()
    {
        return m_data->info;
    }

    const float TextureReference::Ratio()
    {
        return m_data->ratio;
    }

    const int2 & TextureReference::WindowsSize()
    {
        return m_data->WindowsSize;
    }

    const std::string & TextureReference::Tag()
    {
        return m_data->tag;
    }

    //==========================================================================
    //
    // class  : TextureLoader
    // Content: テクスチャ読み込み破棄クラス
    //
    //==========================================================================
    TextureLoader::TextureLoader()
    {
        m_ratio = 1.0f;
        m_WindowsSize = 0;
        m_device = nullptr;
        m_hwnd = nullptr;
        createIDCount = (int64_t)0;
        SetComponentName("TextureLoader");
    }
    TextureLoader::TextureLoader(LPDIRECT3DDEVICE9 device, HWND hWnd, const int2 & winsize, float ratio)
    {
        m_ratio = ratio;
        m_WindowsSize = winsize;
        m_device = device;
        m_hwnd = hWnd;
        createIDCount = (int64_t)0;
        SetComponentName("TextureLoader");
    }
    TextureLoader::~TextureLoader()
    {
        AllDestroyComponent();
        for (auto &itr : m_data)
        {
            if (itr.second.asset != nullptr)
            {
                itr.second.asset->Release();
                itr.second.asset = nullptr;
            }
        }
        m_data.clear();
    }

    //==========================================================================
    /**
    @brief テクスチャを読み込む。
    @param path [in] 読み込み元パス
    @return テクスチャのポインタ
    */
    TextureReference TextureLoader::Load(const std::string & path)
    {
        auto itr = m_data.find(path);

        // 存在しない場合は読み込み処理を開始する
        if (itr == m_data.end())
        {
            auto &data = m_data[path];

            // 読み込み
            Load(path, &data);
        }
        // データが存在する
        else if (itr != m_data.end())
        {
            // テクスチャが存在しない
            if (itr->second.asset == nullptr)
            {
                // 読み込み
                Load(path, &itr->second);
            }
        }

        return &m_data[path];
    }

    //==========================================================================
    /**
    @brief テクスチャを生成する
    @param Width [in], Height [in], Depth [in], Size [in] size in pixels. these must be non-zero
    @param MipLevels [in] number of mip levels desired. if zero or D3DX_DEFAULT, a complete mipmap chain will be created.
    @param Usage [in] Texture usage flags
    @param Format [in] Pixel format.
    @param Pool [in] Memory pool to be used to create texture
    @return テクスチャのポインタ
    */
    TextureReference TextureLoader::CreateTexture(UINT Width, UINT Height, UINT MipLevels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool)
    {
        createIDCount++; // 生成カウンタを回す

        auto name = std::to_string(createIDCount);
        auto &data = m_data[name];

        D3DXCreateTexture(m_device, Width, Height, MipLevels, Usage, Format, Pool, &data.asset);

        data.tag = name;
        data.createID = createIDCount;
        data.LoaderPtr = this;
        data.WindowsSize = m_WindowsSize;
        data.info.Width = Width;
        data.info.Height = Height;
        data.info.Depth = (UINT)0;
        data.info.MipLevels = MipLevels;
        data.info.Format = Format;
        data.info.ResourceType = data.asset->GetType();
        data.info.ImageFileFormat = D3DXIFF_FORCE_DWORD;

        return &data;
    }

    //==========================================================================
    /**
    @brief テクスチャを破棄する。
    @param data [in] テクスチャ
    */
    void TextureLoader::Unload(TextureData * data)
    {
        if (data == nullptr)return;
        if (data->LoaderPtr != this)return;
        auto itr = m_data.find(data->tag);
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }

    //==========================================================================
    /**
    @brief テクスチャを破棄する。
    @param data [in] テクスチャリスト
    */
    void TextureLoader::Unload(TextureData & data)
    {
        if (data.LoaderPtr != this)return;
        auto itr = m_data.find(data.tag);
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }

    //==========================================================================
    /**
    @brief テクスチャの読み込み機能の本体
    @param path [in] 読み込みパス
    @param Out [out] 受け取り先
    */
    void TextureLoader::Load(const std::string & path, TextureData * Out)
    {
        // 読み込みに失敗していない
        if (!FAILED(D3DXCreateTextureFromFile(m_device, path.c_str(), &Out->asset)))
        {
            // 画像データの格納
            D3DXGetImageInfoFromFile(path.c_str(), &Out->info);
            Out->tag = path;
        }
        // 読み込みに失敗時
        else
        {
            Out->asset = nullptr;
            Out->tag = NotTexture;
            Out->info.Width = (UINT)0;
            Out->info.Height = (UINT)0;
            Out->info.Depth = (UINT)0;
            Out->info.MipLevels = (UINT)0;
            Out->info.Format = D3DFMT_FORCE_DWORD;
            Out->info.ResourceType = D3DRTYPE_FORCE_DWORD;
            Out->info.ImageFileFormat = D3DXIFF_FORCE_DWORD;

            // 失敗対象が空以外の時に実行
            if (path != "")
            {
                std::string text = "テクスチャが存在しません \n %s";
                size_t size = snprintf(nullptr, 0, text.c_str(), path.c_str()) + 1; // Extra space for '\0'
                std::unique_ptr<char[]> buf(new char[size]);
                snprintf(buf.get(), size, text.c_str(), path.c_str());
                MessageBox(m_hwnd, std::string(buf.get(), buf.get() + size - 1).c_str(), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
            }
        }
        createIDCount++; // 生成カウンタを回す
        Out->createID = createIDCount;
        Out->LoaderPtr = this;
        Out->WindowsSize = m_WindowsSize;
        Out->ratio = m_ratio;
    }

    //==========================================================================
    //
    // class  : SetTexture
    // Content: テクスチャ登録クラス
    //
    //==========================================================================
    SetTexture::SetTexture() {}
    SetTexture::~SetTexture() {}

    //==========================================================================
    /**
    @brief テクスチャデータの登録
    @param TextureData [in] データ
    */
    void SetTexture::SetTextureData(const TextureReference & TextureData)
    {
        m_TextureData = TextureData;
    }

    //==========================================================================
    /**
    @brief テクスチャデータの取得
    @return テクスチャのポインタ
    */
    TextureReference & SetTexture::GetTextureData()
    {
        return m_TextureData;
    }
}

_MSLIB_END