//==========================================================================
// カメラ[Camera.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "Component.h"
#include "Transform.h"
#include "Activity.h"
#include "Texture.h"

_MSLIB_BEGIN

namespace camera
{
    using transform::Look;
    using transform::Vector;

    //==========================================================================
    //
    // class  : Camera
    // Content: カメラ
    //
    //==========================================================================
    class Camera : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        Camera &operator=(Camera&&) = delete;
    public:
        Camera();
        Camera(const Camera& obj);
        ~Camera();

        /**
        @brief 初期化
        */
        void Init(void);

        /**
        @brief 初期化
        @param pEye [in] 注視点
        @param pAt [in] 座標
        */
        void Init(const D3DXVECTOR3 & pEye, const D3DXVECTOR3 & pAt);

        /**
        @brief カメラが写す全ての物をテクスチャに反映します
        @param win_size [in] {x,y}ウィンドウサイズ
        @param pDevice [in] デバイス
        */
        static void Rendering(const int2 & win_size, LPDIRECT3DDEVICE9 device);

        /**
        @brief 更新
        @param win_size [in] {x,y}ウィンドウサイズ
        @param pDevice [in] デバイス
        */
        static void UpdateAll(const int2 & win_size, LPDIRECT3DDEVICE9 device);

        /**
        @brief 現在メインで使用されているカメラのポインタを取得します
        @return カメラポインタ
        */
        static Camera * GetCamera();

        /**
        @brief ビュー行列の取得
        @return ビュー行列
        */
        D3DXMATRIX * GetViewMatrix();

        /**
        @brief プロジェクション行列の取得
        @return プロジェクション行列
        */
        D3DXMATRIX * GetProjectionMatrix();

        void AddViewRotation(const D3DXVECTOR3 & axis);

        void AddCameraRotation(const D3DXVECTOR3 & axis);

        void AddViewRotation(float x, float y, float z);

        void AddCameraRotation(float x, float y, float z);

        void AddViewRotationY(float y);

        void AddCameraRotationY(float y);

        void AddViewRotationY(float y, float limit);

        void AddCameraRotationY(float y, float limit);

        void AddPosition(const D3DXVECTOR3 & position);

        void AddPosition(float x, float y, float z);

        /**
        @brief 視点変更
        @param Distance [in] 入れた値が加算されます
        @return 視点とカメラの距離
        */
        float DistanceFromView(float Distance);

        /**
        @brief カメラY軸回転情報
        @return 内積
        */
        float GetRestrictionY(void);

        /**
        @brief カメラX軸回転情報
        @return 内積
        */
        float GetRestrictionX(void);

        /**
        @brief カメラZ軸回転情報
        @return 内積
        */
        float GetRestrictionZ(void);

        /**
        @brief カメラ座標をセット
        @param Eye [in] 注視点
        @param At [in] カメラ座標
        @param Up [in] ベクター
        */
        void SetCameraPos(const D3DXVECTOR3 & Eye, const D3DXVECTOR3 & At, const D3DXVECTOR3 & Up);

        /**
        @brief カメラ座標
        @param At [in] カメラ座標
        */
        void SetAt(const D3DXVECTOR3 & At);

        /**
        @brief 注視点
        @param Eye [in] 注視点
        */
        void SetEye(const D3DXVECTOR3 & Eye);



        const Look GetLook1() const;
        const Look GetLook2() const;
        const Vector GetVector() const;

        /**
        @brief メインカメラにします
        */
        void IsMainCamera();

        Camera &operator =(const Camera & obj);

        /**
        @brief カメラが写したテクスチャを取得します
        @return TextureReference
        */
        texture::TextureReference & GetScreenImg();
    private:
        /**
        @brief ビュー行列生成
        @return ビュー行列
        */
        D3DXMATRIX *CreateView(void);

        /**
        @brief 視点変更
        @param pVec [in] ベクトル
        @param pOut1 [out] 出力1
        @param pOut2 [out] 出力2
        @param pSpeed [in] 移動速度
        @return 戻り値は 距離
        */
        float ViewPos(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

        /**
        @brief 内積
        @param pRot [in] 回転情報
        @param pRang [in] 回転速度
        @return 移動可能範囲なら true
        */
        bool Restriction(float axis, float limit);
    private:
        texture::TextureReference m_BlurTexture; // 画面
        LPDIRECT3DSURFACE9 m_BlurSurface = nullptr; // テクスチャ1
        D3DXMATRIX m_ProjectionMatrix; // プロジェクション行列
        D3DXMATRIX m_ViewMatrix; // ビュー行列
        Look m_Look1; // 視線ベクトル
        Look m_Look2; // 視線ベクトル
        Vector m_Vector; // 向きベクトル
        static std::list<Camera*> m_AllCamera; // 全てのカメラ
        static Camera * m_MainCamera; // メインのカメラ
    };


}
namespace camera_develop
{
    class Camera : public component::Component
    {
    public:
        Camera();
        ~Camera();
        transform::Transform & CameraPosition();
        transform::Transform & ViewingPosition();
        static void UpdateAll(const int2 & win_size, LPDIRECT3DDEVICE9 device);
        void IsMainCamera();
        D3DXMATRIX * GetViewMatrix();
        D3DXMATRIX * GetProjectionMatrix();
        void LockOn(transform::Transform * obj);
    protected:
        D3DXMATRIX * CreateView();
    protected:
        D3DXMATRIX m_ProjectionMatrix; // プロジェクション行列
        D3DXMATRIX m_ViewMatrix; // ビュー行列
        transform::Transform * m_target; // 追従対象
        transform::Transform * m_CameraPosition; // カメラ
        transform::Transform * m_ViewingPosition; // 表示方向
        static std::list<Camera*> m_AllCamera; // 全カメラ
        static Camera * m_MainCamera; // メインのカメラ
    };
}
_MSLIB_END