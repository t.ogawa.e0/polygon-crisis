//==========================================================================
// メッシュとプリミティブの間の衝突判定 [MeshCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include "mslib.hpp"
#include "Collider.h"
#include "MeshField.h"

_MSLIB_BEGIN

namespace collider
{
    //==========================================================================
    //
    // class  : MeshCollider
    // Content: メッシュとプリミティブの間の衝突判定
    //
    //==========================================================================
    class MeshCollider : public Collider
    {
    public:
        MeshCollider();
        ~MeshCollider();

        /**
        @brief 初期化
        */
        void Init()override;

        /**
        @brief 更新
        */
        void Update()override;

        /**
        @brief 描画
        */
        void Draw(LPDIRECT3DDEVICE9 device)override;

        /**
        @brief フィールドの登録
        @param data [in] データ
        */
        void SetFieldData(const meshfield::MeshFieldReference & data);

        /**
        @brief 判定位置
        */
        const D3DXVECTOR3 & GetHitPosition()const;
    protected:
        /**
        @brief 検索
        @return area
        */
        wrapper::vector<meshfield::FieldID> * Search();
    protected:
        int m_hitID; // 当たったエリア
        meshfield::MeshFieldReference m_MeshField; // 現在当たっているフィールド
        D3DXVECTOR3 m_hitposition;
    };
}

_MSLIB_END