//==========================================================================
// ダイレクトインプットのマウス[DinputMouse.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "Dinput.h"

_MSLIB_BEGIN

namespace dinput_mouse
{
    class Speed
    {
    public:
        Speed();
        ~Speed();
    public:
        LONG m_lX; // X軸への速度
        LONG m_lY; // Y軸への速度
        LONG m_lZ; // Z軸への速度
    };

    // 有効ボタン
    enum class Button
    {
        Left = 0,	// 左クリック
        Right,	// 右クリック
        Wheel,	// ホイール
        MAX,
    };

    //==========================================================================
    //
    // class  : Mouse
    // Content: マウス
    //
    //==========================================================================
    class Mouse : public dinput::Dinput, public dinput::Input
    {
    private:
        // コピー禁止 (C++11)
        Mouse(const Mouse &) = delete;
        Mouse &operator=(const Mouse &) = delete;
        Mouse &operator=(Mouse&&) = delete;
    public:
        Mouse();
        ~Mouse();

        /**
        @brief 初期化
        @param hInstance [in] インスタンスハンドル
        @param hWnd [in] ウィンドウハンドル
        @return 失敗時 true が返ります
        */
        bool Init(HINSTANCE hInstance, HWND hWnd);

        /**
        @brief 更新
        */
        void Update(void)override;

        /**
        @brief Button Cast(int)
        @param cast [in] キャスト対象
        @return キャストされた値が返ります
        */
        Button KeyCast(int cast);

        /**
        @brief プレス
        @param key [in] 入力キー
        @return 入力されている場合 true が返ります
        */
        bool Press(Button key);

        /**
        @brief トリガー
        @param key [in] 入力キー
        @return 入力されている場合 true が返ります
        */
        bool Trigger(Button key);

        /**
        @brief リピート
        @param key [in] 入力キー
        @return 入力されている場合 true が返ります
        */
        bool Repeat(Button key);

        /**
        @brief リリース
        @param key [in] 入力キー
        @return 入力されている場合 true が返ります
        */
        bool Release(Button key);

        /**
        @brief マウスの速度
        @return マウスの移動速度が返ります
        */
        Speed speed(void);

        /**
        @brief カーソルの座標
        @return 画面内座標が返ります
        */
        POINT WIN32Cursor(void);

        /**
        @brief 左クリック
        @return 入力されている場合値が返ります
        */
        SHORT WIN32LeftClick(void);

        /**
        @brief 右クリック
        @return 入力されている場合値が返ります
        */
        SHORT WIN32RightClick(void);

        /**
        @brief マウスホイールのホールド判定
        @return 入力されている場合値が返ります
        */
        SHORT WIN32WheelHold(void);
    private:
        static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext);
    private: // DirectInput
        DIMOUSESTATE2 m_State; // 入力情報ワーク
        BYTE m_StateTrigger[(int)sizeof(DIMOUSESTATE2::rgbButtons)];	// トリガー情報ワーク
        BYTE m_StateRelease[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リリース情報ワーク
        BYTE m_StateRepeat[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピート情報ワーク
        int m_StateRepeatCnt[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピートカウンタ
    private: // WindowsAPI
        POINT m_mousePos;
        HWND m_hWnd;
    };
}
_MSLIB_END