//==========================================================================
// パーピクセル・ライティング [PerPixelLighting.fx]
// author: tatsuya ogawa
//==========================================================================

//==========================================================================
// グローバル変数
//==========================================================================
float4x4 World; // ワールド変換行列
float4x4 View; // カメラ変換行列
float4x4 Projection; // プロジェクション変換行列

bool TexFlag; // テクスチャのありなし false:なし true:あり
float4 CameraPos; // カメラ位置

// 光
float4 Diffuse; // ディフューズ
float4 Ambient; // 環境光
float4 Specular; // スペキュラー
float4 Emmisive; // エミッシブ

float3 LightDir; // 平行光源の方向

// マテリアル
float4 DiffuseMatrix; // ディフューズ光
float4 EmmisiveMatrix; // エミッシブ光
float4 AmbientMatrix; // 環境光
float4 SpecularMatrix; // スペキュラー
float Power; // スペキュラー光のパワー値

texture Texture; // テクスチャ

//==========================================================================
// サンプラー
//==========================================================================
sampler Sampler = sampler_state
{
    texture = <Texture>;
    MinFilter = LINEAR; // リニアフィルタ（縮小時）
    MagFilter = LINEAR; // リニアフィルタ（拡大時）
};

struct VS_Param
{
    float4 pos : POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : TEXCOORD1;
    float3 posforps : TEXCOORD2;
};

//==========================================================================
// 頂点シェーダ
//==========================================================================
VS_Param main(
in float3 in_pos : POSITION,
in float3 in_normal : NORMAL,
in float2 in_tex1 : TEXCOORD0)
{
    VS_Param Out;

	// 座標変換
    Out.pos = mul(float4(in_pos, 1.0f), World);
    Out.posforps = Out.pos.xyz;
    Out.pos = mul(Out.pos, View);
    Out.pos = mul(Out.pos, Projection);

    // テクスチャ座標をそのまま出力する
    Out.tex = in_tex1;

    // 法線をワールド空間上のベクトルに変換して、単位ベクトル化
    Out.normal = normalize(mul(in_normal, (float3x3) World));

    return Out;
}

//==========================================================================
// ピクセルシェーダ
//==========================================================================
float4 psPerPixelLighting(in VS_Param In) : COLOR0
{
    float3 N; // ワールド空間上の法線ベクトル
    float3 L; // 光の差し込む方向	
    float3 P; // 頂点座標
    float3 V; // 視線ベクトル
    float3 H; // ハーフベクトル
    float4 Out;

    P = In.posforps;
    N = normalize(In.normal);

	// 平行光の差し込む方向	単位ベクトル化
    L = normalize(-LightDir);

    // 視線ベクトルを求める
    V = CameraPos.xyz - P; // カメラ座標-頂点座標

    // 正規化
    V = normalize(V);

    // ハーフベクトルを求める
    H = normalize(V + L);

    // ディフューズカラーを決める
    float4 diffuse = Ambient * AmbientMatrix + Diffuse * DiffuseMatrix * max(0.0, dot(L, N));

    // スペキュラー光による色を計算
    float4 specular = Specular * SpecularMatrix * pow(max(0, dot(N, H)), Power);

    if (TexFlag)
    {
        float4 tex_color = tex2D(Sampler, In.tex);

		// テクスチャの色と頂点の色を掛け合わせる
        Out = saturate(diffuse * tex_color + specular);
    }
    else
    {
        Out = saturate(diffuse + specular);
    }

    return Out;
}

//==========================================================================
// シェーダーのパッケージ
//==========================================================================
technique PerPixelLighting
{
    pass P0
    {
        // 使用するシェーダーを渡す
        VertexShader = compile vs_3_0 main();
        PixelShader = compile ps_3_0 psPerPixelLighting();
        
        // Effect States
        AlphaBlendEnable = false;
        SrcBlend = SRCALPHA;
        DestBlend = INVSRCALPHA;
    }
}
