//==========================================================================
// エネミーの共通 [CommonEnemy.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : EnemyData
	// Content: エネミーの共通データ
	//
	//==========================================================================
	class EnemyData
	{
	public:
		EnemyData();
		EnemyData(mslib::transform::Transform *o, mslib::transform::Transform*m);
		EnemyData(mslib::transform::Transform *o, mslib::transform::Transform*m, mslib::collider::DistanceCollider *d, void*a, void*c);
		virtual ~EnemyData();
	public:
		mslib::transform::Transform * obj = nullptr; // オブジェクト
		mslib::transform::Transform * model = nullptr; // モデル
		void *ai = nullptr; // AI
		void *character = nullptr; // キャラクターデータ
		mslib::collider::DistanceCollider*distance = nullptr; // 距離のコリジョン
		float repulsion = 0.0f; // 反発力
	};
}
