//==========================================================================
// トーチカ [Torchka.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Torchka.h"
#include "TorchkaAI.h"
#include "Asset/TPSCamera.h"
#include "Asset/LightObject.h"
#include "Asset/ControllerObject.h"
#include "Asset/ShaderModel.h"
#include "Asset/Character.h"
#include "Asset/ActionRange.h"
#include "Enemy/EnemyManager.h"
#include "resource_list.h"

namespace enemy
{
	std::list<TorchkaData*> TorchkaData::m_data;

    Torchka::Torchka() : ObjectManager("TorchkaObject")
    {
        m_Init_Call = 0;
        m_NumMaxEnemy = 0;
        m_CreatePoint = 0;
        m_NumEnemy = 0;
        m_init = false;
        m_bCommonObjectActivity = false;
        m_CommonBox = nullptr;
        m_CommonEnemyBox = nullptr;
        m_CommonTargetObject = nullptr;
        m_Common_attack_object_position = nullptr;
		AddActivity();
    }

    Torchka::Torchka(int NumMaxEnemy, int CreatePoint) : ObjectManager("TorchkaObject")
    {
        m_Init_Call = 0;
        m_NumMaxEnemy = NumMaxEnemy;
        m_CreatePoint = CreatePoint;
        m_NumEnemy = 0;
        m_init = false;
        m_bCommonObjectActivity = false;
        m_GeneratedCoordinates.reserve(CreatePoint);
        m_CommonBox = nullptr;
        m_CommonEnemyBox = nullptr;
        m_CommonTargetObject = nullptr;
        m_Common_attack_object_position = nullptr;
		AddActivity();
    }

    Torchka::~Torchka()
    {
    }
    void Torchka::Init()
    {
        m_LightObject = GetParent()->GetComponent<common::LightObject>()->Light()->Get(0);
        m_CameraObject = GetParent()->GetComponent<common::TPSCamera>()->Camera()->Get(0);

        auto model_data = GetXModelLoader()->Load(RESOURCE_Torchka_x);
        auto efect_data = GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx);

        // Torchkaの統括
        auto TorchkaComponent = Object()->Create();
        TorchkaComponent->SetComponentName("TorchkaComponent");

        // 親
        auto object = TorchkaComponent->AddComponent<mslib::object::Object>();
        object->SetComponentName("TorchkaCommonObject");
        object->SetMatrixType(mslib::object::MatrixType::Local);
        m_CommonBox = object->AddComponent<mslib::collider::BoxCollider>();
        m_CommonEnemyBox = object->AddComponent<mslib::collider::BoxCollider>();
        m_CommonTargetObject = object->AddComponent<mslib::object::Object>();
        m_Common_attack_object_position = object->AddComponent<mslib::object::Object>();
        m_CommonEnemyBox->SetComponentName("CommonEnemyBox");
        m_CommonTargetObject->SetComponentName("CommonTargetObject");
        m_Common_attack_object_position->SetComponentName("CommonAttackObjectPosition");


        // モデル
        auto model = object->AddComponent<ShaderModel::PerPixelLightingModel>();
        model->SetComponentName("TorchkaModel");
        model->SetXModelData(model_data);
        model->SetEffectData(efect_data);
        model->SetCamera(m_CameraObject->GetCamera());
        model->SetLight(m_LightObject);
        m_CommonObject = EnemyData(object, model);

        // 出現座標の設定
        for (int i = 0; i < m_CreatePoint; i++)
        {
            auto po = TorchkaComponent->AddComponent<mslib::object::Object>();
            po->SetComponentName("GeneratedCoordinates" + std::to_string(i));
            po->SetMatrixType(mslib::object::MatrixType::Local);
            m_GeneratedCoordinates.push_back(po);
        }

        // エネミーを生成します
        for (int i = 0; i < m_NumMaxEnemy; i++)
        {
            auto obj = TorchkaComponent->AddComponent<mslib::object::Object>();
            obj->SetComponentName("Torchka" + std::to_string(i));
            obj->SetMatrixType(mslib::object::MatrixType::Local);

            auto mod = obj->AddComponent<ShaderModel::PerPixelLightingModel>();
            mod->SetComponentName("TorchkaModel");
            mod->SetXModelData(model_data);
            mod->SetEffectData(efect_data);
            mod->SetCamera(m_CameraObject->GetCamera());
            mod->SetLight(m_LightObject);

            auto pEnemyCharacter = obj->AddComponent<common::EnemyCharacter>();
            auto ai = obj->AddComponent<TorchkaAI>();
			auto di = mod->AddComponent<mslib::collider::DistanceCollider>();

			obj->AddComponent<EnemyManager>()->InitAll();
			obj->AddComponent<TorchkaData>(obj, mod, di, ai, pEnemyCharacter);
        }
    }
    void Torchka::Update()
    {
        CommonObjectInit();

        for (auto & itr : TorchkaData::GetAllTorchkaData())
        {
            auto pEnemyCharacter = mslib::cast<common::EnemyCharacter>(itr->character);

            // コリジョン処理
            pEnemyCharacter->Collider();

            // 壁との判定が出た
            if (pEnemyCharacter->GetVersusWallBox()->Trigger())
            {
            }

            // お互いが衝突しあった
            if (itr->distance->Trigger())
            {
            }
        }
    }
    void Torchka::Debug()
    {
        if (ImGui()->Checkbox("CommonObjectActivity", m_bCommonObjectActivity))
        {
            auto common_model = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.model);
            if (common_model != nullptr)
            {
                common_model->SetActivity(m_bCommonObjectActivity);
            }
            auto common_obj = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.obj);
            if (common_obj != nullptr)
            {
                common_obj->SetActivity(m_bCommonObjectActivity);
            }
        }
    }
    void Torchka::CommonObjectInit()
    {
        if (m_init)return;

        m_Init_Call++;
        if (m_Init_Call != 30)return;

        auto common_renderer = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.model);
        if (common_renderer != nullptr)
        {
            common_renderer->SetActivity(false);
        }
        auto common_obj = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.obj);
        if (common_obj != nullptr)
        {
            common_obj->SetActivity(false);
        }

        int id = 0;
        for (auto & itr : TorchkaData::GetAllTorchkaData())
        {
            auto it = m_GeneratedCoordinates[id];
            auto ai = mslib::cast<TorchkaAI>(itr->ai);
            auto pEnemyCharacter = mslib::cast<common::EnemyCharacter>(itr->character);

            auto model = mslib::cast<ShaderModel::PerPixelLightingModel>(itr->model);
            auto common_model = mslib::cast<ShaderModel::PerPixelLightingModel>(m_CommonObject.model);

            model->SetParameter(*common_model->GetParameter());
            model->SetD3DMaterial9(common_model->GetD3DMaterial9());
            itr->obj->SetParameter(*it->GetParameter());
            ai->ResetLoot();
            ai->GetViewingRangeBox()->Copy(m_CommonBox);
            ai->SetAttackObject(m_Common_attack_object_position);
            //pEnemyCharacter->GetEnemyCollider()->Copy(m_CommonEnemyBox);
            pEnemyCharacter->GetTargetObject()->SetParameter(*m_CommonTargetObject->GetParameter());

            id++;
            for (auto & itr2 : TorchkaData::GetAllTorchkaData())
            {
                if (itr2->distance == itr->distance)continue;

                itr->distance->AddCollider(itr2->distance);
                itr->distance->DeterminationDistance(10);
            }
        }

        m_init = true;
    }
	TorchkaData::TorchkaData()
	{
		SetComponentName("TorchkaData");
		m_data.push_back(this);
	}
	TorchkaData::TorchkaData(mslib::transform::Transform * o, mslib::transform::Transform * m) : EnemyData(o, m)
	{
		SetComponentName("TorchkaData");
		m_data.push_back(this);
	}
	TorchkaData::TorchkaData(mslib::transform::Transform * o, mslib::transform::Transform * m, mslib::collider::DistanceCollider * d, void * a, void * c) : EnemyData(o, m, d, a, c)
	{
		SetComponentName("TorchkaData");
		m_data.push_back(this);
	}
	TorchkaData::~TorchkaData()
	{
		auto itr = std::find(m_data.begin(), m_data.end(), this);
		if (itr != m_data.end())
			m_data.erase(itr);
	}
	std::list<TorchkaData*>& TorchkaData::GetAllTorchkaData()
	{
		return m_data;
	}
}