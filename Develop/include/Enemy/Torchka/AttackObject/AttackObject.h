//==========================================================================
// AttackObject [AttackObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/AttackCollision/EnemyAttackObject.h"
#include "Asset/Character.h"

namespace torchka
{
    class AttackObject : public enemy::EnemyAttackObject
    {
    public:
        AttackObject();
        ~AttackObject();
        mslib::object::Effekseer * GetEffekseerObject();
        void SetTarget(mslib::transform::Transform * target, mslib::transform::Transform * character);
        /**
        @brief �X�V
        */
        virtual void Update() override;
    protected:
        mslib::object::Effekseer * m_EffekseerObj;
        mslib::transform::Transform * m_target;
        common::Character * m_Character;
        bool m_flag;
        int m_time;
    };
}