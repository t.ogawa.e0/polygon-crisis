//==========================================================================
// Patrol [Patrol.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Patrol.h"

namespace torchka
{
    constexpr float __rotation_speed = 0.05f;
    constexpr int __max_update_time = 60;
    constexpr int __min_update_time = 10;
    constexpr int Seconds(int value) {
        return 60 * value;
    }
    Patrol::Patrol()
    {
        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd()); // メルセンヌ・ツイスタの32ビット版、引数は初期シード値

        m_mt = mt;
        __rotation_rand = mslib::rand_float(-__rotation_speed, __rotation_speed);
        __update_time_rand = mslib::rand_int(Seconds(__min_update_time), Seconds(__max_update_time));

        m_rotation_speed = D3DXVECTOR3(__rotation_rand(m_mt), 0, 0);
        m_update_time = __update_time_rand(m_mt);
    }

    Patrol::~Patrol()
    {
    }

    void Patrol::Update(mslib::state::State* this_)
    {
        if (m_character == nullptr)
        {
            this_->ReleaseState();
            return;
        }
        
        m_character->AddRotation(m_rotation_speed);

        // 時間消費
        m_update_time--;

        // 0以下ではない
        if (0 < m_update_time)return;

        // 乱数で再設定
        m_rotation_speed = D3DXVECTOR3(__rotation_rand(m_mt), 0, 0);
        m_update_time = __update_time_rand(m_mt);
    }
}