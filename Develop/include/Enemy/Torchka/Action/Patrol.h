//==========================================================================
// Patrol [Patrol.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Enemy/Action/Action.h"

namespace torchka
{
    class Patrol : public action::Action
    {
    public:
        Patrol();
        ~Patrol();
        void Update(mslib::state::State* this_);
    protected:
        mslib::rand_float __rotation_rand; // 乱数
        mslib::rand_int __update_time_rand; // 乱数
        std::mt19937 m_mt; // メルセンヌ・ツイスタの32ビット版
        D3DXVECTOR3 m_rotation_speed; // 回転角度
        int m_update_time; // 処理時間
    };
}
