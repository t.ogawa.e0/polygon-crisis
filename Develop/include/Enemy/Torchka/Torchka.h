//==========================================================================
// トーチカ [Torchka.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/CommonEnemy.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : TorchkaData
	// Content: データ
	//
	//==========================================================================
	class TorchkaData : public mslib::component::Component, public EnemyData
	{
	public:
		TorchkaData();
		TorchkaData(mslib::transform::Transform *o, mslib::transform::Transform*m);
		TorchkaData(mslib::transform::Transform *o, mslib::transform::Transform*m, mslib::collider::DistanceCollider *d, void*a, void*c);
		~TorchkaData();
		static std::list<TorchkaData*> & GetAllTorchkaData();
	private:
		static std::list<TorchkaData*>m_data;
	};

	//==========================================================================
	//
	// class  : Torchka
	// Content: Torchka
	//
	//==========================================================================
	class Torchka : public mslib::ObjectManager
    {
    public:
        Torchka();
        Torchka(int NumMaxEnemy, int CreatePoint);
        ~Torchka();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    protected:
        void CommonObjectInit();
    protected:
        bool m_init;
        bool m_bCommonObjectActivity;
        int m_NumMaxEnemy; // 最大エネミーの数
        int m_CreatePoint; // 生成ポイント
        int m_NumEnemy; // 出現した回数
        mslib::object::Light * m_LightObject;
        mslib::camera::Camera * m_CameraObject;
        std::vector<mslib::transform::Transform*> m_GeneratedCoordinates; // 生成座標
        EnemyData m_CommonObject;
        int m_Init_Call;
        mslib::collider::BoxCollider * m_CommonBox;
        mslib::collider::BoxCollider * m_CommonEnemyBox;
        mslib::transform::Transform * m_CommonTargetObject;
        mslib::transform::Transform * m_Common_attack_object_position; // 攻撃オブジェクトの座標
    };
}