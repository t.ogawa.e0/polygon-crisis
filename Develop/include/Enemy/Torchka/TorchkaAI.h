//==========================================================================
// トーチカAI [TorchkaAI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/CommonAI.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : TorchkaAI
	// Content: AI
	//
	//==========================================================================
	class TorchkaAI : public CommonAI
    {
    public:
        TorchkaAI();
        ~TorchkaAI();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;
    protected:
        void Attack();
        void Patrol();
        void Rotation();
        void Taiki();
    };
}