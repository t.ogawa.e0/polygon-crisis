//==========================================================================
// 共通のAI [CommonAI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "common/FunctionComponent.h"
#include "common/Initializer.h"
#include "common/State.h"

namespace enemy
{
    float Distance(mslib::transform::Transform*a, mslib::transform::Transform*b);

	//==========================================================================
	//
	// class  : DiscoveryPlayer
	// Content: プレイヤー認識用
	//
	//==========================================================================
	struct DiscoveryPlayer
    {
        mslib::collider::Collider * collider = nullptr; // コリジョン
        mslib::transform::Transform * player = nullptr; // プレイヤー
        bool recognition = false; // 認識しているか
        int recognition_time = 0; // 認識時間
    };

	// アクションID
    enum class ActionID : int
    {
        end = -1,
        Taiki, /// 待機
        Attack, /// 攻撃 
        Move, /// 移動
        Rotation, /// 向き変更
        max = Rotation, /// 最大
    };

    // 秒
    constexpr int Seconds(int value);

    // 分
    constexpr int Minute(int value);

    //==========================================================================
    //
    // class  : CommonAI
    // Content: 共通のAI
    //
    //==========================================================================
    class CommonAI : public mslib::function::FunctionComponent, public mslib::initializer::Initializer
    {
    public:
        CommonAI();
        ~CommonAI();

        /**
        @brief 初期化
        */
        virtual void Init() override;

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 行動範囲データの登録
        */
        void SetActionRangeData(mslib::transform::Transform * data);

        /**
        @brief 行動範囲データの登録
        */
        void SetActionRangeData(const std::list<mslib::transform::Transform*> & data);

        /**
        @brief ルート初期化
        */
        void ResetLoot();

        /**
        @brief 座標
        */
        void AddPosition(const D3DXVECTOR3 & pos);

        /**
        @brief 座標
        */
        void AddPosition(float x, float y, float z);

        /**
        @brief 座標
        */
        void AddPosition(float x, float y, float z, D3DXVECTOR3 angle);

        /**
        @brief 発見しているか
        */
        bool GetDiscovery();

        /**
        @brief 視野範囲の取得(Box)
        */
        mslib::collider::BoxCollider * GetViewingRangeBox();

        /**
        @brief 視野範囲の取得(Distance)
        */
        mslib::collider::DistanceCollider * GetViewingRangeDistance();

        /**
        @brief 攻撃トリガー
        */
        bool AttackTrigger();

        /**
        @brief 移動トリガー
        */
        bool MoveTrigger();

        /**
        @brief 認識時間
        */
        bool AcquisitionTime();

        /**
        @brief 向き変更トリガー
        */
        bool ChangeDirectionTrigger();

        /**
        @brief アクション停止
        */
        void StopAction();

		// 攻撃オブジェクトの登録
        void SetAttackObject(mslib::transform::Transform * obj);
    protected:
        /**
        @brief 発見の処理
        @param distance 発見距離
        */
        void Discovery(float distance);

        /**
        @brief 発見の処理Box
        @param distance 発見距離Box
        */
        void DiscoveryBox();

        /**
        @brief 自分に近い順にソートします。
        */
        void Sort();

        /**
        @brief 移動ルート生成
        */
        void CreateLoot(int save_old_num_loot);

        /**
        @brief 索敵
        */
        mslib::transform::Transform * FindEnemies(int save_old_num_loot, float min_distance);

        /**
        @brief 視野範囲のセッティング(Box)
        */
        void InitializerViewingRangeBox();

        /**
        @brief 視野範囲のセッティング(Distance)
        */
        void InitializerViewingRangeDistance();

        /**
        @brief Stateの初期化
        */
        void InitializerState();

        /**
        @brief 相手を選択
        */
        void SelectTarget();

        /**
        @brief アクションを選択
        */
        ActionID SelectAction();

        /**
        @brief アクション時間更新
        */
        void ActionTimeCounter();

        /**
        @brief アクショントリガーの初期化
        */
        void InitActionTrigger();
    private:
        /**
        @brief 認識対象の更新
        */
        void RecognitionUpdate();
    protected:
        std::list<mslib::transform::Transform*> m_ActionRangeData; // 行動範囲データ  
        std::list<mslib::transform::Transform*> m_old_loot; // 通ったルート
        std::list<mslib::transform::Transform*> m_discovery_loot; // 発見時のルート
        std::unordered_map<mslib::transform::Transform*, DiscoveryPlayer> m_discovery_player; // プレイヤー識別
        std::mt19937 m_mt; // メルセンヌ・ツイスタの32ビット版、引数は初期シード値

        mslib::transform::Transform * m_loot; // 移動ルート
        mslib::collider::BoxCollider * m_BoxViewingRange; // Box視野範囲
        mslib::collider::DistanceCollider * m_DistanceViewingRange; // Distance視野範囲
        mslib::transform::Transform * m_target; // ターゲット
        mslib::rand_int m_time_rand; // 時間乱数
        mslib::rand_int m_action_rand; // アクション選択用乱数
        mslib::state::State * m_state; // ステート
        mslib::state::StateBasic * m_move_state; // ステート
        mslib::transform::Transform * m_attack_object_position; // 攻撃オブジェクトの座標

        bool m_discovery; // 発見
        bool m_attack; // 攻撃
        bool m_move; // 移動
        bool m_change_direction; // 向き変更

        ActionID m_action_id; // 行動ID

        int m_action_time; // アクション実行時間
     private:
        int m_break_time; // 休憩時間
    };
}