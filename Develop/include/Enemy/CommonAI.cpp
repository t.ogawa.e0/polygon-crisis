//==========================================================================
// 共通のAI [CommonAI.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CommonAI.h"
#include "Player/PlayerManager.h"

namespace enemy
{
    using namespace mslib::transform;

    constexpr int Seconds(int value) {
        return 60 * value;
    }
    constexpr int Minute(int value) {
        return Seconds(60) * value;
    }

    CommonAI::CommonAI()
    {
        SetComponentName("CommonAI");
        m_loot = nullptr;
        m_discovery = false;
        m_BoxViewingRange = nullptr;
        m_DistanceViewingRange = nullptr;
        m_attack = false;
        m_target = nullptr;
        m_move = false;
        m_change_direction = false;
        m_state = nullptr;
        m_move_state = nullptr;
        m_attack_object_position = nullptr;
        m_action_time = 0;
        m_action_id = ActionID::end;

        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd()); // メルセンヌ・ツイスタの32ビット版、引数は初期シード値

        // 初期シード値の保存
        m_mt = mt;
        m_time_rand = mslib::rand_int(Minute(1), Minute(5));
        m_action_rand = mslib::rand_int((int)ActionID::Taiki, (int)ActionID::max);

        m_break_time = 60 * 2;

		AddActivity();
    }

    CommonAI::~CommonAI()
    {
    }

    void CommonAI::Init()
    {
    }

    void CommonAI::Update()
    {
        auto obj = GetWarrantyParent<Transform>();
        if (!mslib::nullptr_check(obj))return;
    }

    void CommonAI::SetActionRangeData(Transform * data)
    {
        m_ActionRangeData.push_back(data);
    }
    void CommonAI::SetActionRangeData(const std::list<Transform*>& data)
    {
        m_ActionRangeData = data;
    }
    void CommonAI::ResetLoot()
    {
        m_loot = nullptr;
    }
    void CommonAI::AddPosition(const D3DXVECTOR3 & pos)
    {
        AddPosition(pos.x, pos.y, pos.z);
    }
    void CommonAI::AddPosition(float x, float y, float z)
    {
        auto obj = GetWarrantyParent<Transform>();
        if (!mslib::nullptr_check(obj))return;
        if (!mslib::nullptr_check(m_loot))return;

        auto angle = obj->ToSee(*m_loot);
        angle.y = obj->GetVector()->front.y;

        // 各種対象に向きを変える
        obj->GetLook()->eye = -angle;
        obj->GetVector()->front = angle;
        obj->GetVector()->Normalize();
        obj->AddPosition(x, y, z);
    }
    void CommonAI::AddPosition(float x, float y, float z, D3DXVECTOR3 angle)
    {
        auto obj = GetWarrantyParent<Transform>();

        // 各種対象に向きを変える
        obj->GetLook()->eye = -angle;
        obj->GetVector()->front = angle;
        obj->GetVector()->Normalize();
        obj->AddPosition(x, y, z);
    }
    bool CommonAI::GetDiscovery()
    {
        return m_discovery;
    }
    mslib::collider::BoxCollider * CommonAI::GetViewingRangeBox()
    {
        return m_BoxViewingRange;
    }
    mslib::collider::DistanceCollider * CommonAI::GetViewingRangeDistance()
    {
        return m_DistanceViewingRange;
    }
    bool CommonAI::AttackTrigger()
    {
        return m_attack;
    }
    bool CommonAI::MoveTrigger()
    {
        return m_move;
    }
    bool CommonAI::AcquisitionTime()
    {
        // 行動開始までの時間
        m_break_time--;
        if (0 <= m_break_time)return false;
        m_break_time = -1;

        // 認識した対象をチェｋック
        RecognitionUpdate();

        return m_discovery;
    }
    bool CommonAI::ChangeDirectionTrigger()
    {
        return m_change_direction;
    }
    void CommonAI::StopAction()
    {
        m_action_id = ActionID::end;
        m_action_time = 0;

        if (m_state != nullptr)
        {
            m_state->ReleaseState();
        }

    }
    void CommonAI::SetAttackObject(mslib::transform::Transform * obj)
    {
        if (m_attack_object_position != nullptr)return;
        auto pParent = GetWarrantyParent<Transform>();
        if (!mslib::nullptr_check(pParent))return;

        m_attack_object_position = pParent->AddComponent<mslib::object::Object>();
        m_attack_object_position->SetComponentName("AttackObjectCreate");
        m_attack_object_position->SetParameter(*obj->GetParameter());
    }
    void CommonAI::Discovery(float distance)
    {
        if (m_DistanceViewingRange == nullptr)return;
        if (0 <= m_break_time)return;

        auto & player_list = player::PlayerManager::GetAllPlayer();

        m_DistanceViewingRange->DeterminationDistance(distance);

        // プレイヤー検索
        for (auto & itr1 : player_list)
        {
            // ポインタで生成
            auto & itr2 = m_discovery_player[itr1->GetPlayer()];

            // 情報セット
            itr2.player = itr1->GetPlayer();

            // 対コリジョンの実態が存在しない場合
            if (itr2.collider == nullptr)
            {
                auto dis = itr2.player->AddComponent<mslib::collider::DistanceCollider>();
                itr2.collider = dis;
                itr2.collider->InitAll();
                itr2.collider->SetComponentName("VersusDistanceCollider");
                m_DistanceViewingRange->AddCollider(itr2.collider);
                dis->AddCollider(m_DistanceViewingRange);
            }

            mslib::cast<mslib::collider::DistanceCollider>(itr2.collider)->DeterminationDistance(distance);

            // 既に発見済みの場合無効
            if (itr2.recognition)continue;

            // 指定距離内なら発見とする
            if (itr2.collider->Trigger())
            {
                itr2.recognition = true;
                itr2.recognition_time = m_time_rand(m_mt);
            }
        }
    }
    void CommonAI::DiscoveryBox()
    {
        if (m_BoxViewingRange == nullptr)return;
        if (0 <= m_break_time)return;

        auto & player_list = player::PlayerManager::GetAllPlayer();

        // プレイヤー検索
        for (auto & itr1 : player_list)
        {
            // ポインタで生成
            auto & itr2 = m_discovery_player[itr1->GetPlayer()];

            // 情報セット
            itr2.player = itr1->GetPlayer();

            // 対コリジョンの実態が存在しない場合
            if (itr2.collider == nullptr)
            {
                itr2.collider = itr2.player->AddComponent<mslib::collider::Collider>();
                itr2.collider->InitAll();
                itr2.collider->SetComponentName("VersusDiscoveryBox");
                m_BoxViewingRange->AddCollider(itr2.collider);
            }

            // 既に発見済みの場合無効
            if (itr2.recognition)continue;

            // このコリジョンに対して
            if (itr2.collider->Trigger())
            {
                itr2.recognition = true;
                itr2.recognition_time = m_time_rand(m_mt);
            }
        }
    }
    void CommonAI::Sort()
    {
        auto obj = GetWarrantyParent<Transform>();
        if (!mslib::nullptr_check(obj))return;

        // 距離比較
        m_ActionRangeData.sort([&](Transform*a, Transform*b) {
            return Distance(a, obj) < Distance(b, obj); });
    }
    void CommonAI::CreateLoot(int save_old_num_loot)
    {
        auto itr1 = m_ActionRangeData.begin();
        if (itr1 == m_ActionRangeData.end())return;

        m_loot = nullptr;

        // ルートが未確定
        for (; m_loot == nullptr;)
        {
            if (itr1 == m_ActionRangeData.end())
                --itr1;

            // 仮ルート記録
            m_loot = (*itr1);

            // 一番近く、参照ログにないデータを検索
            for (auto itr2 = m_old_loot.begin(); itr2 != m_old_loot.end(); ++itr2)
            {
                // ログに存在する場合
                if ((*itr1) == (*itr2)) {
                    ++itr1;
                    m_loot = nullptr;
                    break;
                }
            }

            if (m_loot != nullptr)
                m_old_loot.push_back(m_loot);
        }

        // 古い参照ログを破棄
        for (; save_old_num_loot < (int)m_old_loot.size();)
            m_old_loot.erase(m_old_loot.begin());
    }
    mslib::transform::Transform * CommonAI::FindEnemies(int save_old_num_loot, float min_distance)
    {
        auto obj = GetWarrantyParent<Transform>();
        if (!mslib::nullptr_check(obj))return m_loot;

        // ルートが未確定の場合
        if (m_loot == nullptr)
        {
            Sort();
            CreateLoot(save_old_num_loot);
        }

        if (m_loot == nullptr)return m_loot;

        // 位置情報が最小距離以下の場合
        if (Distance(obj, m_loot) < min_distance)
        {
            Sort();
            CreateLoot(save_old_num_loot);
        }
        return m_loot;
    }

    void CommonAI::InitializerViewingRangeBox()
    {
        if (m_BoxViewingRange != nullptr)return;
        
        auto obj = GetWarrantyParent<Transform>();
        m_BoxViewingRange = obj->AddComponent<mslib::collider::BoxCollider>();
        m_BoxViewingRange->SetComponentName("BoxViewingRange");
    }

    void CommonAI::InitializerViewingRangeDistance()
    {
        if (m_DistanceViewingRange != nullptr)return;

        auto obj = GetWarrantyParent<Transform>();
        auto tra = obj->AddComponent<Transform>();

        m_DistanceViewingRange = tra->AddComponent<mslib::collider::DistanceCollider>();
        tra->SetComponentName("ViewingRangeDistanceTransform");
        m_DistanceViewingRange->SetComponentName("ViewingRangeDistance");
    }

    void CommonAI::InitializerState()
    {
        if (m_state != nullptr)return;

        auto obj = GetWarrantyParent<Transform>();
        m_state = obj->AddComponent<mslib::state::State>();
        m_state->SetComponentName("StateAI");
    }

    void CommonAI::SelectTarget()
    {
        // 対象が決まっている場合は無効
        if (m_target != nullptr)return;

        std::vector<mslib::transform::Transform*> tra;

        // キャパシティー設定
        tra.reserve((int)m_discovery_player.size());

        // ターゲット検索
        for (auto & itr : m_discovery_player)
        {
            // 判定が出ていない
            if (!itr.second.recognition)continue;

            // 判定の出たデータを渡す
            tra.push_back(itr.second.player);
        }

        // 対象が存在しない場合終了
        if (tra.size() == 0)return;

        // 乱数生成
        mslib::rand_int __rand = mslib::rand_int(0, (int)tra.size());

        // ターゲット選択
        for (;;)
        {
            // 乱数
            int i = __rand(m_mt);

            // サイズとデータが同じ
            if ((int)tra.size() == i)continue;

            m_target = tra[i];
            break;
        }
    }

    ActionID CommonAI::SelectAction()
    {
        if (m_action_id == ActionID::end)
        {
            m_action_id = (ActionID)m_action_rand(m_mt);
        }
        return m_action_id;
    }

    void CommonAI::ActionTimeCounter()
    {
        if (m_action_id == ActionID::end)return;

        // 実行可能
        if (0 < m_action_time)
        {
            m_action_time--;
        }

        // 実行不可
        if (m_action_time <= 0)
        {
            m_action_id = ActionID::end;
            m_action_time = 0;
        }
    }

    void CommonAI::InitActionTrigger()
    {
        m_discovery = false;
        m_attack = false;
        m_move = false;
        m_change_direction = false;
    }

    void CommonAI::RecognitionUpdate()
    {
        // 見ている対象が存在するかのチェック
        m_discovery = false;
        for (auto & itr : m_discovery_player)
        {
            // 認識されていない場合スルー
            if (!itr.second.recognition)continue;

            // 誰かを発見
            m_discovery = true;

            // 時間減少
            itr.second.recognition_time--;

            // 認識可能時間が0ではない場合スルー
            if (0 != itr.second.recognition_time)continue;

            // 初期化
            itr.second.recognition_time = 0;
            itr.second.recognition = false;
        }
    }

    float Distance(Transform * a, Transform * b)
    {
        if (a == nullptr || b == nullptr)return 0.0f;

        auto * MatrixA = a->GetWorldMatrix();
        auto * MatrixB = b->GetWorldMatrix();

        return
            ((MatrixB->_41) - (MatrixA->_41))*
            ((MatrixB->_41) - (MatrixA->_41)) +
            ((MatrixA->_42) - (MatrixA->_42))*
            ((MatrixA->_42) - (MatrixA->_42)) +
            ((MatrixB->_43) - (MatrixA->_43))*
            ((MatrixB->_43) - (MatrixA->_43));
    }
}