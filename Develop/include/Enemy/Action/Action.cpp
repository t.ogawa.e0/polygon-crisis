//==========================================================================
// Action [Action.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Action.h"

namespace action
{
    using namespace mslib::transform;

    Action::Action()
    {
        SetComponentName("Action");
        m_character = nullptr;
        m_target = nullptr;
        m_discovery = false;
        m_attack = false;
        m_move = false;
        m_change_direction = false;
    }
    Action::~Action()
    {
    }
    void Action::Update(mslib::state::State * this_)
    {
        this_;
    }
    void Action::SetTarget(mslib::transform::Transform * target, mslib::transform::Transform * character)
    {
        m_character = character;
        m_target = target;
    }
    bool Action::DiscoveryTrigger()
    {
        return m_discovery;
    }
    bool Action::AttackTrigger()
    {
        return m_attack;
    }
    bool Action::MoveTrigger()
    {
        return m_move;
    }
    bool Action::RotationTrigger()
    {
        return m_change_direction;
    }
}