//==========================================================================
// Action [Action.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace action
{
	//==========================================================================
	//
	// class  : Action
	// Content: アクション機能
	//
	//==========================================================================
	class Action : public mslib::state::StateBasic
    {
    public:
        Action();
        ~Action();
		// 更新
        virtual void Update(mslib::state::State* this_) override;
		// ターゲットの登録
        void SetTarget(mslib::transform::Transform * target, mslib::transform::Transform * character);
		// 認識したかどうかの判定
        bool DiscoveryTrigger();
		// 攻撃判定
        bool AttackTrigger();
		// 移動判定
        bool MoveTrigger();
		// 方向転換の判定
        bool RotationTrigger();
    protected:
        mslib::transform::Transform * m_character; // キャラ
        mslib::transform::Transform * m_target; // 対象

        bool m_discovery; // 発見
        bool m_attack; // 攻撃
        bool m_move; // 移動
        bool m_change_direction; // 向き変更
    };
}