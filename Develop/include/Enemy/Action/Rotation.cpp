//==========================================================================
// Rotation [Rotation.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Rotation.h"
#include "common/Collider.h"

namespace action
{
    Rotation::Rotation()
    {
        SetComponentName("Rotation");
        m_front = D3DXVECTOR3(0, 0, 0);
        m_init = false;
        m_end = false;
        m_update = false;
    }

    Rotation::~Rotation()
    {
    }

    void Rotation::Update(mslib::state::State * this_)
    {
        Init();

        if (m_character == nullptr || m_target == nullptr)
        {
            this_->ReleaseState();
            return;
        }

        // 向きを変える
        D3DXVECTOR3 vecAxis;
        m_character->AddRotationX(m_front, 0.5f);
        float fVec3Dot = atanf(D3DXVec3Dot(&m_character->GetVector()->right, D3DXVec3Normalize(&vecAxis, &m_front)));
        m_change_direction = true;
        if (fVec3Dot <= 0.01f)
            m_end = true;

        // 解放
        if (m_end)
        {
            m_change_direction = false;
            this_->ReleaseState();
            return;
        }
    }
    void Rotation::Init()
    {
        if (m_init)return;
        m_init = true;
        m_end = true;
        if (m_character == nullptr)return;
        if (m_target == nullptr)return;

        // 実行開始時点で向きベクトルを生成
        m_front = m_character->ToSee(*m_target);
        m_front.y = m_character->GetPosition()->y;
        m_end = false;
    }
}