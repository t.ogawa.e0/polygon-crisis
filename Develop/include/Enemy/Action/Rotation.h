//==========================================================================
// Rotation [Rotation.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Action.h"

namespace action
{
	//==========================================================================
	//
	// class  : Rotation
	// Content: 方向転換
	//
	//==========================================================================
	class Rotation : public action::Action
    {
    public:
        Rotation();
        ~Rotation();
		// 更新
        void Update(mslib::state::State* this_);
    protected:
        // 初期化
        void Init();
    protected:
        D3DXVECTOR3 m_front; // 向きベクトル
        bool m_init; // 初期化
        bool m_end; // 終了
        bool m_update; // 処理中かの判定
    };
}