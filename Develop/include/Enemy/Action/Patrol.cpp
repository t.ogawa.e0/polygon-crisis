//==========================================================================
// Patrol [Patrol.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Patrol.h"

namespace action
{
    constexpr float __move_speed__ = 0.05f;

    Patrol::Patrol()
    {
        SetComponentName("Patrol");
        m_front = D3DXVECTOR3(0, 0, 0);
        m_end = false;
        m_update = false;
        m_rot = false;
    }

    Patrol::~Patrol()
    {
    }
    void Patrol::Update(mslib::state::State * this_)
    {
        if (m_character == nullptr || m_target == nullptr)
        {
            this_->ReleaseState();
            return;
        }

        D3DXVECTOR3 vecAxis;

        m_front = m_character->ToSee(*m_target);
        m_front.y = m_character->GetPosition()->y;
        m_character->AddRotationX(m_front, 0.5f);
        m_character->AddPosition(0, 0, __move_speed__);
        m_move = true;

        this_;
    }
}