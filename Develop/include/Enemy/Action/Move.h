//==========================================================================
// Move [Move.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Action.h"

namespace action
{
	//==========================================================================
	//
	// class  : Move
	// Content: 移動
	//
	//==========================================================================
	class Move : public action::Action
    {
    protected:
		// ルート
        struct MyLoot
        {
            mslib::transform::Transform * begin = nullptr;
            std::list<mslib::transform::Transform*> old_access; // access済み
        };
    public:
        Move();
        ~Move();
		// 更新
        void Update(mslib::state::State* this_);
    protected:
		// 初期化
        void Init();
    protected:
        D3DXVECTOR3 m_front; // 向きベクトル
        D3DXVECTOR3 m_target_pos; // ターゲットの座標
        bool m_rot; // 向き変更
        bool m_end; // 終了
        bool m_update; // 処理中かの判定
        bool m_init; // 初期化
    };
}
