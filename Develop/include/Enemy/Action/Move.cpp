//==========================================================================
// Move [Move.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Move.h"

namespace action
{
    using namespace mslib::transform;
    constexpr float __move_speed__ = 0.1f;
    constexpr float __stop_distance__ = 2.0f;

    Move::Move()
    {
        SetComponentName("Move");
        m_front = D3DXVECTOR3(0, 0, 0);
        m_target_pos = D3DXVECTOR3(0, 0, 0);
        m_end = false;
        m_update = false;
        m_rot = false;
        m_init = false;
    }

    Move::~Move()
    {
    }
    void Move::Update(mslib::state::State * this_)
    {
        Init();

        if (m_character == nullptr || m_target == nullptr)
        {
            this_->ReleaseState();
            return;
        }

        m_character->AddRotationX(m_front, 0.5f);
        m_character->AddPosition(0, 0, __move_speed__);
        m_move = true;

        // ��x�ł���������~������ɓ������ꍇ
        if (mslib::collider::Distance(m_target_pos, m_character) <= __stop_distance__)
        {
            m_end = true;
        }

        // ���
        if (m_end)
        {
            this_->ReleaseState();
            return;
        }
    }
    void Move::Init()
    {
        if (m_init)return;
        m_init = true;
        m_end = true;
        if (m_character == nullptr)return;
        if (m_target == nullptr)return;

        D3DXVECTOR3 vecAxis;
        m_front = m_character->ToSee(*m_target);
        m_front.y = m_character->GetPosition()->y;
        m_target_pos = D3DXVECTOR3(m_target->GetWorldMatrix()->_41, m_target->GetWorldMatrix()->_42, m_target->GetWorldMatrix()->_43);
        m_end = false;
    }
}