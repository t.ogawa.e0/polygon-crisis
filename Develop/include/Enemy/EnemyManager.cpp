//==========================================================================
// エネミーマネージャー [EnemyManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EnemyManager.h"

namespace enemy
{
    std::list<EnemyManager*> EnemyManager::m_enemy; // エネミー
	int EnemyManager::m_generated_number = 0; // 生成された回数

    EnemyManager::EnemyManager()
    {
        SetComponentName("EnemyManager" + mslib::text("%p", this));
        m_enemy.push_back(this);
		m_generated_number++;
        m_obj = nullptr;
    }

    EnemyManager::~EnemyManager()
    {
        auto itr = std::find(m_enemy.begin(), m_enemy.end(), this);
        if (itr != m_enemy.end())
            m_enemy.erase(itr);
    }
    void EnemyManager::Init()
    {
        m_obj = GetWarrantyParent<mslib::transform::Transform>();
    }
    std::list<EnemyManager*>& EnemyManager::GetAllEnemy()
    {
        return m_enemy;
    }
	int EnemyManager::GetGeneratedSize()
	{
		return m_generated_number;
	}
	int EnemyManager::GetSize()
	{
		return (int)m_enemy.size();
	}
	mslib::transform::Transform * EnemyManager::GetEnemy()
    {
        return m_obj;
    }
}