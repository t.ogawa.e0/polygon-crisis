//==========================================================================
// エネミーマネージャー [EnemyManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : EnemyManager
	// Content: エネミーのマネージャー
	//
	//==========================================================================
	class EnemyManager : public mslib::component::Component, public mslib::initializer::Initializer
    {
    public:
        EnemyManager();
        ~EnemyManager();

        /**
        @brief 初期化
        */
        void Init() override;

		// 全てのエネミーの取得
        static std::list<EnemyManager*> & GetAllEnemy();

		// 生成回数の取得
		static int GetGeneratedSize();

		// 管理数の取得
		static int GetSize();

		// エネミーの取得
        mslib::transform::Transform * GetEnemy();
    protected:
        static std::list<EnemyManager*> m_enemy; // エネミー
		static int m_generated_number; // 生成された回数
        mslib::transform::Transform * m_obj; // エネミー
    };
}