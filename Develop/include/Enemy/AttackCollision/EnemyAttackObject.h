//==========================================================================
// EnemyAttackObject [EnemyAttackObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Asset/AttackCollision.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : EnemyAttackObject
	// Content: 攻撃オブジェクト
	//
	//==========================================================================
	class EnemyAttackObject : public common::AttackCollision
    {
    public:
        EnemyAttackObject();
        ~EnemyAttackObject();
		// ダメージ
        void SetDamage(int damage);
		// ダメージ取得
        int GetDamage();
		// エネミー全ての攻撃オブジェクトを取得
        static std::list<EnemyAttackObject*> & GetEnemyAttackObject();
		// エネミーの攻撃オブジェクトを破棄
        static void DestroyEnemyAttackObject(EnemyAttackObject *& obj);
    protected:
        static std::list<EnemyAttackObject*>m_EnemyAttackObject; // 攻撃オブジェクト
        int m_damage; // ダメージ
    };
}