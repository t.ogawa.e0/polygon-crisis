//==========================================================================
// EnemyAttackObject [EnemyAttackObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EnemyAttackObject.h"

namespace enemy
{
    std::list<EnemyAttackObject*>EnemyAttackObject::m_EnemyAttackObject;

    EnemyAttackObject::EnemyAttackObject()
    {
        SetComponentName("EnemyAttackObject");
        m_damage = 0;
        m_EnemyAttackObject.push_back(this);
    }

    EnemyAttackObject::~EnemyAttackObject()
    {
        auto itr = std::find(m_EnemyAttackObject.begin(), m_EnemyAttackObject.end(), this);
        if (itr != m_EnemyAttackObject.end())
            m_EnemyAttackObject.erase(itr);
    }
    void EnemyAttackObject::SetDamage(int damage)
    {
        m_damage = damage;
    }
    int EnemyAttackObject::GetDamage()
    {
        return m_damage;
    }
    std::list<EnemyAttackObject*>& EnemyAttackObject::GetEnemyAttackObject()
    {
        return m_EnemyAttackObject;
    }
    void EnemyAttackObject::DestroyEnemyAttackObject(EnemyAttackObject *& obj)
    {
        if (obj == nullptr)return;

        auto parent = obj->GetParent();
        if (parent->DestroyComponent(obj))
            obj = nullptr;
    }
}