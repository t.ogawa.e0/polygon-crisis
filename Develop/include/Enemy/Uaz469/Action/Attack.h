//==========================================================================
// Attack [Attack.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Enemy/Action/Action.h"

namespace uaz469
{
    class Attack : public action::Action
    {
    public:
        Attack();
        ~Attack();
        void Update(mslib::state::State* this_);
    protected:
        // 初期化
        void Init();
    protected:
        D3DXVECTOR3 m_front; // 向きベクトル
        D3DXVECTOR3 m_target_pos; // ターゲットの座標
        bool m_rot; // 向き変更
        bool m_init; // 初期化
        bool m_end; // 終了
        bool m_update; // 処理中かの判定
        bool m_stop; // 停止
        float m_speed; // 速度
    };
}
