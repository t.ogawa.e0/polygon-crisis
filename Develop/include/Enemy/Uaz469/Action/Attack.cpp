//==========================================================================
// Attack [Attack.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Attack.h"
#include "common/Collider.h"

namespace uaz469
{
    constexpr float __move_speed__ = 0.2f;
    constexpr float __stop_distance__ = 2.0f;

    Attack::Attack()
    {
        SetComponentName("Attack");
        m_front = D3DXVECTOR3(0, 0, 0);
        m_target_pos = D3DXVECTOR3(0, 0, 0);
        m_init = false;
        m_end = false;
        m_update = false;
        m_rot = false;
        m_stop = false;
        m_speed = __move_speed__;
    }

    Attack::~Attack()
    {
    }
    void Attack::Update(mslib::state::State * this_)
    {
        Init();

        if (m_character == nullptr || m_target == nullptr)
        {
            this_->ReleaseState();
            return;
        }

        // 向きを変える
        if (!m_rot)
        {
            D3DXVECTOR3 vecAxis;
            m_character->AddRotationX(m_front, __move_speed__);
            float fVec3Dot = atanf(D3DXVec3Dot(&m_character->GetVector()->right, D3DXVec3Normalize(&vecAxis, &m_front)));
            m_change_direction = true;
            if (fVec3Dot <= 0.01f && -0.01f <= fVec3Dot)
                m_rot = true;
        }

        // 向き変更終わり
        if (m_rot)
        {
            m_change_direction = false;
            m_attack = true;

            // 一度でも距離が停止判定内に入った場合
            if (mslib::collider::Distance(m_target_pos, m_character) <= __stop_distance__)
            {
                m_stop = true;
            }

            if (m_stop)
            {
                m_speed -= 0.01f;
            }

            if (0.0f < m_speed)
            {
                m_character->AddPosition(0, 0, m_speed);
            }
            else if (m_speed <= 0.0f)
            {
                m_end = true;
            }
        }

        // 解放
        if (m_end)
        {
            this_->ReleaseState();
            return;
        }
    }
    void Attack::Init()
    {
        if (m_init)return;
        m_init = true;
        m_end = true;
        if (m_character == nullptr)return;
        if (m_target == nullptr)return;

        // 実行開始時点で向きベクトルを生成
        m_front = m_character->ToSee(*m_target);
        m_front.y = m_character->GetPosition()->y;
        m_target_pos = D3DXVECTOR3(m_target->GetWorldMatrix()->_41, m_target->GetWorldMatrix()->_42, m_target->GetWorldMatrix()->_43);
        m_end = false;
    }
}