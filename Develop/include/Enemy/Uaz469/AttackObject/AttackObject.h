//==========================================================================
// AttackObject [AttackObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/AttackCollision/EnemyAttackObject.h"
#include "Asset/Character.h"

namespace uaz469
{
    class AttackObject : public enemy::EnemyAttackObject
    {
    public:
        AttackObject();
        ~AttackObject();
        mslib::object::Effekseer * GetEffekseerObject();
        void SetTarget(mslib::transform::Transform * target, mslib::transform::Transform * character);
        /**
        @brief �X�V
        */
        virtual void Update() override;

		static void uaz469AttackObjectReleaseAll();
		static int uaz469AttackObjectNumAll();
    protected:
        mslib::object::Effekseer * m_EffekseerObj;
		mslib::transform::Transform * m_target;
		mslib::transform::Transform * m_attack_obj;
        common::Character * m_Character;
        bool m_flag;
        int m_time;
		static std::list<AttackObject*>m_uaz469AttackObject;
    };
}