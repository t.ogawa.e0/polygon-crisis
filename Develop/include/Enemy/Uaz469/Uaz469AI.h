//==========================================================================
// Uaz469AI [Uaz469AI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "AttackObject/AttackObject.h"
#include "Enemy/CommonAI.h"

namespace enemy
{
    class Uaz469AI : public CommonAI
    {
    public:
        Uaz469AI();
        ~Uaz469AI();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

    protected:
        void Taiki();
        void Attack();
        void Move();
        void Patrol();
        void Rotation();
		void DesultoryAttackObject();
	protected:
		uaz469::AttackObject * m_AttackObject;
    };
}