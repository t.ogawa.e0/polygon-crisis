//==========================================================================
// UAZ469型 [Uaz469.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/CommonEnemy.h"
#include "Asset/AnimationModel.h"

namespace enemy
{
    class Uaz469AIAnimation : public mslib::function::FunctionComponent
    {
    public:
        Uaz469AIAnimation();
        ~Uaz469AIAnimation();
        void Update() override;
        void SetAnimationModel(animation_model::AnimationShaderModel*obj);
        // 移動アニメーション
        bool Move(int flame);
        bool CheckMove();
        void MoveEnd();

        // 待機アニメーション
        bool Taiki(int flame);
        bool CheckTaiki();

        // 回転アニメーション
        bool Rotation(int flame);
        bool CheckRotation();
        void RotationEnd();
    private:
        animation_model::AnimationShaderModel * m_model;
    };

    class EnemyDataCustom : public EnemyData
    {
    public:
        EnemyDataCustom() { animation = nullptr; }
        EnemyDataCustom(mslib::transform::Transform *o, mslib::transform::Transform*m, Uaz469AIAnimation * ani) : EnemyData(o, m) { animation = ani; }
        EnemyDataCustom(mslib::transform::Transform *o, mslib::transform::Transform*m, Uaz469AIAnimation * ani, mslib::collider::DistanceCollider *d, void*a, void*c) : EnemyData(o, m, d, a, c) { animation = ani; }
        ~EnemyDataCustom() {}
    public:
        Uaz469AIAnimation * animation = nullptr;
    };

	class Uaz469Data : public mslib::component::Component, public EnemyDataCustom
	{
	public:
		Uaz469Data();
		Uaz469Data(mslib::transform::Transform *o, mslib::transform::Transform*m, Uaz469AIAnimation * ani);
		Uaz469Data(mslib::transform::Transform *o, mslib::transform::Transform*m, Uaz469AIAnimation * ani, mslib::collider::DistanceCollider *d, void*a, void*c);
		~Uaz469Data();
		static std::list<Uaz469Data*> & GetAllUaz469Data();
	private:
		static std::list<Uaz469Data*>m_data;
	};

    class Uaz469 : public mslib::ObjectManager
    {
    public:
        Uaz469();
        Uaz469(int NumMaxEnemy, int CreatePoint);
        ~Uaz469();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    protected:
        void CommonObjectInit();
        animation_model::AnimationShaderModel * InitModel(mslib::transform::Transform * obj);
    protected:
        bool m_init;
        bool m_bCommonObjectActivity;
        int m_NumMaxEnemy; // 最大エネミーの数
        int m_CreatePoint; // 生成ポイント
        int m_NumEnemy; // 出現した回数
        mslib::object::Light * m_LightObject;
        mslib::camera::Camera * m_CameraObject;
        std::vector<mslib::transform::Transform*> m_GeneratedCoordinates; // 生成座標
        EnemyDataCustom m_CommonObject;
        int m_Init_Call;
        mslib::collider::BoxCollider * m_CommonBox;
        mslib::collider::BoxCollider * m_CommonEnemyBox;
        mslib::transform::Transform * m_CommonTargetObject;
        mslib::transform::Transform * m_Common_attack_object_position; // 攻撃オブジェクトの座標
    };
}
