//==========================================================================
// UAZ469型 [Uaz469.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Uaz469.h"
#include "Uaz469AI.h"
#include "Asset/TPSCamera.h"
#include "Asset/LightObject.h"
#include "Asset/ControllerObject.h"
#include "Asset/ShaderModel.h"
#include "Asset/Character.h"
#include "Asset/ActionRange.h"
#include "Enemy/EnemyManager.h"
#include "resource_list.h"
#include "config.h"

namespace enemy
{
    namespace MotionName
    {
        constexpr char* Taiki = "Taiki"; // 待機モーション
        constexpr char* Move = "Move"; // 移動モーション
        constexpr char* Rotation = "Rotation"; // 回転
    }

	std::list<Uaz469Data*> Uaz469Data::m_data;

    Uaz469::Uaz469() : ObjectManager("Uaz469Object")
    {
        m_Init_Call = 0;
        m_NumMaxEnemy = 0;
        m_CreatePoint = 0;
        m_NumEnemy = 0;
        m_init = false;
        m_bCommonObjectActivity = false;
        m_CommonBox = nullptr;
        m_CommonEnemyBox = nullptr;
        m_CommonTargetObject = nullptr;
        m_Common_attack_object_position = nullptr;
		AddActivity();
    }

    Uaz469::Uaz469(int NumMaxEnemy, int CreatePoint) : ObjectManager("Uaz469Object")
    {
        m_Init_Call = 0;
        m_NumMaxEnemy = NumMaxEnemy;
        m_CreatePoint = CreatePoint;
        m_NumEnemy = 0;
        m_init = false;
        m_bCommonObjectActivity = false;
        m_CommonBox = nullptr;
        m_CommonEnemyBox = nullptr;
        m_CommonTargetObject = nullptr;
        m_Common_attack_object_position = nullptr;
        m_GeneratedCoordinates.reserve(CreatePoint);
		AddActivity();
    }

    Uaz469::~Uaz469()
    {
    }
    void Uaz469::Init()
    {
        m_LightObject = GetParent()->GetComponent<common::LightObject>()->Light()->Get(0);
        m_CameraObject = GetParent()->GetComponent<common::TPSCamera>()->Camera()->Get(0);
        auto action_range = GetParent()->GetComponent<common::ActionRange>();

        // Uaz469の統括
        auto Uaz469Component = Object()->Create();
        Uaz469Component->SetComponentName("Uaz469Component");

        // 親
        auto object = Uaz469Component->AddComponent<mslib::object::Object>();
        object->SetComponentName("Uaz469CommonObject");
        object->SetMatrixType(mslib::object::MatrixType::Local);
        m_CommonBox = object->AddComponent<mslib::collider::BoxCollider>();
        m_CommonEnemyBox = object->AddComponent<mslib::collider::BoxCollider>();
        m_CommonTargetObject = object->AddComponent<mslib::object::Object>();
        m_Common_attack_object_position = object->AddComponent<mslib::object::Object>();
        m_CommonEnemyBox->SetComponentName("CommonEnemyBox");
        m_CommonTargetObject->SetComponentName("CommonTargetObject");
        m_Common_attack_object_position->SetComponentName("CommonAttackObjectPosition");

        // モデル取得
        auto model = InitModel(object);
        m_CommonObject = EnemyDataCustom(object, model, nullptr);

        // 出現座標の設定
        for (int i = 0; i < m_CreatePoint; i++)
        {
            auto po = Uaz469Component->AddComponent<mslib::object::Object>();
            po->SetComponentName("GeneratedCoordinates" + std::to_string(i));
            po->SetMatrixType(mslib::object::MatrixType::Local);
            m_GeneratedCoordinates.push_back(po);
        }

        // エネミーを生成します
        for (int i = 0; i < m_NumMaxEnemy; i++)
        {
            auto obj = Uaz469Component->AddComponent<mslib::object::Object>();

            obj->SetComponentName("Uaz469_" + std::to_string(i));
            obj->SetMatrixType(mslib::object::MatrixType::Local);

            auto mod = InitModel(obj);
            auto pEnemyCharacter = obj->AddComponent<common::EnemyCharacter>();
            auto ai = obj->AddComponent<Uaz469AI>();
            auto ani = obj->AddComponent<Uaz469AIAnimation>();
            auto di = mod->AddComponent<mslib::collider::DistanceCollider>();

			ani->SetAnimationModel(mod);
			ai->SetActionRangeData(action_range->GetAllActionRangeData());

			obj->AddComponent<EnemyManager>()->InitAll();
			obj->AddComponent<Uaz469Data>(obj, mod, ani, di, ai, pEnemyCharacter);

        }
    }
    void Uaz469::Update()
    {
        CommonObjectInit();

        for (auto & itr : Uaz469Data::GetAllUaz469Data())
        {
            auto pEnemyCharacter = mslib::cast<common::EnemyCharacter>(itr->character);
            auto ai = mslib::cast<Uaz469AI>(itr->ai);

            // コリジョン処理
            pEnemyCharacter->Collider();

            // 壁との判定が出た
            if (pEnemyCharacter->GetVersusWallBox()->Trigger())
            {
                ai->ResetLoot();
                ai->StopAction();
            }

            // お互いが衝突しあった
            if (itr->distance->Trigger())
            {
                //itr.repulsion -= 0.5f;
                //ai->ResetLoot();
                //ai->StopAction();
            }

            // 反発
            if (itr->repulsion < 0)
            {
                itr->obj->AddPosition(0, 0, itr->repulsion);
                itr->repulsion += 0.3f;
            }

            // 移動アニメーションの
            if (ai->MoveTrigger())
            {
                itr->animation->Move(1);
            }
            // 攻撃アニメーション
            else if (ai->AttackTrigger())
            {
                itr->animation->Move(4);
            }
            else
            {
                itr->animation->MoveEnd();
            }

            // 向き変更アニメーション
            if (ai->ChangeDirectionTrigger())
            {
                itr->animation->Rotation(1);
            }
            else
            {
                itr->animation->RotationEnd();
            }

            // 古い座標記録
            pEnemyCharacter->SetParameter(itr->obj);
        }
    }
    void Uaz469::Debug()
    {
        if (ImGui()->Checkbox("CommonObjectActivity", m_bCommonObjectActivity))
        {
            auto common_model = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.model);
            if (common_model != nullptr)
            {
                common_model->SetActivity(m_bCommonObjectActivity);
                auto model = dynamic_cast<animation_model::AnimationShaderModel*>(m_CommonObject.model);
                if (model != nullptr)
                    for (auto & itr : model->GetPartsList())
                    {
                        itr->SetActivity(m_bCommonObjectActivity);
                    }
            }
            auto common_obj = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.obj);
            if (common_obj != nullptr)
            {
                common_obj->SetActivity(m_bCommonObjectActivity);
                auto model = dynamic_cast<animation_model::AnimationShaderModel*>(m_CommonObject.model);
                if (model != nullptr)
                    for (auto & itr : model->GetPartsList())
                    {
                        itr->SetActivity(m_bCommonObjectActivity);
                    }
            }
        }
    }
    void Uaz469::CommonObjectInit()
    {
        if (m_init)return;

        m_Init_Call++;
        if (m_Init_Call != 30)return;

        auto common_model = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.model);
        if (common_model != nullptr)
        {
            common_model->SetActivity(false);
        }
        auto common_obj = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.obj);
        if (common_obj != nullptr)
        {
            common_obj->SetActivity(false);
        }

        int id = 0;
        for (auto & itr : Uaz469Data::GetAllUaz469Data())
        {
            auto it = m_GeneratedCoordinates[id];
            auto ai = mslib::cast<Uaz469AI>(itr->ai);
            auto pEnemyCharacter = mslib::cast<common::EnemyCharacter>(itr->character);

            itr->model->SetParameter(*m_CommonObject.model->GetParameter());
            itr->obj->SetParameter(*it->GetParameter());
            ai->ResetLoot();
            ai->GetViewingRangeBox()->Copy(m_CommonBox);
            ai->SetAttackObject(m_Common_attack_object_position);
            //pEnemyCharacter->GetEnemyCollider()->Copy(m_CommonEnemyBox);
            pEnemyCharacter->GetTargetObject()->SetParameter(*m_CommonTargetObject->GetParameter());

            id++;
            for (auto & itr2 : Uaz469Data::GetAllUaz469Data())
            {
                if (itr2->distance == itr->distance)continue;

                itr->distance->AddCollider(itr2->distance);
                itr->distance->DeterminationDistance(10);

                auto mo = mslib::cast<animation_model::AnimationShaderModel>(itr->model);
                mo->Play(MotionName::Taiki, true);
            }
        }

        m_init = true;
    }
    animation_model::AnimationShaderModel * Uaz469::InitModel(mslib::transform::Transform * obj)
    {
        auto data = obj->AddComponent<animation_model::AnimationShaderModel>();
        data->SetComponentName("Model");

        auto model = data->AddComponent<animation_model::AnimationShaderModel>();
        model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        model->SetCamera(m_CameraObject->GetCamera());
        model->SetLight(m_LightObject);
        model->SetComponentName("UAZ469");

        model->AddParts2("Body", GetXModelLoader()->Load(RESOURCE_UAZ469_Body_x));

        model->AddParts2("1_ParentLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
        model->AddParts2("1_ChildLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "1_ParentLegR");

        model->AddParts2("2_ParentLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
        model->AddParts2("2_ChildLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "2_ParentLegL");

        model->AddParts2("3_ParentLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
        model->AddParts2("3_ChildLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "3_ParentLegR");

        model->AddParts2("4_ParentLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
        model->AddParts2("4_ChildLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "4_ParentLegL");

        model->Load(FolderStructure::__motion__ + model->GetComponentName());
        model->GenerateAnimation(MotionName::Taiki);
        model->GenerateAnimation(MotionName::Move);
        model->GenerateAnimation(MotionName::Rotation);

        return model;
    }

    Uaz469AIAnimation::Uaz469AIAnimation()
    {
        SetComponentName("Uaz469AIAnimation");
        m_model = nullptr;
    }
    Uaz469AIAnimation::~Uaz469AIAnimation()
    {
    }
    void Uaz469AIAnimation::Update()
    {
        Taiki(1);
    }
    void Uaz469AIAnimation::SetAnimationModel(animation_model::AnimationShaderModel * obj)
    {
        m_model = obj;
    }
    bool Uaz469AIAnimation::Move(int flame)
    {
        if (m_model == nullptr)return false;

        if (!m_model->Check(MotionName::Move))
        {
            m_model->SetAnimationUpdateFlame(flame);
            m_model->Play(MotionName::Move, true);
            return true;
        }
        return false;
    }
    bool Uaz469AIAnimation::CheckMove()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Move);
    }
    void Uaz469AIAnimation::MoveEnd()
    {
        if (m_model == nullptr)return;

        // 実行中である
        if (m_model->Check(MotionName::Move))
        {
            // 停止
            m_model->Stop(MotionName::Move);
        }
    }
    bool Uaz469AIAnimation::Taiki(int flame)
    {
        if (m_model == nullptr)return false;

        if (!m_model->Check(MotionName::Move) &&
            !m_model->Check(MotionName::Taiki) &&
            !m_model->Check(MotionName::Rotation)) {
            m_model->SetAnimationUpdateFlame(flame);
            m_model->Play(MotionName::Taiki, true);
            return true;
        }
        return false;
    }
    bool Uaz469AIAnimation::CheckTaiki()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Taiki);
    }
    bool Uaz469AIAnimation::Rotation(int flame)
    {
        if (m_model == nullptr)return false;

        if (!m_model->Check(MotionName::Rotation))
        {
            m_model->SetAnimationUpdateFlame(flame);
            m_model->Play(MotionName::Rotation, true);
            return true;
        }
        return false;
    }
    bool Uaz469AIAnimation::CheckRotation()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Rotation);
    }
    void Uaz469AIAnimation::RotationEnd()
    {
        if (m_model == nullptr)return;

        // 実行中である
        if (m_model->Check(MotionName::Rotation))
        {
            // 停止
            m_model->Stop(MotionName::Rotation);
        }
    }
	Uaz469Data::Uaz469Data()
	{
		SetComponentName("Uaz469Data");
		m_data.push_back(this);
	}
	Uaz469Data::Uaz469Data(mslib::transform::Transform * o, mslib::transform::Transform * m, Uaz469AIAnimation * ani)
		:EnemyDataCustom(o, m, ani)
	{
		SetComponentName("Uaz469Data");
		m_data.push_back(this);
	}
	Uaz469Data::Uaz469Data(mslib::transform::Transform * o, mslib::transform::Transform * m, Uaz469AIAnimation * ani, mslib::collider::DistanceCollider * d, void * a, void * c)
		: EnemyDataCustom(o, m, ani, d, a, c)
	{
		SetComponentName("Uaz469Data");
		m_data.push_back(this);
	}
	Uaz469Data::~Uaz469Data()
	{
		auto itr = std::find(m_data.begin(), m_data.end(), this);
		if (itr != m_data.end())
			m_data.erase(itr);
	}
	std::list<Uaz469Data*>& Uaz469Data::GetAllUaz469Data()
	{
		return m_data;
	}
}