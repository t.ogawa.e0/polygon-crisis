//==========================================================================
// NucleusAI [NucleusAI.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "NucleusAI.h"
#include "Player/PlayerManager.h"
#include "Enemy/Action/Action.h"
#include "Enemy/Action/Rotation.h"
#include "Enemy/Action/Patrol.h"
#include "Enemy/Action/Move.h"
#include "Action/Attack.h"
#include "AttackObject/AttackObject.h"

namespace enemy
{
    using namespace mslib::transform;

    NucleusAI::NucleusAI()
    {
        SetComponentName("NucleusAI");
    }

    NucleusAI::~NucleusAI()
    {
    }
    void NucleusAI::Init()
    {
        InitializerViewingRangeBox();
        InitializerState();
    }
    void NucleusAI::Update()
    {
        // 初期化
        InitActionTrigger();

        // 認識
        DiscoveryBox();

        // 認識している時の処理
        if (AcquisitionTime())
        {
            // 対象選択
            SelectTarget();

            // 行動選択
            SelectAction();

            // 移動ステート破棄
            if (m_state->ReleaseState(m_move_state))
                m_move_state = nullptr;

            // 行動初期化
            switch (m_action_id)
            {
            case enemy::ActionID::Taiki:
                Taiki();
                break;
            case enemy::ActionID::Attack:
                Attack();
                break;
            case enemy::ActionID::Move:
                Move();
                break;
            case enemy::ActionID::Rotation:
                Rotation();
                break;
            default:
                break;
            }
        }
        // 認識していない
        else
        {
            // 自動
            Patrol();
        }

        // ステートが存在しない
        if (m_state == nullptr)return;
        if (!m_state->Trigger())return;

        // ステート更新
        m_state->Update();

        // モーション操作
        auto act = mslib::cast<action::Action>(m_state->GetState());
        if (act != nullptr)
        {
            m_move = act->MoveTrigger();
            m_change_direction = act->RotationTrigger();
            m_discovery = act->DiscoveryTrigger();
            m_attack = act->AttackTrigger();
        }

        // アクション実行時間
        ActionTimeCounter();

        // ステートがまだ存在する場合
        if (m_state->Trigger())return;

        m_action_id = ActionID::end;
        InitActionTrigger();
        m_action_time = 0;
    }
    void NucleusAI::Taiki()
    {
        // 実体が無い
        if (m_state == nullptr)return;

        // 既に機能が存在する
        if (m_state->Trigger())
        {
            if (m_action_time <= 0)
            {
                m_state->ReleaseState();
                m_action_id = ActionID::end;
                InitActionTrigger();
                m_action_time = 0;
            }
            return;
        }

        // 機能の変更
        auto sys = m_state->ChangeState<action::Action>();
        sys->SetTarget(m_target, GetWarrantyParent<Transform>());

        mslib::rand_int __rand = mslib::rand_int(Seconds(1), Seconds(3));
        m_action_time = __rand(m_mt);
    }
    void NucleusAI::Attack()
    {
        // 実体が無い
        if (m_state == nullptr)return;

        // 既に機能が存在する
        if (m_state->Trigger())
        {
            auto act = mslib::cast<nucleus::Attack>(m_state->GetState());
            if (act->AttackTrigger())
            {
				if (m_attack_object_position != nullptr)
				{
					auto att_obj = m_attack_object_position->AddComponent<nucleus::AttackObject>();

					att_obj->SetTarget(m_target, m_attack_object_position);
				}
				m_state->ReleaseState();
				m_action_id = ActionID::end;
            }
            return;
        }

        auto sys = m_state->ChangeState<nucleus::Attack>();
        sys->SetTarget(m_target, GetWarrantyParent<Transform>());
        sys->SetLoot(m_loot);

        mslib::rand_int __rand = mslib::rand_int(Seconds(30), Seconds(60));
        m_action_time = __rand(m_mt);
    }
    void NucleusAI::Move()
    {
        // 実体が無い
        if (m_state == nullptr)return;

        // 既に機能が存在する
        if (m_state->Trigger())return;

        auto sys = m_state->ChangeState<action::Move>();
        sys->SetTarget(m_target, GetWarrantyParent<Transform>());

        mslib::rand_int __rand = mslib::rand_int(Seconds(3), Seconds(7));
        m_action_time = __rand(m_mt);
    }
    void NucleusAI::Patrol()
    {
        // 実体が無い
        if (m_state == nullptr)return;

        // ルートが初期化されている場合
        if (m_loot == nullptr)
        {
            m_state->ReleaseState();
            m_action_id = ActionID::end;
            InitActionTrigger();
            m_action_time = 0;
            FindEnemies(10, 0.1f);
            return;
        }

        FindEnemies(10, 0.1f);

        // 既に機能が存在する
        if (m_state->Trigger())
        {
            auto act = mslib::cast<action::Action>(m_state->GetState());
            if (act != nullptr)
            {
                act->SetTarget(m_loot, GetWarrantyParent<Transform>());
                return;
            }
        }

        // 機能の変更
        auto sys = m_state->ChangeState<action::Patrol>();
        sys->SetTarget(m_loot, GetWarrantyParent<Transform>());
        m_move_state = sys;

        mslib::rand_int __rand = mslib::rand_int(Seconds(30), Seconds(60));
        m_action_time = __rand(m_mt);
    }
    void NucleusAI::Rotation()
    {
        // 実体が無い
        if (m_state == nullptr)return;

        // 既に機能が存在する
        if (m_state->Trigger())return;

        // 機能の変更
        auto sys = m_state->ChangeState<action::Rotation>();
        sys->SetTarget(m_target, GetWarrantyParent<Transform>());

        mslib::rand_int __rand = mslib::rand_int(Seconds(30), Seconds(60));
        m_action_time = __rand(m_mt);
    }
}