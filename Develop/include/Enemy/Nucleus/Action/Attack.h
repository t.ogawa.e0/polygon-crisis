//==========================================================================
// Attack [Attack.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Enemy/Action/Action.h"

namespace nucleus
{
    class Attack : public action::Action
    {
    public:
        Attack();
        ~Attack();
        void Update(mslib::state::State* this_);
        void SetLoot(mslib::transform::Transform * obj);
        bool AttackTrigger();
        void CallEnd();
    protected:
        // 初期化
        void Init();
    protected:
        D3DXVECTOR3 m_front; // 向きベクトル
        D3DXVECTOR3 m_target_pos; // ターゲットの座標
        mslib::bool3 m_rot; // 回転
        bool m_init; // 初期化
        bool m_end; // 終了
        mslib::transform::Transform * m_loot; // ルート
        bool m_attack_trigger; // 攻撃キー
    };
}
