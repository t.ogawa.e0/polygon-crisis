//==========================================================================
// Attack [Attack.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Attack.h"
#include "common/Collider.h"

namespace nucleus
{
    constexpr float __move_speed__ = 0.2f;
    constexpr float __stop_distance__ = 2.0f;
    using namespace mslib::transform;

    Attack::Attack()
    {
        SetComponentName("Attack");
        m_front = D3DXVECTOR3(0, 0, 0);
        m_target_pos = D3DXVECTOR3(0, 0, 0);
        m_init = false;
        m_rot = false;
        m_end = false;
        m_loot = nullptr;
        m_attack_trigger = false;
    }

    Attack::~Attack()
    {
    }
    void Attack::Update(mslib::state::State * this_)
    {
        Init();

        if (m_character == nullptr || m_loot == nullptr)
        {
            this_->ReleaseState();
            return;
        }

        // 向きを変える
        if (!m_rot.x)
        {
            D3DXVECTOR3 vecAxis;
            m_character->AddRotationX(m_front, __move_speed__);
            float fVec3Dot = atanf(D3DXVec3Dot(&m_character->GetVector()->right, D3DXVec3Normalize(&vecAxis, &m_front)));
            m_change_direction = true;
            if (fVec3Dot <= 0.01f && -0.01f <= fVec3Dot)
                m_rot.x = true;
        }

        // 向き変更終わり
        if (m_rot.x&&!m_rot.y)
        {
            m_change_direction = false;
            m_move = true;

            // 距離を速度に変換し処理
            m_character->AddPosition(0, 0, __move_speed__);
            if (mslib::collider::Distance(m_character, m_loot) <= __stop_distance__)
            {
                m_front = m_character->ToSee(*m_target);
                m_front.y = m_character->GetPosition()->y;
                m_target_pos = D3DXVECTOR3(m_target->GetWorldMatrix()->_41, m_target->GetWorldMatrix()->_42, m_target->GetWorldMatrix()->_43);
                m_rot.y = true;
            }
        }

        // 向きを変える
        if (m_rot.x && m_rot.y && !m_rot.z)
        {
            D3DXVECTOR3 vecAxis;
            m_character->AddRotationX(m_front, __move_speed__);
            float fVec3Dot = atanf(D3DXVec3Dot(&m_character->GetVector()->right, D3DXVec3Normalize(&vecAxis, &m_front)));
            m_change_direction = true;
            m_move = false;
            if (fVec3Dot <= 0.01f && -0.01f <= fVec3Dot)
            {
                m_attack_trigger = true;
                m_rot.z = true;
            }
        }

        // 解放
        if (m_end)
        {
            this_->ReleaseState();
            return;
        }
    }
    void Attack::SetLoot(Transform * obj)
    {
        m_loot = obj;
    }
    bool Attack::AttackTrigger()
    {
        return m_attack_trigger;
    }
    void Attack::CallEnd()
    {
        m_end = true;
    }
    void Attack::Init()
    {
        if (m_init)return;
        m_init = true;
        m_end = true;
        if (m_character == nullptr)return;
        if (m_target == nullptr)return;
        if (m_loot == nullptr)return;

        // 実行開始時点で向きベクトルを生成
        m_front = m_character->ToSee(*m_loot);
        m_front.y = m_character->GetPosition()->y;
        //m_target_pos = D3DXVECTOR3(m_target->GetWorldMatrix()->_41, m_target->GetWorldMatrix()->_42, m_target->GetWorldMatrix()->_43);
        m_end = false;
    }
}