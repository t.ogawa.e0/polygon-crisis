//==========================================================================
// NucleusAI [NucleusAI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/CommonAI.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : NucleusAI
	// Content: AI
	//
	//==========================================================================
	class NucleusAI : public CommonAI
    {
    public:
        NucleusAI();
        ~NucleusAI();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;
    protected:
        void Taiki();
        void Attack();
        void Move();
        void Patrol();
        void Rotation();
    };
}
