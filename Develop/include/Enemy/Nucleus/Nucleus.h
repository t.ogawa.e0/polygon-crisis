//==========================================================================
// コア型エネミー [Nucleus.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/CommonEnemy.h"

namespace enemy
{
	//==========================================================================
	//
	// class  : NucleusData
	// Content: データ
	//
	//==========================================================================
	class NucleusData : public mslib::component::Component, public EnemyData
	{
	public:
		NucleusData();
		NucleusData(mslib::transform::Transform *o, mslib::transform::Transform*m);
		NucleusData(mslib::transform::Transform *o, mslib::transform::Transform*m, mslib::collider::DistanceCollider *d, void*a, void*c);
		~NucleusData();
		// 全てのデータを取得
		static std::list<NucleusData*> & GetAllNucleusData();
	private:
		static std::list<NucleusData*>m_data;
	};
	
	//==========================================================================
	//
	// class  : Nucleus
	// Content: Nucleus
	//
	//==========================================================================
	class Nucleus : public mslib::ObjectManager
    {
    public:
        Nucleus();
        Nucleus(int NumMaxEnemy, int CreatePoint);
        ~Nucleus();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    protected:
        void CommonObjectInit();
    protected:
        bool m_init; // 初期化
        bool m_bCommonObjectActivity; // オブジェクトの有効化キー
        int m_NumMaxEnemy; // 最大エネミーの数
        int m_CreatePoint; // 生成ポイント
        int m_NumEnemy; // 出現した回数
        mslib::object::Light * m_LightObject; // 光源
        mslib::camera::Camera * m_CameraObject; // カメラ
        std::vector<mslib::transform::Transform*> m_GeneratedCoordinates; // 生成座標
        EnemyData m_CommonObject; // コモン
        int m_Init_Call; // 初期化呼び出し
        mslib::collider::BoxCollider * m_CommonBox; // 共通の箱
        mslib::collider::BoxCollider * m_CommonEnemyBox; // エネミーの共通の箱
        mslib::transform::Transform * m_CommonTargetObject; // 対象オブジェクト
        mslib::transform::Transform * m_Common_attack_object_position; // 攻撃オブジェクトの座標
    };
}
