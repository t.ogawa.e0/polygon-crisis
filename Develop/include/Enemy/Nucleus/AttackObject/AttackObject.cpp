//==========================================================================
// AttackObject [AttackObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "AttackObject.h"
#include "common/Collider.h"
#include "Asset/FieldBox.h"
#include "Asset/FieldPlane.h"
#include "Asset/FieldHighGround.h"

namespace nucleus
{
    using namespace mslib::manager;
    constexpr int __limit_time__ = 60 * 5;

    D3DXVECTOR3 ToSeeOrigin(mslib::transform::Transform & my, mslib::transform::Transform & target)
    {
        return D3DXVECTOR3(target.GetWorldMatrix()->_41, target.GetWorldMatrix()->_42, target.GetWorldMatrix()->_43) -
            D3DXVECTOR3(my.GetWorldMatrix()->_41, my.GetWorldMatrix()->_42, my.GetWorldMatrix()->_43);
    }

    AttackObject::AttackObject()
    {
        SetComponentName("NucleusAttackObject");
        m_EffekseerObj = nullptr;
        m_target = nullptr;
        m_Character = nullptr;
        m_flag = false;
        m_time = 0;
        SetDamage(5);
    }

    AttackObject::~AttackObject()
    {
    }
    mslib::object::Effekseer * AttackObject::GetEffekseerObject()
    {
        return m_EffekseerObj;
    }
    void AttackObject::SetTarget(mslib::transform::Transform * target, mslib::transform::Transform * character)
    {
        if (m_EffekseerObj != nullptr)return;

        m_target = target;

        // 自身を発射位置に
        SetPosition(character->GetWorldMatrix()->_41, character->GetWorldMatrix()->_42, character->GetWorldMatrix()->_43);
        SetMatrixType(mslib::object::MatrixType::Local);

        // エフェクト生成ポイントを発射位置に
        m_EffekseerObj = AddComponent<mslib::object::Effekseer>();
        m_EffekseerObj->SetEffekseerData(Manager::GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/NucleusAttack1.efk", "NucleusAttack1"));
        m_EffekseerObj->SetHandle(-1);
        m_EffekseerObj->SetComponentName("NucleusAttackEffect");
        m_EffekseerObj->SetMatrixType(mslib::object::MatrixType::Local);
        m_EffekseerObj->AddActivity();

        m_Character = m_EffekseerObj->AddComponent<common::Character>(false);
        m_Character->InitAll();

        // 平面判定に登録
        for (auto & itr : common::FieldPlane::GetAllFieldPlane())
        {
            itr->AddCollider(m_Character->GetVersusMap());
        }

        // 壁判定に登録
        for (auto & itr : common::FieldBox::GetAllFieldBox())
        {
            itr->AddCollider(m_Character->GetVersusWallBox());
        }

        // 特殊地面判定に登録
        for (auto & itr : common::FieldHighGround::GetAllFieldHighGround())
        {
            itr->GetFieldBox()->AddCollider(m_Character->GetVersusHighGroundBox());
            itr->GetFieldPlane()->AddCollider(m_Character->GetVersusHighGroundUnder());
            itr->GetFieldUpPlane()->AddCollider(m_Character->GetVersusHighGroundUp());
        }

        // プレイヤーに対してコリジョン処理を反映
        for (auto & itr : common::PlayerCharacter::GetAllPlayerCharacter())
        {
            itr->GetPlayerCollider()->AddCollider(GetCollider());
            itr->GetDefenseCollider()->AddCollider(GetCollider());
        }
    }
    void AttackObject::Update()
    {
        // 初回のみ
        if (m_flag == false && m_EffekseerObj != nullptr)
        {
            SetMatrixType(mslib::object::MatrixType::Local);
            UpdateMatrix();

            // 向き変換
            auto pTarget = common::PlayerCharacter::GetPlayerCharacter(m_target);
            if (pTarget == nullptr)
            {
                m_delete_flag = true;
                return;
            }
            auto angle = ToSeeOrigin(*this, *pTarget->GetTargetObject());

            m_Look.eye = -angle;
            m_Vector.front = angle;
            m_Vector.Normalize();
            m_EffekseerObj->Play();
            m_flag = true;
            AddActivity();
        }

        // ここで判定
        if (m_Character != nullptr)
        {
            // 無効化
            m_Character->GetVersusHighGroundUp()->LockUpdate();
            m_Character->GetVersusHighGroundUnder()->UnlockUpdate();
            m_Character->GetVersusHighGroundBox()->UnlockUpdate();

            // 地面に接触した時
            if (m_Character->GetVersusMap()->Trigger()) {
                m_delete_flag = true;
            }

            // 壁のBoxに接触時
            if (m_Character->GetVersusWallBox()->Trigger()) {
                m_delete_flag = true;
            }

            // 高台のBoxに接触時
            if (m_Character->GetVersusHighGroundBox()->Trigger() || m_Character->GetVersusHighGroundUnder()->Trigger()) {
                m_delete_flag = true;
            }
        }

        AddPosition(0, 0, 0.5f);
        SetMatrixType(mslib::object::MatrixType::Local);
        UpdateMatrix();

        if (m_EffekseerObj != nullptr)
        {
            m_EffekseerObj->SetPosition(m_WorldMatrix._41, m_WorldMatrix._42, m_WorldMatrix._43);
        }

        if (__limit_time__ < m_time)
            m_delete_flag = true;

        m_time++;
    }
}