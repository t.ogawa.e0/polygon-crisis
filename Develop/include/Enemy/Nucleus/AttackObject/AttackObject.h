//==========================================================================
// AttackObject [AttackObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Enemy/AttackCollision/EnemyAttackObject.h"
#include "Asset/Character.h"

namespace nucleus
{
	//==========================================================================
	//
	// class  : AttackObject
	// Content: 攻撃オブジェクト
	//
	//==========================================================================
	class AttackObject : public enemy::EnemyAttackObject
    {
    public:
        AttackObject();
        ~AttackObject();
		// エフェクシア
        mslib::object::Effekseer * GetEffekseerObject();
		// 対象の登録
        void SetTarget(mslib::transform::Transform * target, mslib::transform::Transform * character);
        /**
        @brief 更新
        */
        virtual void Update() override;
    protected:
        mslib::object::Effekseer * m_EffekseerObj; // エフェクトデータ
        mslib::transform::Transform * m_target; // 対象
        common::Character * m_Character; // キャラ
        bool m_flag; // 初期化フラグ
        int m_time; // 時間
    };
}