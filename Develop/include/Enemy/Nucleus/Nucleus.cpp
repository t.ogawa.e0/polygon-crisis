//==========================================================================
// コア型エネミー [Nucleus.h]
// author: tatsuya ogawa
//==========================================================================
#include "Nucleus.h"
#include "NucleusAI.h"
#include "Asset/TPSCamera.h"
#include "Asset/LightObject.h"
#include "Asset/ControllerObject.h"
#include "Asset/ShaderModel.h"
#include "Asset/Character.h"
#include "Asset/ActionRange.h"
#include "Enemy/EnemyManager.h"
#include "resource_list.h"

namespace enemy
{
	std::list<NucleusData*> NucleusData::m_data;

    Nucleus::Nucleus() : ObjectManager("NucleusObject")
    {
        m_Init_Call = 0;
        m_NumMaxEnemy = 0;
        m_CreatePoint = 0;
        m_NumEnemy = 0;
        m_init = false;
        m_bCommonObjectActivity = false;
        m_CommonBox = nullptr;
        m_CommonEnemyBox = nullptr;
        m_CommonTargetObject = nullptr;
        m_Common_attack_object_position = nullptr;
		AddActivity();
    }

    Nucleus::Nucleus(int NumMaxEnemy, int CreatePoint) : ObjectManager("NucleusObject")
    {
        m_Init_Call = 0;
        m_NumMaxEnemy = NumMaxEnemy;
        m_CreatePoint = CreatePoint;
        m_NumEnemy = 0;
        m_init = false;
        m_bCommonObjectActivity = false;
        m_CommonBox = nullptr;
        m_CommonEnemyBox = nullptr;
        m_CommonTargetObject = nullptr;
        m_Common_attack_object_position = nullptr;
        m_GeneratedCoordinates.reserve(CreatePoint);
		AddActivity();
    }

    Nucleus::~Nucleus()
    {
    }
    void Nucleus::Init()
    {
        m_LightObject = GetParent()->GetComponent<common::LightObject>()->Light()->Get(0);
        m_CameraObject = GetParent()->GetComponent<common::TPSCamera>()->Camera()->Get(0);
        auto action_range = GetParent()->GetComponent<common::ActionRange>();

        auto model_data = GetXModelLoader()->Load(RESOURCE_Nucleus1_x);
        auto efect_data = GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx);

        // Nucleusの統括
        auto NucleusComponent = Object()->Create();
        NucleusComponent->SetComponentName("NucleusComponent");

        // 親
        auto object = NucleusComponent->AddComponent<mslib::object::Object>();
        object->SetComponentName("NucleusCommonObject");
        object->SetMatrixType(mslib::object::MatrixType::Local);
        m_CommonBox = object->AddComponent<mslib::collider::BoxCollider>();
        m_CommonEnemyBox = object->AddComponent<mslib::collider::BoxCollider>();
        m_CommonTargetObject = object->AddComponent<mslib::object::Object>();
        m_Common_attack_object_position = object->AddComponent<mslib::object::Object>();
        m_CommonEnemyBox->SetComponentName("CommonEnemyBox");
        m_CommonTargetObject->SetComponentName("CommonTargetObject");
        m_Common_attack_object_position->SetComponentName("CommonAttackObjectPosition");

        // モデル
        auto model = object->AddComponent<ShaderModel::PerPixelLightingModel>();
        model->SetComponentName("NucleusModel");
        model->SetXModelData(model_data);
        model->SetEffectData(efect_data);
        model->SetCamera(m_CameraObject->GetCamera());
        model->SetLight(m_LightObject);
        m_CommonObject = EnemyData(object, model);

        // 出現座標の設定
        for (int i = 0; i < m_CreatePoint; i++)
        {
            auto po = NucleusComponent->AddComponent<mslib::object::Object>();
            po->SetComponentName("GeneratedCoordinates" + std::to_string(i));
            po->SetMatrixType(mslib::object::MatrixType::Local);
            m_GeneratedCoordinates.push_back(po);
        }

        // エネミーを生成します
        for (int i = 0; i < m_NumMaxEnemy; i++)
        {
            auto obj = NucleusComponent->AddComponent<mslib::object::Object>();

            obj->SetComponentName("Nucleus" + std::to_string(i));
            obj->SetMatrixType(mslib::object::MatrixType::Local);

            auto mod = obj->AddComponent<ShaderModel::PerPixelLightingModel>();
            mod->SetComponentName("NucleusModel");
            mod->SetXModelData(model_data);
            mod->SetEffectData(efect_data);
            mod->SetCamera(m_CameraObject->GetCamera());
            mod->SetLight(m_LightObject);

            auto pEnemyCharacter = obj->AddComponent<common::EnemyCharacter>();
            auto ai = obj->AddComponent<NucleusAI>();
			auto di = mod->AddComponent<mslib::collider::DistanceCollider>();

            ai->SetActionRangeData(action_range->GetAllActionRangeData());

			obj->AddComponent<EnemyManager>()->InitAll();
			obj->AddComponent<NucleusData>(obj, mod, di, ai, pEnemyCharacter);
        }
    }
    void Nucleus::Update()
    {
        CommonObjectInit();

        for (auto & itr : NucleusData::GetAllNucleusData())
        {
            auto pEnemyCharacter = mslib::cast<common::EnemyCharacter>(itr->character);
            auto ai = mslib::cast<NucleusAI>(itr->ai);

            // コリジョン処理
            pEnemyCharacter->Collider();

            // 壁との判定が出た
            if (pEnemyCharacter->GetVersusWallBox()->Trigger())
            {
                ai->ResetLoot();
                ai->StopAction();
				itr->obj->AddRotation(mslib::ToRadian(180.0f), 0, 0);
            }

            // お互いが衝突しあった
            if (itr->distance->Trigger())
            {
                //itr.repulsion -= 0.5f;
                //ai->ResetLoot();
                //ai->StopAction();
            }

            // 反発
            if (itr->repulsion < 0)
            {
                itr->obj->AddPosition(0, 0, itr->repulsion);
                itr->repulsion += 0.3f;
            }

            pEnemyCharacter->SetParameter(itr->obj);
        }
    }
    void Nucleus::Debug()
    {
        if (ImGui()->Checkbox("CommonObjectActivity", m_bCommonObjectActivity))
        {
            auto common_model = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.model);
            if (common_model != nullptr)
            {
                common_model->SetActivity(m_bCommonObjectActivity);
            }
            auto common_obj = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.obj);
            if (common_obj != nullptr)
            {
                common_obj->SetActivity(m_bCommonObjectActivity);
            }
        }
    }
    void Nucleus::CommonObjectInit()
    {
        if (m_init)return;

        m_Init_Call++;
        if (m_Init_Call != 30)return;

        auto common_model = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.model);
        if (common_model != nullptr)
        {
            common_model->SetActivity(false);
        }
        auto common_obj = dynamic_cast<mslib::renderer::Renderer*>(m_CommonObject.obj);
        if (common_obj != nullptr)
        {
            common_obj->SetActivity(false);
        }

        int id = 0;
        for (auto & itr : NucleusData::GetAllNucleusData())
        {
            auto it = m_GeneratedCoordinates[id];
            auto ai = mslib::cast<NucleusAI>(itr->ai);
            auto pEnemyCharacter = mslib::cast<common::EnemyCharacter>(itr->character);

            itr->model->SetParameter(*m_CommonObject.model->GetParameter());
            itr->obj->SetParameter(*it->GetParameter());
            ai->ResetLoot();
            ai->GetViewingRangeBox()->Copy(m_CommonBox);
            ai->SetAttackObject(m_Common_attack_object_position);
            //pEnemyCharacter->GetEnemyCollider()->Copy(m_CommonEnemyBox);
            pEnemyCharacter->GetTargetObject()->SetParameter(*m_CommonTargetObject->GetParameter());

            id++;
            for (auto & itr2 : NucleusData::GetAllNucleusData())
            {
                if (itr2->distance == itr->distance)continue;

                itr->distance->AddCollider(itr2->distance);
                itr->distance->DeterminationDistance(10);
            }
        }

        m_init = true;
    }
	NucleusData::NucleusData()
	{
		SetComponentName("NucleusData");
		m_data.push_back(this);
	}
	NucleusData::NucleusData(mslib::transform::Transform * o, mslib::transform::Transform * m) 
		: EnemyData(o, m)
	{
		SetComponentName("NucleusData");
		m_data.push_back(this);
	}
	NucleusData::NucleusData(mslib::transform::Transform * o, mslib::transform::Transform * m, mslib::collider::DistanceCollider * d, void * a, void * c) 
		: EnemyData(o, m, d, a, c)
	{
		SetComponentName("NucleusData");
		m_data.push_back(this);
	}
	NucleusData::~NucleusData()
	{
		auto itr = std::find(m_data.begin(), m_data.end(), this);
		if (itr != m_data.end())
			m_data.erase(itr);
	}
	std::list<NucleusData*>& NucleusData::GetAllNucleusData()
	{
		return m_data;
	}
}