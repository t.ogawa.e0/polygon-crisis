//==========================================================================
// エネミーの共通 [CommonEnemy.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CommonEnemy.h"

namespace enemy
{
    EnemyData::EnemyData()
    {
        obj = nullptr;
        model = nullptr;
        ai = nullptr;
        character = nullptr;
        distance = nullptr;
    }
    EnemyData::EnemyData(mslib::transform::Transform * o, mslib::transform::Transform * m)
    {
        obj = o;
        model = m;
        ai = nullptr;
        character = nullptr;
        distance = nullptr;
    }
    EnemyData::EnemyData(mslib::transform::Transform * o, mslib::transform::Transform * m, mslib::collider::DistanceCollider * d, void * a, void * c)
    {
        obj = o;
        model = m;
        ai = a;
        character = c;
        distance = d;
    }
    EnemyData::~EnemyData()
    {
    }
}