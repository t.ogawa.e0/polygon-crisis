//==========================================================================
// Menu [Menu.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Title
{
	class Menu : public mslib::ObjectManager
	{
	public:
		Menu();
		~Menu();
		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
		void ActivityFalse();
	protected:
		mslib::xinput::XInput * m_ControllerObject;
		mslib::sprite::Sprite * m_PlayFrame;
		mslib::sprite::Sprite * m_ExitFrame;
		int m_index_id;
		bool m_init;
		int m_se_label;
	};
}
