//==========================================================================
// PlayerObject [PlayerObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlayerObject.h"
#include "CameraObject.h"
#include "Asset/LightObject.h"
#include "Asset/ShaderModel.h"
#include "Asset/AnimationModel.h"
#include "Player/SetAnimation.h"
#include "config.h"
#include "resource_list.h"

namespace Title
{
	PlayerObject::PlayerObject() : ObjectManager("PlayerObject")
	{
	}

	PlayerObject::~PlayerObject()
	{
	}
	void PlayerObject::Init()
	{
		// 外部からオブジェクトを取得
		auto light = GetParent()->GetComponent<common::LightObject>()->Light();
		auto camera = GetParent()->GetComponent<CameraObject>()->Camera();
		auto obj = Object()->Create();

		obj->SetComponentName("PlayerObject");

		auto pAnimation = obj->AddComponent<PlayerAnimation::SetAnimation>();

		auto model = obj->AddComponent<animation_model::AnimationShaderModel>();
		model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
		model->SetCamera(camera->Get(0)->GetCamera());
		model->SetLight(light->Get(0));
		model->SetComponentName("HumanMotion");

		model->AddParts2("Body", GetXModelLoader()->Load(PlayerAnimation::PartsName::Body));
		model->AddParts2("Head", GetXModelLoader()->Load(PlayerAnimation::PartsName::Head), "Body");
		model->AddParts2("Thigh_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Thigh_L), "Body");
		model->AddParts2("Calf_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Calf_L), "Thigh_L");
		model->AddParts2("Foot_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Foot_L), "Calf_L");
		model->AddParts2("Thigh_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Thigh_R), "Body");
		model->AddParts2("Calf_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Calf_R), "Thigh_R");
		model->AddParts2("Foot_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Foot_R), "Calf_R");
		model->AddParts2("Arms_Parent_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Parent_L), "Body");
		model->AddParts2("Arms_Child_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Child_L), "Arms_Parent_L");
		model->AddParts2("Hand_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Hand_L), "Arms_Child_L");
		model->AddParts2("Arms_Parent_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Parent_R), "Body");
		model->AddParts2("Arms_Child_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Child_R), "Arms_Parent_R");
		auto hand = model->AddParts2("Hand_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Hand_R), "Arms_Child_R");

		model->Load(FolderStructure::__motion__ + model->GetComponentName());
		model->GenerateAnimation(PlayerAnimation::MotionName::Taiki);

		pAnimation->SetAnimationModel(model);

		auto pWeapon = hand->AddComponent<ShaderModel::PerPixelLightingModel>();
		pWeapon->SetXModelData(GetXModelLoader()->Load(RESOURCE_Weapon_x));
		pWeapon->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
		pWeapon->SetCamera(camera->Get(0)->GetCamera());
		pWeapon->SetLight(light->Get(0));
		pWeapon->SetComponentName("Weapon");
	}
	void PlayerObject::Update()
	{
	}
	void PlayerObject::Debug()
	{
	}
}