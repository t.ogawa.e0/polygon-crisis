//==========================================================================
// CameraObject [CameraObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Title
{
	class CameraObject : public mslib::ObjectManager
	{
	public:
		CameraObject();
		~CameraObject();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	};
}