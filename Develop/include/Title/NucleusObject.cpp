//==========================================================================
// NucleusObject [NucleusObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "NucleusObject.h"
#include "CameraObject.h"
#include "Asset/LightObject.h"
#include "Asset/ShaderModel.h"
#include "config.h"
#include "resource_list.h"

namespace Title
{
	NucleusObject::NucleusObject() : ObjectManager("NucleusObject")
	{
	}

	NucleusObject::~NucleusObject()
	{
	}
	void NucleusObject::Init()
	{
		auto light = GetParent()->GetComponent<common::LightObject>()->Light();
		auto camera = GetParent()->GetComponent<CameraObject>()->Camera();
		auto obj = Object()->Create();

		obj->SetComponentName("NucleusObject");

		// ���f��
		auto model = obj->AddComponent<ShaderModel::PerPixelLightingModel>();
		model->SetXModelData(GetXModelLoader()->Load(RESOURCE_Nucleus1_x));
		model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
		model->SetCamera(camera->Get(0)->GetCamera());
		model->SetLight(light->Get(0));
		model->SetComponentName("NucleusModel");
	}
	void NucleusObject::Update()
	{
	}
	void NucleusObject::Debug()
	{
	}
}