//==========================================================================
// TorchkaObject [TorchkaObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TorchkaObject.h"
#include "CameraObject.h"
#include "Asset/LightObject.h"
#include "Asset/ShaderModel.h"
#include "config.h"
#include "resource_list.h"

namespace Title
{
	TorchkaObject::TorchkaObject() : ObjectManager("TorchkaObject")
	{
	}

	TorchkaObject::~TorchkaObject()
	{
	}

	void TorchkaObject::Init()
	{
		auto light = GetParent()->GetComponent<common::LightObject>()->Light();
		auto camera = GetParent()->GetComponent<CameraObject>()->Camera();
		auto obj = Object()->Create();

		obj->SetComponentName("TorchkaObject");

		// ���f��
		auto model = obj->AddComponent<ShaderModel::PerPixelLightingModel>();
		model->SetXModelData(GetXModelLoader()->Load(RESOURCE_Torchka_x));
		model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
		model->SetCamera(camera->Get(0)->GetCamera());
		model->SetLight(light->Get(0));
		model->SetComponentName("TorchkaModel");
	}
	void TorchkaObject::Update()
	{
	}
	void TorchkaObject::Debug()
	{
	}
}