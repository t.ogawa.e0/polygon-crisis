//==========================================================================
// PressAnyKey [PressAnyKey.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PressAnyKey.h"
#include "Asset/ControllerObject.h"
#include "Menu.h"
#include "resource_list.h"

namespace Title
{
	PressAnyKey::PressAnyKey() : ObjectManager("PressAnyKey")
	{
		m_ControllerObject = nullptr;
		m_flag = false;
		m_se_label = 0;
	}

	PressAnyKey::~PressAnyKey()
	{
	}
	void PressAnyKey::Init()
	{
		auto obj = Sprite()->Create();

		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_PressAnyKey_DDS));
		obj->SetComponentName("PressAnyKeySprite");

		auto sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_select_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label);

		m_ControllerObject = GetParent()->GetComponent<common::ControllerObject>()->XInput_();

		m_XInputButton.push_back(mslib::xinput::XInputButton::X);
		m_XInputButton.push_back(mslib::xinput::XInputButton::Y);
		m_XInputButton.push_back(mslib::xinput::XInputButton::A);
		m_XInputButton.push_back(mslib::xinput::XInputButton::B);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_UP);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_DOWN);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_LEFT);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_RIGHT);
		m_XInputButton.push_back(mslib::xinput::XInputButton::START);
		m_XInputButton.push_back(mslib::xinput::XInputButton::BACK);
		m_XInputButton.push_back(mslib::xinput::XInputButton::LEFT_THUMB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::RIGHT_THUMB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::LEFT_LB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::RIGHT_RB);
	}
	void PressAnyKey::Update()
	{
		constexpr int index = 0;

		if (!m_ControllerObject->Check(index))return;
		for (auto & itr : m_XInputButton)
		{
			if (m_ControllerObject->Trigger(itr, index))
			{
				XAudio()->Play(m_se_label);
				m_flag = true;
			}
		}

		ControllColor();
	}
	void PressAnyKey::Debug()
	{
	}
	void PressAnyKey::Re()
	{
		SetActivity(true);
		m_flag = false;
	}
	void PressAnyKey::ControllColor()
	{
		auto obj = Sprite()->Get(0);

		// ���l���Z
		if (m_flag)
		{
			auto color = obj->GetColor();

			obj->AddColor(0, 0, 0, -0.1f);
			if (0.0f < obj->GetColor()->a)return;
			obj->SetColor(color->r, color->g, color->b, 0);
			SetActivity(false);
			GetParent()->GetComponent<Menu>()->ActivityFalse();
		}
		// ���l���Z 
		else
		{
			auto color = obj->GetColor();

			obj->AddColor(0, 0, 0, 0.1f);
			if (obj->GetColor()->a < 1.0f)return;
			obj->SetColor(color->r, color->g, color->b, 1);
		}
	}
}