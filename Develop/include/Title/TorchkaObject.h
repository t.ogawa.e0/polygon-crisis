//==========================================================================
// TorchkaObject [TorchkaObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Title
{
	class TorchkaObject : public mslib::ObjectManager
	{
	public:
		TorchkaObject();
		~TorchkaObject();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	};
}
