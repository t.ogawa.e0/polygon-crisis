//==========================================================================
// タイトルシーン [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "config.h"
#include "CameraObject.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "Asset/TPSCamera.h"
#include "Player/Player.h"
#include "Asset/LightObject.h"
#include "Asset/Map.h"
#include "Asset/SkyDome.h"
#include "Asset/ControllerObject.h"
#include "Asset/ThankYouForPlaying.h"
#include "Asset/xBox_Controller.h"
#include "PlayerObject.h"
#include "Uaz469Object.h"
#include "NucleusObject.h"
#include "TorchkaObject.h"
#include "TitleName.h"
#include "PressAnyKey.h"
#include "Menu.h"
#include "Asset/BGMObject.h"
#include "resource_list.h"

namespace Title
{
    Scene::Scene() : BaseScene("Title", SceneLevel::__Title__)
    {
		AddComponent<common::ControllerObject>(1);
		AddComponent<common::LightObject>();
		AddComponent<common::TPSCamera>();
		AddComponent<Editor::EditorCamera>();
		AddComponent<CameraObject>();
		AddComponent<PlayerObject>();
		AddComponent<Uaz469Object>();
		AddComponent<NucleusObject>();
		AddComponent<TorchkaObject>();
		AddComponent<common::Map>();
		AddComponent<common::SkyDome>();
		AddComponent<TitleName>();
		AddComponent<PressAnyKey>();
		AddComponent<common::xBox_Controller>();
		AddComponent<Menu>();
		AddComponent<common::BGMObject>(RESOURCE_title_bgm_wav);
    }

    Scene::~Scene()
    {
    }
}