//==========================================================================
// Uaz469Object [Uaz469Object.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Title
{
	class Uaz469Object : public mslib::ObjectManager
	{
	public:
		Uaz469Object();
		~Uaz469Object();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	};
}
