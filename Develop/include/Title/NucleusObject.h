//==========================================================================
// NucleusObject [NucleusObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Title
{
	class NucleusObject : public mslib::ObjectManager
	{
	public:
		NucleusObject();
		~NucleusObject();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	};
}