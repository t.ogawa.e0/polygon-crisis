//==========================================================================
// Menu [Menu.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Menu.h"
#include "Asset/ControllerObject.h"
#include "Asset/xBox_Controller.h"
#include "System/Screen.h"
#include "Stage1/Scene.h"
#include "resource_list.h"

namespace Title
{
	Menu::Menu() : ObjectManager("Menu")
	{
		m_ControllerObject = nullptr;
		m_PlayFrame = nullptr;
		m_ExitFrame = nullptr;
		m_init = false;
		m_index_id = 0;
		m_se_label = 0;
	}

	Menu::~Menu()
	{
	}
	void Menu::Init()
	{
		//SetActivity(false);

		auto & WinSize = GetDevice()->GetWindowsSize();
		m_ControllerObject = GetParent()->GetComponent<common::ControllerObject>()->XInput_();

		auto obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Play_DDS));
		obj->SetComponentName("PlaySprite");

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_PlayFrame_DDS));
		obj->SetComponentName("PlayFrameSprite");
		m_PlayFrame = obj;

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Exit_DDS));
		obj->SetComponentName("ExitSprite");

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_ExitFrame_DDS));
		obj->SetComponentName("ExitFrame");
		m_ExitFrame = obj;

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));
		obj->SetComponentName("Background");
		obj->SetPolygonSize((float)WinSize.x, (float)WinSize.y);
		obj->SetPriority(-5);

		auto sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_select_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label);
	}
	void Menu::Update()
	{
		constexpr int index = 0;

		if (m_init == false)
		{
			for (int i = 0; i < Sprite()->Size(); i++)
				Sprite()->Get(i)->SetActivity(false);
			SetActivity(false);
			m_PlayFrame->SetActivity(false);
			m_ExitFrame->SetActivity(false);
			auto com = GetParent()->GetComponent<common::xBox_Controller>();
			if (com != nullptr)
				com->ActivityFlag(false);
			m_init = true;
		}

		if (!m_ControllerObject->Check(index))return;
		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::DPAD_UP, index))
		{
			m_index_id = 0;
			m_PlayFrame->SetActivity(true);
			m_ExitFrame->SetActivity(false);
			XAudio()->Play(m_se_label);
		}
		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::DPAD_DOWN, index))
		{
			m_index_id = 1;
			m_PlayFrame->SetActivity(false);
			m_ExitFrame->SetActivity(true);
			XAudio()->Play(m_se_label);
		}
		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::B, index))
		{
			XAudio()->Play(m_se_label);
			if (m_index_id == 0)
			{
				CScreen::ScreenChange(new Stage1::Scene);
			}
			else if (m_index_id == 1)
			{
				DestroyWindow(GetDevice()->GetHwnd());
			}
		}
	}
	void Menu::Debug()
	{
	}
	void Menu::ActivityFalse()
	{
		m_index_id = 0;
		for (int i = 0; i < Sprite()->Size(); i++)
			Sprite()->Get(i)->SetActivity(true);
		SetActivity(true);
		m_PlayFrame->SetActivity(true);
		m_ExitFrame->SetActivity(false);
		auto com = GetParent()->GetComponent<common::xBox_Controller>();
		if (com != nullptr)
			com->ActivityFlag(true);
	}
}