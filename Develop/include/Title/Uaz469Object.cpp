//==========================================================================
// Uaz469Object [Uaz469Object.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Uaz469Object.h"
#include "CameraObject.h"

#include "Asset/LightObject.h"
#include "Asset/ShaderModel.h"
#include "Asset/AnimationModel.h"
#include "Enemy/Uaz469/Uaz469.h"
#include "config.h"
#include "resource_list.h"

namespace Title
{
	Uaz469Object::Uaz469Object() : ObjectManager("Uaz469Object")
	{
	}


	Uaz469Object::~Uaz469Object()
	{
	}

	void Uaz469Object::Init()
	{
		// 外部からオブジェクトを取得
		auto light = GetParent()->GetComponent<common::LightObject>()->Light();
		auto camera = GetParent()->GetComponent<CameraObject>()->Camera();
		auto obj = Object()->Create();

		obj->SetComponentName("Uaz469Object");
		auto pAnimation = obj->AddComponent<enemy::Uaz469AIAnimation>();

		auto model = obj->AddComponent<animation_model::AnimationShaderModel>();
		model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
		model->SetCamera(camera->Get(0)->GetCamera());
		model->SetLight(light->Get(0));
		model->SetComponentName("UAZ469");

		model->AddParts2("Body", GetXModelLoader()->Load(RESOURCE_UAZ469_Body_x));

		model->AddParts2("1_ParentLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("1_ChildLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "1_ParentLegR");

		model->AddParts2("2_ParentLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("2_ChildLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "2_ParentLegL");

		model->AddParts2("3_ParentLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("3_ChildLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "3_ParentLegR");

		model->AddParts2("4_ParentLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("4_ChildLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "4_ParentLegL");

		model->Load(FolderStructure::__motion__ + model->GetComponentName());
		model->GenerateAnimation("Taiki");

		pAnimation->SetAnimationModel(model);
	}
	void Uaz469Object::Update()
	{
	}
	void Uaz469Object::Debug()
	{
	}
}