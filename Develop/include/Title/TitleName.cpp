//==========================================================================
// TitleName [TitleName.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TitleName.h"
#include "Asset/ControllerObject.h"
#include "resource_list.h"

namespace Title
{
	TitleName::TitleName() : ObjectManager("TitleName")
	{
		m_ControllerObject = nullptr;
		m_flag = false;
	}

	TitleName::~TitleName()
	{
	}

	void TitleName::Init()
	{
		auto obj = Sprite()->Create();

		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_title_DDS));
		obj->SetComponentName("TitleSprite");

		m_ControllerObject = GetParent()->GetComponent<common::ControllerObject>()->XInput_();

		m_XInputButton.push_back(mslib::xinput::XInputButton::X);
		m_XInputButton.push_back(mslib::xinput::XInputButton::Y);
		m_XInputButton.push_back(mslib::xinput::XInputButton::A);
		m_XInputButton.push_back(mslib::xinput::XInputButton::B);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_UP);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_DOWN);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_LEFT);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_RIGHT);
		m_XInputButton.push_back(mslib::xinput::XInputButton::START);
		m_XInputButton.push_back(mslib::xinput::XInputButton::BACK);
		m_XInputButton.push_back(mslib::xinput::XInputButton::LEFT_THUMB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::RIGHT_THUMB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::LEFT_LB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::RIGHT_RB);
	}

	void TitleName::Update()
	{
		constexpr int index = 0;

		if (!m_ControllerObject->Check(index))return;
		for (auto & itr : m_XInputButton)
		{
			if (m_ControllerObject->Trigger(itr, index))
				m_flag = true;
		}

		ControllColor();
	}

	void TitleName::Debug()
	{

	}
	void TitleName::Re()
	{
		SetActivity(true);
		m_flag = false;
	}
	void TitleName::ControllColor()
	{
		auto obj = Sprite()->Get(0);

		// ���l���Z
		if (m_flag)
		{
			auto color = obj->GetColor();

			obj->AddColor(0, 0, 0, -0.1f);
			if (0.0f < obj->GetColor()->a)return;
			obj->SetColor(color->r, color->g, color->b, 0);
			SetActivity(false);
		}
		// ���l���Z 
		else
		{
			auto color = obj->GetColor();

			obj->AddColor(0, 0, 0, 0.1f);
			if (obj->GetColor()->a < 1.0f)return;
			obj->SetColor(color->r, color->g, color->b, 1);
		}
	}
}