//==========================================================================
// PressAnyKey [PressAnyKey.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Title
{
	class PressAnyKey : public mslib::ObjectManager
	{
	public:
		PressAnyKey();
		~PressAnyKey();
		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	public:
		void Re();
	protected:
		void ControllColor();
	protected:
		mslib::xinput::XInput * m_ControllerObject;
		std::list<mslib::xinput::XInputButton> m_XInputButton;
		bool m_flag;
		int m_se_label;
	};
}