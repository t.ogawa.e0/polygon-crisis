//==========================================================================
// CameraObject [CameraObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CameraObject.h"

namespace Title
{
	CameraObject::CameraObject() : ObjectManager("CameraObject")
	{
	}

	CameraObject::~CameraObject()
	{
	}

	void CameraObject::Init()
	{
		D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 1.2f, -3.0f); // 注視点
		D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 1.2f, 0.0f); // カメラ座標

		auto obj1 = Camera()->Create();
		obj1->Init(Eye, At);
		obj1->SetComponentName(mslib::text("%p", obj1));
		obj1->IsMainCamera();

		auto obj2 = Object()->Create();
		obj2->SetScale(-0.8f, -0.8f, -0.8f);
		obj2->SetComponentName(mslib::text("%p", obj2));
	}

	void CameraObject::Update()
	{

	}

	void CameraObject::Debug()
	{

	}
}