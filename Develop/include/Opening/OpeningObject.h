//==========================================================================
// OpeningObject [OpeningObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Opening
{
	class OpeningObject : public mslib::ObjectManager
	{
		struct __OP {
			__OP(mslib::sprite::Sprite * s, int t) {
				sprite = s;
				time = t;
				m_start = false;
				m_end = false;
			}
			mslib::sprite::Sprite * sprite = nullptr;
			int time = 0;
			bool m_start = false;
			bool m_end = false;
		};
	public:
		OpeningObject();
		~OpeningObject();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	private:
		std::vector<__OP> m_sprite;
		int m_now_op;
		bool m_init;
	};
}