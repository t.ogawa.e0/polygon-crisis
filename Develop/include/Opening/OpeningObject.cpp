//==========================================================================
// OpeningObject [OpeningObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "OpeningObject.h"
#include "System/Screen.h"
#include "Title/Scene.h"
#include "resource_list.h"

namespace Opening
{
	constexpr int __NumOP__ = 3; // OPの枚数
	OpeningObject::OpeningObject() : ObjectManager("OpeningObject")
	{
		m_sprite.reserve(__NumOP__);
		m_now_op = 0;
		m_init = false;
	}

	OpeningObject::~OpeningObject()
	{
	}
	void OpeningObject::Init()
	{
		auto & WinSize = GetDevice()->GetWindowsSize();

		auto obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Opening1_DDS));
		obj->SetComponentName("Opening1");
		m_sprite.push_back(__OP(obj, mslib::Seconds(3)));

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Opening2_DDS));
		obj->SetComponentName("Opening2");
		m_sprite.push_back(__OP(obj, mslib::Seconds(2)));

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Opening3_DDS));
		obj->SetComponentName("Opening3");
		m_sprite.push_back(__OP(obj, mslib::Seconds(2)));

		// 色無効
		for (auto & itr : m_sprite)
		{
			itr.sprite->SetColor(1, 1, 1, 0);
		}

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));
		obj->SetComponentName("Background");
		obj->SetPolygonSize((float)WinSize.x, (float)WinSize.y);
		obj->SetColor(0, 0, 0, 1);
	}
	void OpeningObject::Update()
	{
		int __end_count = 0; // 終了カウント

		// 初期化
		if (m_init == false)
		{
			// 色無効
			for (auto & itr : m_sprite)
			{
				itr.sprite->SetColor(1, 1, 1, 0);
				itr.sprite->SetActivity(false);
				itr.sprite->SetPriority(500);
			}
			m_init = true;
		}

		for (auto & itr : m_sprite)
		{
			// 既に処理が終わっている
			if (itr.m_start && itr.m_end)
			{
				__end_count++;
				continue;
			}

			// 開始されていない
			if (!itr.m_start) 
			{
				itr.sprite->SetActivity(true);

				// 色更新
				itr.sprite->AddColor(0, 0, 0, 0.05f);

				// 最大値ではないので終わり
				if (itr.sprite->GetColor()->a < 1.0f)break;

				// 最適化
				itr.sprite->SetColor(1, 1, 1, 1);
				itr.m_start = true;
			}

			// タイマー更新
			itr.time--;

			// 終了値に達していない
			if (0 < itr.time)break;

			// 色更新
			itr.sprite->AddColor(0, 0, 0, -0.05f);

			// 最小値ではないので終わり
			if (0 < itr.sprite->GetColor()->a)break;

			// 終了判定
			itr.m_end = true;
			itr.sprite->SetActivity(false);
			break;
		}

		// 実行された回数が一致
		if ((int)m_sprite.size() == __end_count)
		{
			CScreen::ScreenChange(new Title::Scene);
			SetActivity(false);
		}
	}
	void OpeningObject::Debug()
	{
	}
}