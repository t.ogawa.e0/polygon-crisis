//==========================================================================
// Opening [Opening.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "OpeningObject.h"
#include "config.h"
#include "resource_list.h"

namespace Opening
{
    Scene::Scene() : BaseScene("Opening", FolderStructure::__level__ + std::string("Opening"))
    {
		AddComponent<OpeningObject>();
    }

    Scene::~Scene()
    {
    }
}