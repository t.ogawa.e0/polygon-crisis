//==========================================================================
// コリジョンのチェック [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "CheckCollider.h"
#include "config.h"

namespace CheckCollider
{
    Scene::Scene() : BaseScene("CheckCollider", SceneLevel::__CheckCollider__)
    {
        AddComponent<Editor::EditorCamera>();
        AddComponent<Editor::EditorGrid>();
        AddComponent<CheckCollider>();
    }

    Scene::~Scene()
    {
    }
}