//==========================================================================
// コリダーのチェック [CheckCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CheckCollider.h"

namespace CheckCollider
{
    CheckCollider::CheckCollider() : ObjectManager("CheckCollider")
    {
    }

    CheckCollider::~CheckCollider()
    {
        m_obj.clear();
    }

    void CheckCollider::Init()
    {
        auto obj1 = AddComponent<mslib::object::Object>();
        auto collider1 = obj1->AddComponent<mslib::collider::DistanceCollider>();
        obj1->AddPosition(0, 0, 2);
        m_obj.push_back(obj1);

        auto obj2 = AddComponent<mslib::object::Object>();
        auto collider2 = obj2->AddComponent<mslib::collider::PlaneCollider>();
        obj2->AddPosition(0, 0.1f, 0);
        obj2->AddRotation(0, mslib::ToRadian(120.0f), 0);
        m_obj.push_back(obj2);

        auto obj3 = AddComponent<mslib::object::Object>();
        auto collider3 = obj3->AddComponent<mslib::collider::DistanceCollider>();
        obj3->AddPosition(3, 0, 0);
        m_obj.push_back(obj3);

        auto obj4 = AddComponent<mslib::object::Object>();
        auto collider4 = obj4->AddComponent<mslib::collider::BoxCollider>();
        obj4->AddPosition(-3, 0, 0);
        obj4->AddRotation(mslib::ToRadian(45.0f), mslib::ToRadian(45.0f), 0);
        m_obj.push_back(obj4);

        collider2->AddCollider(collider1);
        collider1->AddCollider(collider2);
        collider1->AddCollider(collider3);
        collider3->AddCollider(collider1);
        collider4->AddCollider(collider1);
    }

    void CheckCollider::Update()
    {
        auto *obj = m_obj[1];

        ControlObject(0);

        obj->AddRotation(0, mslib::ToRadian(90.0f / 60.0f), 0);
    }

    void CheckCollider::Debug()
    {

    }
    void CheckCollider::ControlObject(int label)
    {
        if ((int)m_obj.size() <= label)return;

        auto obj = m_obj[label];

        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_A))
        {
            obj->AddPosition(-0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_D))
        {
            obj->AddPosition(0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_W))
        {
            obj->AddPosition(0, 0, 0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_S))
        {
            obj->AddPosition(0, 0, -0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Q))
        {
            obj->AddRotation(-0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_E))
        {
            obj->AddRotation(0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_R))
        {
            obj->AddRotation(0, -0.05f, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_F))
        {
            obj->AddRotation(0, 0.05f, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Z))
        {
            obj->AddRotation(0, 0, 0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_C))
        {
            obj->AddRotation(0, 0, -0.05f);
        }
    }
}