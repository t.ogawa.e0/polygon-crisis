//==========================================================================
// モーションシーン [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "Editor/Motion/MotionEditor.h"
#include "config.h"

namespace Motion
{
    Scene::Scene() : BaseScene("MotionEditor", SceneLevel::__MotionEditor__)
    {
        AddComponent<Editor::EditorCamera>();
        AddComponent<Editor::EditorGrid>();
        AddComponent<MotionEditor>();
    }

    Scene::~Scene()
    {
    }
}