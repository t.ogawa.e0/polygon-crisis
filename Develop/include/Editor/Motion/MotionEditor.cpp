//==========================================================================
// モーションエディタ [MotionEditor.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "MotionEditor.h"
#include "Editor/common/EditorCamera.h"
#include "common/DefaultSystem.h"
#include "resource_list.h"
#include "config.h"

namespace Motion
{
	enum class floatcase : int
	{
		position,
		rotation,
		scale,
		max
	};

	constexpr const float __move_buff__ = 0.1f;
	constexpr const char* __motion_editor__ = "モーションエディタ";
	constexpr const char* __animation_editor__ = "アニメーションエディタ";
	constexpr const char* __Object__ = "Object";
	constexpr const char* __character_list__ = "キャラクターリスト";
	constexpr const char* __parts_list__ = "パーツリスト";
	constexpr const char* __motion_list__ = "モーションリスト";
	constexpr const char* __paren_object__ = "親オブジェクト";
	constexpr const char* __window_key_list__ = "ウィンドウキーのリスト";
	constexpr const char* __create_animation_data__ = "アニメーションデータ生成";

	constexpr const char* __default__ = "Default";
	constexpr const float __max__ = 8999999999.f;
	constexpr const float __min__ = -__max__;

	namespace PartsName
	{
		constexpr const char* Arms_Child_L = RESOURCE_Human_Arms_Child_L_x;
		constexpr const char* Arms_Child_R = RESOURCE_Human_Arms_Child_R_x;
		constexpr const char* Arms_Parent_L = RESOURCE_Human_Arms_Parent_L_x;
		constexpr const char* Arms_Parent_R = RESOURCE_Human_Arms_Parent_R_x;
		constexpr const char* Body = RESOURCE_Human_Body_x;
		constexpr const char* Calf_L = RESOURCE_Human_Calf_L_x;
		constexpr const char* Calf_R = RESOURCE_Human_Calf_R_x;
		constexpr const char* Foot_L = RESOURCE_Human_Foot_L_x;
		constexpr const char* Foot_R = RESOURCE_Human_Foot_R_x;
		constexpr const char* Hand_L = RESOURCE_Human_Hand_L_x;
		constexpr const char* Hand_R = RESOURCE_Human_Hand_R_x;
		constexpr const char* Head = RESOURCE_Human_Head_x;
		constexpr const char* Thigh_L = RESOURCE_Human_Thigh_L_x;
		constexpr const char* Thigh_R = RESOURCE_Human_Thigh_R_x;
	};

	// ptrが同じ場合trueが返る
	bool boolnullptr(void * ptr1, void * ptr2);


	MotionEditor::MotionEditor() : ObjectManager("MotionEditor")
	{
	}

	MotionEditor::~MotionEditor()
	{
	}

	/**
	@brief 初期化
	*/
	void MotionEditor::Init()
	{
		auto light = Light()->Create();
		light->Enable(0, true);
		light->SetType(D3DLIGHTTYPE::D3DLIGHT_DIRECTIONAL);

		m_EffectData = GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx);

		SetPlayerModel();
		//SetUAZ469Model();

		for (auto & itr : m_AnimationModel)
		{
			itr.second.model->Load(FolderStructure::__motion__ + itr.second.model->GetComponentName());
			if (itr.second.model->GetMotionData().find(__default__) == itr.second.model->GetMotionData().end())
			    itr.second.model->SetMotionData(__default__);
		}
	}

	/**
	@brief 更新
	*/
	void MotionEditor::Update()
	{
		Editor();
	}

	/**
	@brief デバッグ
	*/
	void MotionEditor::Debug()
	{
#if defined(_MSLIB_DEBUG)
		// ノードの表示
		ImGui()->Separator();
		ImGui()->Text("ノード");
		for (auto & itr : m_AnimationModel)
		{
			mslib::DefaultSystem::ObjectData(itr.second.model);
		}

		ImGui()->Separator();
		ImGui()->Checkbox(__window_key_list__, m_system.Get(__window_key_list__));
		ImGui()->Separator();
		if (m_system.Get(__window_key_list__))
		{
			ImGui()->NewWindow(__window_key_list__, m_system.Get(__window_key_list__), mslib::DefaultSystem::SystemWindowFlags);
			ImGui()->Separator();
			ImGui()->Text(__motion_editor__);
			ImGui()->Separator();
			ImGui()->Checkbox(__window_key_list__, m_system.Get(__window_key_list__));
			ImGui()->Separator();

			for (auto & itr : m_system.GetKeyList()) {
				ImGui()->Text(itr);
			}
			for (auto & itr : m_editor_system.GetKeyList()) {
				ImGui()->Text(itr);
			}
			ImGui()->EndWindow();
		}

		// エディタ有効キー
		if (ImGui()->Checkbox(__motion_editor__, m_system.Get(__motion_editor__)))
		{
			m_system.Get(__animation_editor__) = false;
		}
		if (ImGui()->Checkbox(__animation_editor__, m_system.Get(__animation_editor__)))
		{
			m_system.Get(__motion_editor__) = false;
		}
#endif
	}

	//==========================================================================
	// プレイヤーモデル
	void MotionEditor::SetPlayerModel()
	{
		auto CameraComponent = GetParent()->GetComponent<Editor::EditorCamera>();
		auto model = AddComponent<animation_model::AnimationShaderModel>();
		model->SetEffectData(m_EffectData);
		model->SetCamera(CameraComponent->GetCamera());
		model->SetLight(Light()->Get(0));
		model->SetComponentName("HumanMotion");

		m_AnimationModel[model->GetComponentName()].model = model;

		//==========================================================================
		// 体
		model->AddParts2("Body", GetXModelLoader()->Load(PartsName::Body));

		//==========================================================================
		// 体->頭
		model->AddParts2("Head", GetXModelLoader()->Load(PartsName::Head), "Body");

		//==========================================================================
		// 体->左ふともも
		model->AddParts2("Thigh_L", GetXModelLoader()->Load(PartsName::Thigh_L), "Body");

		//==========================================================================
		// 体->左ふともも->左ふくらはぎ
		model->AddParts2("Calf_L", GetXModelLoader()->Load(PartsName::Calf_L), "Thigh_L");

		//==========================================================================
		// 体->左ふともも->左ふくらはぎ->左足
		model->AddParts2("Foot_L", GetXModelLoader()->Load(PartsName::Foot_L), "Calf_L");

		//==========================================================================
		// 体->右ふともも
		model->AddParts2("Thigh_R", GetXModelLoader()->Load(PartsName::Thigh_R), "Body");

		//==========================================================================
		// 体->右ふともも->右ふくらはぎ
		model->AddParts2("Calf_R", GetXModelLoader()->Load(PartsName::Calf_R), "Thigh_R");

		//==========================================================================
		// 体->右ふともも->右ふくらはぎ->右足
		model->AddParts2("Foot_R", GetXModelLoader()->Load(PartsName::Foot_R), "Calf_R");

		//==========================================================================
		// 体->左腕(親)
		model->AddParts2("Arms_Parent_L", GetXModelLoader()->Load(PartsName::Arms_Parent_L), "Body");

		//==========================================================================
		// 体->左腕(親)->左腕(子)
		model->AddParts2("Arms_Child_L", GetXModelLoader()->Load(PartsName::Arms_Child_L), "Arms_Parent_L");

		//==========================================================================
		// 体->左腕(親)->左腕(子)->左手
		model->AddParts2("Hand_L", GetXModelLoader()->Load(PartsName::Hand_L), "Arms_Child_L");

		//==========================================================================
		// 体->右腕(親)
		model->AddParts2("Arms_Parent_R", GetXModelLoader()->Load(PartsName::Arms_Parent_R), "Body");

		//==========================================================================
		// 体->右腕(親)->右腕(子)
		model->AddParts2("Arms_Child_R", GetXModelLoader()->Load(PartsName::Arms_Child_R), "Arms_Parent_R");

		//==========================================================================
		// 体->右腕(親)->右腕(子)->右手
		auto hand = model->AddParts2("Hand_R", GetXModelLoader()->Load(PartsName::Hand_R), "Arms_Child_R");

		// 刀身
		auto CreateModel = hand->AddComponent<ShaderModel::PerPixelLightingModel>();
		CreateModel->SetEffectData(m_EffectData);
		CreateModel->SetCamera(CameraComponent->GetCamera());
		CreateModel->SetLight(Light()->Get(0));
		CreateModel->SetComponentName("CreateModel");
		CreateModel->SetXModelData(GetXModelLoader()->Load(RESOURCE_Weapon_x));
		CreateModel->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
	}

	void MotionEditor::SetUAZ469Model()
	{
		auto CameraComponent = GetParent()->GetComponent<Editor::EditorCamera>();
		auto model = AddComponent<animation_model::AnimationShaderModel>();
		model->SetEffectData(m_EffectData);
		model->SetCamera(CameraComponent->GetCamera());
		model->SetLight(Light()->Get(0));
		model->SetComponentName("UAZ469");

		m_AnimationModel[model->GetComponentName()].model = model;

		model->AddParts2("Body", GetXModelLoader()->Load(RESOURCE_UAZ469_Body_x));

		model->AddParts2("1_ParentLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("1_ChildLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "1_ParentLegR");

		model->AddParts2("2_ParentLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("2_ChildLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "2_ParentLegL");

		model->AddParts2("3_ParentLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("3_ChildLegR", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "3_ParentLegR");

		model->AddParts2("4_ParentLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ParentLeg_x), "Body");
		model->AddParts2("4_ChildLegL", GetXModelLoader()->Load(RESOURCE_UAZ469_ChildLeg_x), "4_ParentLegL");
	}

	void MotionEditor::Editor()
	{
		// アニメーションモデル毎の処理
		ModelMotionEditor();

		// アニメーションエディタ
		AnimationEditor();
	}

	//==========================================================================
	// アニメーションモデル毎の処理
	void MotionEditor::ModelMotionEditor()
	{
#if defined(_MSLIB_DEBUG)
		// エディタの処理
		if (!m_system.Get(__motion_editor__))return;

		// エディタ開始
		ImGui()->NewWindow(__motion_editor__, true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text(__motion_editor__);

		// キャラクターリスト
		CharacterList();

		// アニメーションモデル
		for (auto & itr : m_AnimationModel)
		{
			//==========================================================================
			// キャラごとに
			//==========================================================================
			auto WindowKey = mslib::text("%p", &itr);

			// キャラ毎の処理
			if (!m_system.Get(WindowKey))continue;

			//==========================================================================
			// ウィンドウ生成
			//==========================================================================
			ImGui()->NewWindow(WindowKey, true, mslib::DefaultSystem::SystemWindowFlags);
			ImGui()->Separator();
			ImGui()->Text(itr.second.model->GetComponentName());
			ImGui()->Separator();

			//==========================================================================
			// このウィンドウの表示flag
			//==========================================================================
			ImGui()->Separator();
			ImGui()->Checkbox(itr.second.model->GetComponentName(), m_system.Get(WindowKey));
			ImGui()->Separator();

			// 基礎データ
			BasicData(&itr.second);

			// モーションデータの有効化キー
			MotionActive(&itr.second);

			// モーションの生成
			CreateMotion(&itr.second);

			// モーションの線形データ修正の実行キー
			DisplayActiveData(&itr.second);

			// ポーズデータの表示
			PauseData(&itr.second);

			// 保存/読み取り処理
			SaveAndLoad(&itr.second);

			ImGui()->EndWindow();
		}

		// エディタ終了
		ImGui()->EndWindow();
#endif
	}

	//==========================================================================
	// transform内のパラメーターを操作する処理
	void MotionEditor::ParameterControl(mslib::transform::Transform*obj)
	{
#if defined(_MSLIB_DEBUG)
		if (obj == nullptr)return;
		auto strkey = mslib::text("%p", obj);
		if (!m_editor_system.Get(strkey))return;

		// パラメータを取り出す
		auto & position = obj->GetParameter()->position;
		auto & rotation = obj->GetParameter()->rotation;
		auto & scale = obj->GetParameter()->scale;

		// ウィンドウ生成
		ImGui()->NewWindow(strkey, true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text(obj->GetComponentName());
		ImGui()->Separator();

		// このウィンドウの表示flag
		ImGui()->Separator();
		ImGui()->Checkbox(obj->GetComponentName(), m_editor_system.Get(strkey));
		float f3[(int)floatcase::max][3] = { 0.0f };
		int access_key = 0;
		ImGui()->Separator();

		// ポジション操作
		ImGui()->Separator();
		access_key = (int)floatcase::position;
		ImGui()->_SliderFloat3(mslib::char_convert::multi_to_utf8_winapi(mslib::text("XYZ軸移動")).c_str(), f3[access_key], -__move_buff__, __move_buff__);
		obj->AddPosition(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));

		// 回転操作
		access_key = (int)floatcase::rotation;
		ImGui()->_SliderFloat3(mslib::char_convert::multi_to_utf8_winapi(mslib::text("XYZ軸回転")).c_str(), f3[access_key], -__move_buff__, __move_buff__);
		obj->AddRotation(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));

		// 拡縮操作
		access_key = (int)floatcase::scale;
		ImGui()->_SliderFloat3(mslib::char_convert::multi_to_utf8_winapi(mslib::text("XYZ軸拡縮")).c_str(), f3[access_key], -__move_buff__, __move_buff__);
		obj->AddScale(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
		ImGui()->Separator();

		// ポジション操作 
		ImGui()->Separator();
		access_key = (int)floatcase::position;
		f3[access_key][0] = position.x;
		f3[access_key][1] = position.y;
		f3[access_key][2] = position.z;
		if (ImGui()->_InputFloat3(mslib::char_convert::multi_to_utf8_winapi(mslib::text("XYZ軸移動 入力")).c_str(), f3[access_key], ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetPosition(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
		}

		// 回転操作
		access_key = (int)floatcase::rotation;
		f3[access_key][0] = mslib::ToDegree(rotation.x);
		f3[access_key][1] = mslib::ToDegree(rotation.y);
		f3[access_key][2] = mslib::ToDegree(rotation.z);
		if (ImGui()->_InputFloat3(mslib::char_convert::multi_to_utf8_winapi(mslib::text("XYZ軸回転 入力")).c_str(), f3[access_key], ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetRotation(D3DXVECTOR3(mslib::ToRadian(f3[access_key][0]), mslib::ToRadian(f3[access_key][1]), mslib::ToRadian(f3[access_key][2])));
		}

		// 拡縮操作 
		access_key = (int)floatcase::scale;
		f3[access_key][0] = scale.x;
		f3[access_key][1] = scale.y;
		f3[access_key][2] = scale.z;
		if (ImGui()->_InputFloat3(mslib::char_convert::multi_to_utf8_winapi(mslib::text("XYZ軸拡縮 入力")).c_str(), f3[access_key], ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetScale(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
		}
		ImGui()->Separator();

		// パラメーターの表示
		ImGui()->Separator();
		ImGui()->Text(mslib::text(
			"移動 : X[%.2f],Y[%.2f],Z[%.2f]", position.x, position.y, position.z));
		ImGui()->Text(mslib::text(
			"回転 : X[%.2f],Y[%.2f],Z[%.2f]", mslib::ToDegree(rotation.x), mslib::ToDegree(rotation.y), mslib::ToDegree(rotation.z)));
		ImGui()->Text(mslib::text(
			"拡縮 : X[%.2f],Y[%.2f],Z[%.2f]", scale.x, scale.y, scale.z));
		ImGui()->Separator();

		// 子情報を表示
		ImGui()->Separator();
		ImGui()->Text("Child List");
		ImGui()->Separator();

		// 以下、再帰的にパラメーターを操作する処理
		for (auto & itr : obj->GetChild())
		{
			if (dynamic_cast<mslib::transform::Transform*>(itr) == nullptr)continue;
			ImGui()->Checkbox(itr->GetComponentName(), m_editor_system.Get(mslib::text("%p", itr)));
			ParameterControl((mslib::transform::Transform*)itr);
		}
		ImGui()->Separator();
		ImGui()->EndWindow();
#else
		obj;
#endif
	}

	void MotionEditor::PauseData(EditedData*data)
	{
#if defined(_MSLIB_DEBUG)
		if (data->numbering == nullptr)return;

		// ウィンドウ生成
		auto strkey = mslib::text("%p", data->numbering);
		ImGui()->NewWindow(strkey, true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text(__parts_list__);
		ImGui()->Separator();

		// このウィンドウの表示flag
		ImGui()->Separator();
		bool bkey = true;
		if (ImGui()->Checkbox(std::to_string(data->numbering->ID), bkey))
		{
			data->numbering = nullptr;
			ImGui()->EndWindow();
			return;
		}
		ImGui()->Separator();

		// パーツのチェックボックスの表示
		for (auto & parts : data->numbering->pause)
			ImGui()->Checkbox(parts.second.GetComponentName(), m_editor_system.Get(mslib::text("%p", &parts.second)));

		// パーツの表示
		for (auto & parts : data->numbering->pause)
			ParameterControl(&parts.second);

		// 現在のデータをモデルに反映
		for (auto & parts : data->model->GetPartsList())
		{
			auto itr3 = data->numbering->pause.find(parts->GetComponentName());
			if (itr3 == data->numbering->pause.end())continue;

			parts->SetPosition(itr3->second.GetParameter()->position);
			parts->SetRotation(itr3->second.GetParameter()->rotation);
			parts->SetScale(itr3->second.GetParameter()->scale);
		}

		ImGui()->EndWindow();
#else
		data;
#endif
	}

	// キャラクターリスト
	void MotionEditor::CharacterList()
	{
#if defined(_MSLIB_DEBUG)
		ImGui()->Separator();
		ImGui()->Text(__character_list__);
		ImGui()->Separator();

		// 各アニメーションモデルに対応したチェックボックスの生成
		for (auto & itr : m_AnimationModel)
			ImGui()->Checkbox(itr.second.model->GetComponentName(), m_system.Get(mslib::text("%p", &itr)));
#endif
	}

	void MotionEditor::BasicData(EditedData * data)
	{
#if defined(_MSLIB_DEBUG)
		// この親オブジェクトの操作
		auto strkey = mslib::text("%p", data->model);
		ImGui()->Checkbox(__Object__, m_editor_system.Get(strkey));
		ParameterControl(data->model);
#else
		data;
#endif
	}

	void MotionEditor::MotionActive(EditedData * data)
	{
		// 各モーションデータの有効キー
		ImGui()->Separator();
		ImGui()->Text(__motion_list__);
		ImGui()->Separator();

		// 各モーション有効化
		for (auto & motion : data->model->GetMotionData())
		{
			// モーション選択時に実行
			auto bkey = boolnullptr(data->linear, &motion.second);

			// 押された際に実行
			if (!ImGui()->Checkbox(motion.second.motion_name, bkey))continue;

			// 既に同じデータがある場合は無効化と判定する
			if (boolnullptr(data->linear, &motion.second))
			{
				data->linear = nullptr;
				continue;
			}

			// 対象を保存
			data->linear = &motion.second;
		}
	}

	void MotionEditor::DisplayActiveData(EditedData*data)
	{
		// 対象データが選択されている場合有効
		if (data->linear == nullptr)return;

		// ウィンドウ生成
		ImGui()->NewWindow(mslib::text("%p", data->linear), true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text("モーション名" + std::string(":") + data->linear->motion_name);
		ImGui()->Separator();

		// このモーションの鍵/押されたとき有効
		auto bkey = true;
		if (ImGui()->Checkbox(data->linear->motion_name, bkey))
		{
			data->linear = nullptr;
			ImGui()->EndWindow();
			return;
		}
		ImGui()->Separator();

		// 各線形データ選択
		for (auto & linear : data->linear->linear)
		{
			// モーション選択時に実行
			auto bkey2 = boolnullptr(data->numbering, &linear);

			// 押された際に実行
			if (!ImGui()->Checkbox(std::to_string(linear.ID), bkey2))continue;

			// 既に同じデータがある場合は無効化と判定する
			if (boolnullptr(data->numbering, &linear))
			{
				data->numbering = nullptr;
				continue;
			}

			data->numbering = &linear;
		}

		ImGui()->EndWindow();
	}

	void MotionEditor::CreateMotion(EditedData*data)
	{
		ImGui()->Separator();
		std::string motion_name;
		if (ImGui()->InputText("モーションの追加", motion_name, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			data->model->SetMotionData(motion_name);
		}
		ImGui()->Separator();
	}

	void MotionEditor::SaveAndLoad(EditedData*data)
	{
#if defined(_MSLIB_DEBUG)
		ImGui()->Separator();
		ImGui()->Text("Save/Load");
		ImGui()->Separator();

		ImGui()->_Bullet();
		if (ImGui()->MenuItem("Save"))
			data->model->Save(FolderStructure::__motion__ + data->model->GetComponentName());

		ImGui()->_Bullet();
		if (ImGui()->MenuItem("Load"))
		{
			data->model->Load(FolderStructure::__motion__ + data->model->GetComponentName());
			data->linear = nullptr;
			data->numbering = nullptr;
			m_editor_system.clear();
		}
#else
		data;
#endif
	}

	void MotionEditor::AnimationEditor()
	{
#if defined(_MSLIB_DEBUG)
		// エディタの処理
		if (!m_system.Get(__animation_editor__))return;

		// エディタ開始
		ImGui()->NewWindow(__animation_editor__, true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text(__animation_editor__);

		// キャラクターリスト
		CharacterList();

		for (auto & itr : m_AnimationModel)
		{
			//==========================================================================
			// キャラごとに
			//==========================================================================
			auto WindowKey = mslib::text("%p", &itr);

			// キャラ毎の処理
			if (!m_system.Get(WindowKey))continue;

			//==========================================================================
			// ウィンドウ生成
			//==========================================================================
			ImGui()->NewWindow(WindowKey, true, mslib::DefaultSystem::SystemWindowFlags);
			ImGui()->Separator();
			ImGui()->Text(itr.second.model->GetComponentName());
			ImGui()->Separator();

			//==========================================================================
			// このウィンドウの表示flag
			//==========================================================================
			ImGui()->Separator();
			ImGui()->Checkbox(itr.second.model->GetComponentName(), m_system.Get(WindowKey));
			ImGui()->Separator();

			// 再生ータの有効化キー
			MotionActive(&itr.second);

			// 再生対象
			PlaybackTarget(&itr.second);

			ImGui()->EndWindow();
		}

		// エディタ終了
		ImGui()->EndWindow();
#endif
	}

	void MotionEditor::PlaybackTarget(EditedData * data)
	{
		// 対象データが選択されている場合有効
		if (data->linear == nullptr)return;

		// ウィンドウ生成
		ImGui()->NewWindow(mslib::text("%p", data->linear), true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text("アニメーション名" + std::string(":") + data->linear->motion_name);
		ImGui()->Separator();

		// このモーションの鍵/押されたとき有効
		auto bkey = true;
		if (ImGui()->Checkbox(data->linear->motion_name, bkey))
		{
			data->linear = nullptr;

			ImGui()->EndWindow();
			return;
		}
		ImGui()->Separator();

		// フレームデータ
		FrameData(data);

		// アニメーションのセッティング
		SettingAnimation(data);

		ImGui()->EndWindow();
	}

	void MotionEditor::FrameData(EditedData * data)
	{
		ImGui()->Separator();
		// フレームデータト
		if (ImGui()->NewMenu("フレームデータ"))
		{
			for (auto & linear : data->linear->linear)
			{
				if (!ImGui()->NewMenu(std::to_string(linear.ID)))continue;

				// 現在のデータをモデルに反映
				for (auto & parts : data->model->GetPartsList())
				{
					auto itr3 = linear.pause.find(parts->GetComponentName());
					if (itr3 == linear.pause.end())continue;

					parts->SetPosition(itr3->second.GetParameter()->position);
					parts->SetRotation(itr3->second.GetParameter()->rotation);
					parts->SetScale(itr3->second.GetParameter()->scale);
				}
				ImGui()->EndMenu();
			}
			ImGui()->EndMenu();
		}
	}

	void MotionEditor::SettingAnimation(EditedData * data)
	{
#if defined(_MSLIB_DEBUG)
		if (data->model == nullptr || data->linear == nullptr)return;
		ImGui()->Checkbox(__create_animation_data__, m_editor_system.Get(__create_animation_data__));

		if (!m_editor_system.Get(__create_animation_data__))return;

		// ウィンドウ生成
		ImGui()->NewWindow(__create_animation_data__, true, mslib::DefaultSystem::SystemWindowFlags);
		ImGui()->Separator();
		ImGui()->Text(__create_animation_data__ + std::string(":") + data->linear->motion_name);
		ImGui()->Separator();

		if (ImGui()->NewTreeNode("組み合わせ", false))
		{
			for (auto & itr : data->model->GetAnimationConnectionData(data->linear->motion_name))
			{
				ImGui()->Text(std::to_string(itr));
			}
			ImGui()->EndTreeNode();
		}

		if (ImGui()->NewTreeNode("セッティング", false))
		{
			for (auto & itr : data->linear->linear)
			{
				bool bfrag = false;
				if (ImGui()->Checkbox(std::to_string(itr.ID), bfrag))
				{
					data->model->GetAnimationConnectionData(data->linear->motion_name).push_back(itr.ID);
				}
			}
			ImGui()->EndTreeNode();
		}

		auto flame = data->model->GetAnimationUpdateFlame();
		if (ImGui()->SliderInt("更新フレーム", &flame, 1, 60))
			data->model->SetAnimationUpdateFlame(flame);

		ImGui()->Checkbox("ループ再生", m_editor_system.Get(std::string("ループ再生" + data->linear->motion_name)));

		if (ImGui()->MenuItem("Play"))
		{
			data->model->GenerateAnimation(data->linear->motion_name);
			data->model->Play(data->linear->motion_name, m_editor_system.Get(std::string("ループ再生" + data->linear->motion_name)));
		}
		if (ImGui()->MenuItem("Stop"))
		{
			data->model->StopAll();
		}

		ImGui()->EndWindow();
#else
		data;
#endif
	}

	// ptrが同じ場合trueが返る
	bool boolnullptr(void * ptr1, void * ptr2)
	{
		return ptr1 == ptr2 ? true : false;
	}
}