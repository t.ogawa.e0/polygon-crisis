//==========================================================================
// モーションエディタ [MotionEditor.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Asset/ShaderModel.h"
#include "Asset/AnimationModel.h"

namespace Motion
{
    //==========================================================================
    //
    // class  : MotionEditor
    // Content: モーションエディタ
    //
    //==========================================================================
    class MotionEditor : public mslib::ObjectManager
    {
    public:
        struct EditedData // エディトデータ
        {
            animation_model::AnimationShaderModel *model = nullptr; // モデルデータへのアクセスポインタ
            animation_model::AnimationShaderModel::linear_data *linear = nullptr; // 線形データへのアクセスポインタ
            animation_model::AnimationShaderModel::numbering *numbering = nullptr; // ナンバリングデータへのアクセスポインタ
        };
    public:
        MotionEditor();
        ~MotionEditor();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    private:
        // プレイヤーモデル
        void SetPlayerModel();

        // プレイヤーモデル
        void SetUAZ469Model();

        // エディタ
        void Editor();

        // アニメーションモデル毎の処理
        void ModelMotionEditor();

        // transform内のパラメーターを操作する処理
        void ParameterControl(mslib::transform::Transform*obj);

        // パーツデータの表示
        void PauseData(EditedData*data);

        // キャラクターリスト
        void CharacterList();

        // 基本データ
        void BasicData(EditedData*data);

        // モーションアクティビティの操作
        void MotionActive(EditedData*data);

        // アクティブなデータを表示する
        void DisplayActiveData(EditedData*data);

        // モーションの生成
        void CreateMotion(EditedData*data);

        // 保存と読み込み
        void SaveAndLoad(EditedData*data);

        // アニメーター
        void AnimationEditor();

        // 再生データー
        void PlaybackTarget(EditedData*data);

        // フレームデータ
        void FrameData(EditedData*data);

        // アニメーションのセッティング
        void SettingAnimation(EditedData*data);
    private:
        std::unordered_map<std::string, EditedData>m_AnimationModel;
        std::unordered_map<std::string, mslib::xmodel::XModelReference> m_ModelData;
        mslib::effect::EffectReference m_EffectData;
#if defined(_MSLIB_DEBUG)
        mslib::boollist m_system; // システム
        mslib::boollist m_editor_system; // エディタ
#endif
    };

    using MotionData = MotionEditor::EditedData;
}