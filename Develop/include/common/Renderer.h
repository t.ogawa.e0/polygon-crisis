//==========================================================================
// Renderer [Renderer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "mslib.hpp"
#include "Activity.h"
#include "SetRender.h"

_MSLIB_BEGIN

namespace renderer
{
    // RendererID
    enum class RendererID : int
    {
        Begin = -1,
        Object, // オブジェクト
        Grid, // グリッド
        Field, // フィールド
        Cube, // キューブ
        Mesh, // メッシュ
        Shadow, // 影
        Sphere, // 球体
        Xmodel, // Xモデル
        Billboard, // ビルボード
        Effect, // エフェクト
        Text, // テキスト
        End,
    };

    class Renderer : public activity::Activity , private SetRender
    {
    public:
        Renderer(RendererID id);
        virtual ~Renderer();
        /**
        @brief 更新
        */
        virtual void Update() = 0;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) = 0;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) = 0;

        /**
        @brief ソート処理
        */
        void Sort();

        /**
        @brief 描画リストを一気に更新
        */
        static void UpdateAll();

        /**
        @brief 描画リストを一気に描画
        @param device [in] デバイス
        */
        static void DrawAll(LPDIRECT3DDEVICE9 device);

        static void AllDebugActivity(bool activity);

        static bool GetAllDebugActivity();
    private:
        /**
        @brief 描画機能を切り替えます
        @param device [in] デバイス
        */
        static void ChangeRender(LPDIRECT3DDEVICE9 device, RendererID id);
    protected:
        RendererID renderer_id;
    private:
        static bool m_DebugActivityAll;
        static RendererID m_renderer_id;
        static std::list<Renderer*> m_UpdateList; // 更新対象リスト
        static std::list<Renderer*> m_DrawList; // 描画対象リスト
    };
}

_MSLIB_END