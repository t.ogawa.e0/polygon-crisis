//==========================================================================
// Sprite [Sprite.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <d3d9.h>
#include <d3dx9.h>
#include <list>

#include "mslib.hpp"
#include "mslib_struct.h"
#include "Transform.h"
#include "Texture.h"
#include "Activity.h"
#include "DrawingTechnology.h"

_MSLIB_BEGIN

namespace sprite
{
    //==========================================================================
    //
    // class  : Sprite
    // Content : シェーダーを使った2D
    //
    //==========================================================================
    class Sprite : public transform::Transform , public texture::SetTexture, public activity::Activity, public drawing_technology::DrawingTechnology
    {
    public:
        Sprite();
        virtual ~Sprite();

        /**
        @brief 色の取得
        @return 色
        */
        const D3DXCOLOR * GetColor() const;

        /**
        @brief 描画範囲の取得
        @return 描画範囲
        */
        const D3DXVECTOR2 * GetScreenSize() const;

        /**
        @brief ポリゴンのサイズの取得
        @return ポリゴンのサイズ
        */
        const D3DXVECTOR2 * GetPolygonSize() const;

        /**
        @brief ピボットの取得
        @return ピボット
        */
        const D3DXVECTOR2 * GetPivot() const;

        /**
        @brief UVの取得
        @return UV
        */
        const floatUV * GetUV() const;

        /**
        @brief プライオリティの取得
        @return プライオリティの値
        */
        float GetPriority();

        /**
        @brief 色の登録(0.0f~1.0f)
        @param r [in] R値
        @param g [in] G値
        @param b [in] B値
        @param a [in] A値
        */
        void SetColor(float r, float g, float b, float a);

        /**
        @brief 色の登録(0.0f~1.0f)
        @param color [in] 色
        */
        void SetColor(const D3DXCOLOR & color);

        /**
        @brief 色の加算(0.0f~1.0f)
        @param r [in] R値
        @param g [in] G値
        @param b [in] B値
        @param a [in] A値
        */
        void AddColor(float r, float g, float b, float a);

        /**
        @brief 色の加算(0.0f~1.0f)
        @param color [in] 色
        */
        void AddColor(const D3DXCOLOR & color);

        /**
        @brief 描画範囲の登録
        @param x [in] X軸のサイズ
        @param y [in] Y軸のサイズ
        */
        void SetScreenSize(float x, float y);

        /**
        @brief 描画範囲の登録
        @param size [in] サイズ
        */
        void SetScreenSize(const D3DXVECTOR2 & size);

        /**
        @brief 描画範囲の加算
        @param x [in] X軸のサイズ
        @param y [in] Y軸のサイズ
        */
        void AddScreenSize(float x, float y);

        /**
        @brief 描画範囲の加算
        @param size [in] サイズ
        */
        void AddScreenSize(const D3DXVECTOR2 & size);

        /**
        @brief ポリゴンのサイズの登録
        @param x [in] X軸のサイズ
        @param y [in] Y軸のサイズ
        */
        void SetPolygonSize(float x, float y);

        /**
        @brief ポリゴンのサイズの登録
        @param size [in] サイズ
        */
        void SetPolygonSize(const D3DXVECTOR2 & size);

        /**
        @brief ポリゴンのサイズの加算
        @param x [in] X軸のサイズ
        @param y [in] Y軸のサイズ
        */
        void AddPolygonSize(float x, float y);

        /**
        @brief ポリゴンのサイズの加算
        @param size [in] サイズ
        */
        void AddPolygonSize(const D3DXVECTOR2 & size);

        /**
        @brief ピボットの登録
        @param x [in] ピボットのX座標
        @param y [in] ピボットのY座標
        */
        void SetPivot(float x, float y);

        /**
        @brief ピボットの登録
        @param pivot [in] ピボットの位置
        */
        void SetPivot(const D3DXVECTOR2 & pivot);

        /**
        @brief ピボットの加算
        @param x [in] ピボットのX座標
        @param y [in] ピボットのY座標
        */
        void AddPivot(float x, float y);

        /**
        @brief ピボットの加算
        @param pivot [in] ピボットの位置
        */
        void AddPivot(const D3DXVECTOR2 & pivot);

        /**
        @brief UVの登録
        @param uv [in] UV
        */
        void SetUV(const floatUV & uv);

        /**
        @brief UVの登録
        @param Left [in] 左
        @param Top [in] 上
        @param Width [in] 幅
        @param Height [in] 高さ
        */
        void SetUV(float Left, float Top, float Width, float Height);

        /**
        @brief UVの登録
        @param Left [in] 左
        @param Top [in] 上
        */
        void SetUV_LT(float Left, float Top);

        /**
        @brief UVの登録
        @param Width [in] 幅
        @param Height [in] 高さ
        */
        void SetUV_WH(float Width, float Height);

        /**
        @brief UVの加算
        @param uv [in] UV
        */
        void AddUV(const floatUV & uv);

        /**
        @brief UVの加算
        @param Left [in] 左
        @param Top [in] 上
        @param Width [in] 幅
        @param Height [in] 高さ
        */
        void AddUV(float Left, float Top, float Width, float Height);

        /**
        @brief UVの加算
        @param Left [in] 左
        @param Top [in] 上
        */
        void AddUV_LT(float Left, float Top);

        /**
        @brief UVの加算
        @param Width [in] 幅
        @param Height [in] 高さ
        */
        void AddUV_WH(float Width, float Height);

        /**
        @brief プライオリティの設定
        @param priority [in] プライオリティ
        */
        void SetPriority(float priority);

        /**
        @brief テクスチャの登録
        @param texture [in] テクスチャ
        */
        void SetTextureData(const texture::TextureReference & TextureData) override;

        /**
        @brief ソート処理
        */
        void Sort();

        /**
        @brief 描画(非推奨)
        @param device [in] デバイス
        */
        void Draw(LPDIRECT3DDEVICE9 device);

        /**
        @brief 座標調節に使用している割合の取得
        @return 座標調節に使用している割合
        */
        const D3DXVECTOR2 * GetPercentPos()const;

		/**
		@brief 割合座標 ウィンドウサイズの割合座標の設定ができます
		@param x [in] x軸割合
		@param y [in] y軸割合
		*/
		void SetPercentPos(float x,float y);

        /**
        @brief 割合座標 ウィンドウサイズの割合座標の設定ができます
        @param Proportion [in] {x,y}軸割合
        */
        void SetPercentPos(const D3DXVECTOR2 & proportion);

        /**
        @brief グローバルマトリクスの生成
        @return マトリクス
        */
        D3DXMATRIX * CreateWorldMatrix();

        /**
        @brief ローカルマトリクスの生成
        @return マトリクス
        */
        D3DXMATRIX * CreateLocalMatrix();

        /**
        @brief スプライト描画機能の生成
        @param device [in] デバイス
        */
        static void CreateRenderer(LPDIRECT3DDEVICE9 device);

        /**
        @brief 描画リストを一気に更新
        */
        static void UpdateAll();

        /**
        @brief 描画リストを一気に描画
        @param device [in] デバイス
        */
        static void DrawAll(LPDIRECT3DDEVICE9 device);

        /**
        @brief スプライト描画機能の破棄
        */
        static void DeleteRenderer();
    protected:
        static LPDIRECT3DVERTEXBUFFER9 m_Buffer; // 共通単位サイズ板ポリ頂点バッファ
        static LPDIRECT3DVERTEXDECLARATION9 m_Declaration; // 共通頂点宣言
        static LPD3DXEFFECT m_Effect; // 共通エフェクト
        static std::list<Sprite*> m_DrawList; // 描画対象リスト
        static std::list<Sprite*> m_UpdateList; // 更新対象リスト
        D3DXCOLOR m_color; // 色
        D3DXVECTOR2 m_PercentPos; // 位置割合座標
        D3DXVECTOR2 m_screen; // スクリーンサイズ
        D3DXVECTOR2 m_polygon; // ポリゴンサイズ
        D3DXVECTOR2 m_pivot; // ピボット
        floatUV m_uv; // UV
    };

    //==========================================================================
    //
    // class  : SpriteAnimation
    // Content : シェーダーを使った2Dアニメーション
    //
    //==========================================================================
    class SpriteAnimation : public Sprite
    {
    public:
        SpriteAnimation();
        virtual ~SpriteAnimation();
        void PlayAnimation(bool flag);
        void StopAnimation();

		/**
		@param frame [in] 更新タイミング
		@param pattern [in] アニメーションのパターン数
		@param direction [in] 一行のアニメーション数
		*/
		void SetAnimationData(int frame, int pattern, int direction);
        void AddAnimationCounter(int count);
		void SetAnimationCounter(int count);
    protected:
        int m_Count; // アニメーションカウンタ
        int m_Frame; // 更新タイミング
        int m_Pattern; // アニメーションのパターン数
        int m_Direction; // 一行のアニメーション数
        bool m_loop; // ループ再生
        bool m_lock; // ロック
    };
}

_MSLIB_END