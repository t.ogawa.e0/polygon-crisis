//==========================================================================
// オブジェクト管理[ObjectManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ObjectManager.h"
#include "DefaultSystem.h"

_MSLIB_BEGIN

//==========================================================================
/**
@brief コンストラクタ
@param ObjectID [in] オブジェクトID
*/
ObjectManager::ObjectManager(const std::string & ObjectName)
{
    SetComponentName(ObjectName);
    m_Grid = nullptr;
    m_Text = nullptr;
    m_XInput = nullptr;
    m_XAudio = nullptr;
    m_Timer = nullptr;
    m_Light = nullptr;
    m_initializer = false;
    m_mt = nullptr;
    m_rand_int = nullptr;
    m_rand_float = nullptr;
    m_Fog = nullptr;
    m_Sprite = nullptr;
    m_SpriteAnimation = nullptr;
    m_XModel = nullptr;
    m_Cube = nullptr;
    m_Sphere = nullptr;
    m_Object = nullptr;
    m_Billboard = nullptr;
    m_BillboardAnimation = nullptr;
    m_Camera = nullptr;
    m_Mesh = nullptr;
    m_MeshField = nullptr;
    m_Effekseer = nullptr;
}

//==========================================================================
/**
@brief デストラクタ
*/
ObjectManager::~ObjectManager()
{
    if (m_mt != nullptr)
    {
        delete m_mt;
        m_mt = nullptr;
    }
}

//==========================================================================
/**
@brief オブジェクト名の取得
@return オブジェクト名
*/
const std::string * ObjectManager::GetObjectName(void)const
{
    return &GetComponentName();
}

//==========================================================================
/**
@brief 処理時間の設定
@param label [in] 処理フェイズ
@param time [in] 時間
*/
void ObjectManager::SetProcessTime(float time)
{
    m_processing_time = time;
}

float ObjectManager::GetProcessTime()
{
    return m_processing_time;
}

//==========================================================================
/**
@brief 初期化ロック
*/
void ObjectManager::Lock(void)
{
    m_initializer = true;
}

//==========================================================================
/**
@brief 初期化ロックの取得
@return 未ロックの場合は false が返る
*/
bool ObjectManager::GetLock(void)
{
    return m_initializer;
}

//==========================================================================
/**
@brief グリッド
@return グリッドのインスタンス
*/
Grid * ObjectManager::Grid_(void)
{
    return m_Grid != nullptr ? m_Grid : m_Grid = AddComponent<Grid>();
}

//==========================================================================
/**
@brief テキスト
@return テキストのインスタンス
*/
Text * ObjectManager::Text_(void)
{
    return m_Text != nullptr ? m_Text : m_Text = AddComponent<Text>(
        GetDevice()->GetD3DDevice());
}

//==========================================================================
/**
@brief ImGui
@return ImGuiのインスタンス
*/
MsImGui * ObjectManager::ImGui(void)
{
    return &m_ImGui;
}

//==========================================================================
/**
@brief XInput
@return XInputのインスタンス
*/
xinput::XInput * ObjectManager::XInput_(void)
{
    return m_XInput != nullptr ? m_XInput : m_XInput = AddComponent<xinput::XInput>();
}

//==========================================================================
/**
@brief XAudio
@return XAudioのインスタンス
*/
xaudio2::XAudio2 * ObjectManager::XAudio(void)
{
    return m_XAudio != nullptr ? m_XAudio : m_XAudio = AddComponent<xaudio2::XAudio2>();
}

//==========================================================================
/**
@brief カメラ
@return カメラのインスタンス
*/
wrapper::vector_component<camera::Camera>* ObjectManager::Camera(void)
{
    if (m_Camera == nullptr)
    {
        m_Camera = AddComponent<wrapper::vector_component<camera::Camera>>();
        m_Camera->SetComponentName("vector_component<Camera>");
    }
    return m_Camera;
}

//==========================================================================
/**
@brief タイマー
@return タイマーのインスタンス
*/
wrapper::vector_component<timer::Timer>* ObjectManager::Timer(void)
{
    if (m_Timer == nullptr)
    {
        m_Timer = AddComponent<wrapper::vector_component<timer::Timer>>();
        m_Timer->SetComponentName("vector_component<Timer>");
    }
    return m_Timer;
}

//==========================================================================
/**
@brief メルセンヌ・ツイスタの32ビット版
@return 初期シード値の取得
*/
std::mt19937 & ObjectManager::GetMt19937(void)
{
    // 初期シード値がメモリ確保されていないとき
    if (m_mt == nullptr)
    {
        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd()); // メルセンヌ・ツイスタの32ビット版、引数は初期シード値

        // メモリの確保
        m_mt = new std::mt19937;

        // 初期シード値の保存
        *m_mt = mt;
    }

    return *m_mt;
}

//==========================================================================
/**
@brief 範囲の一様乱数管理関数(int)
@return 範囲の一様乱数
*/
wrapper::vector<rand_int>* ObjectManager::_rand_int(void)
{
    if (m_rand_int == nullptr)
    {
        m_rand_int = AddComponent<wrapper::vector<rand_int>>();
        m_rand_int->SetComponentName("vector<rand_int>");
    }
    return m_rand_int;
}

//==========================================================================
/**
@brief 範囲の一様乱数管理関数(float)
@return 範囲の一様乱数
*/
wrapper::vector<rand_float>* ObjectManager::_rand_float(void)
{
    if (m_rand_float == nullptr)
    {
        m_rand_float = AddComponent<wrapper::vector<rand_float>>();
        m_rand_float->SetComponentName("vector<rand_float>");
    }
    return m_rand_float;
}

Fog * ObjectManager::Fog_(void)
{
    if (m_Fog == nullptr)
    {
        m_Fog = AddComponent<Fog>(GetDevice()->GetD3DDevice());
        m_Fog->SetComponentName("Fog");
    }
    return m_Fog;
}

void ObjectManager::UIEditor()
{
#if defined(_MSLIB_DEBUG)
    DefaultSystem::UIEditor(m_Sprite);
    DefaultSystem::UIEditor(m_SpriteAnimation);
#endif
}

void ObjectManager::ObjectEditor()
{
#if defined(_MSLIB_DEBUG)
    DefaultSystem::TransformOperation(m_XModel);
    DefaultSystem::TransformOperation(m_Cube);
    DefaultSystem::TransformOperation(m_Sphere);
    DefaultSystem::TransformOperation(m_Billboard);
    DefaultSystem::TransformOperation(m_BillboardAnimation);
    DefaultSystem::TransformOperation(m_Object);
    DefaultSystem::TransformOperation(m_Mesh);
    DefaultSystem::TransformOperation(m_MeshField);
    DefaultSystem::TransformOperation(m_Light);
    DefaultSystem::TransformOperation(m_Effekseer);
#endif
}

void ObjectManager::ObjectList()
{
#if defined(_MSLIB_DEBUG)
    DefaultSystem::ObjectData(m_Sprite);
    DefaultSystem::ObjectData(m_SpriteAnimation);
    DefaultSystem::ObjectData(m_XModel);
    DefaultSystem::ObjectData(m_Cube);
    DefaultSystem::ObjectData(m_Sphere);
    DefaultSystem::ObjectData(m_Billboard);
    DefaultSystem::ObjectData(m_BillboardAnimation);
    DefaultSystem::ObjectData(m_Object);
    DefaultSystem::ObjectData(m_Mesh);
    DefaultSystem::ObjectData(m_MeshField);
    DefaultSystem::ObjectData(m_Light);
    DefaultSystem::ObjectData(m_Effekseer);
#endif
}

void ObjectManager::Save(std::ofstream & ofstream)
{
#if defined(_MSLIB_DEBUG)
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Sprite);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_SpriteAnimation);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_XModel);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Cube);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Sphere);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Billboard);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_BillboardAnimation);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Object);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Mesh);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_MeshField);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Light);
    DefaultSystem::SaverTemplate(ofstream, GetComponentName(), m_Effekseer);
#else
    ofstream;
#endif
}

void ObjectManager::Load(std::ifstream & ifstream)
{
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Sprite);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_SpriteAnimation);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_XModel);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Cube);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Sphere);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Billboard);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_BillboardAnimation);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Object);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Mesh);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_MeshField);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Light);
    DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), m_Effekseer);
}

//==========================================================================
/**
@brief ファイル指定
@param file_pass [in] ファイルパス
*/
void ObjectManager::SetFilePass(const std::string file_pass)
{
    m_file_name = file_pass;
}

//==========================================================================
/**
@brief スプライト
@return スプライトのインスタンス
*/
wrapper::vector_component<sprite::Sprite>* ObjectManager::Sprite()
{
    if (m_Sprite == nullptr)
    {
        m_Sprite = AddComponent<wrapper::vector_component<sprite::Sprite>>();
        m_Sprite->SetComponentName("vector_component<sprite::Sprite>");
    }
    return m_Sprite;
}

//==========================================================================
/**
@brief スプライトアニメーション
@return スプライトアニメーションのインスタンス
*/
wrapper::vector_component<sprite::SpriteAnimation>* ObjectManager::SpriteAnimation()
{
    if (m_SpriteAnimation == nullptr)
    {
        m_SpriteAnimation = AddComponent<wrapper::vector_component<sprite::SpriteAnimation>>();
        m_SpriteAnimation->SetComponentName("vector_component<sprite::SpriteAnimation>");
    }
    return m_SpriteAnimation;
}

//==========================================================================
/**
@brief Xモデル
@return Xモデルのインスタンス
*/
wrapper::vector_component<object::XModel>* ObjectManager::XModel()
{
    if (m_XModel == nullptr)
    {
        m_XModel = AddComponent<wrapper::vector_component<object::XModel>>();
        m_XModel->SetComponentName("vector_component<object::XModel>");
    }
    return m_XModel;
}

//==========================================================================
/**
@brief キューブ
@return キューブのインスタンス
*/
wrapper::vector_component<object::Cube>* ObjectManager::Cube()
{
    if (m_Cube == nullptr)
    {
        m_Cube = AddComponent<wrapper::vector_component<object::Cube>>();
        m_Cube->SetComponentName("vector_component<object::Cube>");
    }
    return m_Cube;
}

//==========================================================================
/**
@brief 球体
@return 球体のインスタンス
*/
wrapper::vector_component<object::Sphere>* ObjectManager::Sphere()
{
    if (m_Sphere == nullptr)
    {
        m_Sphere = AddComponent<wrapper::vector_component<object::Sphere>>();
        m_Sphere->SetComponentName("vector_component<object::Sphere>");
    }
    return m_Sphere;
}

//==========================================================================
/**
@brief オブジェクト
@return オブジェクトのインスタンス
*/
wrapper::vector_component<object::Object>* ObjectManager::Object()
{
    if (m_Object == nullptr)
    {
        m_Object = AddComponent<wrapper::vector_component<object::Object>>();
        m_Object->SetComponentName("vector_component<object::Object>");
    }
    return m_Object;
}

//==========================================================================
/**
@brief ビルボード
@return ビルボードのインスタンス
*/
wrapper::vector_component<object::Billboard>* ObjectManager::Billboard()
{
    if (m_Billboard == nullptr)
    {
        m_Billboard = AddComponent<wrapper::vector_component<object::Billboard>>();
        m_Billboard->SetComponentName("vector_component<object::Billboard>");
    }
    return m_Billboard;
}

//==========================================================================
/**
@brief ビルボードアニメーション
@return ビルボードアニメーションのインスタンス
*/
wrapper::vector_component<object::BillboardAnimation>* ObjectManager::BillboardAnimation()
{
    if (m_BillboardAnimation == nullptr)
    {
        m_BillboardAnimation = AddComponent<wrapper::vector_component<object::BillboardAnimation>>();
        m_BillboardAnimation->SetComponentName("vector_component<object::BillboardAnimation>");
    }
    return m_BillboardAnimation;
}

//==========================================================================
/**
@brief メッシュ
@return メッシュのインスタンス
*/
wrapper::vector_component<object::Mesh>* ObjectManager::Mesh()
{
    if (m_Mesh == nullptr)
    {
        m_Mesh = AddComponent<wrapper::vector_component<object::Mesh>>();
        m_Mesh->SetComponentName("vector_component<object::Mesh>");
    }
    return m_Mesh;
}

//==========================================================================
/**
@brief メッシュフィールド
@return メッシュフィールドのインスタンス
*/
wrapper::vector_component<object::MeshField>* ObjectManager::MeshField()
{
    if (m_MeshField == nullptr)
    {
        m_MeshField = AddComponent<wrapper::vector_component<object::MeshField>>();
        m_MeshField->SetComponentName("vector_component<object::MeshField>");
    }
    return m_MeshField;
}

wrapper::vector_component<object::Light>* ObjectManager::Light()
{
    if (m_Light == nullptr)
    {
        m_Light = AddComponent<wrapper::vector_component<object::Light>>();
        m_Light->SetComponentName("vector_component<object::Light>");
    }
    return m_Light;
}

wrapper::vector_component<object::Effekseer>* ObjectManager::Effekseer()
{
    if (m_Effekseer == nullptr)
    {
        m_Effekseer = AddComponent<wrapper::vector_component<object::Effekseer>>();
        m_Effekseer->SetComponentName("vector_component<object::Effekseer>");
    }
    return m_Effekseer;
}

//==========================================================================
/**
@brief オブジェクト情報を保存しておくためのファイル名
@param file_name [in] ファイル名
*/
void ObjectManager::SetFileName(const std::string & file_name)
{
    if (m_initializer != true)
    {
        m_file_name = file_name;
    }
}

_MSLIB_END