//==========================================================================
// テクスチャ[Texture.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <list>
#include <unordered_map>
#include <memory>

#include "mslib.hpp"
#include "mslib_struct.h"
#include "Component.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace texture
{
    class TextureLoader;

    //==========================================================================
    //
    // class  : TextureData
    // Content: テクスチャデータ
    //
    //==========================================================================
    class TextureData : public ireference::ReferenceData<LPDIRECT3DTEXTURE9>
    {
    public:
        TextureData();
        ~TextureData();
    public:
        float ratio; // 修正比率
        int2 WindowsSize; // ウィンドウサイズ
        D3DXIMAGE_INFO info; // 画像情報
        int64_t createID; // 生成ID
        TextureLoader * LoaderPtr; // ローダーのポインタ
        std::string tag; // タグ
    };

    //==========================================================================
    //
    // class  : TextureReference
    // Content: 参照用
    //
    //==========================================================================
    class TextureReference : public ireference::ReferenceOperator<TextureData>
    {
    public:
        TextureReference();
        ~TextureReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        const D3DXIMAGE_INFO & Info();
        const float Ratio();
        const int2 & WindowsSize();
        const std::string & Tag();
    };

    //==========================================================================
    //
    // class  : TextureLoader
    // Content: テクスチャ読み込み破棄クラス
    //
    //==========================================================================
    class TextureLoader : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        TextureLoader(const TextureLoader &) = delete;
        TextureLoader &operator=(const TextureLoader &) = delete;
        TextureLoader &operator=(TextureLoader&&) = delete;
    public:
        TextureLoader();
        TextureLoader(LPDIRECT3DDEVICE9 device, HWND hWnd, const int2 & winsize, float ratio);
        ~TextureLoader();

        /**
        @brief テクスチャを読み込む。
        @param path [in] 読み込みパス
        @return テクスチャのポインタ
        */
        TextureReference Load(const std::string & path);

        /**
        @brief テクスチャを生成する
        @param Width [in], Height [in], Depth [in], Size [in] size in pixels. these must be non-zero
        @param MipLevels [in] number of mip levels desired. if zero or D3DX_DEFAULT, a complete mipmap chain will be created.
        @param Usage [in] Texture usage flags
        @param Format [in] Pixel format.
        @param Pool [in] Memory pool to be used to create texture
        @return テクスチャのポインタ
        */
        TextureReference CreateTexture(UINT Width, UINT Height, UINT MipLevels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool);

        /**
        @brief テクスチャを破棄する。
        @param data [in] テクスチャ
        */
        void Unload(TextureData * data);

        /**
        @brief テクスチャを破棄する。
        @param data [in] テクスチャリスト
        */
        void Unload(TextureData & data);

        std::unordered_map<std::string, TextureData> * GetData() { return &m_data; }
    private:
        /**
        @brief テクスチャの読み込み機能の本体
        @param path [in] 読み込み元パス
        @param Out [out] 受け取り先
        */
        void Load(const std::string & path,TextureData * Out);
    private:
        float m_ratio; // 誤差の比率
        int2 m_WindowsSize; // ウィンドウサイズ
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
        int64_t createIDCount; // カウンタ
        std::unordered_map<std::string, TextureData> m_data; // データ
    };

    //==========================================================================
    //
    // class  : SetTexture
    // Content: テクスチャ登録クラス
    //
    //==========================================================================
    class SetTexture
    {
    public:
        SetTexture();
        virtual ~SetTexture();

        /**
        @brief テクスチャデータの登録
        @param TextureData [in] テクスチャ
        */
        virtual void SetTextureData(const TextureReference & TextureData);

        /**
        @brief テクスチャデータの取得
        */
        TextureReference & GetTextureData();
    protected:
        TextureReference m_TextureData; // テクスチャデータ
    };
}

_MSLIB_END