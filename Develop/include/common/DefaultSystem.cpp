//==========================================================================
// DefaultSystem [DefaultSystem.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DefaultSystem.h"
#include "ObjectManager.h"
#include "Activity.h"
#include "Renderer.h"

_MSLIB_BEGIN

namespace DefaultSystem
{
#if defined(_MSLIB_DEBUG)
    boollist bool_system; // システム
#endif
    enum class floatcase : int
    {
        position,
        rotation,
        scale,
        max
    };
    const char * __address__ = " [address : %p]";
    constexpr const float __move_buff__ = 0.1f;
    namespace stack
    {
        std::list<std::ofstream> Stack::ofstream_stack;
        std::list<std::ifstream> Stack::ifstream_stack;

        std::ofstream & Stack::BeginOfstream()
        {
            ofstream_stack.push_back(std::ofstream());
            return (*--ofstream_stack.end());
        }
        void Stack::EndOfstream()
        {
            if (ofstream_stack.size() == 0)return;

            auto itr = --ofstream_stack.end();
            itr->close();
            ofstream_stack.erase(itr);
        }
        std::ifstream & Stack::BeginIfstream()
        {
            ifstream_stack.push_back(std::ifstream());
            return (*--ifstream_stack.end());
        }
        void Stack::EndIfstream()
        {
            if (ifstream_stack.size() == 0)return;

            auto itr = --ifstream_stack.end();
            itr->close();
            ifstream_stack.erase(itr);
        }
    }
}

namespace DefaultSystem
{
    //==========================================================================
    // 内部定数
    //========================================================================== 
    constexpr char * cAccessCheck = "AccessCheck";

    //==========================================================================
    // 内部関数
    //==========================================================================
    std::string CheckClass(transform::Transform * obj);
    void CheckActivity(transform::Transform * obj);
    void CheckMaterialLighting(transform::Transform * obj);
    std::string strbool(bool label);

    //==========================================================================
    // UIエディタ
    //==========================================================================
    void UIEditor(sprite::Sprite * obj)
    {
		if (obj == nullptr)return;
#if defined(_MSLIB_DEBUG)
		MsImGui imgui;
		bool bkey = false;
		auto strkey = mslib::text("%p", obj);

		// 鍵が存在する
		if (bool_system.Check(strkey))
			bkey = bool_system.Get(strkey);

		// チェックボックス
		if (imgui.Checkbox(obj->GetComponentName(), bkey))
		{
			// 鍵の生成
			if (bkey)
				bool_system.Get(strkey) = bkey;
			// 鍵の破棄
			if (!bkey)
				bool_system.Release(strkey);
		}

		// 処理が有効でない場合終了
		if (!bkey)return;

		// ウィンドウ生成
		imgui.NewWindow(strkey, true, mslib::DefaultSystem::SystemWindowFlags);
		imgui.Separator();
		imgui.Text(obj->GetComponentName());
		imgui.Separator();

		// このウィンドウの表示flag
		imgui.Separator();
		imgui.Checkbox(obj->GetComponentName(), bool_system.Get(strkey));
		imgui.Separator();
		auto renderer = dynamic_cast<renderer::Renderer*>(obj);
		if (renderer != nullptr)
		{
			bool bactivity = renderer->GetActivity();
			if (imgui.Checkbox("Activity", bactivity))
				renderer->SetActivity(bactivity);
			imgui.Separator();
			bool bdebug_activity = renderer->GetDebugActivity();
			if (imgui.Checkbox("DebugActivity", bdebug_activity))
				renderer->SetDebugActivity(bdebug_activity);
			imgui.Separator();
		}

		auto position = *obj->GetPosition();
		auto rotation = *obj->GetRotation();
		auto scale = *obj->GetScale();
		auto color = *obj->GetColor();
		auto percent = *obj->GetPercentPos();
		auto activity = obj->GetActivity();
		auto priority = obj->GetPriority();
		auto &texture = obj->GetTextureData();

		if (texture.Existence())
		{
			auto &info = texture.Info();
			if ((&texture) != nullptr)
				imgui.Image(&texture, { (float)info.Width,(float)info.Height }, { color.r, color.g, color.b, color.a }, { color.r, color.g, color.b, color.a }, 300);
		}

		float fpercent2[2] = { percent.x, percent.y };
		float fscale2[2] = { scale.x, scale.y };
		float fscale = 0.0f;
		float fpriority = 0.0f;

		imgui.Separator();
		if (imgui._InputFloat("[Input] Priority", &priority, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetPriority(priority);
		}
		if (imgui._InputFloat2("[Input] Coordinate adjustment by ratio [X/Y]", fpercent2, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetPercentPos(fpercent2[0], fpercent2[1]);
		}
		if (imgui._InputFloat2("[Input] XY Scale", fscale2, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetScale(fscale2[0], fscale2[1], scale.z);
		}
		float frotation1 = mslib::ToDegree(rotation.z);
		if (imgui._InputFloat("[Input] Angle", &frotation1, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			obj->SetRotation(0, 0, mslib::ToRadian(frotation1));
		}
		imgui.Separator();
		if (imgui._SliderFloat("[Slider] Priority", &fpriority, -1.0f, 1.0f))
		{
			priority += fpriority;
			obj->SetPriority(priority);
		}
		if (imgui._SliderFloat2("[Slider] Coordinate adjustment by ratio [X/Y]", fpercent2, -1.0f, 1.0f))
		{
			obj->SetPercentPos(fpercent2[0], fpercent2[1]);
		}
		if (imgui._SliderFloat("[Slider] Scale", &fscale, -1.0f, 1.0f))
		{
			scale.x += ToRadian(fscale);
			scale.y += ToRadian(fscale);
			obj->SetScale(scale);
		}
		if (imgui._SliderAngle("[Slider] Angle", &rotation.z))
		{
			obj->SetRotation(0, 0, rotation.z);
		}
		imgui.Separator();
		auto strCollarEditor = imgui.CreateText("Collar Editor [%p]", obj);
		imgui.Checkbox(strCollarEditor, bool_system.Get(strCollarEditor));
		imgui.Separator();

		if (bool_system.Get(strCollarEditor))
		{
			imgui.NewWindow(strCollarEditor, true, mslib::DefaultSystem::SystemWindowFlags);
			imgui.Separator();
			imgui.Text(strCollarEditor);
			imgui.Separator();
			imgui.Checkbox(strCollarEditor, bool_system.Get(strCollarEditor));
			imgui.Separator();
			auto ImV4 = imgui.ColorEdit({ color.r, color.g, color.b,color.a });
			obj->SetColor(D3DXCOLOR(ImV4.x, ImV4.y, ImV4.z, ImV4.w));
			imgui.EndWindow();
		}

		// パラメーターの表示
		imgui.Separator();
		if (imgui.Checkbox(imgui.CreateText("Activity") + "[" + strbool(obj->GetActivity()) + "]", activity))
			obj->SetActivity(activity);
		imgui.Text(imgui.CreateText("Priority [%.2f]", priority));
		imgui.Text(imgui.CreateText("位置情報 [x:%.2f,y:%.2f]", position.x, position.y));
		imgui.Text(imgui.CreateText("割合位置 [x:%.2f,y:%.2f]", percent.x, percent.y));
		imgui.Text(imgui.CreateText("回転情報 [%.2f]", rotation.z));
		imgui.Text(imgui.CreateText("拡縮情報 [x:%.2f,y:%.2f]", scale.x, scale.y));
		imgui.Text(imgui.CreateText("色情報 [r:%.2f,g:%.2f,b:%.2f,a:%.2f]", color.r, color.g, color.b, color.a));
		imgui.Separator();

		// 子情報を表示
		imgui.Separator();
		imgui.Text("Child List");
		imgui.Separator();

		// 以下、再帰的にパラメーターを操作する処理
		for (auto & itr : obj->GetChild())
		{
			if (dynamic_cast<sprite::Sprite*>(itr) == nullptr)continue;
			UIEditor((sprite::Sprite*)itr);
		}
		imgui.Separator();
		imgui.EndWindow();
#endif
	}

    //==========================================================================
    // テクスチャリソース表示
    //==========================================================================
    void ResourceManager(mslib::texture::TextureLoader * loader)
    {
        if (loader != nullptr)
        {
            MsImGui imgui;

            for (auto &itr : *loader->GetData())
            {
                auto & info = itr.second.info;
                if (itr.second.asset == nullptr)
                    continue;

                imgui.Separator();
                imgui.Text(imgui.CreateText("texture pass : %s", itr.second.tag.c_str()));
                imgui.Text(imgui.CreateText("Width : %d", (int)info.Width));
                imgui.Text(imgui.CreateText("Height : %d", (int)info.Height));
                imgui.Text(imgui.CreateText("Depth : %d", (int)info.Depth));
                imgui.Text(imgui.CreateText("MipLevels : %d", (int)info.MipLevels));
                imgui.Text(imgui.CreateText("Format : %d", (int)info.Format));
                imgui.Text(imgui.CreateText("Access : %d", itr.second.GetRef()));
                switch (info.ResourceType)
                {
                case D3DRESOURCETYPE::D3DRTYPE_SURFACE:
                    imgui.Text("ResourceType : Surface resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_VOLUME:
                    imgui.Text("ResourceType : Volume resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_TEXTURE:
                    imgui.Text("ResourceType : Texture resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_VOLUMETEXTURE:
                    imgui.Text("ResourceType : Volume texture resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_CUBETEXTURE:
                    imgui.Text("ResourceType : Cube texture resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_VERTEXBUFFER:
                    imgui.Text("ResourceType : Vertex buffer resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_INDEXBUFFER:
                    imgui.Text("ResourceType : Index buffer resource");
                    break;
                default:
                    break;
                }
                switch (info.ImageFileFormat)
                {
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_BMP:
                    imgui.Text("ImageFileFormat : BMP");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_JPG:
                    imgui.Text("ImageFileFormat : JPG");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_TGA:
                    imgui.Text("ImageFileFormat : TGA");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_PNG:
                    imgui.Text("ImageFileFormat : PNG");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_DDS:
                    imgui.Text("ImageFileFormat : DDS");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_PPM:
                    imgui.Text("ImageFileFormat : PPM");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_DIB:
                    imgui.Text("ImageFileFormat : DIB");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_HDR:
                    imgui.Text("ImageFileFormat : HDR");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_PFM:
                    imgui.Text("ImageFileFormat : PFM");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_FORCE_DWORD:
                    imgui.Text("ImageFileFormat : NONE");
                    break;
                default:
                    break;
                }
                imgui.Image(itr.second.asset, { (float)info.Width,(float)info.Height }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, 400);
                imgui.Separator();
            }
        }
    }

    //==========================================================================
    // ノード探索
    //==========================================================================
    void NodeSearch(component::Component * child)
    {
        if (child != nullptr)
        {
            MsImGui imgui;

            imgui.Separator();
            imgui.Text("Node Name >> " + child->GetComponentName() + imgui.CreateText(__address__, child));
            imgui.Separator();
            imgui.Separator();
            imgui.Text("## Node Search ##");
            imgui.Separator();
            imgui.Separator();
            imgui.Text("## Search Parent ##");
            imgui.Separator();
            if (imgui.NewMenu("Parent Name >> " + child->GetParent()->GetComponentName() + imgui.CreateText(__address__, child->GetParent())))
            {
                NodeSearch(child->GetParent());
                imgui.EndMenu();
            }
            imgui.Separator();
            imgui.Text("## Search Child ##");
            imgui.Separator();
            for (auto * itr : child->GetChild())
            {
                if (imgui.NewMenu("Child Name >> " + itr->GetComponentName() + imgui.CreateText(__address__, itr)))
                {
                    NodeSearch(itr);
                    imgui.EndMenu();
                }
            }
        }
    }

    //==========================================================================
    // 管理されているオブジェクト表示
    //==========================================================================
    void ObjectData(component::Component * this_)
    {
		MsImGui imgui;
		auto name = this_->GetComponentName();
		name = name + imgui.CreateText(__address__, this_);
		if (imgui.NewMenu(name))
		{
			auto obj = dynamic_cast<transform::Transform*>(this_);
			if (obj != nullptr)
			{
				auto param = obj->GetParameter();
				auto vector = obj->GetVector();
				auto look = obj->GetLook();
				auto WorldMat = obj->GetWorldMatrix();
				auto LocalMat = obj->GetLocalMatrix();

				imgui.Separator();
				imgui.Text("Name >> " + obj->GetComponentName());
				CheckActivity(obj);
				imgui.Separator();

				imgui.Text(imgui.CreateText("位置情報 [x:%.2f,y:%.2f,z:%.2f]", param->position.x, param->position.y, param->position.z));
				imgui.Text(imgui.CreateText("回転情報 [x:%.2f,y:%.2f,z:%.2f]", param->rotation.x, param->rotation.y, param->rotation.z));
				imgui.Text(imgui.CreateText("拡縮情報 [x:%.2f,y:%.2f,z:%.2f]", param->scale.x, param->scale.y, param->scale.z));

				imgui.Text(imgui.CreateText("Vector Front [x:%.2f,y:%.2f,z:%.2f]", vector->front.x, vector->front.y, vector->front.z));
				imgui.Text(imgui.CreateText("Vector Right [x:%.2f,y:%.2f,z:%.2f]", vector->right.x, vector->right.y, vector->right.z));
				imgui.Text(imgui.CreateText("Vector Up [x:%.2f,y:%.2f,z:%.2f]", vector->up.x, vector->up.y, vector->up.z));

				imgui.Text(imgui.CreateText("Look Eye [x:%.2f,y:%.2f,z:%.2f]", look->eye.x, look->eye.y, look->eye.z));
				imgui.Text(imgui.CreateText("Look At [x:%.2f,y:%.2f,z:%.2f]", look->at.x, look->at.y, look->at.z));
				imgui.Text(imgui.CreateText("Look Up [x:%.2f,y:%.2f,z:%.2f]", look->up.x, look->up.y, look->up.z));

				CheckMaterialLighting(obj);

				imgui.Separator();
				imgui.Text("WorldMatrix");
				imgui.Separator();
				imgui.Text(imgui.CreateText("[11:%.2f][12:%.2f][13:%.2f][14:%.2f]", WorldMat->_11, WorldMat->_12, WorldMat->_13, WorldMat->_14));
				imgui.Text(imgui.CreateText("[21:%.2f][22:%.2f][23:%.2f][24:%.2f]", WorldMat->_21, WorldMat->_22, WorldMat->_23, WorldMat->_24));
				imgui.Text(imgui.CreateText("[31:%.2f][32:%.2f][33:%.2f][34:%.2f]", WorldMat->_31, WorldMat->_32, WorldMat->_33, WorldMat->_34));
				imgui.Text(imgui.CreateText("[41:%.2f][42:%.2f][43:%.2f][44:%.2f]", WorldMat->_41, WorldMat->_42, WorldMat->_43, WorldMat->_44));
				imgui.Separator();
				imgui.Text("LocalMatrix");
				imgui.Separator();
				imgui.Text(imgui.CreateText("[11:%.2f][12:%.2f][13:%.2f][14:%.2f]", LocalMat->_11, LocalMat->_12, LocalMat->_13, LocalMat->_14));
				imgui.Text(imgui.CreateText("[21:%.2f][22:%.2f][23:%.2f][24:%.2f]", LocalMat->_21, LocalMat->_22, LocalMat->_23, LocalMat->_24));
				imgui.Text(imgui.CreateText("[31:%.2f][32:%.2f][33:%.2f][34:%.2f]", LocalMat->_31, LocalMat->_32, LocalMat->_33, LocalMat->_34));
				imgui.Text(imgui.CreateText("[41:%.2f][42:%.2f][43:%.2f][44:%.2f]", LocalMat->_41, LocalMat->_42, LocalMat->_43, LocalMat->_44));
				imgui.Separator();
			}
			imgui.Separator();
			imgui.Text("Child List");
			imgui.Separator();
			auto child = this_->GetChild();
			for (auto &itr : child)
			{
				ObjectData(itr);
			}
			imgui.EndMenu();
		}
	}

    std::string strbool(bool label)
    {
        return label == true ? "true" : "false";
    }

    void CheckMaterialLighting(transform::Transform * obj)
    {
        auto object = dynamic_cast<object::MaterialLighting*>(obj);

        if (object != nullptr)
        {
            MsImGui imgui;
            auto &material = object->GetD3DMaterial9();
            imgui.Separator();
            imgui.Text("Material");
			imgui.Separator();
            imgui.Text(imgui.CreateText("Material Ambient [r:%.2f,g:%.2f,b:%.2f,a:%.2f]", material.Ambient.r, material.Ambient.g, material.Ambient.b, material.Ambient.a));
            imgui.Text(imgui.CreateText("Material Diffuse [r:%.2f,g:%.2f,b:%.2f,a:%.2f]", material.Diffuse.r, material.Diffuse.g, material.Diffuse.b, material.Diffuse.a));
            imgui.Text(imgui.CreateText("Material Ambient [r:%.2f,g:%.2f,b:%.2f,a:%.2f]", material.Emissive.r, material.Emissive.g, material.Emissive.b, material.Emissive.a));
            imgui.Text(imgui.CreateText("Material Specular [r:%.2f,g:%.2f,b:%.2f,a:%.2f]", material.Specular.r, material.Specular.g, material.Specular.b, material.Specular.a));
            imgui.Text(imgui.CreateText("Material Power [%.2f]", material.Power));
        }
    }

    void CheckActivity(transform::Transform * obj)
    {
        auto object = dynamic_cast<activity::Activity*>(obj);

        if (object != nullptr)
        {
            MsImGui imgui;
            auto activity = object->GetActivity();
            imgui.Checkbox(imgui.CreateText("Activity") + "[" + strbool(object->GetActivity()) + "]", activity);
            object->SetActivity(activity);
        }
    }

    //==========================================================================
    // 保存機能
    //==========================================================================
    void SaverTemplate(std::ofstream & ofstream, const std::string & manager_object_name, transform::Transform * obj, int data_id)
    {
        if (obj == nullptr) return;
        auto class_name = CheckClass(obj);
        if (class_name == "") return;

        auto component_name = obj->GetComponentName();

        // 基礎データ
        auto &position = obj->GetParameter()->position;
        auto &rotation = obj->GetParameter()->rotation;
        auto &scale = obj->GetParameter()->scale;
        bool activity = true; // アクティビティ
        D3DXCOLOR color = D3DXCOLOR(1, 1, 1, 1); // 色
        D3DMATERIAL9 material = D3DMATERIAL9();
        D3DXVECTOR2 percent_pos = D3DXVECTOR2(0, 0);

        // データの呼び出し
        if (dynamic_cast<activity::Activity*>(obj) != nullptr)
        {
            activity = dynamic_cast<activity::Activity*>(obj)->GetActivity();
        }
        if (dynamic_cast<sprite::Sprite*>(obj) != nullptr)
        {
            color = *dynamic_cast<sprite::Sprite*>(obj)->GetColor();
            percent_pos = *dynamic_cast<sprite::Sprite*>(obj)->GetPercentPos();
        }
        if (dynamic_cast<object::MaterialLighting*>(obj) != nullptr)
        {
            material = dynamic_cast<object::MaterialLighting*>(obj)->GetD3DMaterial9();
        }

        // アクセスキーの登録
        SetAccessKey(ofstream, manager_object_name, class_name, component_name, data_id);

        // 共通データ
        ofstream.write((char *)&position, sizeof(position));
        ofstream.write((char *)&rotation, sizeof(rotation));
        ofstream.write((char *)&scale, sizeof(scale));
        ofstream.write((char *)&activity, sizeof(activity));
        ofstream.write((char *)&color, sizeof(color));
        ofstream.write((char *)&percent_pos, sizeof(percent_pos));
        ofstream.write((char *)&material, sizeof(material));

        int _count_id = 0;
        for (auto & itr : obj->GetChild())
        {
            auto itr_ptr = dynamic_cast<transform::Transform*>(itr);
            if (itr_ptr == nullptr)continue;
            SaverTemplate(ofstream, manager_object_name + ">>" + obj->GetComponentName(), itr_ptr, _count_id);
            _count_id++;
        }
    }

    //==========================================================================
    // 読み取り機能
    //==========================================================================
    void LoaderTemplate(std::ifstream & ifstream, const std::string & manager_object_name, transform::Transform * obj, int data_id)
    {
        if (obj == nullptr) return;
        auto class_name = CheckClass(obj);
        if (class_name == "") return;
        if (ifstream.fail()) return;

        ifstream.clear();
        ifstream.seekg(0, ifstream.beg);

        std::string reading_line_buffer;
        auto component_name = obj->GetComponentName();
        int key_count = 0;

        D3DXVECTOR3 position = D3DXVECTOR3();
        D3DXVECTOR3 rotation = D3DXVECTOR3();
        D3DXVECTOR3 scale = D3DXVECTOR3();
        bool activity = true; // アクティビティ
        D3DXCOLOR color = D3DXCOLOR(1, 1, 1, 1); // 色
        D3DMATERIAL9 material = D3DMATERIAL9();
        D3DXVECTOR2 percent_pos = D3DXVECTOR2(0, 0);

        // ファイルの末尾でない場合
        for (; !ifstream.eof(); )
        {
            // read by line
            std::getline(ifstream, reading_line_buffer);

            // read by delimiter on reading "one" line
            const char delimiter = ' ';
            std::string separated_string_buffer;
            std::istringstream line_separater(reading_line_buffer);
            std::getline(line_separater, separated_string_buffer, delimiter);

            // チェックキーと一致読み取りラインを移動
            if (cAccessCheck == separated_string_buffer)
            {
                ifstream.read((char *)&position, sizeof(position));
                ifstream.read((char *)&rotation, sizeof(rotation));
                ifstream.read((char *)&scale, sizeof(scale));
                ifstream.read((char *)&activity, sizeof(activity));
                ifstream.read((char *)&color, sizeof(color));
                ifstream.read((char *)&percent_pos, sizeof(percent_pos));
                ifstream.read((char *)&material, sizeof(material));
                continue;
            }

			// 読み取りキー
            switch (key_count)
            {
            case 0: // マネージャー名との整合性チェック
                if (manager_object_name == separated_string_buffer)
                    key_count++;
                continue;
                break;
            case 1: // 対象クラス名との整合性チェック
                if (class_name == separated_string_buffer)
                    key_count++;
                continue;
                break;
            case 2: // コンポーネント名との整合性チェック
                if (component_name == separated_string_buffer)
                    key_count++;
                continue;
                break;
            case 3: // データIDとの整合性チェック
                if (std::to_string(data_id) != separated_string_buffer)continue;
                break;
            default:
                break;
            }

            // データの読み取り
            std::getline(ifstream, reading_line_buffer);
            ifstream.read((char *)&position, sizeof(position));
            ifstream.read((char *)&rotation, sizeof(rotation));
            ifstream.read((char *)&scale, sizeof(scale));
            ifstream.read((char *)&activity, sizeof(activity));
            ifstream.read((char *)&color, sizeof(color));
            ifstream.read((char *)&percent_pos, sizeof(percent_pos));
            ifstream.read((char *)&material, sizeof(material));

			// 初期化
            obj->SetParameter(transform::Parameter());
            obj->SetLook(transform::Look());
            obj->SetVector(transform::Vector());

			obj->SetPosition(position);
			obj->AddRotation(rotation);
			obj->SetScale(scale);

			auto pActivity = dynamic_cast<activity::Activity*>(obj);
            if (pActivity != nullptr)
            {
				pActivity->SetActivity(activity);
            }

			auto pMaterial = dynamic_cast<object::MaterialLighting*>(obj);
            if (pMaterial != nullptr)
            {
				pMaterial->SetD3DMaterial9(material);
            }

			auto pSprite = dynamic_cast<sprite::Sprite*>(obj);
			if (pSprite != nullptr)
            {
				pSprite->SetColor(color);
				pSprite->SetPercentPos(percent_pos);
				pSprite->Sort();
            }
            break;
        }

        ifstream.clear();
        ifstream.seekg(0, ifstream.beg);

        int _count_id = 0;
        for (auto & itr : obj->GetChild())
        {
            auto itr_ptr = dynamic_cast<transform::Transform*>(itr);
            if (itr_ptr == nullptr)continue;
            LoaderTemplate(ifstream, manager_object_name + ">>" + obj->GetComponentName(), itr_ptr, _count_id);
            _count_id++;
        }

        ifstream.clear();
        ifstream.seekg(0, ifstream.beg);
    }

    std::string CheckClass(transform::Transform * obj)
    {
        // 拡張タイプ
        if (dynamic_cast<sprite::SpriteAnimation*>(obj) != nullptr) return "SpriteAnimation";
        if (dynamic_cast<object::BillboardAnimation*>(obj) != nullptr) return "BillboardAnimation";

        // 各オブジェクト
        if (dynamic_cast<sprite::Sprite*>(obj) != nullptr) return "Sprite";
        if (dynamic_cast<object::XModel*>(obj) != nullptr) return "XModel";
        if (dynamic_cast<object::Cube*>(obj) != nullptr) return "Cube";
        if (dynamic_cast<object::Sphere*>(obj) != nullptr) return "Sphere";
        if (dynamic_cast<object::Billboard*>(obj) != nullptr) return "Billboard";
        if (dynamic_cast<object::Object*>(obj) != nullptr) return "Object";
        if (dynamic_cast<object::Mesh*>(obj) != nullptr) return "Mesh";

        // 基礎
        if (dynamic_cast<object::InheritanceObject*>(obj) != nullptr) return "InheritanceObject";

        // 大本
        if (dynamic_cast<transform::Transform*>(obj) != nullptr) return "Transform";

        return "";
    }

    //==========================================================================
    // バイナリ書き込み開始
    //========================================================================== 
    std::ofstream & BeginFstreamWritingBinary(const std::string & filename)
    {
        auto &ofstream = stack::Stack::BeginOfstream();
        ofstream.open(filename, std::ios::out | std::ios::binary);
        return ofstream;
    }

    //==========================================================================
    // バイナリ書き込み終了
    //==========================================================================
    void EndFstreamWritingBinary()
    {
        stack::Stack::EndOfstream();
    }

    //==========================================================================
    // バイナリ読み取り開始
    //==========================================================================
    std::ifstream & BeginFstreamReadingBinary(const std::string & filename)
    {
        auto &ifstream = stack::Stack::BeginIfstream();
        ifstream.open(filename, std::ios::in | std::ios::binary);
        return ifstream;
    }

    //==========================================================================
    // バイナリ読み取り終了
    //==========================================================================
    void EndFstreamReadingBinary()
    {
        stack::Stack::EndIfstream();
    }

    //==========================================================================
    // アクセスキーの登録
    //==========================================================================
    void SetAccessKey(std::ofstream & ofstream, const std::string & manager_object_name, const std::string & class_name, const std::string & component_name, int id)
    {
        ofstream << manager_object_name << std::endl;
        ofstream << class_name << std::endl;
        ofstream << component_name << std::endl;
        ofstream << id << std::endl;
        ofstream << cAccessCheck << std::endl;
    }

    //==========================================================================
    // transformを操作する処理
    //==========================================================================
    void TransformOperation(transform::Transform * obj)
    {
        if (obj == nullptr)return;
#if defined(_MSLIB_DEBUG)
        MsImGui imgui;
        bool bkey = false;
        auto strkey = mslib::text("%p", obj);

        // 鍵が存在する
        if (bool_system.Check(strkey))
            bkey = bool_system.Get(strkey);

        // チェックボックス
        if (imgui.Checkbox(obj->GetComponentName(), bkey))
        {
            // 鍵の生成
            if (bkey)
                bool_system.Get(strkey) = bkey;
            // 鍵の破棄
            if (!bkey)
                bool_system.Release(strkey);
        }

        // 処理が有効でない場合終了
        if (!bkey)return;

        // パラメータを取り出す
        auto & position = obj->GetParameter()->position;
        auto & rotation = obj->GetParameter()->rotation;
        auto & scale = obj->GetParameter()->scale;

        // ウィンドウ生成
        imgui.NewWindow(strkey, true, mslib::DefaultSystem::SystemWindowFlags);
        imgui.Separator();
        imgui.Text(obj->GetComponentName());
        imgui.Separator();

        // このウィンドウの表示flag
        imgui.Separator();
        imgui.Checkbox(obj->GetComponentName(), bool_system.Get(strkey));
        float f3[(int)floatcase::max][3] = { 0.0f };
        int access_key = 0;
        imgui.Separator();
        auto renderer = dynamic_cast<renderer::Renderer*>(obj);
        if (renderer != nullptr)
        {
            bool bactivity = renderer->GetActivity();
            if (imgui.Checkbox("Activity", bactivity))
                renderer->SetActivity(bactivity);
            imgui.Separator();
            bool bdebug_activity = renderer->GetDebugActivity();
            if (imgui.Checkbox("DebugActivity", bdebug_activity))
                renderer->SetDebugActivity(bdebug_activity);
            imgui.Separator();
        }

        // ポジション操作
        imgui.Separator();
        access_key = (int)floatcase::position;
		if (imgui._SliderFloat3("[Slider] XYZ Position", f3[access_key], -__move_buff__, __move_buff__))
		{
			obj->AddPosition(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
		}

        // 回転操作
        access_key = (int)floatcase::rotation;
		if (imgui._SliderFloat3("[Slider] XYZ Rotation", f3[access_key], -__move_buff__, __move_buff__))
		{
			obj->AddRotation(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
		}

        // 拡縮操作
        access_key = (int)floatcase::scale;
		if (imgui._SliderFloat3("[Slider] XYZ Scale", f3[access_key], -__move_buff__, __move_buff__))
		{
			obj->AddScale(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
		}
        imgui.Separator();

        // ポジション操作 
        imgui.Separator();
        access_key = (int)floatcase::position;
        f3[access_key][0] = position.x;
        f3[access_key][1] = position.y;
        f3[access_key][2] = position.z;
        if (imgui._InputFloat3("[Input] XYZ Position", f3[access_key], ImGuiInputTextFlags_EnterReturnsTrue))
        {
            obj->SetPosition(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
        }

        // 回転操作
        access_key = (int)floatcase::rotation;
        f3[access_key][0] = mslib::ToDegree(rotation.x);
        f3[access_key][1] = mslib::ToDegree(rotation.y);
        f3[access_key][2] = mslib::ToDegree(rotation.z);
        if (imgui._InputFloat3("[Input] XYZ Rotation", f3[access_key], ImGuiInputTextFlags_EnterReturnsTrue))
        {
            obj->SetRotation(D3DXVECTOR3(mslib::ToRadian(f3[access_key][0]), mslib::ToRadian(f3[access_key][1]), mslib::ToRadian(f3[access_key][2])));
        }

        // 拡縮操作 
        access_key = (int)floatcase::scale;
        f3[access_key][0] = scale.x;
        f3[access_key][1] = scale.y;
        f3[access_key][2] = scale.z;
        if (imgui._InputFloat3("[Input] XYZ Scale", f3[access_key], ImGuiInputTextFlags_EnterReturnsTrue))
        {
            obj->SetScale(D3DXVECTOR3(f3[access_key][0], f3[access_key][1], f3[access_key][2]));
        }
        imgui.Separator();

        imgui.Separator();
        imgui.Text("Material Collar");
        imgui.Separator();
        auto material = dynamic_cast<object::MaterialLighting*>(obj);
        if (material != nullptr)
        {
            auto & Ambient = material->GetD3DMaterial9().Ambient;
            auto & Diffuse = material->GetD3DMaterial9().Diffuse;
            auto & Emissive = material->GetD3DMaterial9().Emissive;
            auto & Specular = material->GetD3DMaterial9().Specular;
            float fPower = material->GetD3DMaterial9().Power;

            auto strAmbient = imgui.CreateText("Ambient [%p]", material);
            auto strDiffuse = imgui.CreateText("Diffuse [%p]", material);
            auto strEmissive = imgui.CreateText("Emissive [%p]", material);
            auto strSpecular = imgui.CreateText("Specular [%p]", material);

            imgui.Checkbox(strAmbient, bool_system.Get(strAmbient));
            imgui.Checkbox(strDiffuse, bool_system.Get(strDiffuse));
            imgui.Checkbox(strEmissive, bool_system.Get(strEmissive));
            imgui.Checkbox(strSpecular, bool_system.Get(strSpecular));

            if (imgui.SliderFloat("Power", &fPower, 0.0f, fPower + 1.0f))
            {
                material->SetMaterialPower(fPower);
            }

            if (bool_system.Get(strAmbient))
            {
                imgui.NewWindow(strAmbient, true, mslib::DefaultSystem::SystemWindowFlags);
                imgui.Separator();
                imgui.Text(strAmbient);
                imgui.Separator();
				imgui.Checkbox(strAmbient, bool_system.Get(strAmbient));
				imgui.Separator();
                auto ImV4 = imgui.ColorEdit({ Ambient.r,Ambient.g,Ambient.b,Ambient.a });
                material->SetAmbient({ ImV4.x ,ImV4.y,ImV4.z,ImV4.w });
                imgui.EndWindow();
            }
            if (bool_system.Get(strDiffuse))
            {
                imgui.NewWindow(strDiffuse, true, mslib::DefaultSystem::SystemWindowFlags);
                imgui.Separator();
                imgui.Text(strDiffuse);
                imgui.Separator();
				imgui.Checkbox(strDiffuse, bool_system.Get(strDiffuse));
				imgui.Separator();
                auto ImV4 = imgui.ColorEdit({ Diffuse.r,Diffuse.g,Diffuse.b,Diffuse.a });
                material->SetDiffuse({ ImV4.x ,ImV4.y,ImV4.z,ImV4.w });
                imgui.EndWindow();
            }
            if (bool_system.Get(strEmissive))
            {
                imgui.NewWindow(strEmissive, true, mslib::DefaultSystem::SystemWindowFlags);
                imgui.Separator();
                imgui.Text(strEmissive);
                imgui.Separator();
				imgui.Checkbox(strEmissive, bool_system.Get(strEmissive));
				imgui.Separator();
                auto ImV4 = imgui.ColorEdit({ Emissive.r,Emissive.g,Emissive.b,Emissive.a });
                material->SetEmissive({ ImV4.x ,ImV4.y,ImV4.z,ImV4.w });
                imgui.EndWindow();
            }
            if (bool_system.Get(strSpecular))
            {
                imgui.NewWindow(strSpecular, true, mslib::DefaultSystem::SystemWindowFlags);
                imgui.Separator();
                imgui.Text(strSpecular);
                imgui.Separator();
				imgui.Checkbox(strSpecular, bool_system.Get(strSpecular));
				imgui.Separator();
                auto ImV4 = imgui.ColorEdit({ Specular.r,Specular.g,Specular.b,Specular.a });
                material->SetSpecular({ ImV4.x ,ImV4.y,ImV4.z,ImV4.w });
                imgui.EndWindow();
            }
        }

        // パラメーターの表示
        imgui.Separator();
		imgui.Text(mslib::text("Position : X[%.2f],Y[%.2f],Z[%.2f]", position.x, position.y, position.z));
		imgui.Text(mslib::text("Rotation : X[%.2f],Y[%.2f],Z[%.2f]", mslib::ToDegree(rotation.x), mslib::ToDegree(rotation.y), mslib::ToDegree(rotation.z)));
		imgui.Text(mslib::text("Scale : X[%.2f],Y[%.2f],Z[%.2f]", scale.x, scale.y, scale.z));
		if (material != nullptr)
        {
            auto & Ambient = material->GetD3DMaterial9().Ambient;
            auto & Diffuse = material->GetD3DMaterial9().Diffuse;
            auto & Emissive = material->GetD3DMaterial9().Emissive;
            auto & Specular = material->GetD3DMaterial9().Specular;

            imgui.Text(mslib::text("Ambient : R[%.2f],G[%.2f],B[%.2f],A[%.2f]", Ambient.r, Ambient.g, Ambient.b, Ambient.a));
            imgui.Text(mslib::text("Diffuse : R[%.2f],G[%.2f],B[%.2f],A[%.2f]", Diffuse.r, Diffuse.g, Diffuse.b, Diffuse.a));
            imgui.Text(mslib::text("Emissive : R[%.2f],G[%.2f],B[%.2f],A[%.2f]", Emissive.r, Emissive.g, Emissive.b, Emissive.a));
            imgui.Text(mslib::text("Specular : R[%.2f],G[%.2f],B[%.2f],A[%.2f]", Specular.r, Specular.g, Specular.b, Specular.a));
            imgui.Text(mslib::text("Specular : Power[%.2f]", material->GetD3DMaterial9().Power));
        }
        imgui.Separator();

        // 子情報を表示
        imgui.Separator();
        imgui.Text("Child List");
        imgui.Separator();

        // 以下、再帰的にパラメーターを操作する処理
        for (auto & itr : obj->GetChild())
        {
            if (dynamic_cast<transform::Transform*>(itr) == nullptr)continue;
            TransformOperation((transform::Transform*)itr);
        }
        imgui.Separator();
        imgui.EndWindow();
#endif
    }
}
_MSLIB_END

