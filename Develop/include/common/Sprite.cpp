//==========================================================================
// Sprite [Sprite.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Sprite.h"

_MSLIB_BEGIN

namespace sprite
{
    //==========================================================================
    // 実体
    //==========================================================================
    LPDIRECT3DVERTEXBUFFER9 Sprite::m_Buffer = nullptr;
    LPDIRECT3DVERTEXDECLARATION9 Sprite::m_Declaration = nullptr;
    LPD3DXEFFECT Sprite::m_Effect = nullptr;
    std::list<Sprite*> Sprite::m_DrawList;
    std::list<Sprite*> Sprite::m_UpdateList;

    Sprite::Sprite() :
        m_polygon(128.0f, 128.0f),
        m_pivot(m_polygon / 2.0f),
        m_uv(0.0f, 0.0f, 1.0f, 1.0f),
        m_color(1.0f, 1.0f, 1.0f, 1.0f),
        m_screen(1920, 1080),
        m_PercentPos(0.0f, 0.0f)
    {
        SetComponentName("Sprite");
        SetPriority(0.0f);
        m_DrawList.push_back(this);
        m_UpdateList.push_back(this);
        Sort();
    }

    Sprite::~Sprite()
    {
        auto itr1 = std::find(m_DrawList.begin(), m_DrawList.end(), this);
        if (itr1 != m_DrawList.end())
            m_DrawList.erase(itr1);

        auto itr2 = std::find(m_UpdateList.begin(), m_UpdateList.end(), this);
        if (itr2 != m_UpdateList.end())
            m_UpdateList.erase(itr2);
    }

    //==========================================================================
    /**
    @brief 色の取得
    @return 色
    */
    const D3DXCOLOR * Sprite::GetColor() const
    {
        return &m_color;
    }

    //==========================================================================
    /**
    @brief 描画範囲の取得
    @return 描画範囲
    */
    const D3DXVECTOR2 * Sprite::GetScreenSize() const
    {
        return &m_screen;
    }

    //==========================================================================
    /**
    @brief ポリゴンのサイズの取得
    @return ポリゴンのサイズ
    */
    const D3DXVECTOR2 * Sprite::GetPolygonSize() const
    {
        return &m_polygon;
    }

    //==========================================================================
    /**
    @brief ピボットの取得
    @return ピボット
    */
    const D3DXVECTOR2 * Sprite::GetPivot() const
    {
        return &m_pivot;
    }

    //==========================================================================
    /**
    @brief UVの取得
    @return UV
    */
    const floatUV * Sprite::GetUV() const
    {
        return &m_uv;
    }

    //==========================================================================
    /**
    @brief プライオリティの取得
    @return プライオリティの値
    */
    float Sprite::GetPriority()
    {
        return m_Parameter.position.z;
    }

    //==========================================================================
    /**
    @brief 色の登録(0.0f~1.0f)
    @param r [in] R値
    @param g [in] G値
    @param b [in] B値
    @param a [in] A値
    */
    void Sprite::SetColor(float r, float g, float b, float a)
    {
        SetColor(D3DXCOLOR(r, g, b, a));
    }

    //==========================================================================
    /**
    @brief 色の登録(0.0f~1.0f)
    @param color [in] 色
    */
    void Sprite::SetColor(const D3DXCOLOR & color)
    {
        m_color = color;
    }

    //==========================================================================
    /**
    @brief 色の加算(0.0f~1.0f)
    @param r [in] R値
    @param g [in] G値
    @param b [in] B値
    @param a [in] A値
    */
    void Sprite::AddColor(float r, float g, float b, float a)
    {
        AddColor(D3DXCOLOR(r, g, b, a));
    }

    //==========================================================================
    /**
    @brief 色の加算(0.0f~1.0f)
    @param color [in] 色
    */
    void Sprite::AddColor(const D3DXCOLOR & color)
    {
        m_color += color;
    }

    //==========================================================================
    /**
    @brief 描画範囲の登録
    @param x [in] X軸のサイズ
    @param y [in] Y軸のサイズ
    */
    void Sprite::SetScreenSize(float x, float y)
    {
        SetScreenSize(D3DXVECTOR2(x, y));
    }

    //==========================================================================
    /**
    @brief 描画範囲の登録
    @param size [in] サイズ
    */
    void Sprite::SetScreenSize(const D3DXVECTOR2 & size)
    {
        m_screen = size;
    }

    //==========================================================================
    /**
    @brief 描画範囲の加算
    @param x [in] X軸のサイズ
    @param y [in] Y軸のサイズ
    */
    void Sprite::AddScreenSize(float x, float y)
    {
        AddScreenSize(D3DXVECTOR2(x, y));
    }

    //==========================================================================
    /**
    @brief 描画範囲の加算
    @param size [in] サイズ
    */
    void Sprite::AddScreenSize(const D3DXVECTOR2 & size)
    {
        m_screen += size;
    }

    //==========================================================================
    /**
    @brief ポリゴンのサイズの登録
    @param x [in] X軸のサイズ
    @param y [in] Y軸のサイズ
    */
    void Sprite::SetPolygonSize(float x, float y)
    {
        SetPolygonSize(D3DXVECTOR2(x, y));
    }

    //==========================================================================
    /**
    @brief ポリゴンのサイズの登録
    @param size [in] サイズ
    */
    void Sprite::SetPolygonSize(const D3DXVECTOR2 & size)
    {
        m_polygon = size;
        m_pivot = size / 2.0f;
    }

    //==========================================================================
    /**
    @brief ポリゴンのサイズの加算
    @param x [in] X軸のサイズ
    @param y [in] Y軸のサイズ
    */
    void Sprite::AddPolygonSize(float x, float y)
    {
        AddPolygonSize(D3DXVECTOR2(x, y));
    }

    //==========================================================================
    /**
    @brief ポリゴンのサイズの加算
    @param size [in] サイズ
    */
    void Sprite::AddPolygonSize(const D3DXVECTOR2 & size)
    {
        m_polygon += size;
    }

    //==========================================================================
    /**
    @brief ピボットの登録
    @param x [in] ピボットのX座標
    @param y [in] ピボットのY座標
    */
    void Sprite::SetPivot(float x, float y)
    {
        SetPivot(D3DXVECTOR2(x, y));
    }

    //==========================================================================
    /**
    @brief ピボットの登録
    @param pivot [in] ピボットの位置
    */
    void Sprite::SetPivot(const D3DXVECTOR2 & pivot)
    {
        m_pivot = pivot;
    }

    //==========================================================================
    /**
    @brief ピボットの加算
    @param x [in] ピボットのX座標
    @param y [in] ピボットのY座標
    */
    void Sprite::AddPivot(float x, float y)
    {
        AddPivot(D3DXVECTOR2(x, y));
    }

    //==========================================================================
    /**
    @brief ピボットの加算
    @param pivot [in] ピボットの位置
    */
    void Sprite::AddPivot(const D3DXVECTOR2 & pivot)
    {
        m_pivot += pivot;
    }

    //==========================================================================
    /**
    @brief UVの登録
    @param uv [in] UV
    */
    void Sprite::SetUV(const floatUV & uv)
    {
        m_uv = uv;
    }

    //==========================================================================
    /**
    @brief UVの登録
    @param Left [in] 左
    @param Top [in] 上
    @param Width [in] 幅
    @param Height [in] 高さ
    */
    void Sprite::SetUV(float Left, float Top, float Width, float Height)
    {
        SetUV(floatUV(Left, Top, Width, Height));
    }

    //==========================================================================
    /**
    @brief UVの登録
    @param Left [in] 左
    @param Top [in] 上
    */
    void Sprite::SetUV_LT(float Left, float Top)
    {
        SetUV(floatUV(Left, Top, m_uv.u1, m_uv.v1));
    }

    //==========================================================================
    /**
    @brief UVの登録
    @param Width [in] 幅
    @param Height [in] 高さ
    */
    void Sprite::SetUV_WH(float Width, float Height)
    {
        SetUV(floatUV(m_uv.u0, m_uv.v0, Width, Height));
    }

    //==========================================================================
    /**
    @brief UVの加算
    @param uv [in] UV
    */
    void Sprite::AddUV(const floatUV & uv)
    {
        m_uv += uv;
    }

    //==========================================================================
    /**
    @brief UVの加算
    @param Left [in] 左
    @param Top [in] 上
    @param Width [in] 幅
    @param Height [in] 高さ
    */
    void Sprite::AddUV(float Left, float Top, float Width, float Height)
    {
        AddUV(floatUV(Left, Top, Width, Height));
    }

    //==========================================================================
    /**
    @brief UVの加算
    @param Left [in] 左
    @param Top [in] 上
    */
    void Sprite::AddUV_LT(float Left, float Top)
    {
        AddUV(floatUV(Left, Top, 0.0f, 0.0f));
    }

    //==========================================================================
    /**
    @brief UVの加算
    @param Width [in] 幅
    @param Height [in] 高さ
    */
    void Sprite::AddUV_WH(float Width, float Height)
    {
        AddUV(floatUV(0.0f, 0.0f, Width, Height));
    }

    //==========================================================================
    /**
    @brief プライオリティの設定
    @param priority [in] プライオリティ
    */
    void Sprite::SetPriority(float priority)
    {
        m_Parameter.position.z = priority;
        Sort();
    }

    //==========================================================================
    /**
    @brief テクスチャの登録
    @param texture [in] テクスチャ
    */
    void Sprite::SetTextureData(const texture::TextureReference & TextureData)
    {
        m_TextureData = TextureData;
        auto & info = m_TextureData.Info();
        auto ratio = m_TextureData.Ratio();
        auto winsize = m_TextureData.WindowsSize();

        m_polygon = D3DXVECTOR2(info.Width*ratio, info.Height*ratio);
        m_pivot = m_polygon / 2.0f;
        m_screen = winsize.D3DXVECTOR2_CAST();
    }

    //==========================================================================
    /**
    @brief ソート処理
    */
    void Sprite::Sort()
    {
        // ソート処理
        m_DrawList.sort([&](Sprite * a, Sprite * b)
        {
            return a->GetPriority() < b->GetPriority() ? true : false;
        });
    }

    //==========================================================================
    /**
    @brief 描画(非推奨)
    @param device [in] デバイス
    */
    void Sprite::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれた時点で非アクティビティ
        m_Activity = false;

        // 呼ばれたら更新の無効化
        LockUpdate();

        // 描画機能を変更
        device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
        device->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

        // 頂点バッファ・頂点宣言設定
        device->SetStreamSource(0, m_Buffer, 0, sizeof(float) * 5);
        device->SetVertexDeclaration(m_Declaration);

        // 2D描画用射影変換行列
        D3DXMATRIX proj;
        D3DXMatrixIdentity(&proj);
        proj._41 = -1.0f;
        proj._42 = 1.0f;

        // シェーダ開始
        UINT numPass = 0;
        m_Effect->SetTechnique("MainShader2D");
        m_Effect->Begin(&numPass, 0);
        m_Effect->BeginPass(0);

        // テクスチャがあり、アクティブであるとき
        if (m_TextureData.Existence())
        {
            proj._11 = 2.0f / m_screen.x;
            proj._22 = -2.0f / m_screen.y;

            auto v4color = D3DXVECTOR4(m_color.r, m_color.g, m_color.b, m_color.a);

            m_Effect->SetMatrix("World", &m_WorldMatrix);
            m_Effect->SetMatrix("Projection", &proj);
            m_Effect->SetTexture("Texture", &m_TextureData);
            m_Effect->SetFloat("uvLeft", m_uv.u0);
            m_Effect->SetFloat("uvTop", m_uv.v0);
            m_Effect->SetFloat("uvWidth", m_uv.u1);
            m_Effect->SetFloat("uvHeight", m_uv.v1);
            m_Effect->SetVector("Color", &v4color);
            m_Effect->CommitChanges();
            device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
        }

        m_Effect->EndPass();
        m_Effect->End();

        // 描画機能を変更
        device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
        device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
    }

    //==========================================================================
    /**
    @brief 座標調節に使用している割合の取得
    @return 座標調節に使用している割合
    */
    const D3DXVECTOR2 * Sprite::GetPercentPos() const
    {
        return &m_PercentPos;
    }

    //==========================================================================
    /**
    @brief 割合座標 ウィンドウサイズの割合座標の設定ができます
    @param Proportion [in] {x,y}軸割合
    @param WinSize [in] {x,y}ウィンドウサイズ
    */
	void Sprite::SetPercentPos(float x, float y)
	{
		SetPercentPos(D3DXVECTOR2(x, y));
	}
	void Sprite::SetPercentPos(const D3DXVECTOR2 & proportion)
    {
		m_update_flag = true;
        m_PercentPos = proportion;
        m_Parameter.position = D3DXVECTOR3(m_screen.x * m_PercentPos.x, m_screen.y * m_PercentPos.y, m_Parameter.position.z);
    }

    D3DXMATRIX * Sprite::CreateWorldMatrix()
    {
        auto pParentSprite = (Sprite*)GetWarrantyParent<Transform>();

        if (!m_update_flag && !pParentSprite->m_update_flag)
            return &m_WorldMatrix;

        CreateLocalMatrix();

        if (pParentSprite != this)
        {
            // 自身のローカル姿勢と親のワールド変換行列の掛け算
            m_WorldMatrix = m_LocalMatrix * pParentSprite->m_WorldMatrix;
        }
        else if (pParentSprite == this)
        {
            m_WorldMatrix = m_LocalMatrix;
        }

        m_update_flag = true;

        return &m_WorldMatrix;
    }

    D3DXMATRIX * Sprite::CreateLocalMatrix()
    {
        if (!m_update_flag)
            return &m_LocalMatrix;

        // 座標の複製
        D3DXMATRIX polygon; // ポリゴンサイズ
        D3DXMATRIX scale; // ローカルスケール
        D3DXMATRIX rotation; // ローカルスケール

        m_PercentPos = D3DXVECTOR2(m_Parameter.position.x / m_screen.x, m_Parameter.position.y / m_screen.y);

        D3DXMatrixScaling(&polygon, m_polygon.x, m_polygon.y, 1.0f); 
        D3DXMatrixScaling(&scale, m_Parameter.scale.x, m_Parameter.scale.y, 0.0f);
        m_Vector.MatrixVector(rotation);

        // ピボット分オフセット
        polygon._41 = -m_pivot.x;
        polygon._42 = -m_pivot.y;

        // マトリクスの合成
		m_LocalMatrix = polygon * rotation * scale;

        // ピボット分オフセット
        m_LocalMatrix._41 += m_Parameter.position.x + m_pivot.x;
        m_LocalMatrix._42 += m_Parameter.position.y + m_pivot.y;

        m_LocalMatrix._14 = m_LocalMatrix._24 = m_LocalMatrix._34 = 0.0f;
        m_LocalMatrix._44 = 1.0f;

        return &m_LocalMatrix;
    }

    //==========================================================================
    /**
    @brief スプライト描画機能の生成
    @param device [in] デバイス
    */
    void Sprite::CreateRenderer(LPDIRECT3DDEVICE9 device)
    {
        // バッファが存在しないとき
        if (m_Buffer == nullptr)
        {
            // テンプレートバーテックスの宣言
            float commonVtx[] = {
                0.0f, 0.0f, 0.0f,   0.0f, 0.0f,  // 0
                1.0f, 0.0f, 0.0f,   1.0f, 0.0f,  // 1
                0.0f, 1.0f, 0.0f,   0.0f, 1.0f,  // 2
                1.0f, 1.0f, 0.0f,   1.0f, 1.0f,  // 3
            };

            // 基礎バッファを生成
            device->CreateVertexBuffer(sizeof(commonVtx), 0, 0, D3DPOOL_MANAGED, &m_Buffer, nullptr);

            // バッファが存在する際に、バッファ内部にテンプレートバーテックスを登録します
            if (m_Buffer != nullptr)
            {
                float *p = nullptr;
                m_Buffer->Lock(0, 0, (void**)&p, 0);
                memcpy(p, commonVtx, sizeof(commonVtx));
                m_Buffer->Unlock();
            }
        }

        // シェーダ作成
        if (m_Effect == nullptr)
        {
            LPD3DXBUFFER error = 0;
            if (FAILED(D3DXCreateEffectFromFile(device, "resource/Shader/Sprite2D.fx", 0, 0, 0, 0, &m_Effect, &error)))
            {
                OutputDebugStringA((const char*)error->GetBufferPointer());
                return;
            }
        }

        // 頂点宣言作成
        if (m_Declaration == nullptr)
        {
            D3DVERTEXELEMENT9 elems[] = {
                {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
                {0, sizeof(float) * 3, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
                D3DDECL_END()
            };
            device->CreateVertexDeclaration(elems, &m_Declaration);
        }
    }

    //==========================================================================
    /**
    @brief スプライト描画機能の破棄
    */
    void Sprite::DeleteRenderer()
    {
        if (m_Buffer != nullptr)
        {
            m_Buffer->Release();
            m_Buffer = nullptr;
        }
        if (m_Effect != nullptr)
        {
            m_Effect->Release();
            m_Effect = nullptr;
        }
        if (m_Declaration != nullptr)
        {
            m_Declaration->Release();
            m_Declaration = nullptr;
        }
        m_DrawList.clear();
        m_UpdateList.clear();
    }

    //==========================================================================
    /**
    @brief 描画リストを一気に更新
    */
    void Sprite::UpdateAll()
    {
        for (auto &itr : m_UpdateList)
        {
            if (!itr->m_Activity)continue;

            itr->CreateWorldMatrix();
        }
    }

    //==========================================================================
    /**
    @brief 描画リストを一気に描画
    @param device [in] デバイス
    */
    void Sprite::DrawAll(LPDIRECT3DDEVICE9 device)
    {
        if (m_Buffer == nullptr || m_Effect == nullptr || m_Declaration == nullptr)return;	// 描画不可

        // 描画機能を変更
        device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
        device->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

        // 頂点バッファ・頂点宣言設定
        device->SetStreamSource(0, m_Buffer, 0, sizeof(float) * 5);
        device->SetVertexDeclaration(m_Declaration);

        // 2D描画用射影変換行列
        D3DXMATRIX proj;
        D3DXMatrixIdentity(&proj);
        proj._41 = -1.0f;
        proj._42 = 1.0f;

        // シェーダ開始
        UINT numPass = 0;
        m_Effect->SetTechnique("MainShader2D");
        m_Effect->Begin(&numPass, 0);
        m_Effect->BeginPass(0);

        // 描画リストに登録されているスプライトを一気に描画する
        for (auto &itr : m_DrawList)
        {
            if (!itr->m_Activity)continue;

            // 呼ばれたら更新の無効化
            itr->LockUpdate();

            // テクスチャがあり、アクティブであるとき
            if (itr->m_Activity == true && itr->m_TextureData.Existence())
            {
                if (itr->m_TechnologyAdd)
                {
                    itr->ZwriteenableStart(device);
                    itr->Sub(device);
                }

                proj._11 = 2.0f / itr->m_screen.x;
                proj._22 = -2.0f / itr->m_screen.y;

                auto v4color = D3DXVECTOR4(itr->m_color.r, itr->m_color.g, itr->m_color.b, itr->m_color.a);

                m_Effect->SetMatrix("World", &itr->m_WorldMatrix);
                m_Effect->SetMatrix("Projection", &proj);
                m_Effect->SetTexture("Texture", &itr->m_TextureData);
                m_Effect->SetFloat("uvLeft", itr->m_uv.u0);
                m_Effect->SetFloat("uvTop", itr->m_uv.v0);
                m_Effect->SetFloat("uvWidth", itr->m_uv.u1);
                m_Effect->SetFloat("uvHeight", itr->m_uv.v1);
                m_Effect->SetVector("Color", &v4color);

                if (&itr->m_TextureData != nullptr)
                {
                    m_Effect->SetFloat("TexFlag", true);
                }
                else if (&itr->m_TextureData == nullptr)
                {
                    m_Effect->SetFloat("TexFlag", false);
                }

                m_Effect->CommitChanges();
                device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

                if (itr->m_TechnologyAdd)
                {
                    itr->ZwriteenableEnd(device);
                    itr->Add(device);
                }
            }
        }
        m_Effect->EndPass();
        m_Effect->End();

        // 描画機能を変更
        device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
        device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
    }

    SpriteAnimation::SpriteAnimation()
    {
        SetComponentName("SpriteAnimation");
        m_Count = 0;
        m_Frame = 0;
        m_Pattern = 0;
        m_Direction = 0;
        m_loop = false;
        m_lock = false;
    }
    SpriteAnimation::~SpriteAnimation()
    {
    }
    void SpriteAnimation::PlayAnimation(bool loop)
    {
        m_loop = loop;
        m_lock = true;
    }
    void SpriteAnimation::StopAnimation()
    {
        m_lock = false;
    }
    void SpriteAnimation::SetAnimationData(int frame, int pattern, int direction)
    {
        m_Frame = frame;
        m_Pattern = pattern;
        m_Direction = direction;

        int nCount = 0;
        m_uv.u1 = 1.f / m_Direction;
        // 縦の枚数を検索
        for (int i = 0;; i += m_Direction)
        {
            if (m_Pattern <= i) { break; }
            nCount++;
        }
        m_uv.v1 = 1.f / nCount;

        auto & info = m_TextureData.Info();
        auto ratio = m_TextureData.Ratio();
        auto winsize = m_TextureData.WindowsSize();

        m_polygon = D3DXVECTOR2((info.Width*ratio)* m_uv.u1, (info.Height*ratio)* m_uv.v1);
        m_pivot = m_polygon / 2.0f;
        m_screen = winsize.D3DXVECTOR2_CAST();
    }
    void SpriteAnimation::AddAnimationCounter(int count)
    {
		SetAnimationCounter(m_Count + count);
    }
	void SpriteAnimation::SetAnimationCounter(int count)
	{
		if (m_lock)
		{
			m_Count = count;

			if ((m_Frame*m_Pattern) <= m_Count)
			{
				m_Count = 0;
				if (!m_loop)m_lock = false;
			}

			int PattanNum = (m_Count / m_Frame) % m_Pattern; // フレームに1回	パターン数
			int patternV = PattanNum % m_Direction; // 横方向のパターン
			int patternH = PattanNum / m_Direction; // 縦方向のパターン

			m_uv.u0 = patternV * m_uv.u1;
			m_uv.v0 = patternH * m_uv.v1;
			m_Activity = true;
		}

		if (!m_lock)m_Activity = false;
	}
}

_MSLIB_END