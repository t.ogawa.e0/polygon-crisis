//==========================================================================
// Effekseer [MsEffekseer.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "MsEffekseer.h"
#include "Camera.h"
#include "Manager.h"

_MSLIB_BEGIN

namespace MsEffekseer
{
    using renderer::RendererID;

    Effekseer::Manager* EffekseerLoader::m_manager = nullptr; // マネージャー
    EffekseerRendererDX9::Renderer* EffekseerLoader::m_renderer = nullptr; // レンダラー
    EffekseerSound::Sound* EffekseerLoader::m_sound = nullptr; // サウンド

    MsEffekseer::MsEffekseer() : Renderer(RendererID::Effect)
    {
        SetComponentName("Effekseer");
		m_manager = nullptr;
		m_renderer = nullptr;
		m_sound = nullptr;
	}

    //==========================================================================
    /**
    @param device [in] DX9のデバイス
    @param squareMaxCount [in] 使用する頂点数
    */
    MsEffekseer::MsEffekseer(LPDIRECT3DDEVICE9 device, int32_t squareMaxCount) : Renderer(RendererID::Effect)
    {
        SetComponentName("Effekseer");
        // 描画用インスタンスの生成
        if (m_renderer == nullptr)
            m_renderer = ::EffekseerRendererDX9::Renderer::Create(device, squareMaxCount);

        // エフェクト管理用インスタンスの生成
        if (m_manager == nullptr) {
            m_manager = ::Effekseer::Manager::Create(squareMaxCount);

            // 描画用インスタンスから描画機能を設定
            m_manager->SetSpriteRenderer(m_renderer->CreateSpriteRenderer());
            m_manager->SetRibbonRenderer(m_renderer->CreateRibbonRenderer());
            m_manager->SetRingRenderer(m_renderer->CreateRingRenderer());
            m_manager->SetTrackRenderer(m_renderer->CreateTrackRenderer());
            m_manager->SetModelRenderer(m_renderer->CreateModelRenderer());

            // 描画用インスタンスからテクスチャの読込機能を設定
            // 独自拡張可能、現在はファイルから読み込んでいる。
            m_manager->SetTextureLoader(m_renderer->CreateTextureLoader());
            m_manager->SetModelLoader(m_renderer->CreateModelLoader());
        }

        // 音再生用インスタンスの生成
        //m_sound = ::EffekseerSound::Sound::Create(g_xa2, 16, 16);

        // 音再生用インスタンスから再生機能を指定
        //m_manager->SetSoundPlayer(m_sound->CreateSoundPlayer());

        // 音再生用インスタンスからサウンドデータの読込機能を設定
        // 独自拡張可能、現在はファイルから読み込んでいる。
        //m_manager->SetSoundLoader(m_sound->CreateSoundLoader());
    }

    MsEffekseer::~MsEffekseer()
    {
        // エフェクトの停止
        if (m_manager != nullptr)
			for (auto & itr : m_handle_list)
				m_manager->StopEffect(itr.GetEffectHandle());

        // ハンドル全破棄
        m_handle_list.clear();

        // エフェクトの破棄
        m_effect_list.clear();

        // 先にエフェクト管理用インスタンスを破棄
        if (m_manager != nullptr)
		{
            m_manager->Destroy();
            m_manager = nullptr;
        }

        // 次に音再生用インスタンスを破棄
		if (m_sound != nullptr)
		{
			m_sound->Destroy();
			m_sound = nullptr;
		}

        // 次に描画用インスタンスを破棄
        if (m_renderer != nullptr)
		{
            m_renderer->Destroy();
            m_renderer = nullptr;
        }
    }

    //==========================================================================
    /**
    @brief エフェクトの読み込み
    @param path [in] エフェクトのファイルパス >> (const EFK_CHAR*)L"effect_name"
    @param effect_name [in] エフェクト名
    @return エフェクトのポインタ
    */
    Effekseer::Effect * MsEffekseer::EffectLoad(const EFK_CHAR* path, const std::string & effect_name)
    {
        // エフェクトの読込
        if (m_manager != nullptr)
        {
            auto * effect = Effekseer::Effect::Create(m_manager, path);
            m_effect_list.emplace_back(effect_name, effect);
            return effect;
        }
        return nullptr;
    }

    //==========================================================================
    /**
    @brief マネージャーの取得
    @return マネージャーのポインタ
    */
    Effekseer::Manager * MsEffekseer::GetManager()
    {
        return m_manager;
    }

    //==========================================================================
    /**
    @brief レンダラーの取得
    @return レンダラーのポインタ
    */
    EffekseerRendererDX9::Renderer * MsEffekseer::GetRenderer()
    {
        return m_renderer;
    }

    //==========================================================================
    /**
    @brief エフェクトの再生情報の記録
    @param handle [in] ハンドル
    @param effect [in] エフェクトポインタ
    @param effect_name [in] エフェクト名
    */
    void MsEffekseer::SetPlayData(Effekseer::Handle handle, Effekseer::Effect * effect, const std::string & effect_name)
    {
        m_handle_list.emplace_back(handle, effect, effect_name);
    }

    //==========================================================================
    /**
    @brief エフェクトの再生情報の取得
    @return 再生情報格納コンテナ
    */
    std::list<MsEffekseer::HandleList>& MsEffekseer::GetHandleList()
    {
        return m_handle_list;
    }

    //==========================================================================
    /**
    @brief エフェクトコンテナの取得
    @return エフェクトコンテナ
    */
    std::list<MsEffekseer::Effect>& MsEffekseer::GetEffectList()
    {
        return m_effect_list;
    }

    //==========================================================================
    /**
    @brief エフェクトの再生情報の破棄
    @param handle [in] ハンドルコンテナ
    */
    void MsEffekseer::DestroyHandle(HandleList & handle)
    {
        for (auto itr = m_handle_list.begin(); itr != m_handle_list.end();)
        {
            if (itr->GetEffectHandle() == handle.GetEffectHandle())
            {
                itr = m_handle_list.erase(itr);
            }
            else
            {
                ++itr;
            }
        }
    }

    //==========================================================================
    /**
    @brief エフェクトの再生情報の破棄
    @param handle [in] ハンドル
    */
    void MsEffekseer::DestroyHandle(Effekseer::Handle handle)
    {
        for (auto itr = m_handle_list.begin(); itr != m_handle_list.end();)
        {
            if (itr->GetEffectHandle() == handle)
            {
                itr = m_handle_list.erase(itr);
            }
            else
            {
                ++itr;
            }
        }
    }

    //==========================================================================
    /**
    @brief エフェクトの再生情報の破棄
    @param effect [in] エフェクトポインタ
    */
    void MsEffekseer::DestroyHandle(Effekseer::Effect * effect)
    {
        for (auto itr = m_handle_list.begin(); itr != m_handle_list.end();)
        {
            if (itr->GetEffect() == effect)
            {
                itr = m_handle_list.erase(itr);
            }
            else
            {
                ++itr;
            }
        }
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void MsEffekseer::Update()
    {
		if (m_manager == nullptr)return;

        m_manager->Update();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void MsEffekseer::Draw(LPDIRECT3DDEVICE9 device)
    {
        device;

		if(m_renderer == nullptr || m_manager == nullptr)return;

        // エフェクトの描画開始処理を行う。
        m_renderer->BeginRendering();

        // エフェクトの描画を行う。
        m_manager->Draw();

        // エフェクトの描画終了処理を行う。
        m_renderer->EndRendering();
    }

    void MsEffekseer::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        device;
    }
    EffekseerParam::EffekseerParam()
    {
        m_manager = nullptr;
        m_renderer = nullptr;
        m_sound = nullptr;
        m_effect = nullptr;
    }
    EffekseerParam::~EffekseerParam()
    {
    }
    EffekseerData::EffekseerData()
    {
        LoaderPtr = nullptr;
    }
    EffekseerData::~EffekseerData()
    {
    }
    EffekseerReference::EffekseerReference()
    {
        m_handle = stop_handle;
    }
    EffekseerReference::~EffekseerReference()
    {
        Release();
    }
    void EffekseerReference::Release()
    {
        if (m_data == nullptr)return;
		if (m_data->LoaderPtr == nullptr)nullptr;
		if (m_data->asset.m_manager == nullptr)nullptr;
		if (stop_handle != m_handle) {
			m_data->asset.m_manager->StopEffect(m_handle);
			m_handle = stop_handle;
		}
		if (m_data->Release()) {
            m_data->LoaderPtr->Unload(m_data);
        }
        m_data = nullptr;
    }
    //void EffekseerReference::Play(const D3DXVECTOR3 & vec3)
    //{
    //    if (m_data == nullptr)return;
    //    if (m_data->m_manager == nullptr)return;
    //    if (m_data->asset == nullptr)return;
    //    if (GetShown())return;
    //    m_handle = m_data->m_manager->Play(m_data->asset, vec3.x, vec3.y, vec3.z);
    //}
    //void EffekseerReference::Stop()
    //{
    //    if (m_data == nullptr)return;
    //    if (m_data->m_manager == nullptr)return;
    //    if (m_handle == stop_handle)return;
    //    m_data->m_manager->StopEffect(m_handle);
    //    m_handle = stop_handle;
    //}
    //bool EffekseerReference::GetShown()
    //{
    //    if (m_data == nullptr)return false;
    //    if (m_data->m_manager == nullptr)return false;
    //    if (m_handle == stop_handle)return false;
    //    if (m_data->m_manager->GetShown(m_handle)) return true;
    //    m_data->m_manager->StopEffect(m_handle);
    //    m_handle = stop_handle;
    //    return false;
    //}
    Effekseer::Handle EffekseerReference::GetHandle()
    {
        return m_handle;
    }
    void EffekseerReference::SetHandle(Effekseer::Handle handle)
    {
        m_handle = handle;
    }
    const std::string & EffekseerReference::Tag()
    {
        return m_data->tag;
    }
    EffekseerLoader::EffekseerLoader()
    {
        m_device = nullptr;
    }
    EffekseerLoader::EffekseerLoader(LPDIRECT3DDEVICE9 device)
    {
        m_device = device;
    }
    EffekseerLoader::~EffekseerLoader()
    {
        AllDestroyComponent();

        for (auto & itr : m_data)
			if (itr.second.asset.m_effect != nullptr)
			{
				itr.second.asset.m_effect->Release();
				itr.second.asset.m_effect = nullptr;
			}
		m_data.clear();
        Release();
    }
    void EffekseerLoader::UpdateAll()
    {
        if (m_manager == nullptr)return;

        m_manager->Update();
    }
    void EffekseerLoader::DrawAll()
    {
		if (m_manager == nullptr || m_renderer == nullptr)return;

		auto view = camera::Camera::GetCamera();
		if (view == nullptr)return;

        // 投影行列を設定
        m_renderer->SetProjectionMatrix(
            ::Effekseer::Matrix44().PerspectiveFovLH(D3DXToRadian(60), (float)manager::Manager::GetDevice()->GetWindowsSize().x / (float)manager::Manager::GetDevice()->GetWindowsSize().y, 0.1f, 1000.0f));

        auto vEye = view->GetLook1().eye + view->GetLook2().eye;
        auto vAt = view->GetLook1().at + view->GetLook2().at;
        auto vUp = view->GetLook1().up;

        m_renderer->SetCameraMatrix(
            ::Effekseer::Matrix44().LookAtLH(Effekseer::Vector3D(vEye.x, vEye.y, vEye.z), ::Effekseer::Vector3D(vAt.x, vAt.y, vAt.z), ::Effekseer::Vector3D(vUp.x, vUp.y, vUp.z)));

        // エフェクトの描画開始処理を行う。
        if (m_renderer->BeginRendering())
        {
            // エフェクトの描画を行う。
            m_manager->Draw();

            // エフェクトの描画終了処理を行う。
            m_renderer->EndRendering();
        }
    }
    EffekseerReference EffekseerLoader::Load(const EFK_CHAR* path, const std::string & effect_name)
    {
        if (m_device == nullptr)return EffekseerReference();

        // 描画用インスタンスの生成
        if (m_renderer == nullptr)
            m_renderer = ::EffekseerRendererDX9::Renderer::Create(m_device, 256000);

        // エフェクト管理用インスタンスの生成
        if (m_manager == nullptr) {
            m_manager = ::Effekseer::Manager::Create(10000);

            // 描画用インスタンスから描画機能を設定
            m_manager->SetSpriteRenderer(m_renderer->CreateSpriteRenderer());
            m_manager->SetRibbonRenderer(m_renderer->CreateRibbonRenderer());
            m_manager->SetRingRenderer(m_renderer->CreateRingRenderer());
            m_manager->SetTrackRenderer(m_renderer->CreateTrackRenderer());
            m_manager->SetModelRenderer(m_renderer->CreateModelRenderer());

            // 描画用インスタンスからテクスチャの読込機能を設定
            // 独自拡張可能、現在はファイルから読み込んでいる。
            m_manager->SetTextureLoader(m_renderer->CreateTextureLoader());
            m_manager->SetModelLoader(m_renderer->CreateModelLoader());
        }

        auto itr = m_data.find(effect_name);

        // 存在しない場合は読み込み処理を開始する
        if (itr == m_data.end())
        {
            auto &data = m_data[effect_name];

            data.LoaderPtr = this;
            data.tag = effect_name;
            data.asset.m_effect = Effekseer::Effect::Create(m_manager, path);
            data.asset.m_manager = m_manager;
            data.asset.m_renderer = m_renderer;
            data.asset.m_sound = m_sound;
        }

        return &m_data[effect_name];

        // 音再生用インスタンスの生成
        //m_sound = ::EffekseerSound::Sound::Create(g_xa2, 16, 16);

        // 音再生用インスタンスから再生機能を指定
        //m_manager->SetSoundPlayer(m_sound->CreateSoundPlayer());

        // 音再生用インスタンスからサウンドデータの読込機能を設定
        // 独自拡張可能、現在はファイルから読み込んでいる。
        //m_manager->SetSoundLoader(m_sound->CreateSoundLoader());
    }
    void EffekseerLoader::Unload(EffekseerData * data)
    {
        if (data == nullptr)return;
        if (data->LoaderPtr != this)return;
        auto itr = m_data.find(data->tag);
        if (itr == m_data.end())return;
        if (m_manager != nullptr) {
            m_manager->StopRoot(itr->second.asset.m_effect);
        }
        if (itr->second.asset.m_effect != nullptr) {
            itr->second.asset.m_effect->Release();
            itr->second.asset.m_effect = nullptr;
        }
        m_data.erase(itr);
        if (m_data.size() == 0) {
            m_manager->StopAllEffects();
            Release();
        }
    }
    void EffekseerLoader::Unload(EffekseerData & data)
    {
        if (data.LoaderPtr != this)return;
        auto itr = m_data.find(data.tag);
        if (itr == m_data.end())return;
        if (m_manager != nullptr) {
            m_manager->StopRoot(itr->second.asset.m_effect);
        }
        if (itr->second.asset.m_effect != nullptr) {
            itr->second.asset.m_effect->Release();
            itr->second.asset.m_effect = nullptr;
        }
        m_data.erase(itr);
        if (m_data.size() == 0) {
            m_manager->StopAllEffects();
            Release();
        }
    }
    void EffekseerLoader::Release()
    {
        // 先にエフェクト管理用インスタンスを破棄
        if (m_manager != nullptr) {
            m_manager->Destroy();
            m_manager = nullptr;
        }

        // 次に音再生用インスタンスを破棄
        if (m_sound != nullptr) {
            m_sound->Destroy();
            m_sound = nullptr;
        }

        // 次に描画用インスタンスを破棄
        if (m_renderer != nullptr) {
            m_renderer->Destroy();
            m_renderer = nullptr;
        }
    }
    SetEffekseer::SetEffekseer()
    {

    }
    SetEffekseer::~SetEffekseer()
    {
    }
    void SetEffekseer::SetEffekseerData(const EffekseerReference & effekseer)
    {
        m_EffekseerData = effekseer;
    }
    EffekseerReference & SetEffekseer::GetEffekseerData()
    {
        return m_EffekseerData;
    }
    Effekseer::Manager * SetEffekseer::GetManager()
    {
        return (&m_EffekseerData).m_manager;
    }
    Effekseer::Effect * SetEffekseer::GetEffect()
    {
        return (&m_EffekseerData).m_effect;
    }
    EffekseerSound::Sound * SetEffekseer::GetSound()
    {
        return (&m_EffekseerData).m_sound;
    }
    Effekseer::Handle SetEffekseer::GetHandle()
    {
        return m_EffekseerData.GetHandle();
    }
    void SetEffekseer::SetHandle(Effekseer::Handle handle)
    {
        m_EffekseerData.SetHandle(handle);
    }
}

_MSLIB_END
