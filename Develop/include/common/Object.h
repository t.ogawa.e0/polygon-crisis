//==========================================================================
// オブジェクト[Object.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "mslib_struct.h"

//==========================================================================
// lib include
//==========================================================================
#include "Transform.h"
#include "Renderer.h"
#include "Texture.h"
#include "DrawingTechnology.h"
#include "Mesh.h"
#include "XModel.h"
#include "Sphere.h"
#include "Cube.h"
#include "Billboard.h"
#include "MeshField.h"
#include "MsEffekseer.h"

#include "DX9_Vertex3D.h"

_MSLIB_BEGIN

namespace object
{
    /**
    @brief 使うマトリクスの種類
    */
    enum class MatrixType : int
    {
        /// <summary>
        /// 描画機能でおすすめの行列が使用されます
        /// </summary>
        Default = 0,
        /// <summary>
        /// ワールド行列を生成します
        /// </summary>
        World,
        /// <summary>
        /// ローカル行列を生成します
        /// </summary>
        Local,
        /// <summary>
        /// ワールド行列を生成します
        /// </summary>
        WorldView,
        /// <summary>
        /// ローカル行列を生成します
        /// </summary>
        LocalView,
    };

    //==========================================================================
    //
    // class  : MaterialLighting
    // Content: マテリアルのライティング
    //
    //==========================================================================
    class MaterialLighting
    {
    public:
        MaterialLighting();
        virtual ~MaterialLighting();

        /**
        @brief アンビエントライト
        @param color [in] ライト設定(0.0f〜1.0f)
        */
        void SetAmbient(const D3DXCOLOR & color);

        /**
        @brief ディレクショナルライト
        @param color [in] ライト設定(0.0f〜1.0f)
        */
        void SetDiffuse(const D3DXCOLOR & color);

        /**
        @brief スペキュラー
        @param color [in] ライト設定(0.0f〜1.0f)
        */
        void SetSpecular(const D3DXCOLOR & color);

        /**
        @brief 放射性
        @param color [in] ライト設定(0.0f〜1.0f)
        */
        void SetEmissive(const D3DXCOLOR & color);

        /**
        @brief スペキュラー光のパワー値
        */
        void SetMaterialPower(float power);

        /**
        @brief マテリアルのライティングの設定
        */
        void SetD3DMaterial9(const D3DMATERIAL9 & material);

        /**
        @brief マテリアルのライティングの取得
        @return マテリアルのライティング情報
        */
        const D3DMATERIAL9 & GetD3DMaterial9() const;
    protected:
        D3DMATERIAL9 MeshLighting;
    };

    //==========================================================================
    //
    // class  : InheritanceObject
    // Content: 継承オブジェクト
    //
    //==========================================================================
    class InheritanceObject : public transform::Transform , public MaterialLighting
    {
    public:
        InheritanceObject();
        InheritanceObject(const InheritanceObject& obj);
        virtual ~InheritanceObject();
        /**
        @brief 初期化
        */
        void Init();

        /**
        @brief オブジェクトの更新
        */
        const D3DXMATRIX * UpdateMatrix();

        /**
        @brief ラジコン回転
        @param vecRight [in] 向かせたい方向ベクトル
        @param speed [in] 向かせる速度
        */
        void RadioControl(const D3DXVECTOR3 & vecRight, float speed);

        /**
        @brief 対象の方向に向かせる(ベクトル計算を行い、処理を行います)
        @param Input [in] ターゲット
        @param power 向かせる力
        */
        void LockOn(transform::Transform & Input, float power);

        /**
        @brief 対象の方向に向かせる(直接ベクトルを操作します)
        @param Input [in] ターゲット
        */
        void LockOn(transform::Transform & Input);

        /**
        @brief 使うマトリクスの種類
        @param type [in] マトリクスのタイプ
        */
        void SetMatrixType(const MatrixType & type);

        /**
        @brief 使用中のマトリクスの種類
        @return マトリクスの種類
        */
        MatrixType GetMatrixType() const;

        /**
        @brief サードパーソン用の座標取得
        @param move_pos [in] 対象オブジェクトを始点とした座標を入力してください
        @return サードパーソン用の座標
        */
        D3DXVECTOR3 GetThirdPerson(const D3DXVECTOR3 & move_pos);

        /**
        @brief 向きベクトル描画
        */
        void DrawVector(LPDIRECT3DDEVICE9 device);
    protected: // operator
        InheritanceObject &operator=(const InheritanceObject & obj);
    protected:
        MatrixType m_MatType; // マトリクスのタイプ
    };

    //==========================================================================
    //
    // class  : Object
    // Content: オブジェクト
    //
    //==========================================================================
    class Object : public InheritanceObject, public renderer::Renderer
    {
    public:
        Object();
        virtual ~Object();

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : Mesh
    // Content: メッシュ
    //
    //==========================================================================
    class Mesh : public InheritanceObject, public renderer::Renderer, public texture::SetTexture, public mesh::SetMesh, private DX9_VERTEX_3D
    {
    public:
        Mesh();
        virtual ~Mesh();

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : XModel
    // Content: Xモデル
    //
    //==========================================================================
    class XModel : public InheritanceObject, public renderer::Renderer, public xmodel::SetXModel
    {
    public:
        XModel();
        virtual ~XModel();

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : Sphere
    // Content: 球体
    //
    //==========================================================================
    class Sphere : public InheritanceObject, public renderer::Renderer, public texture::SetTexture, public sphere::SetSphere
    {
    public:
        Sphere();
        virtual ~Sphere();

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : Cube
    // Content: キューブ 
    //
    //==========================================================================
    class Cube : public InheritanceObject, public renderer::Renderer, public texture::SetTexture, public cube::SetCube, public DX9_VERTEX_3D
    {
    public:
        Cube();
        virtual ~Cube();

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : Billboard
    // Content: ビルボード
    //
    //==========================================================================
    class Billboard : public InheritanceObject, public renderer::Renderer, public texture::SetTexture, public billboard::SetBillboard, private DX9_VERTEX_3D, public drawing_technology::DrawingTechnology
    {
    public:
        Billboard();
        virtual ~Billboard();

        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    protected:
        floatUV m_uv; // UV
        intColor m_color; // 色
        billboard::Polygon m_polygon; // ポリゴン
    };

    //==========================================================================
    //
    // class  : BillboardAnimation
    // Content: アニメーションビルボード
    //
    //==========================================================================
    class BillboardAnimation : public Billboard
    {
    public:
        BillboardAnimation();
        virtual ~BillboardAnimation();
        void PlayAnimation(bool loop);
        void StopAnimation();
        void SetAnimationData(int count, int frame, int pattern, int direction);
        void AddAnimationCounter(int count);
    protected:
        floatTexvec m_cut; // 切り取り情報
        intTexvec m_texsize; // テクスチャのサイズ
        int m_Count; // アニメーションカウンタ
        int m_Frame; // 更新タイミング
        int m_Pattern; // アニメーションのパターン数
        int m_Direction; // 一行のアニメーション数
        bool m_loop; // ループ再生
        bool m_lock; // ロック
    };

    //==========================================================================
    //
    // class  : MeshField
    // Content: メッシュフィールド
    //
    //==========================================================================
    class MeshField : public object::InheritanceObject, public renderer::Renderer, public texture::SetTexture, public meshfield::SetMeshField, private DX9_VERTEX_3D
    {
    public:
        MeshField();
        virtual ~MeshField();
        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : Light
    // Content: 光源
    //
    //==========================================================================
    class Light : public object::InheritanceObject, public renderer::Renderer
    {
    public:
        Light();
        virtual ~Light();
        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief 光源の向きの取得
        @param obj [in] 光を当てるオブジェクト
        @param LightDir [in] 光源の向きの取得
        */
        void GetDirection(const transform::Transform & obj, D3DXVECTOR3 & LightDir);

        /**
        @brief 光源の向きの取得
        @param obj [in] 光を当てるオブジェクト
        @param LightDir [in] 光源の向きの取得
        */
        void GetDirection(const transform::Transform & obj, D3DXVECTOR4 & LightDir);

        /**
        @brief 光源のタイプ
        @param type [in] ライトの種類
        */
        void SetType(const D3DLIGHTTYPE & type);

        /**
        @brief 光源の使用可否
        @param frag [in] frag
        */
        void Enable(int index, bool frag);
    protected:
        D3DLIGHT9 m_light; // 光源
        int m_index; // 光源ID
        bool m_enable; // 使用フラグ
    };

    //==========================================================================
    //
    // class  : Effekseer
    // Content: Effekseer
    //
    //==========================================================================
    class Effekseer : public object::InheritanceObject, public renderer::Renderer, public MsEffekseer::SetEffekseer
    {
    public:
        Effekseer();
        virtual ~Effekseer();
        /**
        @brief 更新
        */
        virtual void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        virtual void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        virtual void DebugDraw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief 再生
        */
        void Play();

        /**
        @brief 再生
        */
        void Play(transform::Transform * obj);

        /**
        @brief 停止
        */
        void Stop();

		/**
		@brief エフェクトが再生されているかのチェック
		@return 再生中なら true 再生されていないなら false が返る
		*/
		bool Check();

        /**
        @brief ターゲットの登録
        */
        void SetTarget(transform::Transform * obj);
    protected:
        transform::Transform * m_target;
		MsEffekseer::Matrix43 m_mat43;
    };
}

_MSLIB_END