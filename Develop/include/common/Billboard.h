//==========================================================================
// ビルボード [Billboard.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "mslib.hpp"
#include "Component.h"
#include "CreateBuffer.h"
#include "DX9_Vertex3D.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace billboard
{
    class CreateBillboard;

    //==========================================================================
    //
    // class  : BillboardData
    // Content: ビルボードデータ
    //
    //==========================================================================
    class BillboardData : public ireference::ReferenceData<create_buffer::Buffer>
    {
    public:
        BillboardData();
        ~BillboardData();
    public:
        CreateBillboard * CreatePtr; // 生成元のポインタ
    };

    //==========================================================================
    //
    // class  : BillboardReference
    // Content: 参照用
    //
    //==========================================================================
    class BillboardReference : public ireference::ReferenceOperator<BillboardData>
    {
    public:
        BillboardReference();
        ~BillboardReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
    };

    //==========================================================================
    // 頂点情報
    //==========================================================================
    struct Polygon
    {
        D3DXVECTOR3 upper_left = D3DXVECTOR3(-0.5f, 0.5f, 0.0f); // 左上
        D3DXVECTOR3 upper_right = D3DXVECTOR3(0.5f, 0.5f, 0.0f); // 右上
        D3DXVECTOR3 bottom_right = D3DXVECTOR3(0.5f, -0.5f, 0.0f); // 右下
        D3DXVECTOR3 bottom_left = D3DXVECTOR3(-0.5f, -0.5f, 0.0f); // 左下
    };

    //==========================================================================
    //
    // class  : CreateBillboard
    // Content: ビルボード生成
    //
    //==========================================================================
    class CreateBillboard : public component::Component, public create_buffer::CreateBuffer, private DX9_VERTEX_3D
    {
    private:
        // コピー禁止 (C++11)
        CreateBillboard (const CreateBillboard &) = delete;
        CreateBillboard &operator=(const CreateBillboard &) = delete;
        CreateBillboard &operator=(CreateBillboard&&) = delete;
    public:
        CreateBillboard();
        CreateBillboard(LPDIRECT3DDEVICE9 device, HWND hWnd);
        ~CreateBillboard();

        /**
        @brief ビルボードを生成する。
        */
        BillboardReference Create();

        /**
        @brief ビルボードを破棄する。
        @param data [in] データ
        */
        void Delete(BillboardData * data);
    private:
        /**
        @brief バーテックスの生成
        @param out [out] 受け取り
        @param NumRectangle [in] 数
        */
        void CreateVertex(VERTEX_4 * out);

        /**
        @brief Indexの生成
        @param out [out] 受け取り
        @param NumRectangle [in] 数
        */
        void CreateIndex(WORD * Output, int NumRectangle);
    private:
        BillboardData * m_BillboardData; // データ
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
    };

    //==========================================================================
    //
    // class  : SetBillboard
    // Content: ビルボード登録クラス
    //
    //==========================================================================
    class SetBillboard
    {
    public:
        SetBillboard();
        virtual ~SetBillboard();

        /**
        @brief ビルボードの登録
        @param data [in] ビルボード
        */
        void SetBillboardData(const BillboardReference & data);

        /**
        @brief ビルボードの取得
        */
        BillboardReference & GetBillboardData();
    protected:
        BillboardReference m_BillboardData; // ビルボードデータ
    };
}

_MSLIB_END