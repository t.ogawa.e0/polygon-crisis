//==========================================================================
// キーボード入力処理[DinputKeyboard.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DinputKeyboard.h"

_MSLIB_BEGIN

namespace dinput_keyboard
{
    Keyboard::Keyboard()
    {
        SetComponentName("Keyboard");
    }

    Keyboard::~Keyboard()
    {
    }

    //==========================================================================
    /**
    @brief 初期化
    @param hInstance [in] インスタンスハンドル
    @param hWnd [in] ウィンドウハンドル
    @return 失敗時に true が返ります
    */
    bool Keyboard::Init(HINSTANCE hInstance, HWND hWnd)
    {
        // DirectInputオブジェクトの作成
        if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_DInput, nullptr)))
        {
            MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_ICONWARNING);
            return true;
        }

        // デバイスの作成
        if (FAILED(m_DInput->CreateDevice(GUID_SysKeyboard, &m_DIDevice, nullptr)))
        {
            MessageBox(hWnd, "キーボードがありませんでした", "警告", MB_ICONWARNING);
            return true;
        }

        if (m_DIDevice != nullptr)
        {
            // データフォーマットを設定
            if (FAILED(m_DIDevice->SetDataFormat(&c_dfDIKeyboard)))
            {
                MessageBox(hWnd, "キーボードのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
                return true;
            }

            // 協調モードを設定（フォアグラウンド＆非排他モード）
            if (FAILED(m_DIDevice->SetCooperativeLevel(hWnd, (DISCL_FOREGROUND/*DISCL_BACKGROUND*/ | DISCL_NONEXCLUSIVE))))
            {
                MessageBox(hWnd, "キーボードの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
                return true;
            }

            // キーボードへのアクセス権を獲得(入力制御開始)
            m_DIDevice->Acquire();
        }

        return false;
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void Keyboard::Update(void)
    {
        BYTE aState[256];

        if (m_DIDevice == nullptr)
        {
            return;
        }

        // デバイスからデータを取得
        if (SUCCEEDED(m_DIDevice->GetDeviceState(sizeof(aState), aState)))
        {
            // aKeyState[m_KeyMax]&0x80
            for (int i = 0; i < 256; i++)
            {
                // キートリガー・リリース情報を生成
                m_StateTrigger[i] = (m_State[i] ^ aState[i]) & aState[i];
                m_StateRelease[i] = (m_State[i] ^ aState[i]) & m_State[i];

                // キーリピート情報を生成
                if (aState[i])
                {
                    if (m_StateRepeatCnt[i] < 20)
                    {
                        m_StateRepeatCnt[i]++;
                        if (m_StateRepeatCnt[i] == 1 || m_StateRepeatCnt[i] >= 20)
                        {// キーを押し始めた最初のフレーム、または一定時間経過したらキーリピート情報ON
                            m_StateRepeat[i] = aState[i];
                        }
                        else
                        {
                            m_StateRepeat[i] = 0;
                        }
                    }
                }
                else
                {
                    m_StateRepeatCnt[i] = 0;
                    m_StateRepeat[i] = 0;
                }

                // キープレス情報を保存
                m_State[i] = aState[i];
            }
        }
        else
        {
            // キーボードへのアクセス権を取得
            m_DIDevice->Acquire();
        }
    }

    //==========================================================================
    /**
    @brief Key Cast(int)
    @param cast [in] キャスト対象
    @return キャストされた値が返ります
    */
    Button Keyboard::KeyCast(int cast)
    {
        return (Button)cast;
    };

    //==========================================================================
    /**
    @brief プレス
    @param key [in] 入力キー
    @return 押されている場合 true が返ります
    */
    bool Keyboard::Press(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_State[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief トリガー
    @param key [in] 入力キー
    @return 押されている場合 true が返ります
    */
    bool Keyboard::Trigger(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateTrigger[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief リピート
    @param key [in] 入力キー
    @return 押されている場合 true が返ります
    */
    bool Keyboard::Repeat(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateRelease[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief リリース
    @param key [in] 入力キー
    @return 押されている場合 true が返ります
    */
    bool Keyboard::Release(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateRelease[(int)key] & 0x80) ? true : false;
    }
}
_MSLIB_END