//==========================================================================
// オブジェクト管理[ObjectManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <d3d9.h>
#include <d3dx9.h>
#include <XInput.h>
#include <XAudio2.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include <dinput.h>
#include <tchar.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "Component.h" // コンポーネント
#include "Transform.h" // トランスフォーム
#include "Activity.h" // アクティビティ
#include "Sprite.h" // スプライト
#include "Object.h" // オブジェクト
#include "Manager.h" // マネージャー
#include "Camera.h" // カメラ
#include "Grid.h" // グリッド
#include "Text.h" // テキスト
#include "MsImGui.h" // ImGui
#include "Fog.h" // フォグ
#include "XInput.h" // 入力処理
#include "enum_system.hpp" // enum演算子
#include "vector_wrapper.h" // ベクターのラッパー
#include "list_wrapper.h"// リスト管理オブジェクト
#include "MsEffekseer.h" // エフェクシア
#include "Timer.h" // タイマー
#include "Collider.h" // コリジョン
#include "PlaneCollider.h" // コリジョン
#include "BoxCollider.h" // コリジョン
#include "SphereCollider.h" // コリジョン
#include "MeshCollider.h" // コリジョン
#include "DistanceCollider.h" // コリジョン
#include "MeshField.h" // メッシュフィールド
#include "SetSampler.h" // サンプラー 
#include "Gravity.h" // 重力
#include "VirtualController.h" // 仮想コントローラー
#include "State.h" // ステート

_MSLIB_BEGIN

//==========================================================================
//
// class  : ObjectManager
// Content: オブジェクト管理クラス
//
//==========================================================================
class ObjectManager : public component::Component, public manager::Manager, public activity::Activity
{
private:
    /**
    @brief コピー禁止 (C++11)
    */
    ObjectManager(const ObjectManager &) = delete;
    ObjectManager &operator=(const ObjectManager &) = delete;
    ObjectManager &operator=(ObjectManager&&) = delete;
public:
    /**
    @brief コンストラクタ
    @param ObjectID [in] オブジェクトID
    */
    ObjectManager(const std::string & ObjectName);

    /**
    @brief デストラクタ
    */
    virtual ~ObjectManager();

    /**
    @brief 継承初期化
    */
    virtual void Init(void) = 0;

    /**
    @brief 継承更新
    */
    virtual void Update(void) = 0;

    /**
    @brief 継承デバッグ
    */
    virtual void Debug(void) = 0;

    /**
    @brief オブジェクト名の取得
    @return オブジェクト名
    */
    const std::string * GetObjectName(void) const;

    /**
    @brief 初期化ロック
    */
    void Lock(void);

    /**
    @brief 初期化ロックの取得
    @return 未ロックの場合は false が返る
    */
    bool GetLock(void);

    /**
    @brief オブジェクト情報を保存しておくためのファイル名
    @param file_name [in] ファイル名
    */
    void SetFileName(const std::string & file_name);

    /**
    @brief スプライト
    @return スプライトのインスタンス
    */
    wrapper::vector_component<sprite::Sprite> * Sprite();

    /**
    @brief スプライトアニメーション
    @return スプライトアニメーションのインスタンス
    */
    wrapper::vector_component<sprite::SpriteAnimation> * SpriteAnimation();

    /**
    @brief Xモデル
    @return Xモデルのインスタンス
    */
    wrapper::vector_component<object::XModel> * XModel();

    /**
    @brief キューブ
    @return キューブのインスタンス
    */
    wrapper::vector_component<object::Cube> * Cube();

    /**
    @brief 球体
    @return 球体のインスタンス
    */
    wrapper::vector_component<object::Sphere> * Sphere();

    /**
    @brief オブジェクト
    @return オブジェクトのインスタンス
    */
    wrapper::vector_component<object::Object> * Object();

    /**
    @brief ビルボード
    @return ビルボードのインスタンス
    */
    wrapper::vector_component<object::Billboard> * Billboard();

    /**
    @brief ビルボードアニメーション
    @return ビルボードアニメーションのインスタンス
    */
    wrapper::vector_component<object::BillboardAnimation> * BillboardAnimation();

    /**
    @brief メッシュ
    @return メッシュのインスタンス
    */
    wrapper::vector_component<object::Mesh> * Mesh();

    /**
    @brief メッシュフィールド
    @return メッシュフィールドのインスタンス
    */
    wrapper::vector_component<object::MeshField> * MeshField();

    /**
    @brief 光源
    @return 光源のインスタンス
    */
    wrapper::vector_component<object::Light> * Light();

    /**
    @brief Effekseer
    @return Effekseerのインスタンス
    */
    wrapper::vector_component<object::Effekseer> * Effekseer();

    /**
    @brief グリッド
    @return グリッドのインスタンス
    */
    Grid * Grid_(void);

    /**
    @brief テキスト
    @return テキストのインスタンス
    */
    Text * Text_(void);

    /**
    @brief ImGui
    @return ImGuiのインスタンス
    */
    MsImGui * ImGui(void);

    /**
    @brief XInput
    @return XInputのインスタンス
    */
    xinput::XInput *XInput_(void);

    /**
    @brief XAudio
    @return XAudioのインスタンス
    */
    xaudio2::XAudio2 *XAudio(void);

    /**
    @brief カメラ
    @return カメラのインスタンス
    */
    wrapper::vector_component<camera::Camera> *Camera(void);

    /**
    @brief タイマー
    @return タイマーのインスタンス
    */
    wrapper::vector_component<timer::Timer> * Timer(void);

    /**
    @brief メルセンヌ・ツイスタの32ビット版
    @return 初期シード値の取得
    */
    std::mt19937 & GetMt19937(void);

    /**
    @brief 範囲の一様乱数管理関数(int)
    @return 範囲の一様乱数
    */
    wrapper::vector<rand_int> * _rand_int(void);

    /**
    @brief 範囲の一様乱数管理関数(float)
    @return 範囲の一様乱数
    */
    wrapper::vector<rand_float> * _rand_float(void);

    /**
    @brief フォグ
    @return フォグイスタンス
    */
    Fog * Fog_(void);

    /**
    @brief 現在の時間<型指定>
    @return 時間
    */
    template<typename _Ty>
    _Ty GetTimeSec(void);

    void UIEditor();

    void ObjectEditor();

    void ObjectList();

    void Save(std::ofstream & ofstream);

    void Load(std::ifstream & ifstream);

    /**
    @brief ファイル指定
    @param file_pass [in] ファイルパス
    */
    void SetFilePass(const std::string file_pass);

    /**
    @brief 処理時間の設定
    @param label [in] 処理フェイズ
    @param time [in] 時間
    */
    void SetProcessTime(float time);

    /**
    @brief 処理時間の取得
    */
    float GetProcessTime();
private:
    wrapper::vector<rand_int> * m_rand_int; // 範囲の一様乱数
    wrapper::vector<rand_float> * m_rand_float; // 範囲の一様乱数
    wrapper::vector_component<timer::Timer> * m_Timer; // タイマー
    wrapper::vector_component<camera::Camera> * m_Camera; // カメラ座標インスタンス管理
    wrapper::vector_component<sprite::Sprite> * m_Sprite; // スプライト
    wrapper::vector_component<sprite::SpriteAnimation> * m_SpriteAnimation; // スプライトアニメーション
    wrapper::vector_component<object::XModel> * m_XModel; // Xモデル
    wrapper::vector_component<object::Cube> * m_Cube; // キューブ
    wrapper::vector_component<object::Sphere> * m_Sphere; // 球体
    wrapper::vector_component<object::Billboard> * m_Billboard; // ビルボード
    wrapper::vector_component<object::BillboardAnimation> * m_BillboardAnimation; // ビルボードアニメーション
    wrapper::vector_component<object::Object> * m_Object; // オブジェクト
    wrapper::vector_component<object::Mesh> * m_Mesh; // メッシュ
    wrapper::vector_component<object::MeshField> * m_MeshField; // メッシュフィールド
    wrapper::vector_component<object::Light> * m_Light; // 光源
    wrapper::vector_component<object::Effekseer> * m_Effekseer; // エフェクシア
    xaudio2::XAudio2 *m_XAudio; // XAudio
    xinput::XInput *m_XInput; // XInput
    std::mt19937 *m_mt; // メルセンヌ・ツイスタの32ビット版、引数は初期シード値
    Grid * m_Grid; // グリッド
    Text * m_Text; // テキスト
    MsImGui m_ImGui; // ImGui
    Fog * m_Fog; // フォグ
    bool m_initializer; // 初期化フラグ
    std::string m_file_name; // file pass
    float m_processing_time; // 処理時間
};

//==========================================================================
/**
@brief 現在の時間<型指定>
@return 時間
*/
template<typename _Ty>
inline _Ty ObjectManager::GetTimeSec(void)
{
    return static_cast<_Ty>(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()) / (_Ty)1000000000;
}

_MSLIB_END