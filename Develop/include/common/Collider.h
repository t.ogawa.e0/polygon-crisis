//==========================================================================
// Collider [Collider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include <cstdio>
#include "mslib.hpp"
#include "Component.h"
#include "MeshField.h"
#include "FunctionComponent.h"
#include "Transform.h"
#include "Initializer.h"
#include "Activity.h"

_MSLIB_BEGIN

namespace collider
{
    //==========================================================================
    //
    // class  : Collider
    // Content: コリダーの基礎
    //
    //==========================================================================
    class Collider : public component::Component, public initializer::Initializer, public activity::Activity
    {
    private:
        // コピー禁止 (C++11)
        Collider(const Collider &) = delete;
        Collider &operator=(const Collider &) = delete;
        Collider &operator=(Collider&&) = delete;
    public:
        Collider();
        virtual ~Collider();

        /**
        @brief 全て更新
        */
        static void UpdateAll();

        /**
        @brief 全て描画
        */
        static void DrawAll(LPDIRECT3DDEVICE9 device);

        /**
        @brief コリジョンの保存
        */
        //static void Save(std::ofstream & ofstream);

        /**
        @brief コリジョンの読み取り
        */
        //static void Load(std::ifstream & ifstream);

        /**
        @brief 判定の取得
        */
        bool Trigger();

        /**
        @brief 判定の登録
        */
        void SetTrigger(bool label);

        /**
        @brief ターゲットの登録[拡張可]
        */
        void AddCollider(Collider * target);

        /**
        @brief ターゲットの解放
        */
        void ReleaseCollider(Collider * target);

        /**
        @brief ターゲットの解放
        */
        void ReleaseCollider();

        /**
        @brief 更新フラグの取得
        */
        bool GetUpdateFlag();

        /**
        @brief 更新ロックを解除する
        */
        void UnlockUpdate();

        /**
        @brief 更新ロックを有効にする
        */
        void LockUpdate();
    private:
        void CreateWorldMatrix();

        /**
        @brief ターゲットの解放
        */
        void ReleaseTargetCollider(Collider * target);

        /**
        @brief アクセスターゲットの解放
        */
        void ReleaseAccessCollider(Collider * target);

        /**
        @brief 関係図
        */
        void RelationshipDiagram(LPDIRECT3DDEVICE9 device);
	public:
        virtual void Init();
        virtual void Update();
        virtual void Draw(LPDIRECT3DDEVICE9 device);
    protected:
        static std::list<Collider*> m_collider; // コリジョンの処理管理
        std::list<Collider*> m_target_collider; // ターゲット
        std::list<Collider*> m_access_collider; // アクセスしてきているコリダー
        std::vector<transform::Transform*> m_transform; // トランスフォーム
        bool m_collision; // 判定
    private:
        bool m_update; // 更新判定
    };

    //==========================================================================
    //
    // class  : Target
    // Content: ターゲット格納用
    //
    //==========================================================================
    template<typename _Ty>
    class Target
    {
    public:
        Target() {}
        virtual ~Target() { 
            ReleaseTarget();
        }

        /**
        @brief ターゲットの登録
        */
        template<typename _Ty2, bool isExtended1 = std::is_base_of<_Ty, _Ty2>::value, bool isExtended2 = std::is_base_of<Target, _Ty2>::value>
        void AddTarget(_Ty2 * target) {
            static_assert(isExtended1 || isExtended2, "SetTarget<> : _Ty is not inherited from Target Class");

            // 既に登録済みかをチェック
            for (auto & itr : m_target) {
                if (itr == target)return;
            }

            // お互いを相互登録
            AddTargets(target);
            target->AddTargets((_Ty2*)this);
        }

        /**
        @brief ターゲットの解放
        */
        template<typename _Ty2, bool isExtended1 = std::is_base_of<_Ty, _Ty2>::value, bool isExtended2 = std::is_base_of<Target, _Ty2>::value>
        void ReleaseTarget(_Ty2 * target) {
            static_assert(isExtended1 || isExtended2, "ReleaseTarget<> : _Ty is not inherited from Target Class");
            if (target == nullptr)return;

            ReleaseTargets(target);
            target->ReleaseTargets((_Ty2*)this);
        }

        /**
        @brief ターゲットの解放
        */
        void ReleaseTarget() {
            for (auto & itr : m_target) {
                itr->ReleaseTargets((_Ty*)this);
            }
            m_target.clear();
        }
    protected:
        void AddTargets(_Ty * target) {
            m_target.push_back(target);
        }
        void ReleaseTargets(_Ty * target) {
            for (auto itr = m_target.begin(); itr != m_target.end();) {
                if ((*itr) == target) {
                    itr = m_target.erase(itr);
                }
                else if ((*itr) != target) {
                    ++itr;
                }
            }
        }
    protected:
        std::list<_Ty*> m_target; // ターゲット
    };

    /**
    @brief 距離の計算
    */
    float Distance(const transform::Transform * t1, const transform::Transform * t2);

    /**
    @brief 距離の計算
    */
    float Distance(const D3DXVECTOR3 & t1, const D3DXVECTOR3 & t2);

    /**
    @brief 距離の計算
    */
    float Distance(const transform::Transform * t1, const D3DXVECTOR3 & t2);

    /**
    @brief 距離の計算
    */
    float Distance(const D3DXVECTOR3 & t1, const transform::Transform * t2);

    /**
    @brief 球体のコリジョン
    */
    bool Sphere(const transform::Transform * t1, const transform::Transform* t2, float scale);
}

_MSLIB_END