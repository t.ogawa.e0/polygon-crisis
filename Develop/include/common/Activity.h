//==========================================================================
// Activity [Activity.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "mslib.hpp"
#include <list>

_MSLIB_BEGIN

namespace activity
{
    //==========================================================================
    //
    // class  : Activity
    // Content : アクティビティ
    //
    //==========================================================================
    class Activity
    {
    public:
        Activity();
        Activity(const Activity& obj);
        virtual ~Activity();

        /**
        @brief アクティビティの取得
        @return false = アクティブではない : true = アクティブである
        */
        bool GetActivity() const;

        /**
        @brief アクティビティの登録(false = アクティブではない : true = アクティブである)
        @param activity [in] アクティビティフラグ
        */
        void SetActivity(bool activity);

        /**
        @brief アクティビティの一括管理機能に登録
        */
        void AddActivity();

        /**
        @brief アクティビティの一括管理機能から破棄
        */
        void DestroyActivity();

		/**
		@brief アクティビティの取得
		@return false = アクティブではない : true = アクティブである
		*/
		bool GetDebugActivity();

		/**
		@brief アクティビティの登録(false = アクティブではない : true = アクティブである)
		@param activity [in] アクティビティフラグ
		*/
		void SetDebugActivity(bool activity);

		/**
        @brief アクティビティの一括管理機能の取得
        */
        static std::list<Activity*> & GetAllActivity();
    public: // operator
        Activity &operator=(const Activity & obj);
    protected:
        bool m_Activity; // アクティビティ
		bool m_DebugActivity; // アクティビティ
        static std::list<Activity*> m_AllActivity; // 全てのActivity
    };
}

_MSLIB_END