//==========================================================================
// Effekseer [MsEffekseer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <Effekseer.h>
#include <EffekseerRendererDX9.h>
#include <EffekseerSoundXAudio2.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "Renderer.h"
#include "IReference.h"

#if _DEBUG
#pragma comment(lib, "VS2015/Debug/Effekseer.lib" )
#pragma comment(lib, "VS2015/Debug/EffekseerRendererDX9.lib" )
#pragma comment(lib, "VS2015/Debug/EffekseerSoundXAudio2.lib" )
#else
#pragma comment(lib, "VS2015/Release/Effekseer.lib" )
#pragma comment(lib, "VS2015/Release/EffekseerRendererDX9.lib" )
#pragma comment(lib, "VS2015/Release/EffekseerSoundXAudio2.lib" )
#endif

_MSLIB_BEGIN

namespace MsEffekseer
{
    constexpr const int stop_handle = -1; // 停止ハンドル

    class EffekseerLoader;

    class Renderer
    {
    public:
        Renderer();
        ~Renderer();

    private:

    };

    class EffekseerParam {
    public:
        EffekseerParam();
        ~EffekseerParam();
    public:
        Effekseer::Manager* m_manager; // マネージャー
        EffekseerRendererDX9::Renderer* m_renderer; // レンダラー
        EffekseerSound::Sound* m_sound; // サウンド
        Effekseer::Effect * m_effect; // エフェクト
    };

	using Effekseer::Matrix43;
	using Effekseer::Matrix44;
	using Effekseer::Vector3D;

    //==========================================================================
    //
    // class  : EffekseerData
    // Content: エフェクシアのデータ
    //
    //==========================================================================
    class EffekseerData : public ireference::ReferenceData<EffekseerParam>
    {
    public:
        EffekseerData();
        ~EffekseerData();
    public:
        EffekseerLoader * LoaderPtr; // ローダーのポインタ
        std::string tag; // タグ
    };

    //==========================================================================
    //
    // class  : EffekseerReference
    // Content: 参照用
    //
    //==========================================================================
    class EffekseerReference : public ireference::ReferenceOperator<EffekseerData>
    {
    public:
        EffekseerReference();
        ~EffekseerReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        ///**
        //@brief 再生する。
        //@param vec3	[in] 座標
        //*/
        //void Play(const D3DXVECTOR3 & vec3);
        ///**
        //@brief 停止
        //*/
        //void Stop();
        ///**
        //@brief 再生中かどうか取得する
        //*/
        //bool GetShown();
        /**
        @brief ハンドル
        */
        Effekseer::Handle GetHandle();
        /**
        @brief ハンドル
        */
        void SetHandle(Effekseer::Handle handle);
        const std::string & Tag();
    protected:
        Effekseer::Handle m_handle; // ハンドル
    };

    //==========================================================================
    //
    // class  : EffekseerLoader
    // Content: エフェクト読み込み破棄クラス
    //
    //==========================================================================
    class EffekseerLoader : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        EffekseerLoader(const EffekseerLoader &) = delete;
        EffekseerLoader &operator=(const EffekseerLoader &) = delete;
        EffekseerLoader &operator=(EffekseerLoader&&) = delete;
    public:
        EffekseerLoader();
        EffekseerLoader(LPDIRECT3DDEVICE9 device);
        ~EffekseerLoader();

        static void UpdateAll();
        static void DrawAll();

        /**
        @brief エフェクトを読み込む。
        @param path [in] 読み込みパス
        @return エフェクトのポインタ
        */
        EffekseerReference Load(const EFK_CHAR* path, const std::string & effect_name);

        /**
        @brief エフェクトを破棄する。
        @param data [in] エフェクト
        */
        void Unload(EffekseerData * data);

        /**
        @brief エフェクトを破棄する。
        @param data [in] エフェクト
        */
        void Unload(EffekseerData & data);
    private:
        void Release();
    private:
        LPDIRECT3DDEVICE9 m_device;
        std::unordered_map<std::string, EffekseerData> m_data; // データ
        static Effekseer::Manager* m_manager; // マネージャー
        static EffekseerRendererDX9::Renderer* m_renderer; // レンダラー
        static EffekseerSound::Sound* m_sound; // サウンド
    };

    //==========================================================================
    //
    // class  : SetEffekseer
    // Content: エフェクト登録クラス
    //
    //==========================================================================
    class SetEffekseer
    {
    public:
        SetEffekseer();
        ~SetEffekseer();

        /**
        @brief エフェクトデータの登録
        @param TextureData [in] テクスチャ
        */
        void SetEffekseerData(const EffekseerReference & effekseer);

        /**
        @brief エフェクトデータの取得
        */
        EffekseerReference & GetEffekseerData();

        /**
        @brief マネージャーの取得
        */
        Effekseer::Manager*GetManager();
        /**
        @brief エフェクトの取得
        */
        Effekseer::Effect*GetEffect();
        /**
        @brief サウンドの取得
        */
        EffekseerSound::Sound*GetSound();
        /**
        @brief ハンドル
        */
        Effekseer::Handle GetHandle();
        /**
        @brief ハンドル
        */
        void SetHandle(Effekseer::Handle handle);
    protected:
        EffekseerReference m_EffekseerData; // テクスチャデータ

    };

    //==========================================================================
    //
    // class  : MsEffekseer
    // Content: Effekseer
    //
    //==========================================================================
    class MsEffekseer : public component::Component, public renderer::Renderer
    {
    private:
        // コピー禁止 (C++11)
        MsEffekseer(const MsEffekseer &) = delete;
        MsEffekseer &operator=(const MsEffekseer &) = delete;
        MsEffekseer &operator=(MsEffekseer&&) = delete;
    private:
        //==========================================================================
        // 情報管理用
        //==========================================================================
        class Effect : public component::Component
        {
        private:
            // コピー禁止 (C++11)
            Effect(const Effect &) = delete;
            Effect &operator=(const Effect &) = delete;
            Effect &operator=(Effect&&) = delete;
        public:
            Effect() {
                m_effect = nullptr;
            }

            /**
            @param label [in] エフェクト名
            @param effect [in/out] エフェクトポインタ
            */
            Effect(const std::string & label, Effekseer::Effect*effect) {
                m_effect_name = label;
                m_effect = effect;
            }
            ~Effect() {
                ES_SAFE_RELEASE(m_effect);
            }

            /**
            @brief エフェクトの取得
            @return エフェクトのポインタ
            */
            Effekseer::Effect* GetEffect() {
                return m_effect;
            }

            /**
            @brief エフェクト名の取得
            @return エフェクトの名前
            */
            const std::string & GetEffectName() const {
                return m_effect_name;
            }
        private:
            Effekseer::Effect* m_effect; // エフェクトポインタ
            std::string m_effect_name; // エフェクトの名前
        };

        class HandleList : public component::Component
        {
        private:
            // コピー禁止 (C++11)
            HandleList(const HandleList &) = delete;
            HandleList &operator=(const HandleList &) = delete;
            HandleList &operator=(HandleList&&) = delete;
        public:
            HandleList() {
                m_handle = -1;
                m_effect = nullptr;
            }
            /**
            @param handle [in] ハンドル
            @param effect [in] エフェクトポインタ
            @param effect_name [in] エフェクト名
            */
            HandleList(Effekseer::Handle handle, Effekseer::Effect* effect, const std::string & effect_name) {
                m_handle = handle;
                m_effect = effect;
                m_effect_name = effect_name;
            }
            ~HandleList() {
                m_handle = -1;
                m_effect = nullptr;
            }

            /**
            @brief エフェクトの取得
            @return エフェクトのポインタ
            */
            Effekseer::Effect* GetEffect() {
                return m_effect;
            }

            /**
            @brief エフェクトのハンドルの取得
            @return エフェクトのハンドル
            */
            Effekseer::Handle GetEffectHandle() {
                return m_handle;
            }

            /**
            @brief エフェクトの名前取得
            @return エフェクトの名前
            */
            const std::string & GetEffectName() const {
                return m_effect_name;
            }
        private:
            Effekseer::Handle m_handle; // ハンドル
            Effekseer::Effect* m_effect; // エフェクトのポインタ
            std::string m_effect_name; // エフェクトの名前
        };
    public:
        MsEffekseer();
        /**
        @param device [in] DX9のデバイス
        @param squareMaxCount [in] 使用する頂点数
        */
        MsEffekseer(LPDIRECT3DDEVICE9 device, int32_t squareMaxCount);
        ~MsEffekseer();

        /**
        @brief エフェクトの読み込み
        @param path [in] エフェクトのファイルパス >> (const EFK_CHAR*)L"effect_name"
        @param effect_name [in] エフェクト名
        @return エフェクトのポインタ
        */
        Effekseer::Effect * EffectLoad(const EFK_CHAR* path, const std::string & effect_name);

        /**
        @brief マネージャーの取得
        @return マネージャーのポインタ
        */
        Effekseer::Manager * GetManager();

        /**
        @brief レンダラーの取得
        @return レンダラーのポインタ
        */
        EffekseerRendererDX9::Renderer * GetRenderer();

        /**
        @brief エフェクトの再生情報の記録
        @param handle [in] ハンドル
        @param effect [in] エフェクトポインタ
        @param effect_name [in] エフェクト名
        */
        void SetPlayData(Effekseer::Handle handle, Effekseer::Effect* effect, const std::string & effect_name);

        /**
        @brief エフェクトの再生情報の取得
        @return 再生情報格納コンテナ
        */
        std::list<HandleList> & GetHandleList();

        /**
        @brief エフェクトコンテナの取得
        @return エフェクトコンテナ
        */
        std::list<Effect> & GetEffectList();

        /**
        @brief エフェクトの再生情報の破棄
        @param handle [in] ハンドルコンテナ
        */
        void DestroyHandle(HandleList & handle);

        /**
        @brief エフェクトの再生情報の破棄
        @param handle [in] ハンドル
        */
        void DestroyHandle(Effekseer::Handle handle);

        /**
        @brief エフェクトの再生情報の破棄
        @param effect [in] エフェクトポインタ
        */
        void DestroyHandle(Effekseer::Effect* effect);

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief 描画
        @param device [in] デバイス
        */
        void Draw(LPDIRECT3DDEVICE9 device) override;

        /**
        @brief デバッグ描画
        @param device [in] デバイス
        */
        void DebugDraw(LPDIRECT3DDEVICE9 device) override;
    private:
		Effekseer::Manager* m_manager; // マネージャー
		EffekseerRendererDX9::Renderer* m_renderer; // レンダラー
		EffekseerSound::Sound* m_sound; // サウンド
		std::list<Effect> m_effect_list; // エフェクトコンテナ
		std::list<HandleList> m_handle_list; // ハンドルコンテナ
	};
}

_MSLIB_END