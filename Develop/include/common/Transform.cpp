//==========================================================================
// トランスフォーム[Transform.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Transform.h"

_MSLIB_BEGIN

namespace transform
{
    Look::Look()
    {
        *this = Look(math::Vector3(0, 0, -1), math::Vector3(0, 0, 0), math::Vector3(0, 1, 0));
    }

    Look::Look(const Look & Input)
    {
        *this = Input;
    }

    Look::Look(const math::Vector3 & _eye, const math::Vector3 & _at, const math::Vector3 & _up)
    {
        eye = _eye;
        at = _at;
        up = _up;
    }

    Look::~Look()
    {
    }

    void Look::Identity()
    {
        *this = Look();
    }

    Look Look::GetIdentity()
    {
        return Look();
    }

	D3DXMATRIX * Look::MatrixEyeAtUp(D3DXMATRIX & Out)
    {
        math::Vector3 X, Y, Z;

        D3DXVec3Normalize(&Z, &eye);
        D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &up), &Z);
        D3DXVec3Normalize(&X, &X);
        D3DXVec3Normalize(&Y, D3DXVec3Cross(&Y, &Z, &X));

        Out._11 = X.x; Out._12 = X.y; Out._13 = X.z; Out._14 = 0;
        Out._21 = Y.x; Out._22 = Y.y; Out._23 = Y.z; Out._24 = 0;
        Out._31 = Z.x; Out._32 = Z.y; Out._33 = Z.z; Out._34 = 0;
        Out._41 = 0.0f; Out._42 = 0.0f; Out._43 = 0.0f; Out._44 = 1.0f;

        return &Out;
    }

	D3DXMATRIX * Look::MatrixEyeAtUpAxisFix(D3DXMATRIX & Out)
    {
        math::Vector3 X, Y, Z, D;
        D3DXVec3Normalize(&D, &eye);
        D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &up), &D);
        D3DXVec3Normalize(&X, &X);
        D3DXVec3Normalize(&Z, D3DXVec3Cross(&Z, &X, &Y));

        Out._11 = X.x; Out._12 = X.y; Out._13 = X.z; Out._14 = 0;
        Out._21 = Y.x; Out._22 = Y.y; Out._23 = Y.z; Out._24 = 0;
        Out._31 = Z.x; Out._32 = Z.y; Out._33 = Z.z; Out._34 = 0;
        Out._41 = 0; Out._42 = 0; Out._43 = 0; Out._44 = 0;

        return &Out;
    }

    Look * Look::operator()()
    {
        *this = Look();
        return this;
    }

    Look * Look::operator()(const Look & Input)
    {
        *this = Look(Input);
        return this;
    }

    Look * Look::operator()(const math::Vector3 & _eye, const math::Vector3 & _at, const math::Vector3 & _up)
    {
        *this = Look(_eye, _at, _up);
        return this;
    }

    Look Look::operator+(const Look & v)
    {
        Look lock;

        lock.eye = eye + v.eye;
        lock.at = at + v.at;
        lock.up = up + v.up;

        return lock;
    }

    Look Look::operator-(const Look & v)
    {
        Look lock;

        lock.eye = eye - v.eye;
        lock.at = at - v.at;
        lock.up = up - v.up;

        return lock;
    }

    Vector::Vector()
    {
        *this = Vector(math::Vector3(0, 0, 1), math::Vector3(1, 0, 0), math::Vector3(0, 1, 0));
    }

    Vector::Vector(const Vector & Input)
    {
        D3DXVec3Normalize(&front, &Input.front);
        D3DXVec3Normalize(&right, &Input.right);
        D3DXVec3Normalize(&up, &Input.up);
    }

    Vector::Vector(const math::Vector3 & _front, const math::Vector3 & _right, const math::Vector3 & _up)
    {
        D3DXVec3Normalize(&front, &_front);
        D3DXVec3Normalize(&right, &_right);
        D3DXVec3Normalize(&up, &_up);
    }

    Vector::~Vector()
    {
    }

    Vector * Vector::Normalize()
    {
        D3DXVec3Normalize(&front, &front);
        D3DXVec3Normalize(&right, &right);
        D3DXVec3Normalize(&up, &up);

        return this;
    }

    Vector * Vector::TransformNormal(const D3DXMATRIX & pM)
    {
        D3DXVec3TransformNormal(&front, &front, &pM);
        D3DXVec3TransformNormal(&right, &right, &pM);
        D3DXVec3TransformNormal(&up, &up, &pM);
        return this;
    }

    math::Vector3 * Vector::CrossRight()
    {
        return (math::Vector3*)D3DXVec3Cross(&right, &up, &front);
    }

    math::Vector3 * Vector::VectoeMultiply(math::Vector3 & Out, const math::Vector3 & V1)
    {
        Out += right * V1.x;
        Out += up * V1.y;
        Out += front * V1.z;

        return &Out;
    }

    void Vector::Identity()
    {
        *this = Vector();
    }

    Vector Vector::GetIdentity()
    {
        return Vector();
    }

	D3DXMATRIX * Vector::MatrixVector(D3DXMATRIX & Out)
    {
        Normalize();

        Out._11 = right.x; Out._12 = right.y; Out._13 = right.z; Out._14 = 0;
        Out._21 = up.x; Out._22 = up.y; Out._23 = up.z; Out._24 = 0;
        Out._31 = front.x; Out._32 = front.y; Out._33 = front.z; Out._34 = 0;
        Out._41 = 0.0f; Out._42 = 0.0f; Out._43 = 0.0f; Out._44 = 1.0f;

        return &Out;
    }

    Vector * Vector::operator()()
    {
        *this = Vector();
        return this;
    }

    Vector * Vector::operator()(const Vector & Input)
    {
        *this = Vector(Input);
        return this;
    }

    Vector * Vector::operator()(const math::Vector3 & _front, const math::Vector3 & _right, const math::Vector3 & _up)
    {
        *this = Vector(_front, _right, _up);
        return this;
    }

    Vector Vector::operator+(const Vector & v)
    {
        Vector vector;

        vector.front = front + v.front;
        vector.right = right + v.right;
        vector.up = up + v.up;

        return vector;
    }

    Vector Vector::operator-(const Vector & v)
    {
        Vector vector;

        vector.front = front - v.front;
        vector.right = right - v.right;
        vector.up = up - v.up;

        return vector;
    }

    Parameter::Parameter()
    {
        *this = Parameter(math::Vector3(0, 0, 0), math::Vector3(0, 0, 0), math::Vector3(1, 1, 1));
    }

    Parameter::Parameter(const Parameter & Input)
    {
        *this = Input;
    }

    Parameter::Parameter(const math::Vector3 & _position, const math::Vector3 & _rotation, const math::Vector3 & _scale)
    {
        position = _position;
        rotation = _rotation;
        scale = _scale;
    }

    Parameter::~Parameter()
    {
    }

	D3DXMATRIX * Parameter::MatrixTranslation(D3DXMATRIX & Out)
    {
		D3DXMatrixTranslation(&Out, position.x, position.y, position.z);
        return D3DXMatrixTranslation(&Out, position.x, position.y, position.z);
    }

	D3DXMATRIX * Parameter::MatrixScaling(D3DXMATRIX & Out)
    {
        return D3DXMatrixScaling(&Out, scale.x, scale.y, scale.z);
    }

    void Parameter::Identity()
    {
        *this = Parameter();
    }

    Parameter Parameter::GetIdentity()
    {
        return Parameter();
    }

    Parameter * Parameter::operator()()
    {
        *this = Parameter();
        return this;
    }

    Parameter * Parameter::operator()(const Parameter & Input)
    {
        *this = Parameter(Input);
        return this;
    }

    Parameter * Parameter::operator()(const math::Vector3 & _position, const math::Vector3 & _rotation, const math::Vector3 & _scale)
    {
        *this = Parameter(_position, _rotation, _scale);
        return this;
    }

    Parameter Parameter::operator+(const Parameter & v)
    {
        Parameter param;

        param.position = position + v.position;
        param.rotation = rotation + v.rotation;
        param.scale = scale + v.scale;

        return param;
    }

    Parameter Parameter::operator-(const Parameter & v)
    {
        Parameter param;

        param.position = position - v.position;
        param.rotation = rotation - v.rotation;
        param.scale = scale - v.scale;

        return param;
    }

    Transform::Transform()
    {
        SetComponentName("Transform");
        D3DXMatrixIdentity(&m_LocalMatrix);
        D3DXMatrixIdentity(&m_WorldMatrix);
        m_Look.Identity();
        m_Vector.Identity();
        m_Parameter.Identity();
        m_MatrixView = nullptr;
        m_update_flag = true;
    }

    Transform::Transform(const Transform & obj)
    {
        m_MatrixView = nullptr;
        *this = obj;
        m_update_flag = true;
    }

    Transform::~Transform()
    {
    }

    void Transform::Identity()
    {
        *this = Transform();
    }

    const math::Matrix * Transform::GetWorldMatrix() const
    {
        return &m_WorldMatrix;
    }

    const math::Matrix * Transform::GetLocalMatrix() const
    {
        return &m_LocalMatrix;
    }

    void Transform::SetWorldMatrix(const math::Matrix & Input)
    {
        m_update_flag = true;
        m_WorldMatrix = Input;
    }

    void Transform::SetLocalMatrix(const math::Matrix & Input)
    {
        m_LocalMatrix = Input;
    }

    const math::Matrix * Transform::GetMatrixView()const
    {
        return m_MatrixView;
    }

    void Transform::SetMatrixView(math::Matrix * Input)
    {
        m_update_flag = true;
        m_MatrixView = Input;
    }

    void Transform::SetLook(const Look & Input)
    {
        m_update_flag = true;
        m_Look = Input;
    }

    void Transform::SetVector(const Vector & Input)
    {
        m_update_flag = true;
        m_Vector = Input;
    }

    void Transform::SetParameter(const Parameter & Input)
    {
        m_update_flag = true;
        SetPosition(Input.position);
        SetRotation(Input.rotation);
        SetScale(Input.scale);
    }

    Look * Transform::GetLook()
    {
        return &m_Look;
    }

    Vector * Transform::GetVector()
    {
        return &m_Vector;
    }

    Parameter * Transform::GetParameter()
    {
        return &m_Parameter;
    }

    const math::Vector3 * Transform::GetPosition() const
    {
        return &m_Parameter.position;
    }

    const math::Vector3 * Transform::GetRotation()
    {
        return &m_Parameter.rotation;
    }

    const math::Vector3 * Transform::GetScale() const
    {
        return &m_Parameter.scale;
    }

    void Transform::AddScale(float x, float y)
    {
        AddScale(math::Vector3(x, y, 0));
    }

    void Transform::AddScale(float x, float y, float z)
    {
        AddScale(math::Vector3(x, y, z));
    }

    void Transform::AddScale(const D3DXVECTOR2 & scale)
    {
        AddScale(math::Vector3(scale.x, scale.y, 0));
    }

    void Transform::AddScale(const math::Vector3 & scale)
    {
        m_update_flag = true;
        m_Parameter.scale += scale;
    }

    void Transform::SetRotation(float x, float y)
    {
        SetRotation(D3DXVECTOR2(x, y));
    }

    void Transform::SetRotation(float x, float y, float z)
    {
        SetRotation(math::Vector3(x, y, z));
    }

    void Transform::SetRotation(const D3DXVECTOR2 & axis)
    {
        m_update_flag = true;
        m_Parameter.rotation = math::Vector3(axis.x, axis.y, 0);
    }

    void Transform::SetRotation(const math::Vector3 & axis)
    {
        D3DXMatrixIdentity(&m_LocalMatrix);
        m_Look.Identity();
        m_Vector.Identity();
        m_Parameter.rotation = 0;
        AddRotation(axis);
    }

    void Transform::SetScale(float x, float y)
    {
        SetScale(D3DXVECTOR2(x, y));
    }

    void Transform::SetScale(float x, float y, float z)
    {
        SetScale(math::Vector3(x, y, z));
    }

    void Transform::SetScale(const D3DXVECTOR2 & scale)
    {
        m_update_flag = true;
        m_Parameter.scale = math::Vector3(scale.x, scale.y, 1);
    }

    void Transform::SetScale(const math::Vector3 & scale)
    {
        m_update_flag = true;
        m_Parameter.scale = scale;
    }

    void Transform::SetPosition(float x, float y)
    {
        SetPosition(D3DXVECTOR2(x, y));
    }

    void Transform::SetPosition(float x, float y, float z)
    {
        SetPosition(math::Vector3(x, y, z));
    }

    void Transform::SetPosition(const D3DXVECTOR2 & position)
    {
        m_update_flag = true;
        m_Parameter.position = math::Vector3(position.x, position.y, m_Parameter.position.z);
    }

    void Transform::SetPosition(const math::Vector3 & position)
    {
        m_update_flag = true;
        m_Parameter.position = position;
    }

    void Transform::AddPosition(float x, float y)
    {
        AddPosition(math::Vector3(x, y, 0));
    }

    void Transform::AddPosition(float x, float y, float z)
    {
        AddPosition(math::Vector3(x, y, z));
    }

    void Transform::AddPosition(const D3DXVECTOR2 & position)
    {
        AddPosition(math::Vector3(position.x, position.y, 0));
    }

    void Transform::AddPosition(const math::Vector3 & position)
    {
        // 正規化します
        m_Vector.Normalize();

        // ベクトルを利用して動かします
        m_Vector.VectoeMultiply(m_Parameter.position, position);

        m_update_flag = true;
    }

    void Transform::AddRotation(float x, float y)
    {
        AddRotation(math::Vector3(x, y, 0));
    }

    void Transform::AddRotation(float x, float y, float z)
    {
        AddRotation(math::Vector3(x, y, z));
    }

    void Transform::AddRotation(const D3DXVECTOR2 & axis)
    {
        AddRotation(math::Vector3(axis.x, axis.y, 0));
    }

    void Transform::AddRotation(const math::Vector3 & axis)
    {
        D3DXMATRIX Matrix, AddMatrix; //回転行列

        m_Parameter.rotation += axis;

        // 外積を求めます
        m_Vector.CrossRight();

        // マトリクスの初期化
        D3DXMatrixIdentity(&Matrix);

        // 生成した回転マトリクスを合成していきます
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationY(&AddMatrix, axis.x), &Matrix);
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationAxis(&AddMatrix, &m_Vector.right, axis.y), &Matrix);
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationAxis(&AddMatrix, &m_Vector.front, axis.z), &Matrix);

        // 向きベクトルに合成します
        m_Vector.TransformNormal(Matrix);

        // 視線ベクトルを更新します
        m_Look.up = m_Vector.up;
        m_Look.eye = -m_Vector.front;

        m_update_flag = true;
    }

    void Transform::AddRotationX(const D3DXVECTOR2 & axis)
    {
        AddRotationX(axis, 1);
    }

    void Transform::AddRotationX(const math::Vector3 & axis)
    {
        AddRotationX(axis, 1);
    }

    void Transform::AddRotationY(const D3DXVECTOR2 & axis)
    {
        AddRotationY(axis, 1);
    }

    void Transform::AddRotationY(const math::Vector3 & axis)
    {
        AddRotationY(axis, 1);
    }

    void Transform::AddRotationZ(const math::Vector3 & axis)
    {
        AddRotationZ(axis, 1);
    }

    void Transform::AddRotationX(const D3DXVECTOR2 & axis, float angle)
    {
        AddRotationX(math::Vector3(axis.x, axis.y, 0), angle);
    }

    void Transform::AddRotationX(const math::Vector3 & axis, float angle)
    {
        math::Vector3 vecAxis;

        // 外積を求めます
        m_Vector.CrossRight();

        // 角度を求める
        float fVec3Dot = atanf(D3DXVec3Dot(&m_Vector.right, D3DXVec3Normalize(&vecAxis, &axis)));

        AddRotation(math::Vector3(fVec3Dot*angle, 0, 0));
    }

    void Transform::AddRotationY(const D3DXVECTOR2 & axis, float angle)
    {
        AddRotationY(math::Vector3(axis.x, axis.y, 0), angle);
    }

    void Transform::AddRotationY(const math::Vector3 & axis, float angle)
    {
        math::Vector3 vecAxis;

        // 外積を求めます
        m_Vector.CrossRight();

        // 角度を求める
        float fVec3Dot = atanf(D3DXVec3Dot(&m_Vector.right, D3DXVec3Normalize(&vecAxis, &axis)));

        AddRotation(math::Vector3(0, fVec3Dot*angle, 0));
    }

    void Transform::AddRotationZ(const math::Vector3 & axis, float angle)
    {
        math::Vector3 vecAxis;

        // 外積を求めます
        m_Vector.CrossRight();

        // 角度を求める
        float fVec3Dot = atanf(D3DXVec3Dot(&m_Vector.front, D3DXVec3Normalize(&vecAxis, &axis)));

        AddRotation(math::Vector3(0, 0, fVec3Dot*angle));
    }

    void Transform::AddRotationXYZ(const math::Vector3 & X, const math::Vector3 & Y, const math::Vector3 & Z, float angle)
    {
        math::Vector3 vecAxis, rotation;

        // 外積を求めます
        m_Vector.CrossRight();

        // 角度を求める
        rotation.x = atanf(D3DXVec3Dot(&m_Vector.right, D3DXVec3Normalize(&vecAxis, &X)));
        rotation.y = atanf(D3DXVec3Dot(&m_Vector.right, D3DXVec3Normalize(&vecAxis, &Y)));
        rotation.z = atanf(D3DXVec3Dot(&m_Vector.front, D3DXVec3Normalize(&vecAxis, &Z)));

        AddRotation(rotation*angle);
    }

    void Transform::AddRotationXY(const D3DXVECTOR2 & X, const D3DXVECTOR2 & Y, float angle)
    {
        AddRotationXYZ(math::Vector3(X.x, X.y, 0), math::Vector3(Y.x, Y.y, 0), math::Vector3(0, 0, 0), angle);
    }

    void Transform::AddRotationLimitY(float y, float limit)
    {
        D3DXMATRIX Rot;
        math::Vector3 Vec, dir = math::Vector3(0, 1.0f, 0);

        D3DXMatrixRotationAxis(&Rot, m_Vector.CrossRight(), limit);
        D3DXVec3TransformNormal(&Vec, &m_Vector.front, &Rot);
        float fVec3Dot = atanf(D3DXVec3Dot(&Vec, &dir));

        if (-limit < fVec3Dot&&limit>fVec3Dot)
        {
            AddRotation(math::Vector3(0, y, 0));
        }
    }

	math::Matrix * Transform::CreateWorldMatrix()
    {
        auto pParentTransform = GetWarrantyParent<Transform>();

        if (!m_update_flag && !pParentTransform->m_update_flag)
            return &m_WorldMatrix;

        CreateLocalMatrix();

        // 親オブジェクトと合成
        if (pParentTransform != this)
        {
            // 自身のローカル姿勢と親のワールド変換行列の掛け算
            m_WorldMatrix = m_LocalMatrix * pParentTransform->m_WorldMatrix;

            // 合成後に、値が反転するため再度反転
            m_WorldMatrix._11 = -m_WorldMatrix._11;
            m_WorldMatrix._12 = -m_WorldMatrix._12;
            m_WorldMatrix._13 = -m_WorldMatrix._13;
            m_WorldMatrix._31 = -m_WorldMatrix._31;
            m_WorldMatrix._32 = -m_WorldMatrix._32;
            m_WorldMatrix._33 = -m_WorldMatrix._33;
        }

        m_update_flag = true;

        return &m_WorldMatrix;
    }

	math::Matrix * Transform::CreateLocalMatrix()
    {
        if (!m_update_flag)
            return &m_LocalMatrix;

        D3DXMATRIX MatRot;

        m_Look.MatrixEyeAtUp(MatRot);

        m_LocalMatrix._11 = m_Parameter.scale.x*MatRot._11;
        m_LocalMatrix._12 = m_Parameter.scale.x*MatRot._12;
        m_LocalMatrix._13 = m_Parameter.scale.x*MatRot._13;

        m_LocalMatrix._21 = m_Parameter.scale.y*MatRot._21;
        m_LocalMatrix._22 = m_Parameter.scale.y*MatRot._22;
        m_LocalMatrix._23 = m_Parameter.scale.y*MatRot._23;

        m_LocalMatrix._31 = m_Parameter.scale.z*MatRot._31;
        m_LocalMatrix._32 = m_Parameter.scale.z*MatRot._32;
        m_LocalMatrix._33 = m_Parameter.scale.z*MatRot._33;

        m_LocalMatrix._41 = m_Parameter.position.x;
        m_LocalMatrix._42 = m_Parameter.position.y;
        m_LocalMatrix._43 = m_Parameter.position.z;

        m_LocalMatrix._14 = m_LocalMatrix._24 = m_LocalMatrix._34 = 0.0f;
        m_LocalMatrix._44 = 1.0f;

        m_WorldMatrix = m_LocalMatrix;

        return &m_WorldMatrix;
    }

	math::Matrix * Transform::CreateWorldViewMatrix()
    {
        if (!m_update_flag)
            return &m_WorldMatrix;

        if (m_MatrixView == nullptr)
            return CreateWorldMatrix();

        D3DXMATRIX MtxView;

        D3DXMatrixTranspose(&MtxView, m_MatrixView);
        MtxView._14 = 0.0f;
        MtxView._24 = 0.0f;
        MtxView._34 = 0.0f;

		CreateWorldMatrix();

		// 合成後に、値が反転するため再度反転
		m_WorldMatrix._11 = -m_WorldMatrix._11;
		m_WorldMatrix._12 = -m_WorldMatrix._12;
		m_WorldMatrix._13 = -m_WorldMatrix._13;
		m_WorldMatrix._31 = -m_WorldMatrix._31;
		m_WorldMatrix._32 = -m_WorldMatrix._32;
		m_WorldMatrix._33 = -m_WorldMatrix._33;

		m_WorldMatrix = MtxView * m_WorldMatrix;

        return &m_WorldMatrix;
    }

	math::Matrix * Transform::CreateLocalViewMatrix()
    {
        if (!m_update_flag)
            return &m_LocalMatrix;

        if (m_MatrixView == nullptr)
            return CreateLocalMatrix();

        D3DXMATRIX MtxView;

        D3DXMatrixTranspose(&MtxView, m_MatrixView);
        MtxView._14 = 0.0f;
        MtxView._24 = 0.0f;
        MtxView._34 = 0.0f;

		CreateLocalMatrix();

		m_LocalMatrix = MtxView * m_LocalMatrix;

        return &m_LocalMatrix; // 行列の合成
    }

    void Transform::UnlockUpdate()
    {
        m_update_flag = true;
    }

    void Transform::LockUpdate()
    {
        m_update_flag = false;
    }

    bool Transform::UpdateKey()
    {
        return m_update_flag;
    }

    math::Vector3 Transform::ToSee(Transform & target)
    {
        target.CreateWorldMatrix();
        CreateWorldMatrix();
        return math::Vector3(target.m_WorldMatrix._41, target.m_WorldMatrix._42, target.m_WorldMatrix._43) -
            math::Vector3(m_WorldMatrix._41, m_WorldMatrix._42, m_WorldMatrix._43);
    }

    Transform & Transform::operator=(const Transform & obj)
    {
        m_Look = obj.m_Look;
        m_Vector = obj.m_Vector;
        m_Parameter = obj.m_Parameter;
        m_LocalMatrix = obj.m_LocalMatrix;
        m_WorldMatrix = obj.m_WorldMatrix;
        m_MatrixView = obj.m_MatrixView;

        return *this;
    }

    math::Vector3 Transform::ToRadian(const math::Vector3 & degree)
    {
        return math::Vector3(D3DXToRadian(degree.x), D3DXToRadian(degree.y), D3DXToRadian(degree.z));
    }

    math::Vector3 Transform::ToRadian(float x, float y, float z)
    {
        return ToRadian(math::Vector3(x, y, z));
    }

    math::Vector3 Transform::ToDegree(const math::Vector3 & radian)
    {
        return math::Vector3(D3DXToDegree(radian.x), D3DXToDegree(radian.y), D3DXToDegree(radian.z));
    }

    math::Vector3 Transform::ToDegree(float x, float y, float z)
    {
        return ToDegree(math::Vector3(x, y, z));
    }
}

_MSLIB_END