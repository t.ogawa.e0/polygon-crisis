//==========================================================================
// オブジェクト[Object.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Object.h"

_MSLIB_BEGIN

namespace object
{
    using renderer::RendererID;

    struct VectorDrawData
    {
        D3DXVECTOR3 pos; // 座標変換が必要
        D3DCOLOR color = (D3DCOLOR)0; // ポリゴンの色
    };

    //==========================================================================
    //
    // class  : InheritanceObject
    // Content: 継承オブジェクト
    //
    //==========================================================================
    InheritanceObject::InheritanceObject()
    {
        m_MatType = MatrixType::Default;
        SetComponentName("InheritanceObject");
        Init();
    }
    InheritanceObject::InheritanceObject(const InheritanceObject & obj)
    {
        m_MatType = MatrixType::Default;
        *this = obj;
        SetComponentName("InheritanceObject");
    }
    InheritanceObject::~InheritanceObject()
    {
    }

    //==========================================================================
    /**
    @brief 初期化
    */
    void InheritanceObject::Init()
    {
        m_MatType = MatrixType::Default;
        m_Look.Identity();
        m_Vector.Identity();
        m_Parameter.Identity();
    }

    //==========================================================================
    /**
    @brief オブジェクトの更新
    */
    const D3DXMATRIX * InheritanceObject::UpdateMatrix()
    {
        switch (m_MatType)
        {
        case MatrixType::Default:
            return CreateWorldMatrix();
            break;
        case MatrixType::World:
            return CreateWorldMatrix();
            break;
        case MatrixType::Local:
            return CreateLocalMatrix();
            break;
        case MatrixType::WorldView:
            return CreateWorldViewMatrix();
            break;
        case MatrixType::LocalView:
            return CreateLocalViewMatrix();
            break;
        default:
            return CreateWorldMatrix();
            break;
        }
    }

    //==========================================================================
    /**
    @brief ラジコン回転
    @param vecRight [in] 向かせたい方向ベクトル
    @param speed [in] 向かせる速度
    */
    void InheritanceObject::RadioControl(const D3DXVECTOR3 & vecRight, float speed)
    {
        D3DXVECTOR3 vecCross;

        D3DXVec3Cross(&vecCross, &m_Vector.front, &vecRight);
        AddRotation(D3DXVECTOR3((vecCross.y < 0.0f ? -0.01f : 0.01f)*speed, 0, 0));
    }

    //==========================================================================
    /**
    @brief 対象の方向に向かせる(ベクトル計算を行い、処理を行います)
    @param Input [in] ターゲット
    @param power 向かせる力
    */
    void InheritanceObject::LockOn(transform::Transform & Input, float power)
    {
        AddRotationX(ToSee(Input), power);
    }

    //==========================================================================
    /**
    @brief 対象の方向に向かせる(直接ベクトルを操作します)
    @param Input [in] ターゲット
    */
    void InheritanceObject::LockOn(transform::Transform & Input)
    {
        auto angle = ToSee(Input);

        m_Look.eye = -angle;
        m_Vector.front = angle;
        m_Vector.Normalize();
        m_update_flag = true;
    }

    //==========================================================================
    /**
    @brief 使うマトリクスの種類
    @param type [in] マトリクスのタイプ
    */
    void InheritanceObject::SetMatrixType(const MatrixType & type)
    {
        m_MatType = type;
    }

    //==========================================================================
    /**
    @brief 使用中のマトリクスの種類
    @return マトリクスの種類
    */
    MatrixType InheritanceObject::GetMatrixType() const
    {
        return m_MatType;
    }

    //==========================================================================
    /**
    @brief サードパーソン用の座標取得
    @param move_pos [in] 対象オブジェクトを始点とした座標を入力してください
    @return サードパーソン用の座標
    */
    D3DXVECTOR3 InheritanceObject::GetThirdPerson(const D3DXVECTOR3 & move_pos)
    {
        // オブジェクトの複製
        auto obj = *this;

        obj.AddPosition(move_pos);

        return *obj.GetPosition();
    }

    //==========================================================================
    /**
    @brief 向きベクトル描画
    */
    void InheritanceObject::DrawVector(LPDIRECT3DDEVICE9 device)
    {
        // 向きベクトル情報
        constexpr float fPosZ[] = { -1.0f,1.0f,0.8f,0.8f ,1.0f };
        constexpr float fPosX[] = { 0.0f,0.0f,0.1f,-0.1f,0.0f };
        constexpr int nNumSize = sizeof(fPosZ) / sizeof(float);
        int NumLine = 30;
        float C_R = D3DX_PI * 2 / NumLine;	// 円周率

        auto *RoundX = new VectorDrawData[NumLine + 1];
        auto *RoundY = new VectorDrawData[NumLine + 1];
        auto *lineX = new VectorDrawData[nNumSize];
        auto *lineY = new VectorDrawData[nNumSize];
        auto *lineZ = new VectorDrawData[nNumSize];

        // 円の生成
        for (int i = 0; i < (NumLine + 1); i++)
        {
            RoundX[i].pos.x = ((0.0f + cosf(C_R * i)));
            RoundX[i].pos.y = 0.0f;
            RoundX[i].pos.z = ((0.0f + sinf(C_R * i)));
            RoundX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

            RoundY[i].pos.x = 0.0f;
            RoundY[i].pos.y = ((0.0f + cosf(C_R * i)));
            RoundY[i].pos.z = ((0.0f + sinf(C_R * i)));
            RoundY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);
        }

        // 矢印の生成
        for (int i = 0; i < nNumSize; i++)
        {
            lineX[i].pos.z = fPosX[i];
            lineX[i].pos.x = fPosZ[i];
            lineX[i].pos.y = 0.0f;
            lineX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

            lineY[i].pos.z = fPosX[i];
            lineY[i].pos.x = 0.0f;
            lineY[i].pos.y = fPosZ[i];
            lineY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);

            lineZ[i].pos.z = -fPosZ[i];
            lineZ[i].pos.x = fPosX[i];
            lineZ[i].pos.y = 0.0f;
            lineZ[i].color = D3DCOLOR_RGBA(0, 0, 255, 255);
        }

        device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);
        device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);
        device->SetTexture(0, nullptr);

        // 描画
        device->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, RoundX, sizeof(VectorDrawData));
        device->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, RoundY, sizeof(VectorDrawData));
        device->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), lineZ, sizeof(VectorDrawData));
        device->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), lineX, sizeof(VectorDrawData));
        device->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), lineY, sizeof(VectorDrawData));

        // メモリの解放
        delete[]RoundX;
        delete[]RoundY;
        delete[]lineZ;
        delete[]lineX;
        delete[]lineY;
    }

    InheritanceObject & InheritanceObject::operator=(const InheritanceObject & obj)
    {
        m_Look = obj.m_Look;
        m_Vector = obj.m_Vector;
        m_Parameter = obj.m_Parameter;
        m_WorldMatrix = obj.m_WorldMatrix;
        m_LocalMatrix = obj.m_LocalMatrix;
        m_MatType = obj.m_MatType;
        m_MatrixView = obj.m_MatrixView;
        MeshLighting = obj.MeshLighting;
        return *this;
    }

    //==========================================================================
    //
    // class  : MaterialLighting
    // Content: マテリアルのライティング
    //
    //==========================================================================
    MaterialLighting::MaterialLighting()
    {
        ZeroMemory(&MeshLighting, sizeof(D3DMATERIAL9));
        SetAmbient(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
        SetDiffuse(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
        SetSpecular(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
        SetEmissive(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
        SetMaterialPower(1.0f);
    }
    MaterialLighting::~MaterialLighting()
    {
    }

    //==========================================================================
    /**
    @brief アンビエントライト
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void MaterialLighting::SetAmbient(const D3DXCOLOR & color)
    {
        MeshLighting.Ambient = color;
    }

    //==========================================================================
    /**
    @brief ディレクショナルライト
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void MaterialLighting::SetDiffuse(const D3DXCOLOR & color)
    {
        MeshLighting.Diffuse = color;
    }

    //==========================================================================
    /**
    @brief スペキュラー
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void MaterialLighting::SetSpecular(const D3DXCOLOR & color)
    {
        MeshLighting.Specular = color;
    }

    //==========================================================================
    /**
    @brief 放射性
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void MaterialLighting::SetEmissive(const D3DXCOLOR & color)
    {
        MeshLighting.Emissive = color;
    }

    //==========================================================================
    /**
    @brief スペキュラー光のパワー値
    */
    void MaterialLighting::SetMaterialPower(float power)
    {
        MeshLighting.Power = power;
    }

    //==========================================================================
    /**
    @brief マテリアルのライティングの設定
    */
    void MaterialLighting::SetD3DMaterial9(const D3DMATERIAL9 & material)
    {
        MeshLighting = material;
    }

    //==========================================================================
    /**
    @brief マテリアルのライティングの取得
    @return マテリアルのライティング情報
    */
    const D3DMATERIAL9 & MaterialLighting::GetD3DMaterial9() const
    {
        return MeshLighting;
    }

    //==========================================================================
    //
    // class  : Object
    // Content: オブジェクト
    //
    //==========================================================================
    Object::Object() : Renderer(RendererID::Object)
    {
        SetComponentName("Object");
    }
    Object::~Object(){}

    //==========================================================================
    /**
    @brief 更新
    */
    void Object::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void Object::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        device;
    }

    void Object::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : Mesh
    // Content: メッシュ
    //
    //==========================================================================
    Mesh::Mesh() : Renderer(RendererID::Mesh)
    {
        SetComponentName("Mesh");
    }
    Mesh::~Mesh() {}

    //==========================================================================
    /**
    @brief 更新
    */
    void Mesh::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void Mesh::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_MeshData.Existence() && m_TextureData.Existence())
        {
            // FVFの設定
            device->SetFVF(FVF_VERTEX_4);

            // サイズ
            device->SetStreamSource(0, (&m_MeshData).VertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

            device->SetIndices((&m_MeshData).IndexBuffer);

            // マテリアル情報をセット
            device->SetMaterial(&GetD3DMaterial9());

            // 各種行列の設定
            device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);

            device->SetTexture(0, &m_TextureData);
            device->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_MeshData.Info().MaxVertex, 0, m_MeshData.Info().MaxPrimitive);
            device->SetTexture(0, nullptr);
        }
    }

    void Mesh::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : XModel
    // Content: Xモデル
    //
    //==========================================================================
    XModel::XModel() : Renderer(RendererID::Xmodel)
    {
        SetComponentName("XModel");
    }

    XModel::~XModel() {}

    //==========================================================================
    /**
    @brief 更新
    */
    void XModel::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void XModel::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_XModelData.Existence())
        {
            device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);
            device->SetMaterial(&GetD3DMaterial9());
            device->SetFVF(m_XModelData->GetFVF());
            for (int i = 0; i < (int)m_XModelData.NumMesh(); i++)
            {
                auto &texture = m_XModelData.Texture()[i];
                if (!texture.Existence())continue;
                device->SetTexture(0, &texture);
                m_XModelData->DrawSubset(i);
                device->SetTexture(0, nullptr);
            }
        }
    }

    void XModel::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : Sphere
    // Content: 球体
    //
    //==========================================================================
    Sphere::Sphere() : Renderer(RendererID::Sphere)
    {
        SetComponentName("Sphere");
    }
    Sphere::~Sphere() {}

    //==========================================================================
    /**
    @brief 更新
    */
    void Sphere::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void Sphere::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_SphereData.Existence() && m_TextureData.Existence())
        {
            device->SetFVF(m_SphereData->GetFVF());
            device->SetMaterial(&GetD3DMaterial9());
            device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);
            device->SetTexture(0, &m_TextureData);
            m_SphereData->DrawSubset(0);
            device->SetTexture(0, nullptr);
        }
    }

    void Sphere::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : Cube
    // Content: キューブ 
    //
    //==========================================================================
    Cube::Cube() : Renderer(RendererID::Cube)
    {
        SetComponentName("Cube");
    }

    Cube::~Cube() {}

    //==========================================================================
    /**
    @brief 更新
    */
    void Cube::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void Cube::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_CubeData.Existence() && m_TextureData.Existence())
        {
            device->SetStreamSource(0, (&m_CubeData).VertexBuffer, 0, sizeof(VERTEX_4));
            device->SetIndices((&m_CubeData).IndexBuffer);
            device->SetFVF(FVF_VERTEX_4);
            device->SetMaterial(&GetD3DMaterial9());
            device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);
            device->SetTexture(0, &m_TextureData);
            device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_CubeData.Info().NumVertex, 0, m_CubeData.Info().NumTriangle);
            device->SetTexture(0, nullptr);
        }
    }

    void Cube::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : Billboard
    // Content: ビルボード
    //
    //==========================================================================
    Billboard::Billboard() : Renderer(RendererID::Billboard)
    {
        m_uv = floatUV(0.0f, 0.0f, 1.0f, 1.0f);
        m_color = 255;
        SetComponentName("Billboard");
    }

    Billboard::~Billboard()
    {

    }

    //==========================================================================
    /**
    @brief 更新
    */
    void Billboard::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::WorldView;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void Billboard::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_BillboardData.Existence() && m_TextureData.Existence())
        {
            VERTEX_4* pseudo = nullptr;

            if (m_TechnologyAdd)
            {
                AlpharefEnd(device);
                ZwriteenableStart(device);
                Sub(device);
            }

            // FVFの設定
            device->SetFVF(FVF_VERTEX_4);

            (&m_BillboardData).VertexBuffer->Lock(0, 0, (void**)&pseudo, D3DLOCK_DISCARD);
            pseudo[0].Tex = D3DXVECTOR2(m_uv.u0, m_uv.v0);
            pseudo[1].Tex = D3DXVECTOR2(m_uv.u1, m_uv.v0);
            pseudo[2].Tex = D3DXVECTOR2(m_uv.u1, m_uv.v1);
            pseudo[3].Tex = D3DXVECTOR2(m_uv.u0, m_uv.v1);

            pseudo[0].color = m_color.get();
            pseudo[1].color = m_color.get();
            pseudo[2].color = m_color.get();
            pseudo[3].color = m_color.get();

            pseudo[0].pos = m_polygon.upper_left;
            pseudo[1].pos = m_polygon.upper_right;
            pseudo[2].pos = m_polygon.bottom_right;
            pseudo[3].pos = m_polygon.bottom_left;
            (&m_BillboardData).VertexBuffer->Unlock();

            device->SetStreamSource(0, (&m_BillboardData).VertexBuffer, 0, sizeof(VERTEX_4));

            // インデックス登録
            device->SetIndices((&m_BillboardData).IndexBuffer);

            // トランスフォーム
            device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);

            device->SetTexture(0, &m_TextureData);
            device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
            device->SetTexture(0, nullptr);

            if (m_TechnologyAdd)
            {
                AlpharefStart(device, 100);
                ZwriteenableEnd(device);
                Add(device);
            }
        }
    }

    void Billboard::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : BillboardAnimation
    // Content: アニメーションビルボード
    //
    //==========================================================================
    BillboardAnimation::BillboardAnimation()
    {
        SetComponentName("BillboardAnimation");
        m_Count = 0;
        m_Frame = 0;
        m_Pattern = 0;
        m_Direction = 0;
        m_cut = 0;
        m_texsize = intTexvec(0, 0, 1, 1);
        m_loop = false;
        m_lock = false;
    }

    BillboardAnimation::~BillboardAnimation()
    {
    }

    void BillboardAnimation::PlayAnimation(bool loop)
    {
        m_loop = loop;
        m_lock = true;
    }

    void BillboardAnimation::StopAnimation()
    {
        m_lock = false;
    }

    void BillboardAnimation::SetAnimationData(int count, int frame, int pattern, int direction)
    {
        int nCount = 0;
        m_Count = count;
        m_Frame = frame;
        m_Pattern = pattern;
        m_Direction = direction;

        m_cut.w = (float)m_texsize.w / m_Direction;

        // 切り取る縦幅を計算
        for (int i = 0;; i += m_Direction)
        {
            if (m_Pattern <= i)
                break;

            nCount++;
        }
        m_cut.h = (float)m_texsize.h / nCount;
        m_cut.x = m_cut.y = 0.0f;
    }

    void BillboardAnimation::AddAnimationCounter(int count)
    {
        if (m_lock)
        {
            m_Count += count;

            if ((m_Frame*m_Pattern) <= m_Count)
            {
                m_Count = 0;
                if (!m_loop)m_lock = false;
            }

            int PattanNum = (m_Count / m_Frame) % m_Pattern; // パターン数
            int patternV = PattanNum % m_Direction; // 横方向のパターン
            int patternH = PattanNum / m_Direction; // 縦方向のパターン

            m_cut.x = patternV * m_cut.w; // 切り取り座標X
            m_cut.y = patternH * m_cut.h; // 切り取り座標Y

            m_uv.u0 = (float)m_cut.x / m_texsize.w;
            m_uv.v0 = (float)m_cut.y / m_texsize.h;
            m_uv.u1 = (float)(m_cut.x + m_cut.w) / m_texsize.w;
            m_uv.v1 = (float)(m_cut.y + m_cut.h) / m_texsize.h;

            m_Activity = true;
        }
        if (!m_lock)m_Activity = false;
    }

    //==========================================================================
    //
    // class  : MeshField
    // Content: メッシュフィールド
    //
    //==========================================================================
    MeshField::MeshField() : Renderer(RendererID::Mesh)
    {
        SetComponentName("MeshField");
    }
    MeshField::~MeshField() {}

    //==========================================================================
    /**
    @brief 更新
    */
    void MeshField::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();
    }

    //==========================================================================
    /**
    @brief 描画
    @param device [in] デバイス
    */
    void MeshField::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_MeshFieldData.Existence() && m_TextureData.Existence())
        {
            // FVFの設定
            device->SetFVF(FVF_VERTEX_4);

            // サイズ
            device->SetStreamSource(0, (&m_MeshFieldData).VertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

            device->SetIndices((&m_MeshFieldData).IndexBuffer);

            // マテリアル情報をセット
            device->SetMaterial(&GetD3DMaterial9());

            // 各種行列の設定
            device->SetTransform(D3DTS_WORLD, &m_WorldMatrix);

            device->SetTexture(0, &m_TextureData);
            device->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_MeshFieldData.Info().MaxVertex, 0, m_MeshFieldData.Info().MaxPrimitive);
            device->SetTexture(0, nullptr);
        }
    }

    void MeshField::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }

    //==========================================================================
    //
    // class  : Light
    // Content: 光源
    //
    //==========================================================================
    Light::Light() : Renderer(RendererID::Object)
    {
        SetComponentName("Light");

        D3DXMatrixIdentity(&m_WorldMatrix);

        // ライトの設定
        ZeroMemory(&m_light, sizeof(m_light));
        auto vec = D3DXVECTOR3(-m_WorldMatrix._41, -m_WorldMatrix._42, -m_WorldMatrix._43);
        D3DXVec3Normalize((D3DXVECTOR3*)&m_light.Direction, &vec); // 正規化

        SetDiffuse({ 1.0f ,1.0f ,1.0f ,1.0f });
        SetAmbient({ 0.3f ,0.3f ,0.3f ,0.3f });
        SetSpecular(m_light.Specular);

        m_light.Diffuse = MeshLighting.Diffuse;
        m_light.Ambient = MeshLighting.Ambient;

        m_index = 0;
        m_enable = false;
    }
    Light::~Light()
    {
    }
    void Light::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();

        m_light.Position = D3DXVECTOR3(m_WorldMatrix._41, m_WorldMatrix._42, m_WorldMatrix._43);
        m_light.Diffuse = MeshLighting.Diffuse;
        m_light.Ambient = MeshLighting.Ambient;
        m_light.Specular = MeshLighting.Specular;

        auto vec = -D3DXVECTOR3(m_WorldMatrix._41, m_WorldMatrix._42, m_WorldMatrix._43);
        D3DXVec3Normalize((D3DXVECTOR3*)&m_light.Direction, &vec); // 正規化
    }
    void Light::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (!m_enable)return;

        //光源の設定
        device->SetLight(m_index, &m_light); // ライトの種類
        device->LightEnable(m_index, m_enable); // 使用の許可

        // グローバルアンビエントの設定
        device->SetRenderState(D3DRS_AMBIENT, D3DXCOLOR(m_light.Ambient));
    }
    void Light::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }
    void Light::GetDirection(const transform::Transform & obj, D3DXVECTOR3 & LightDir)
    {
        //平行光源の位置ベクトルから方向ベクトルを計算する
        LightDir = -(*GetPosition() - *obj.GetPosition());
        D3DXVec3Normalize(&LightDir, &LightDir);
    }
    void Light::GetDirection(const transform::Transform & obj, D3DXVECTOR4 & LightDir)
    {
        //平行光源の位置ベクトルから方向ベクトルを計算する
        auto dir3 = -(*GetPosition() - *obj.GetPosition());
        auto dir4 = D3DXVECTOR4(dir3.x, dir3.y, dir3.z, 0.0f);
        D3DXVec3Normalize((D3DXVECTOR3*)&LightDir, (D3DXVECTOR3*)&dir4);
    }
    void Light::SetType(const D3DLIGHTTYPE & type)
    {
        m_light.Type = type; 
    }
    void Light::Enable(int index, bool frag)
    {
        m_enable = frag;
        m_index = index;
    }

    //==========================================================================
    //
    // class  : Effekseer
    // Content: Effekseer
    //
    //==========================================================================
    Effekseer::Effekseer() : Renderer(RendererID::Effect)
    {
        SetComponentName("Effekseer");
        m_target = nullptr;
		m_mat43.Indentity();
    }
    Effekseer::~Effekseer()
    {
        Stop();
    }
    void Effekseer::Update()
    {
        // デフォルトの場合設定
        if (m_MatType == MatrixType::Default)
            m_MatType = MatrixType::World;

        UpdateMatrix();

        if (!m_EffekseerData.Existence())return;

        auto & eff = (&m_EffekseerData);
        auto & mat = m_WorldMatrix;
        auto & scale = m_Parameter.scale;
        auto & rotation = m_Parameter.rotation;

        eff.m_manager->SetLocation(GetHandle(), mat._41, mat._42, mat._43);
        eff.m_manager->SetScale(GetHandle(), scale.x, scale.y, scale.z);
        eff.m_manager->SetRotation(GetHandle(), rotation.y, rotation.x, rotation.z);

        if (m_target == nullptr)return;
        auto mat2 = m_target->GetWorldMatrix();
        eff.m_manager->SetTargetLocation(GetHandle(), mat2->_41, mat2->_42, mat2->_43);
    }
    void Effekseer::Draw(LPDIRECT3DDEVICE9 device)
    {
        device;
    }
    void Effekseer::DebugDraw(LPDIRECT3DDEVICE9 device)
    {
        DrawVector(device);
    }
    void Effekseer::Play()
    {
        if (!m_EffekseerData.Existence())return;
        auto & mat = m_WorldMatrix;

        SetHandle(GetManager()->Play(GetEffect(), mat._41, mat._42, mat._43));
    }
    void Effekseer::Play(transform::Transform * obj)
    {
        if (!m_EffekseerData.Existence())return;
        if (obj == nullptr)return;
        auto mat = obj->GetWorldMatrix();

        SetRotation(*obj->GetRotation());
        SetScale(*obj->GetScale());
        SetPosition(mat->_41, mat->_42, mat->_43);
        SetHandle(GetManager()->Play(GetEffect(), mat->_41, mat->_42, mat->_43));
    }
    void Effekseer::Stop()
    {
        if (!m_EffekseerData.Existence())return;
        if(GetHandle() == -1)return;

        GetManager()->StopEffect(GetHandle());
        SetHandle(-1);
    }
	bool Effekseer::Check()
	{
		if (!m_EffekseerData.Existence())return false;
		if (GetHandle() == -1)return false;
		
		return GetManager()->GetShown(GetHandle());
	}
    void Effekseer::SetTarget(transform::Transform * obj)
    {
        m_target = obj;
    }
}

_MSLIB_END