//==========================================================================
// 球体 [Sphere.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Sphere.h"

_MSLIB_BEGIN

namespace sphere
{
    //==========================================================================
    //
    // class  : SphereData
    // Content: 球体のデータ
    //
    //==========================================================================
    SphereData::SphereData()
    {
        quality = 0;
        asset = nullptr;
        mesh = nullptr;
        buffer = nullptr;
        createID = (int64_t)0;
        CreatePtr = nullptr;
    }
    SphereData::~SphereData()
    {
        if (GetRef() == 0)
        {
            if (asset != nullptr)
            {
                asset->Release();
                asset = nullptr;
            }
            if (mesh != nullptr)
            {
                mesh->Release();
                mesh = nullptr;
            }
            if (buffer != nullptr)
            {
                buffer->Release();
                buffer = nullptr;
            }
            CreatePtr = nullptr;
        }
    }

    //==========================================================================
    //
    // class  : SphereReference
    // Content: 参照用
    //
    //==========================================================================
    SphereReference::SphereReference()
    {
        m_data = nullptr;
    }

    SphereReference::~SphereReference()
    {
        Release();
    }

    void SphereReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->CreatePtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->CreatePtr->Delete(m_data);
        m_data = nullptr;
    }

    CreateSphere::CreateSphere()
    {
        SetComponentName("CreateSphere");
        m_device = nullptr;
        m_hwnd = nullptr;
        createIDCount = (int64_t)0;
    }

    CreateSphere::CreateSphere(LPDIRECT3DDEVICE9 device, HWND hWnd)
    {
        SetComponentName("CreateSphere");
        m_device = device;
        m_hwnd = hWnd;
        createIDCount = (int64_t)0;
    }

    CreateSphere::~CreateSphere()
    {
        AllDestroyComponent();
        for (auto &itr : m_data)
        {
            if (itr.second.buffer != nullptr)
            {
                itr.second.buffer->Release();
                itr.second.buffer = nullptr;
            }
            if (itr.second.asset != nullptr)
            {
                itr.second.asset->Release();
                itr.second.asset = nullptr;
            }
            if (itr.second.mesh != nullptr)
            {
                itr.second.mesh->Release();
                itr.second.mesh = nullptr;
            }
        }
        m_data.clear();
    }

    //==========================================================================
    /**
    @brief キューブを生成する。
    @param Quality [in] 品質
    */
    SphereReference CreateSphere::Create(int Quality)
    {
        auto itr = m_data.find(Quality);

        // 存在しない場合は処理を開始する
        if (itr == m_data.end())
        {
            auto &data = m_data[Quality];

            if (Quality < 2)
                Quality = 2;

            Create(Quality, &data);
        }
        // データが存在する
        else if (itr != m_data.end())
        {
            // バッファが存在しない
            if (itr->second.buffer == nullptr)
            {
                Create(Quality, &itr->second);
            }
        }

        return &m_data[Quality];
    }

    //==========================================================================
    /**
    @brief キューブを破棄する。
    @param data [in] データ
    */
    void CreateSphere::Delete(SphereData * data)
    {
        if (data == nullptr)return;
        if (data->CreatePtr != this)return;
        auto itr = m_data.find(data->quality);
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }

    //==========================================================================
    /**
    @brief 生成
    @param Quality [in] クオリティ
    @param data [out] データ
    */
    void CreateSphere::Create(int Quality, SphereData * Out)
    {
        createIDCount++;
        Out->createID = createIDCount;
        Out->CreatePtr = this;
        Out->quality = Quality;
        D3DXCreateSphere(m_device, 1.0f, Out->quality * 2, Out->quality, &Out->mesh, nullptr);
        Out->mesh->CloneMeshFVF(0, FVF_VERTEX_4, m_device, &Out->asset);
        Out->asset->GetVertexBuffer(&Out->buffer);
        CreateUV(Out);
    }

    //==========================================================================
    /**
    @brief UVの生成
    @param Out [out] データ
    */
    void CreateSphere::CreateUV(SphereData * Out)
    {
        VERTEX_4* pseudo;// 頂点バッファのロック
        float r = 1.0f;
        Out->buffer->Lock(0, 0, (void**)&pseudo, 0);
        for (DWORD i = 0; i < Out->asset->GetNumVertices(); i++)
        {
            float q = 0.0f;
            float q2 = 0.0f;
            auto* pPseudo = &pseudo[i];

            q = atan2(pPseudo->pos.z, pPseudo->pos.x);

            pPseudo->Tex.x = q / (2.0f * 3.1415f);
            q2 = asin(pPseudo->pos.y / r);
            pPseudo->Tex.y = (1.0f - q2 / (3.1415f / 2.0f)) / 2.0f;
            if (pPseudo->Tex.x > 1.0f)
            {
                pPseudo->Tex.x = 1.0f;
            }

            pPseudo->Tex = -pPseudo->Tex;
            pPseudo->color = D3DCOLOR_RGBA(255, 255, 255, 255);
            pPseudo->Normal = D3DXVECTOR3(1, 1, 1);
        }
        Out->buffer->Unlock();
    }

    //==========================================================================
    //
    // class  : SetSphere
    // Content: 球体登録クラス
    //
    //==========================================================================
    SetSphere::SetSphere() {}
    SetSphere::~SetSphere() {}

    //==========================================================================
    /**
    @brief 球体の登録
    @param data [in] 球体
    */
    void SetSphere::SetSphereData(const SphereReference & data)
    {
        m_SphereData = data;
    }

    //==========================================================================
    /**
    @brief 球体の取得
    */
    SphereReference & SetSphere::GetSphereData()
    {
        return m_SphereData;
    }
}

_MSLIB_END