//==========================================================================
// ダイレクインプット[Dinput.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Dinput.h"

_MSLIB_BEGIN

namespace dinput
{
    std::list<Dinput*> Dinput::m_DirectInput; // デバイスの格納

    Dinput::Dinput()
    {
        m_DirectInput.emplace_back(this);
    }
    Dinput::~Dinput()
    {
        auto itr = std::find(m_DirectInput.begin(), m_DirectInput.end(), this);
        if (itr != m_DirectInput.end())
            m_DirectInput.erase(itr);
    }

    //==========================================================================
    /**
    @brief 登録済みデバイスの更新
    */
    void Dinput::UpdateAll()
    {
        for (auto &itr : m_DirectInput)
        {
            itr->Update();
        }
    }

    //==========================================================================
    /**
    @brief インプットリスト
    */
    std::list<Dinput*>& Dinput::GetInputList()
    {
        return m_DirectInput;
    }
    Input::Input()
    {
        m_DInput = nullptr;
        m_DIDevice = nullptr;
    }
    Input::~Input()
    {
        Release();
    }

    //==========================================================================
    /**
    @brief 解放
    */
    void Input::Release()
    {
        AllDestroyComponent();

        if (m_DIDevice != nullptr)
        {// 入力デバイス(キーボード)の開放
         // キーボードへのアクセス権を開放(入力制御終了)
            m_DIDevice->Unacquire();

            m_DIDevice->Release();
            m_DIDevice = nullptr;
        }

        if (m_DInput != nullptr)
        {// DirectInputオブジェクトの開放
            m_DInput->Release();
            m_DInput = nullptr;
        }
    }
}
_MSLIB_END