//==========================================================================
// シーン遷移[SceneManager.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SceneManager.h"
#include "DefaultSystem.h"
#include "Collider.h"
#include "Initializer.h"
#include "Renderer.h"

_MSLIB_BEGIN

namespace scene_manager
{
    using namespace collider;
    using namespace initializer;
    BaseScene::BaseScene(const std::string & SceneName, const std::string & file_name) : m_SceneName(SceneName), m_file_name(file_name)
    {
        SetComponentName(SceneName);
    }

    BaseScene::~BaseScene()
    {
        m_object.clear();

        // ファイル名破棄
        m_file_name.clear();
    }

    //==========================================================================
    /**
    @brief 初期化
    @return 初期化終了時にtrueが返ります。
    */
    bool BaseScene::Init()
    {
        return InitAll();
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void BaseScene::Update()
    {
        UpdateAll();
    }

    //==========================================================================
    /**
    @brief シーン名取得
    @return シーン名
    */
    const std::string & BaseScene::GetSceneName() const
    {
        return m_SceneName;
    }

	const std::string & BaseScene::GetFileName() const
	{
		return m_file_name;
	}

    //==========================================================================
    /**
    @brief 全初期化
    @return 全初期化終了時にtrueが返ります。
    */
    bool BaseScene::InitAll()
    {
        int max_data = (int)GetNumChild(); // 最大インスタンス数
        int now_data = 0; // 初期化済みデータ数

        // 子要素を全て回す
        for (auto & itr : GetChild())
        {
            // コンポーネントの取得
            auto obj = itr->GetWarrantyParent<ObjectManager>();

            // 初期化カウンタを回す
            now_data++;

            // コンポーネントの取得に成功するまで
            if (obj == nullptr)continue;

            // 初期化済みなら無効
            if (obj->GetLock())continue;

            // ファイル名の記録
            obj->SetFileName(m_file_name);
            obj->Init();
            Initializer::InitAll();

            // 終了時にデータを読み取る
            auto &ifstream = DefaultSystem::BeginFstreamReadingBinary(m_file_name);
            obj->Load(ifstream);
            DefaultSystem::EndFstreamReadingBinary();

            m_object.push_back(obj);
            obj->Lock();
            break;
        }

#if defined(_MSLIB_DEBUG)
        float win_y_size = 45.0f;
        ImGuiWindowFlags flags = ImGuiWindowFlags_NoTitleBar |
            ImGuiWindowFlags_NoResize |
            ImGuiWindowFlags_NoSavedSettings |
            ImGuiWindowFlags_NoCollapse;

        m_Imgui.SetNextWindowPos({ 0.0f,(float)GetDevice()->GetWindowsSize()->y - win_y_size });
        m_Imgui.NewWindow("ProgressBar", false, { (float)GetDevice()->GetWindowsSize()->x,win_y_size }, flags, 0.0f);
        m_Imgui._ProgressBar((float)now_data / (float)max_data);
        m_Imgui.EndWindow();
#endif
        // 初期化が終了
        return max_data == now_data ? true : false;
    }

    //==========================================================================
    /**
    @brief 全更新
    */
    void BaseScene::UpdateAll()
    {
#if defined(_MSLIB_DEBUG)
        m_Imgui.Separator();
        m_Imgui.Text("Scene Name : " + GetComponentName());
        m_Imgui.Separator();
#endif
        // イテレーターの先頭から最後まで回す
        for (auto &itr : m_object)
        {
#if defined(_MSLIB_DEBUG)
            auto start = GetTimeSec();
#endif
            if (itr->GetActivity())
                itr->Update();
#if defined(_MSLIB_DEBUG)
            itr->SetProcessTime(GetTimeSec() - start);
#endif
        }
#if defined(_MSLIB_DEBUG)
        System();
#endif
    }

    //==========================================================================
    /**
    @brief 時間の取得
    @return 時間
    */
    float BaseScene::GetTimeSec()
    {
        return static_cast<float>(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()) / (float)1000000000;
    }

    //==========================================================================
    /**
    @brief システム
    */
    void BaseScene::System()
    {
#if defined(_MSLIB_DEBUG)
        m_Imgui.Separator();
        m_Imgui.Text("Default");
        m_Imgui.Separator();

        //==========================================================================
        // All Debug Activity
        //==========================================================================
        m_Imgui.Separator();
        bool activ = renderer::Renderer::GetAllDebugActivity();
        if (m_Imgui.Checkbox("All Debug Activity", activ))
        {
            renderer::Renderer::AllDebugActivity(activ);
        }
        m_Imgui.Separator();

        //==========================================================================
        // Processing load
        //==========================================================================
        bool & bProcessingTime = m_system.Get("Processing Load");
        m_Imgui.Checkbox("Processing Load", bProcessingTime);
        if (bProcessingTime)
        {
            m_Imgui.NewWindow("Processing Load", bProcessingTime, DefaultSystem::SystemWindowFlags);
            m_Imgui.Separator();
            m_Imgui.Text("Processing Load");
            m_Imgui.Separator();

            for (auto &itr : m_object)
            {
                m_Imgui._Bullet();
                m_Imgui.Text(itr->GetComponentName() + " : " + m_Imgui.CreateText("update time = %.3f ms", itr->GetProcessTime()*1000.0f));
            }

            m_Imgui.Separator();
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // Camera List
        //==========================================================================
        bool & bCameraList = m_system.Get("Camera List");
        m_Imgui.Checkbox("Camera List", bCameraList);
        if (bCameraList)
        {
            m_Imgui.NewWindow("Camera List", bCameraList, DefaultSystem::SystemWindowFlags);
            m_Imgui.Separator();
            m_Imgui.Text("Camera List");
            m_Imgui.Separator();
            for (auto &itr : m_object)
            {
                for (int i = 0; i < itr->Camera()->Size(); i++)
                {
                    auto pCamera = itr->Camera()->Get(i);
                    bool flag = false;
                    // 一致した場合は有効である
                    if (pCamera->GetCamera() == pCamera)
                    {
                        flag = true;
                    }

                    auto text = m_Imgui.CreateText(" [%p]", pCamera);
                    if (m_Imgui.Checkbox(pCamera->GetComponentName() + text, flag))
                        pCamera->IsMainCamera();
                }
            }
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // Object list
        //==========================================================================
        bool & bObjectList = m_system.Get("Object List");
        m_Imgui.Checkbox("Object List", bObjectList);
        if (bObjectList)
        {
            m_Imgui.NewWindow("Object List", bObjectList, DefaultSystem::SystemWindowFlags);
            m_Imgui.Separator();
            m_Imgui.Text("Object List");
            m_Imgui.Separator();
            for (auto &itr : m_object)
            {
                itr->ObjectList();
            }
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // Object Editor
        //==========================================================================
        bool & bObjectEditor = m_system.Get("Object Editor");
        m_Imgui.Checkbox("Object Editor", bObjectEditor);
        if (bObjectEditor)
        {
            m_Imgui.NewWindow("Object Editor", bObjectEditor, DefaultSystem::SystemWindowFlags);
            m_Imgui.Separator();
            m_Imgui.Text("Object Editor");
            m_Imgui.Separator();
            SaveLoad();
            for (auto &itr : m_object)
            {
                itr->ObjectEditor();
            }
            m_Imgui.Separator();
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // 2DObject Editor
        //==========================================================================
        bool & b2DUIEditor = m_system.Get("2DObject Editor");
        m_Imgui.Checkbox("2DObject Editor", b2DUIEditor);
        if (b2DUIEditor)
        {
            m_Imgui.NewWindow("2DObject Editor", b2DUIEditor, DefaultSystem::SystemWindowFlags);
            m_Imgui.Separator();
            m_Imgui.Text("2DObject Editor");
            m_Imgui.Separator();
            SaveLoad();
            for (auto &itr : m_object)
            {
                itr->UIEditor();
            }
            m_Imgui.Separator();
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // Resource Manager
        //==========================================================================
        bool & bResourceManager = m_system.Get("Resource Manager");
        m_Imgui.Checkbox("Resource Manager", bResourceManager);
        if (bResourceManager)
        {
            m_Imgui.NewWindow("Resource Manager", bResourceManager, DefaultSystem::SystemWindowFlags);
            m_Imgui.Separator();
            m_Imgui.Text("Resource Manager");
            m_Imgui.Separator();
            DefaultSystem::ResourceManager(GetTextureLoader());
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // Node Search
        //==========================================================================
        bool & bNodeSearch = m_system.Get("Node Search");
        m_Imgui.Checkbox("Node Search", bNodeSearch);
        if (bNodeSearch)
        {
            m_Imgui.NewWindow("Node Search", bNodeSearch, DefaultSystem::SystemWindowFlags);
            DefaultSystem::NodeSearch(this);
            m_Imgui.EndWindow();
        }

        //==========================================================================
        // User Defined
        //==========================================================================
        m_Imgui.Separator();
        m_Imgui.Text("User Defined");
        m_Imgui.Separator();
        for (auto &itr : m_object)
        {
            bool & bkey = m_system.Get(itr->GetComponentName());
            m_Imgui.Checkbox(itr->GetComponentName(), bkey);
            if (bkey)
            {
                m_Imgui.NewWindow(itr->GetComponentName(), bkey, DefaultSystem::SystemWindowFlags);
                m_Imgui.Separator();
                m_Imgui.Text(itr->GetComponentName());
                m_Imgui.Separator();                
                itr->Debug();
                m_Imgui.EndWindow();
            }
        }
#endif
    }

    void BaseScene::SaveLoad()
    {
        m_Imgui._Bullet();
        if (m_Imgui.MenuItem("Save"))
        {
            auto &ofstream = DefaultSystem::BeginFstreamWritingBinary(m_file_name);
            for (auto &itr : m_object)
            {
                itr->Save(ofstream);
            }
            DefaultSystem::EndFstreamWritingBinary();
        }
        m_Imgui._Bullet();
        if (m_Imgui.MenuItem("Load"))
        {
            auto &ifstream = DefaultSystem::BeginFstreamReadingBinary(m_file_name);
            for (auto &itr : m_object)
            {
                itr->Load(ifstream);
            }
            DefaultSystem::EndFstreamReadingBinary();
        }
        m_Imgui.Separator();
    }

    SceneManager::SceneManager()
    {
        SetComponentName("SceneManager");
        m_SceneName = "Not Scene";
        m_scene = nullptr;
    }

    SceneManager::~SceneManager()
    {
        Uninit();
    }

    //==========================================================================
    /**
    @brief 初期化
    @return 初期化終了時にtrueが返ります。
    */
    bool SceneManager::Init()
    {
        if (m_scene != nullptr)
        {
            return m_scene->Init();
        }
        return true;
    }

    //==========================================================================
    /**
    @brief 解放
    */
    void SceneManager::Uninit()
    {
        AllDestroyComponent();
        m_scene = nullptr;
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void SceneManager::Update()
    {
        if (m_scene != nullptr)
        {
            m_scene->Update();
        }
    }

    //==========================================================================
    /**
    @brief シーン切り替え関数
    @param scene [in] シーンポインタ
    */
    void SceneManager::ChangeScene(BaseScene * scene)
    {
        // シーンの破棄
        if (DestroyComponent<BaseScene>(m_SceneName))
        {
            m_SceneName = "";
            m_scene = nullptr;
        }

        // コンポーネントの登録、登録が成功したら以下の処理を実行します
        if (SetComponent<BaseScene>(scene))
        {
            m_scene = scene;
            m_SceneName = scene->GetComponentName();
        }
    }

    //==========================================================================
    /**
    @brief シーン名取得
    @return シーン名が返ります
    */
    const std::string SceneManager::GetSceneName() const
    {
        return m_SceneName;
    }
}

_MSLIB_END