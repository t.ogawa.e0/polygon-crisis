//==========================================================================
// list [list.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include <vector>
#include <algorithm>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "Component.h"

_MSLIB_BEGIN

namespace wrapper
{
    //==========================================================================
    //
    // class  : list
    // Content: std::list >> list
    //
    //==========================================================================
    template<typename _Ty>
    class list : public Component
    {
    private:
        // コピー禁止 (C++11)
        list(const list &) = delete;
        list &operator=(const list &) = delete;
        list &operator=(list&&) = delete;
    public:
        list() {
            SetComponentName("list<>");
        }
        ~list() {
            Release();
        }

        /**
        @brief 解放
        */
        void Release(void) {
            for (auto &itr : m_list) {
                Delete(itr);
            }
            m_list.clear();
            for (auto &itr : m_list_sub) {
                Delete(itr);
            }
            m_list_sub.clear();
            m_route.clear();
        }

        /**
        @brief インスタンスの生成
        @return インスタンス
        */
        _Ty * Create(void) {
            _Ty * Object = nullptr;
            m_list.emplace_back(New(Object));
            return Object;
        }

        /**
        @brief 生成したデータの読み込み
        */
        void LoadData(void) {
            m_route.clear();
            if (m_route.capacity() < m_list.size()) {
                m_route.reserve(m_list.size());
            }
            for (_Ty* pObj : m_list) {
                m_route.push_back(pObj);
            }
        }

        /**
        @brief サブメモリに管理権限を移します
        @param Object [in] 移動インスタンス
        @return インスタンス
        */
        _Ty * GetDelete(_Ty * Object) {
            auto itr = std::find(m_list.begin(), m_list.end(), Object);
            if (itr != m_list.end()) {
                // メインメモリから破棄
                m_list.erase(itr);
                // サブメモリに管理権限を持たせる
                m_list_sub.emplace_back(Object);
                LoadData();
            }
            else if (itr == m_list.end()) {
                return nullptr;
            }
            return Object; // 戻り値として返す
        }

        /**
        @brief サブメモリに管理権限を移します
        @param label [in] 管理番号
        @return インスタンス
        */
        _Ty * GetDelete(int label) {
            return GetDelete(Get(label));
        }

        /**
        @brief メインメモリに管理権限を戻す
        @param Object [in] インスタンス
        */
        void PushBack(_Ty * Object) {
            auto itr = std::find(m_list_sub.begin(), m_list_sub.end(), Object);
            if (itr != m_list_sub.end()) {
                // メインメモリから破棄
                m_list_sub.erase(itr);
                // サブメモリに管理権限を持たせる
                m_list.emplace_back(Object);
                LoadData();
            }
        }

        /**
        @brief 特定のObjectの破棄
        @param Object [in] 破棄するオブジェクトのアドレスを入れてください
        */
        void PinpointRelease(_Ty * Object) {
            auto itr1 = std::find(m_list.begin(), m_list.end(), Object);
            auto itr2 = std::find(m_list_sub.begin(), m_list_sub.end(), Object);
            if (itr1 != m_list.end()) {
                Delete((*itr1));
                m_list.erase(itr1);
                LoadData();
            }
            if (itr2 != m_list_sub.end()) {
                Delete((*itr2));
                m_list_sub.erase(itr2);
                LoadData();
            }
        }

        /**
        @brief 特定のObjectの破棄
        @param Object [in] 破棄する管理IDを入れてください
        */
        void PinpointRelease(int label) {
            PinpointRelease(Get(label));
        }

        /**
        @brief 管理しているObject数
        @return データ数
        */
        int Size(void) {
            return (int)m_route.size();
        }

        /**
        @brief Objectの取得
        @param label [in] 管理IDを入れてください
        @return 取得したい情報
        */
        _Ty * Get(int label) {
            // メモリ領域内の時
            if (0 <= label && label < Size()) {
                return m_route[label];
            }
            return nullptr;
        }
    private:
        /**
        @brief インスタンス生成
        @param Object [in] インスタンス生成対象
        @return インスタンス
        */
        _Ty * New(_Ty *& Object) {
            Object = new _Ty;
            return Object;
        }

        /**
        @brief インスタンスの破棄
        @param Object [in] インスタンス破棄対象
        @return nullptr が返ります
        */
        _Ty * Delete(_Ty *& Object) {
            // 破棄メモリがある時
            if (Object != nullptr) {
                delete Object;
                Object = nullptr;
            }
            return Object;
        }
    private:
        std::list<_Ty*>m_list; // メインメモリ
        std::list<_Ty*>m_list_sub; // サブメモリ
        std::vector<_Ty*>m_route; // アクセスルート
    };
}

_MSLIB_END