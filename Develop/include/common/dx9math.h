//==========================================================================
// dx9math [dx9math.h]
// Content: math types and functions
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN
namespace math 
{
	//==========================================================================
	//
	// class  : Vector2
	//
	//==========================================================================
	class Vector2 : public D3DXVECTOR2
	{
	public:
		Vector2();
		~Vector2();

		// constructor
		using D3DXVECTOR2::D3DXVECTOR2;

		// casting
		using D3DXVECTOR2::operator const FLOAT *;
		using D3DXVECTOR2::operator FLOAT *;

		// assignment operators
		using D3DXVECTOR2::operator+=;
		using D3DXVECTOR2::operator-=;
		using D3DXVECTOR2::operator*=;
		using D3DXVECTOR2::operator/=;

		// binary operators
		using D3DXVECTOR2::operator+;
		using D3DXVECTOR2::operator-;
		using D3DXVECTOR2::operator*;
		using D3DXVECTOR2::operator/;

		using D3DXVECTOR2::operator==;
		using D3DXVECTOR2::operator!=;

		// initializer Operators
		void operator()();
		void operator()(CONST FLOAT *);
		void operator()(CONST D3DXFLOAT16 *);
		void operator()(FLOAT, FLOAT);

		// Member Selection
		const Vector2* operator->() const noexcept;
		// Member Selection
		Vector2* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Vector2 &operator ++();
		Vector2 &operator --();
		Vector2 operator ++(int);
		Vector2 operator --(int);

		// Assignment Operators
		Vector2 &operator =(const Vector2 &);
		Vector2 &operator =(const float);
	};

	//==========================================================================
	//
	// class  : Vector3
	//
	//==========================================================================
	class Vector3 : public D3DXVECTOR3
	{
	public:
		Vector3();
		~Vector3();

		// constructor
		using D3DXVECTOR3::D3DXVECTOR3;
	
		// casting
		using D3DXVECTOR3::operator const FLOAT *;
		using D3DXVECTOR3::operator FLOAT *;

		// assignment operators
		using D3DXVECTOR3::operator+=;
		using D3DXVECTOR3::operator-=;
		using D3DXVECTOR3::operator*=;
		using D3DXVECTOR3::operator/=;

		// binary operators
		using D3DXVECTOR3::operator+;
		using D3DXVECTOR3::operator-;
		using D3DXVECTOR3::operator*;
		using D3DXVECTOR3::operator/;

		using D3DXVECTOR3::operator==;
		using D3DXVECTOR3::operator!=;

		// initializer Operators
		void operator()();
		void operator()(CONST FLOAT *);
		void operator()(CONST D3DVECTOR&);
		void operator()(CONST D3DXFLOAT16 *);
		void operator()(FLOAT, FLOAT, FLOAT);

		// Member Selection
		const Vector3* operator->() const noexcept;
		// Member Selection
		Vector3* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Vector3 &operator ++();
		Vector3 &operator --();
		Vector3 operator ++(int);
		Vector3 operator --(int);

		// Assignment Operators
		Vector3 &operator =(const Vector3 &);
		Vector3 &operator =(const float);
	};

	//==========================================================================
	//
	// class  : Vector4
	//
	//==========================================================================
	class Vector4 : public D3DXVECTOR4
	{
	public:
		Vector4();
		~Vector4();

		// constructor
		using D3DXVECTOR4::D3DXVECTOR4;

		// casting
		using D3DXVECTOR4::operator const FLOAT *;
		using D3DXVECTOR4::operator FLOAT *;

		// assignment operators
		using D3DXVECTOR4::operator+=;
		using D3DXVECTOR4::operator-=;
		using D3DXVECTOR4::operator*=;
		using D3DXVECTOR4::operator/=;

		// binary operators
		using D3DXVECTOR4::operator+;
		using D3DXVECTOR4::operator-;
		using D3DXVECTOR4::operator*;
		using D3DXVECTOR4::operator/;

		using D3DXVECTOR4::operator==;
		using D3DXVECTOR4::operator!=;

		// initializer Operators
		void operator()();
		void operator()(CONST FLOAT*);
		void operator()(CONST D3DXFLOAT16*);
		void operator()(CONST D3DVECTOR&, FLOAT);
		void operator()(FLOAT, FLOAT, FLOAT, FLOAT);

		// Member Selection
		const Vector4* operator->() const noexcept;
		// Member Selection
		Vector4* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Vector4 &operator ++();
		Vector4 &operator --();
		Vector4 operator ++(int);
		Vector4 operator --(int);

		// Assignment Operators
		Vector4 &operator =(const Vector4 &);
		Vector4 &operator =(const float);
	};

	//==========================================================================
	//
	// class  : Matrix
	//
	//==========================================================================
	class Matrix : public D3DXMATRIX
	{
	public:
		Matrix();
		~Matrix();

		// constructor
		using D3DXMATRIX::D3DXMATRIX;

		// access grants
		using D3DXMATRIX::operator();

		// casting operators
		using D3DXMATRIX::operator const FLOAT *;
		using D3DXMATRIX::operator FLOAT *;

		// assignment operators
		using D3DXMATRIX::operator+=;
		using D3DXMATRIX::operator-=;
		using D3DXMATRIX::operator*=;
		using D3DXMATRIX::operator/=;

		// binary operators
		using D3DXMATRIX::operator+;
		using D3DXMATRIX::operator-;
		using D3DXMATRIX::operator*;
		using D3DXMATRIX::operator/;

		using D3DXMATRIX::operator==;
		using D3DXMATRIX::operator!=;

		// initializer Operators
		void operator()();
		void operator()(CONST FLOAT *);
		void operator()(CONST D3DMATRIX&);
		void operator()(CONST D3DXFLOAT16 *);
		void operator()(FLOAT, FLOAT, FLOAT, FLOAT,
			FLOAT, FLOAT, FLOAT, FLOAT,
			FLOAT, FLOAT, FLOAT, FLOAT,
			FLOAT, FLOAT, FLOAT, FLOAT);

		// Member Selection
		const Matrix* operator->() const noexcept;
		// Member Selection
		Matrix* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Matrix &operator ++();
		Matrix &operator --();
		Matrix operator ++(int);
		Matrix operator --(int);

		// Assignment Operators
		Matrix &operator =(const Matrix &);
		Matrix &operator =(const float);
	};

	//==========================================================================
	//
	// class  : Quaternion
	//
	//==========================================================================
	class Quaternion : public D3DXQUATERNION
	{
	public:
		Quaternion();
		~Quaternion();

		// constructor
		using D3DXQUATERNION::D3DXQUATERNION;

		// casting
		using D3DXQUATERNION::operator const FLOAT *;
		using D3DXQUATERNION::operator FLOAT *;

		// assignment operators
		using D3DXQUATERNION::operator+=;
		using D3DXQUATERNION::operator-=;
		using D3DXQUATERNION::operator*=;
		using D3DXQUATERNION::operator/=;

		// binary operators
		using D3DXQUATERNION::operator+;
		using D3DXQUATERNION::operator-;
		using D3DXQUATERNION::operator*;
		using D3DXQUATERNION::operator/;

		using D3DXQUATERNION::operator==;
		using D3DXQUATERNION::operator!=;

		// initializer Operators
		void operator()();
		void operator()(CONST FLOAT *);
		void operator()(CONST D3DXFLOAT16 *);
		void operator()(FLOAT, FLOAT, FLOAT, FLOAT);

		// Member Selection
		const Quaternion* operator->() const noexcept;
		// Member Selection
		Quaternion* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Quaternion &operator ++();
		Quaternion &operator --();
		Quaternion operator ++(int);
		Quaternion operator --(int);

		// Assignment Operators
		Quaternion &operator =(const Quaternion &);
		Quaternion &operator =(const float);
	};

	//==========================================================================
	//
	// class  : Planes
	//
	//==========================================================================
	class Plane : public D3DXPLANE
	{
	public:
		Plane();
		~Plane();

		// constructor
		using D3DXPLANE::D3DXPLANE;

		// casting
		using D3DXPLANE::operator const FLOAT *;
		using D3DXPLANE::operator FLOAT *;

		// assignment operators
		using D3DXPLANE::operator*=;
		using D3DXPLANE::operator/=;

		// binary operators
		using D3DXPLANE::operator+;
		using D3DXPLANE::operator-;
		using D3DXPLANE::operator*;
		using D3DXPLANE::operator/;

		using D3DXPLANE::operator==;
		using D3DXPLANE::operator!=;

		// initializer Operators
		void operator()();
		void operator()(CONST FLOAT*);
		void operator()(CONST D3DXFLOAT16*);
		void operator()(FLOAT, FLOAT, FLOAT, FLOAT);

		// Member Selection
		const Plane* operator->() const noexcept;
		// Member Selection
		Plane* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Plane &operator ++();
		Plane &operator --();
		Plane operator ++(int);
		Plane operator --(int);

		// Assignment Operators
		Plane &operator =(const Plane &);
		Plane &operator =(const float);
	};

	//==========================================================================
	//
	// class  : Colors
	//
	//==========================================================================
	class Color : public D3DXCOLOR
	{
	public:
		Color();
		~Color();

		// constructor
		using D3DXCOLOR::D3DXCOLOR;

		// casting
		using D3DXCOLOR::operator const D3DCOLORVALUE &;
		using D3DXCOLOR::operator const D3DCOLORVALUE *;
		using D3DXCOLOR::operator const FLOAT *;
		using D3DXCOLOR::operator D3DCOLORVALUE &;
		using D3DXCOLOR::operator D3DCOLORVALUE *;
		using D3DXCOLOR::operator DWORD;
		using D3DXCOLOR::operator FLOAT *;

		// assignment operators
		using D3DXCOLOR::operator+=;
		using D3DXCOLOR::operator-=;
		using D3DXCOLOR::operator*=;
		using D3DXCOLOR::operator/=;

		// binary operators
		using D3DXCOLOR::operator+;
		using D3DXCOLOR::operator-;
		using D3DXCOLOR::operator*;
		using D3DXCOLOR::operator/;

		using D3DXCOLOR::operator==;
		using D3DXCOLOR::operator!=;

		// initializer Operators
		void operator()();
		void operator()(DWORD argb);
		void operator()(CONST FLOAT *);
		void operator()(CONST D3DXFLOAT16 *);
		void operator()(CONST D3DCOLORVALUE&);
		void operator()(FLOAT, FLOAT, FLOAT, FLOAT);

		// Member Selection
		const Color* operator->() const noexcept;
		// Member Selection
		Color* operator->() noexcept;

		// Prefix Increment / Decremrnt
		Color &operator ++();
		Color &operator --();
		Color operator ++(int);
		Color operator --(int);

		// Assignment Operators
		Color &operator =(const Color &);
		Color &operator =(const float);
	};
}
_MSLIB_END