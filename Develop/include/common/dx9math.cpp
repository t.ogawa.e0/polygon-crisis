//==========================================================================
// dx9math [dx9math.cpp]
// Content: math types and functions
// author: tatsuya ogawa
//==========================================================================
#include "dx9math.h"

_MSLIB_BEGIN
namespace math
{
	Vector2::Vector2()
	{
		x = y = 0.0f;
	}
	Vector2::~Vector2() {}
	void Vector2::operator()()
	{
		x = y = 0.0f;
	}
	void Vector2::operator()(const FLOAT *f)
	{
		*this = Vector2(f);
	}
	void Vector2::operator()(const D3DXFLOAT16 *m)
	{
		*this = Vector2(m);
	}
	void Vector2::operator()(FLOAT fx, FLOAT fy)
	{
		*this = Vector2(fx, fy);
	}
	const Vector2 * Vector2::operator->() const noexcept
	{
		return this;
	}
	Vector2 * Vector2::operator->() noexcept
	{
		return this;
	}
	Vector2 & Vector2::operator++()
	{
		x++;
		y++;
		return *this;
	}
	Vector2 & Vector2::operator--()
	{
		x--;
		y--;
		return *this;
	}
	Vector2 Vector2::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Vector2 Vector2::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Vector2 & Vector2::operator=(const Vector2 &v)
	{
		x = v.x;
		y = v.y;
		return *this;
	}
	Vector2 & Vector2::operator=(const float f)
	{
		x = y = f;
		return *this;
	}
	Vector3::Vector3()
	{
		x = y = z = 0.0f;
	}
	Vector3::~Vector3() {}
	void Vector3::operator()()
	{
		x = y = z = 0.0f;
	}
	void Vector3::operator()(const FLOAT *f)
	{
		*this = Vector3(f);
	}
	void Vector3::operator()(const D3DVECTOR &v)
	{
		*this = Vector3(v);
	}
	void Vector3::operator()(const D3DXFLOAT16 *m)
	{
		*this = Vector3(m);
	}
	void Vector3::operator()(FLOAT fx, FLOAT fy, FLOAT fz)
	{
		*this = Vector3(fx, fy, fz);
	}
	const Vector3 * Vector3::operator->() const noexcept
	{
		return this;
	}
	Vector3 * Vector3::operator->() noexcept
	{
		return this;
	}
	Vector3 & Vector3::operator++()
	{
		x++;
		y++;
		z++;
		return *this;
	}
	Vector3 & Vector3::operator--()
	{
		x--;
		y--;
		z--;
		return *this;
	}
	Vector3 Vector3::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Vector3 Vector3::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Vector3 & Vector3::operator=(const Vector3 &v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}
	Vector3 & Vector3::operator=(const float f)
	{
		x = y = z = f;
		return *this;
	}
	Vector4::Vector4()
	{
		x = y = z = w = 0.0f;
	}
	Vector4::~Vector4() {}
	void Vector4::operator()()
	{
		x = y = z = w = 0.0f;
	}
	void Vector4::operator()(const FLOAT *f)
	{
		*this = Vector4(f);
	}
	void Vector4::operator()(const D3DXFLOAT16 *f)
	{
		*this = Vector4(f);
	}
	void Vector4::operator()(const D3DVECTOR & xyz, FLOAT fw)
	{
		*this = Vector4(xyz, fw);
	}
	void Vector4::operator()(FLOAT fx, FLOAT fy, FLOAT fz, FLOAT fw)
	{
		*this = Vector4(fx, fy, fz, fw);
	}
	const Vector4 * Vector4::operator->() const noexcept
	{
		return this;
	}
	Vector4 * Vector4::operator->() noexcept
	{
		return this;
	}
	Vector4 & Vector4::operator++()
	{
		x++;
		y++;
		z++;
		w++;
		return *this;
	}
	Vector4 & Vector4::operator--()
	{
		x--;
		y--;
		z--;
		w--;
		return *this;
	}
	Vector4 Vector4::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Vector4 Vector4::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Vector4 & Vector4::operator=(const Vector4 &v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;
		return *this;
	}
	Vector4 & Vector4::operator=(const float f)
	{
		x = y = z = w = f;
		return *this;
	}
	Matrix::Matrix()
	{
		D3DXMatrixIdentity(this);
	}
	Matrix::~Matrix() {}
	void Matrix::operator()()
	{
		D3DXMatrixIdentity(this);
	}
	void Matrix::operator()(const FLOAT *f)
	{
		*this = Matrix(f);
	}
	void Matrix::operator()(const D3DMATRIX &fm)
	{
		*this = Matrix(fm);
	}
	void Matrix::operator()(const D3DXFLOAT16 *f)
	{
		*this = Matrix(f);
	}
	void Matrix::operator()(FLOAT f11, FLOAT f12, FLOAT f13, FLOAT f14, FLOAT f21, FLOAT f22, FLOAT f23, FLOAT f24, FLOAT f31, FLOAT f32, FLOAT f33, FLOAT f34, FLOAT f41, FLOAT f42, FLOAT f43, FLOAT f44)
	{
		*this = Matrix(f11, f12, f13, f14, f21, f22, f23, f24, f31, f32, f33, f34, f41, f42, f43, f44);
	}
	const Matrix * Matrix::operator->() const noexcept
	{
		return this;
	}
	Matrix * Matrix::operator->() noexcept
	{
		return this;
	}
	Matrix & Matrix::operator++()
	{
		_11++; _12++; _13++; _14++;
		_21++; _22++; _23++; _24++;
		_31++; _32++; _33++; _34++;
		_41++; _42++; _43++; _44++;
		return *this;
	}
	Matrix & Matrix::operator--()
	{
		_11--; _12--; _13--; _14--;
		_21--; _22--; _23--; _24--;
		_31--; _32--; _33--; _34--;
		_41--; _42--; _43--; _44--;
		return *this;
	}
	Matrix Matrix::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Matrix Matrix::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Matrix & Matrix::operator=(const Matrix &fm)
	{
		_11 = fm._11; _12 = fm._12; _13 = fm._13; _14 = fm._14;
		_21 = fm._21; _22 = fm._22; _23 = fm._23; _24 = fm._24;
		_31 = fm._31; _32 = fm._32; _33 = fm._33; _34 = fm._34;
		_41 = fm._41; _42 = fm._42; _43 = fm._43; _44 = fm._44;
		return *this;
	}
	Matrix & Matrix::operator=(const float f)
	{
		_11 = f; _12 = f; _13 = f; _14 = f;
		_21 = f; _22 = f; _23 = f; _24 = f;
		_31 = f; _32 = f; _33 = f; _34 = f;
		_41 = f; _42 = f; _43 = f; _44 = f;
		return *this;
	}
	Quaternion::Quaternion()
	{
		D3DXQuaternionIdentity(this);
	}
	Quaternion::~Quaternion()
	{
	}
	void Quaternion::operator()()
	{
		D3DXQuaternionIdentity(this);
	}
	void Quaternion::operator()(const FLOAT *f)
	{
		*this = Quaternion(f);
	}
	void Quaternion::operator()(const D3DXFLOAT16 *f)
	{
		*this = Quaternion(f);
	}
	void Quaternion::operator()(FLOAT fx, FLOAT fy, FLOAT fz, FLOAT fw)
	{
		*this = Quaternion(fx, fy, fz, fw);
	}
	const Quaternion * Quaternion::operator->() const noexcept
	{
		return this;
	}
	Quaternion * Quaternion::operator->() noexcept
	{
		return this;
	}
	Quaternion & Quaternion::operator++()
	{
		x++;
		y++;
		z++;
		w++;
		return *this;
	}
	Quaternion & Quaternion::operator--()
	{
		x--;
		y--;
		z--;
		w--;
		return *this;
	}
	Quaternion Quaternion::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Quaternion Quaternion::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Quaternion & Quaternion::operator=(const Quaternion &q)
	{
		x = q.x;
		y = q.y;
		z = q.z;
		w = q.w;
		return *this;
	}
	Quaternion & Quaternion::operator=(const float f)
	{
		x = y = z = w = f;
		return *this;
	}
	Plane::Plane()
	{
		a = b = c = d = 0.0f;
	}
	Plane::~Plane()
	{
	}
	void Plane::operator()()
	{
		a = b = c = d = 0.0f;
	}
	void Plane::operator()(const FLOAT *f)
	{
		*this = Plane(f);
	}
	void Plane::operator()(const D3DXFLOAT16 *f)
	{
		*this = Plane(f);
	}
	void Plane::operator()(FLOAT fa, FLOAT fb, FLOAT fc, FLOAT fd)
	{
		*this = Plane(fa, fb, fc, fd);
	}
	const Plane * Plane::operator->() const noexcept
	{
		return this;
	}
	Plane * Plane::operator->() noexcept
	{
		return this;
	}
	Plane & Plane::operator++()
	{
		a++;
		b++;
		c++;
		d++;
		return *this;
	}
	Plane & Plane::operator--()
	{
		a--;
		b--;
		c--;
		d--;
		return *this;
	}
	Plane Plane::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Plane Plane::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Plane & Plane::operator=(const Plane &p)
	{
		a = p.a;
		b = p.b;
		c = p.c;
		d = p.d;
		return *this;
	}
	Plane & Plane::operator=(const float f)
	{
		a = f;
		b = f;
		c = f;
		d = f;
		return *this;
	}
	Color::Color()
	{
		r = g = b = a = 0.0f;
	}
	Color::~Color()
	{
	}
	void Color::operator()()
	{
		r = g = b = a = 0.0f;
	}
	void Color::operator()(DWORD argb)
	{
		*this = Color(argb);
	}
	void Color::operator()(const FLOAT *f)
	{
		*this = Color(f);
	}
	void Color::operator()(const D3DXFLOAT16 *f)
	{
		*this = Color(f);
	}
	void Color::operator()(const D3DCOLORVALUE &c)
	{
		*this = Color(c);
	}
	void Color::operator()(FLOAT fr, FLOAT fg, FLOAT fb, FLOAT fa)
	{
		*this = Color(fr, fg, fb, fa);
	}
	const Color * Color::operator->() const noexcept
	{
		return this;
	}
	Color * Color::operator->() noexcept
	{
		return this;
	}
	Color & Color::operator++()
	{
		r++;
		g++;
		b++;
		a++;
		return *this;
	}
	Color & Color::operator--()
	{
		r--;
		g--;
		b--;
		a--;
		return *this;
	}
	Color Color::operator++(int)
	{
		auto out = *this;
		++*this;
		return out;
	}
	Color Color::operator--(int)
	{
		auto out = *this;
		--*this;
		return out;
	}
	Color & Color::operator=(const Color &c)
	{
		r = c.r;
		g = c.g;
		b = c.b;
		a = c.a;
		return *this;
	}
	Color & Color::operator=(const float f)
	{
		r = f;
		g = f;
		b = f;
		a = f;
		return *this;
	}
}
_MSLIB_END