//==========================================================================
// Renderer [Renderer.h]
// author: tatsuya ogawa
//==========================================================================
#include "Renderer.h"
#include "Object.h"
#include "MsEffekseer.h"

_MSLIB_BEGIN

namespace renderer
{
    std::list<Renderer*> Renderer::m_UpdateList; // 更新対象リスト
    std::list<Renderer*> Renderer::m_DrawList; // 描画対象リスト
    RendererID Renderer::m_renderer_id = RendererID::Begin;
    bool Renderer::m_DebugActivityAll = false;

    Renderer::Renderer(RendererID id)
    {
        renderer_id = id;
        m_DrawList.push_back(this);
        m_UpdateList.push_back(this);
        Sort();
    }
    Renderer::~Renderer()
    {
        auto itr1 = std::find(m_DrawList.begin(), m_DrawList.end(), this);
        if (itr1 != m_DrawList.end())
            m_DrawList.erase(itr1);

        auto itr2 = std::find(m_UpdateList.begin(), m_UpdateList.end(), this);
        if (itr2 != m_UpdateList.end())
            m_UpdateList.erase(itr2);
    }

    /**
    @brief ソート処理
    */
    void Renderer::Sort()
    {
        // ソート処理
        m_DrawList.sort([&](Renderer * a, Renderer * b)
        {
            return a->renderer_id < b->renderer_id ? true : false;
        });
    }
    void Renderer::UpdateAll()
    {
        for(auto &itr : m_UpdateList)
        {
            itr->Update();
        }
        MsEffekseer::EffekseerLoader::UpdateAll();
    }
    void Renderer::DrawAll(LPDIRECT3DDEVICE9 device)
    {
        for (auto &itr : m_DrawList)
        {
            if (!itr->m_Activity) continue;

            ChangeRender(device, itr->renderer_id);
            itr->Draw(device);
        }

        // 後処理
        ChangeRender(device, RendererID::Effect);
        MsEffekseer::EffekseerLoader::DrawAll();
        ChangeRender(device, RendererID::End);
#if defined(_MSLIB_DEBUG)
        device->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
        for (auto &itr : m_DrawList)
        {
            // 無効時には無視
            if (!itr->m_DebugActivity)continue;

            itr->DebugDraw(device);
        }
        device->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
#endif
    }
    void Renderer::AllDebugActivity(bool activity)
    {
        for (auto & itr : m_DrawList)
        {
            itr->m_DebugActivity = activity;
        }
		m_DebugActivityAll = activity;
    }
    bool Renderer::GetAllDebugActivity()
    {
        return m_DebugActivityAll;
    }
    void Renderer::ChangeRender(LPDIRECT3DDEVICE9 device, RendererID id)
    {
        if (m_renderer_id != id)
        {
            switch (m_renderer_id)
            {
            case RendererID::Begin:
                break;
            case RendererID::Grid:
                break;
            case RendererID::Field:
                break;
            case RendererID::Cube:
                break;
            case RendererID::Mesh:
                SetRenderALPHAREF_END(device);
                break;
            case RendererID::Shadow:
                SetRenderADD(device);
                break;
            case RendererID::Sphere:
                SetRenderALPHAREF_END(device);
                device->SetRenderState(D3DRS_WRAP0, 0);
                device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // カリングします
                break;
            case RendererID::Xmodel:
                break;
            case RendererID::Billboard:
                // パンチ抜き終了
                SetRenderALPHAREF_END(device);
                break;
            case RendererID::Effect:
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
                device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // カリングします
                break;
            case RendererID::Text:
                break;
            case RendererID::End:
                break;
            default:
                break;
            }
            switch (id)
            {
            case RendererID::Begin:
                break;
            case RendererID::Grid:
                // ライト設定
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
                break;
            case RendererID::Field:
                // ライト設定
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
                break;
            case RendererID::Cube:
                break;
            case RendererID::Mesh:
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE); // ライト設定
                // アルファ画像によるブレンド
                SetRenderALPHAREF_START(device, 100);                
                break;
            case RendererID::Shadow:
                // 減算処理
                SetRenderREVSUBTRACT(device);
                break;
            case RendererID::Sphere:
                // パンチ抜き
                SetRenderALPHAREF_START(device, 100);
                device->SetRenderState(D3DRS_WRAP0, D3DWRAPCOORD_0);
                device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW); // 反時計回りの面を消去
                break;
            case RendererID::Xmodel:
                // ライト設定
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
                break;
            case RendererID::Billboard:
                // ライト設定
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

                // パンチ抜き
                SetRenderALPHAREF_START(device, 100);
                break;
            case RendererID::Effect:
                device->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
                device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW); // カリングしません
                break;
            case RendererID::Text:
                break;
            case RendererID::End:
                break;
            default:
                break;
            }
            m_renderer_id = id;
        }
    }
}

_MSLIB_END