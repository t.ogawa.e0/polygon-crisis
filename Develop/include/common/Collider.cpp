//==========================================================================
// Collider [Collider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Collider.h"
#include "DefaultSystem.h"

_MSLIB_BEGIN

namespace collider
{
    constexpr const char* __connector__ = ">>"; // 頂点数
    constexpr const int __number_of_vertices__ = 2; // 頂点数
    struct VectorDrawData
    {
        D3DXVECTOR3 pos = D3DXVECTOR3(0, 0, 0); // 座標変換が必要
        D3DCOLOR color = D3DCOLOR_RGBA(255, 255, 255, 255); // ポリゴンの色
    };

    std::list<Collider*> Collider::m_collider;

    Collider::Collider()
    {
        m_collider.push_back(this);
        SetComponentName("Collider");
        m_collision = false;
        m_update = true;
    }

    Collider::~Collider()
    {
        auto itr = std::find(m_collider.begin(), m_collider.end(), this);
        if (itr != m_collider.end())
            m_collider.erase(itr);

        ReleaseCollider();
    }

    void Collider::UpdateAll()
    {
        // flagの初期化
        for (auto &itr : m_collider)
        {
            itr->m_collision = false;
            for (auto & target : itr->m_target_collider)
            {
                target->m_collision = false;
            }
        }

        // 処理
        for (auto &itr : m_collider)
        {
            // 無効時には無視
            if (!itr->m_Activity || !itr->m_update)continue;

            itr->CreateWorldMatrix();
            itr->Update();
        }
    }

    void Collider::DrawAll(LPDIRECT3DDEVICE9 device)
    {
#if defined(_MSLIB_DEBUG)

        device->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
        for (auto &itr : m_collider)
        {
            // 無効時には無視
            if (!itr->m_Activity)continue;

            itr->RelationshipDiagram(device);
            itr->Draw(device);
            itr->UnlockUpdate();
        }
        device->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
#else
        device;
#endif
    }

    bool Collider::Trigger()
    {
        return m_collision;
    }

    void Collider::SetTrigger(bool label)
    {
        m_collision = label;
    }

    void Collider::AddCollider(Collider * target)
    {
        // 既に登録済みかをチェック
        auto itr = std::find(m_target_collider.begin(), m_target_collider.end(), target);
        if (itr != m_target_collider.end())return;

        m_target_collider.push_back(target);
        target->m_access_collider.push_back(this);
    }

    void Collider::ReleaseCollider(Collider * target)
    {
        if (target == nullptr)return;

        ReleaseTargetCollider(target);
        target->ReleaseAccessCollider(this);
    }

    void Collider::ReleaseCollider()
    {
        for (auto & itr : m_access_collider)
        {
            itr->ReleaseTargetCollider(this);
        }
        for (auto & itr : m_target_collider)
        {
            itr->ReleaseAccessCollider(this);
        }
        m_target_collider.clear();
        m_access_collider.clear();
    }

    bool Collider::GetUpdateFlag()
    {
        return m_update;
    }

    void Collider::CreateWorldMatrix()
    {
        for (auto & itr : m_transform)
        {
            itr->CreateWorldMatrix();
        }
    }

    void Collider::ReleaseTargetCollider(Collider * target)
    {
        // 対象を破棄
        auto itr = std::find(m_target_collider.begin(), m_target_collider.end(), target);
        if (itr == m_target_collider.end())return;
        m_target_collider.erase(itr);
    }

    void Collider::ReleaseAccessCollider(Collider * target)
    {
        // 対象を破棄
        auto itr = std::find(m_access_collider.begin(), m_access_collider.end(), target);
        if (itr == m_access_collider.end())return;
        m_access_collider.erase(itr);
    }

    void Collider::RelationshipDiagram(LPDIRECT3DDEVICE9 device)
    {
        auto targetA = GetWarrantyParent<transform::Transform>();
        if (targetA == nullptr)return;

        int size = __number_of_vertices__ + 1;
        auto *pVector = new VectorDrawData[size];

        auto matA = targetA->GetWorldMatrix();
        pVector[0].pos = pVector[__number_of_vertices__].pos = D3DXVECTOR3(matA->_41, matA->_42, matA->_43);

        for (auto & itr : m_target_collider)
        {
            auto targetB = itr->GetWarrantyParent<transform::Transform>();
            if (targetB == nullptr)continue;
            auto matB = targetB->GetWorldMatrix();
            pVector[1].pos = D3DXVECTOR3(matB->_41, matB->_42, matB->_43);

            D3DXMATRIX mat;

            device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);
            device->SetTransform(D3DTS_WORLD, D3DXMatrixIdentity(&mat));
            device->SetTexture(0, nullptr);
            device->DrawPrimitiveUP(D3DPT_LINESTRIP, __number_of_vertices__, pVector, sizeof(VectorDrawData));
        }

        delete[] pVector;
    }

    void Collider::Init()
    {
    }

    void Collider::Update()
    {
    }

    void Collider::Draw(LPDIRECT3DDEVICE9 device)
    {
        device;
    }

    void Collider::UnlockUpdate()
    {
        m_update = true;
    }

    void Collider::LockUpdate()
    {
        m_update = false;
    }

    float Distance(const transform::Transform * t1, const transform::Transform * t2)
    {
        auto * MatrixA = t1->GetWorldMatrix();
        auto * MatrixB = t2->GetWorldMatrix();

        return Distance(D3DXVECTOR3(MatrixA->_41, MatrixA->_42, MatrixA->_43), D3DXVECTOR3(MatrixB->_41, MatrixB->_42, MatrixB->_43));
    }

    float Distance(const D3DXVECTOR3 & t1, const D3DXVECTOR3 & t2)
    {
        auto t3 = t2 - t1;
        return D3DXVec3LengthSq(&t3);
    }

    float Distance(const transform::Transform * t1, const D3DXVECTOR3 & t2)
    {
        auto * MatrixA = t1->GetWorldMatrix();

        return Distance(D3DXVECTOR3(MatrixA->_41, MatrixA->_42, MatrixA->_43), t2);
    }

    float Distance(const D3DXVECTOR3 & t1, const transform::Transform * t2)
    {
        auto * MatrixB = t2->GetWorldMatrix();

        return Distance(t1, D3DXVECTOR3(MatrixB->_41, MatrixB->_42, MatrixB->_43));
    }

    bool Sphere(const transform::Transform * t1, const transform::Transform * t2, float scale)
    {
        return Distance(t1, t2) < scale ? true : false;
    }
}
_MSLIB_END