//==========================================================================
// 参照カウンタ [IReference.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <atomic> // c++ アトミック
#include "mslib.hpp"

_MSLIB_BEGIN

namespace ireference
{
    //==========================================================================
    //
    // class  : IReference
    // Content: 参照カウンタクラス
    //
    //==========================================================================
    class IReference
    {
    public:
        IReference();
        virtual ~IReference();
        /**
        @brief	参照カウンタを加算する。
        @return	加算後の参照カウンタ
        */
        int AddRef();

        /**
        @brief	参照カウンタを取得する。
        @return	参照カウンタ
        */
        int GetRef();

        /**
        @brief	参照カウンタを減算する。0になった時、消去可能フラグが立つ
        @return	消去可能フラグ
        */
        bool Release();
    private:
        mutable std::atomic<int> m_reference; // カウンタ
    };

    //==========================================================================
    //
    // class  : ReferenceData
    // Content: 参照カウンタ付きデータ構造
    //
    //==========================================================================
    template <class _Ty>
    class ReferenceData : public IReference
    {
    public:
        using type = _Ty;
    public:
        ReferenceData() {}
        virtual ~ReferenceData() {}
    public:
        _Ty asset; // アセット
    };

    //==========================================================================
    //
    // class  : ReferenceOperator
    // Content: 参照中継
    // Inheritance : _Ty must inherit the ReferenceData class
    //
    //==========================================================================
    template <typename _Ty>
    class ReferenceOperator
    {
    public:
        using type = _Ty;
        static_assert(std::is_base_of <ReferenceData<_Ty::type>, _Ty > ::value, "ReferenceOperator<> : _Ty is not inherited from ReferenceData Class");
    public:
        ReferenceOperator() {
            m_data = nullptr;
        }
        ReferenceOperator(_Ty &data) {
            m_data = nullptr;
            *this = data;
        }
        ReferenceOperator(_Ty *data) {
            m_data = nullptr;
            *this = data;
        }
        ReferenceOperator(nullptr_t) {
            m_data = nullptr;
            *this = nullptr;
        }
        virtual ~ReferenceOperator() {
        }
        virtual ReferenceOperator & operator=(_Ty &data) final {
            auto newdata = &data;
            newdata->AddRef();
            ReleaseCall();
            m_data = newdata;
            return *this;
        }
        virtual ReferenceOperator & operator=(_Ty *data) final {
            if (data == nullptr)return *this;
            auto newdata = data;
            newdata->AddRef();
            ReleaseCall();
            m_data = newdata;
            return *this;
        }
        virtual ReferenceOperator & operator=(nullptr_t) final {
            ReleaseCall();
            m_data = nullptr;
            return *this;
        }
        virtual ReferenceOperator & operator=(const ReferenceOperator &data) final {
            *this = data.m_data;
            return *this;
        }
        virtual ReferenceOperator & operator=(const ReferenceOperator *data) final {
            *this = data->m_data;
            return *this;
        }
        auto & operator->() {
            return m_data->asset;
        }
        auto & operator&() {
            return m_data->asset;
        }
        virtual bool Existence() final {
            return m_data != nullptr ? true : false;
        }
        virtual void Release() = 0;
    private:
        void ReleaseCall() {
            if (m_data != nullptr) {
                if (m_data->GetRef() != 0) {
                    m_data->Release();
                    m_data = nullptr;
                }
            }
        }
    protected:
        _Ty * m_data;
    };
}

_MSLIB_END