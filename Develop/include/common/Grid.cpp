//==========================================================================
// グリッド[Grid.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Grid.h"

_MSLIB_BEGIN

using renderer::RendererID;

Grid::Grid() : Renderer(RendererID::Grid)
{
    SetComponentName("Grid");
    m_pos = nullptr;
    m_num = 0;
    m_scale = 0;
}

Grid::~Grid()
{
    Release();
}

void Grid::Init(int scale)
{
	m_scale = scale; // サイズの記録
	m_num = 4; // 十字を作るための線
	float X = (float)m_scale; // サイズの記録
	float Z = (float)m_scale; // サイズの記録

	// サイズ分の外枠の線を追加
	for (int i = 0; i < m_scale; i++)
	{
		m_num += 8; 
	}

    if (m_pos != nullptr)
    {
        delete[]m_pos;
        m_pos = nullptr;
    }

    m_pos = new VERTEX_2[m_num];

	int nNumLine = (int)(m_num / 2);

	for (int i = 0; i < nNumLine; i += 2, X--, Z--)
	{
		m_pos[i].pos = D3DXVECTOR3(1.0f * X, 0.0f, (float)m_scale); // x座標に線
		m_pos[i + 1].pos = D3DXVECTOR3(1.0f * X, 0.0f, -(float)m_scale); // x座標に線
		m_pos[nNumLine + i].pos = D3DXVECTOR3((float)m_scale, 0.0f, 1.0f * Z); // z座標に線
		m_pos[nNumLine + i + 1].pos = D3DXVECTOR3(-(float)m_scale, 0.0f, 1.0f * Z); // z座標に線

		m_pos[i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		m_pos[i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		m_pos[nNumLine + i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		m_pos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);

		if (i == (int)((float)m_scale * 2))
		{
			m_pos[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			m_pos[i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			m_pos[nNumLine + i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			m_pos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
		}
	}
}

void Grid::Release(void)
{
    if (m_pos != nullptr)
    {
        delete[]m_pos;
        m_pos = nullptr;
    }
}

void Grid::Update()
{
}

void Grid::Draw(LPDIRECT3DDEVICE9 device)
{
	D3DXMATRIX m_MtxWorld; // ワールド行列

	D3DXMatrixIdentity(&m_MtxWorld);

	// 各種行列の設定
    device->SetTransform(D3DTS_WORLD, &m_MtxWorld);

	// FVFの設定
    device->SetFVF(FVF_VERTEX_2);

    device->SetTexture(0, nullptr);
    device->DrawPrimitiveUP(D3DPT_LINELIST, (int)(m_num / 2), m_pos, sizeof(VERTEX_2));
}

void Grid::DebugDraw(LPDIRECT3DDEVICE9 device)
{
    device;
}

_MSLIB_END