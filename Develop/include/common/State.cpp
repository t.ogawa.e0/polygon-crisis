//==========================================================================
// State パターン [State.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "State.h"

_MSLIB_BEGIN

namespace state
{
    State::State()
    {
        SetComponentName("State");
        m_state = nullptr;
    }
    State::~State()
    {
    }
    StateBasic * State::GetState()
    {
        return m_state;
    }
    void State::Update()
    {
        if (m_state == nullptr)return;
        m_state->Update(this);
    }
    bool State::Trigger()
    {
        return m_state != nullptr ? true : false;
    }
    void State::ReleaseState()
    {
        GetParent()->DestroyComponent(m_state);
        m_state = nullptr;
    }
    bool State::ReleaseState(StateBasic * this_)
    {
        // 消す対象と同じではない
        if (this_ != m_state) return false;

        ReleaseState();

        return m_state == nullptr ? true : false;
    }
    StateBasic::StateBasic()
    {
        SetComponentName("StateBasic");
    }
    StateBasic::~StateBasic()
    {
    }
}
_MSLIB_END