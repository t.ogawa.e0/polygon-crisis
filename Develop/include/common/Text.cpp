//==========================================================================
// テキスト表示[Text.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Text.h"

_MSLIB_BEGIN

Text::Text(LPDIRECT3DDEVICE9 pDevice)
{
    SetComponentName("Text");
    m_Device = pDevice;
    m_color = 255;
    m_pos = 0;
    m_font = nullptr;
    m_fontheight = 0;
    m_fontheight_master = 20;
    m_Lock = false;
}

Text::~Text()
{
    Release();
}

//==========================================================================
/**
@brief 初期化 フォントの種類はデフォルトで Terminal になってます。
@param w [in] 画面の横幅
@param h [in] 画面の高さ
*/
void Text::Init(int w, int h)
{
    Init(w, h, "Terminal");
}

//==========================================================================
/**
@brief 初期化 フォントの種類はデフォルトで Terminal になってます。
@param w [in] 画面の横幅
@param h [in] 画面の高さ
@param font [in] フォント
*/
void Text::Init(int w, int h, const char * font)
{
	m_color = intColor(255, 255, 255, 255);
	m_pos = intTexvec(m_pos.x, m_pos.y, w, h);
	m_fontheight = 0;
	m_Lock = false;

	D3DXCreateFont
	(
        m_Device,				    // デバイス
		m_fontheight_master,	    // 文字の高さ
		0,							// 文字の幅
		FW_REGULAR,					// フォントの太さ
		0,							// MIPMAPのレベル
		FALSE,						// イタリックか？
		SHIFTJIS_CHARSET,			// 文字セット
		OUT_DEFAULT_PRECIS,			// 出力精度
		DEFAULT_QUALITY,			// 出力品質
		FIXED_PITCH | FF_SCRIPT,	// フォントピッチとファミリ
		TEXT(font),					// フォント名
		&m_font				        // Direct3Dフォントへのポインタへのアドレス
	);
}

//==========================================================================
/**
@brief 解放
*/
void Text::Release(void)
{
    if (m_font != nullptr)
    {
        m_font->Release();
        m_font = nullptr;
    }
}

//==========================================================================
/**
@brief 色
@param r [in] 色
@param g [in] 色
@param b [in] 色
@param a [in] 色
*/
void Text::color(int r, int g, int b, int a)
{
    m_color = intColor(r, g, b, a);
}

//==========================================================================
/**
@brief 色
@param input [in] {r,g,b,a}
*/
void Text::color(const intColor & input)
{
    m_color = input;
}

//==========================================================================
/**
@brief 座標
@param x [in] X座標
@param y [in] Y座標
*/
void Text::pos(int x, int y)
{
    m_pos = intTexvec(x, y, m_pos.w, m_pos.h);
}

//==========================================================================
/**
@brief テキスト表示
@param input [in] 表示内容
*/
void Text::text(const char * input, ...)
{
	if (m_Lock)
	{
		va_list argp;
		RECT rect = { m_pos.x, m_pos.y + m_fontheight, m_pos.w, m_pos.h };
		char strBuf[1024] = { 0 };

        va_start(argp, input);
        vsprintf_s(strBuf, 1024, input, argp);
        va_end(argp);
        m_font->DrawText(nullptr, strBuf, -1, &rect, DT_LEFT, m_color.get());
		m_fontheight += m_fontheight_master;
	}
}

//==========================================================================
/**
@brief テキストの始まりに必ずセットしよう。
*/
void Text::textnew(void)
{
    m_Lock = true; m_fontheight = 0;
}

//==========================================================================
/**
@brief テキストの終わりに必ずセットしよう。
*/
void Text::textend(void)
{
    m_Lock = false;
}

//==========================================================================
/**
@brief 文字のサイズ
@param input [in] フォントサイズ
*/
void Text::size(int input)
{
    m_fontheight_master = input;
}

_MSLIB_END