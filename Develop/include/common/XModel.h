//==========================================================================
// Xモデル[XModel.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <vector>

#include "mslib.hpp"
#include "Component.h"
#include "Texture.h"
#include "vector_wrapper.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace xmodel
{
    class XModelLoader;

    //==========================================================================
    //
    // class  : XModelData
    // Content: Xモデルデータ
    //
    //==========================================================================
    class XModelData : public ireference::ReferenceData<LPD3DXMESH>
    {
    public:
        XModelData();
        ~XModelData();
    public:
        DWORD num; // マテリアル数
        D3DVERTEXELEMENT9 elements[MAX_FVF_DECL_SIZE]; // 頂点構造体
        std::vector<D3DMATERIAL9> MatD3D; // マトリクスのライティング
        std::vector<texture::TextureReference> texture; // テクスチャ
        int64_t createID; // 生成ID
        XModelLoader * LoaderPtr; // ローダーのポインタ
        std::string tag; // タグ
    };

    //==========================================================================
    //
    // class  : XModelReference
    // Content: 参照用
    //
    //==========================================================================
    class XModelReference : public ireference::ReferenceOperator<XModelData>
    {
    public:
        XModelReference();
        ~XModelReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        const DWORD & NumMesh();
        const std::vector<D3DMATERIAL9> & Material();
        std::vector<texture::TextureReference> & Texture();
        std::vector<D3DMATERIAL9> & MatD3D();
        const std::string & Tag();
    };

    //==========================================================================
    //
    // class  : XModelLoader
    // Content: Xモデル読み込み破棄クラス
    //
    //==========================================================================
    class XModelLoader : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        XModelLoader(const XModelLoader &) = delete;
        XModelLoader &operator=(const XModelLoader &) = delete;
        XModelLoader &operator=(XModelLoader&&) = delete;
    public:
        XModelLoader();
        XModelLoader(LPDIRECT3DDEVICE9 device, HWND hWnd, texture::TextureLoader * TextureLoader);
        ~XModelLoader();

        /**
        @brief Xモデルを読み込む。
        @param path [in] 読み込みパス
        @return Xモデルのポインタ
        */
        XModelReference Load(const std::string & path);

        /**
        @brief Xモデルを破棄する。
        @param data [in] データ
        */
        void Unload(XModelData * data);
    private:
        /**
        @brief Xモデルの読み込み機能の本体
        @param path [in] 読み込みパス
        @param Out [out] 受け取り先
        */
        void Load(const std::string & path, XModelData * Out);

        /**
        @brief 読み込み
        @param path [in] 読み込みパス
        @param Out [out] 受け取り先
        @param pAdjacency [in] ID3DXBuffer*
        @param pMaterialBuffer [in] ID3DXBuffer*
        @param pDevice [in] デバイス
        @return Component Object Model defines, and macros
        */
        HRESULT read(const std::string & path, XModelData * Out, LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer);

        /**
        @brief 最適化
        @param Out [out] 受け取り先
        @param pAdjacency [in] ID3DXBuffer*
        @return Component Object Model defines, and macros
        */
        HRESULT optimisation(XModelData * Out, LPD3DXBUFFER pAdjacency);

        /**
        @brief 頂点の宣言
        @param Out [out] 受け取り先
        @param pElements [in] D3DVERTEXELEMENT9
        @return Component Object Model defines, and macros
        */
        HRESULT declaration(XModelData * Out, D3DVERTEXELEMENT9 *pElements);

        /**
        @brief 複製
        @param Out [out] 受け取り先
        @param pElements [in] D3DVERTEXELEMENT9
        @param pTempMesh [in] ID3DXMesh
        @param pDevice [in] デバイス
        @return Component Object Model defines, and macros
        */
        HRESULT replication(XModelData * Out, D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh);

        /**
        @brief ファイルパスの生成
        @param In [in] 読み込み対象
        @param Out [out] 読み込み対象へのファイルパス受け取り
        */
        void CreateTexturePass(const std::string & In, std::string & Out);

        /**
        @brief テクスチャの読み込み
        @param filepass [in] 読み込み対象へのファイルパス
        @param Out [out] 受け取り先
        @param Input [in] マテリアル
        @return ANSI (Multi-byte Character) types
        */
        void LoadTexture(XModelData * Out, const std::string & filepass, const LPD3DXMATERIAL Input);

        /**
        @brief エラーメッセージの表示
        @param text [in] メッセージ内容
        @param path [in] エラーが出たもの
        */
        void ErrorMessage(const std::string & text, const std::string & path);
    private:
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
        int64_t createIDCount; // カウンタ
        std::unordered_map<std::string, XModelData> m_data; // データ
        texture::TextureLoader * m_TextureLoader; // テクスチャ読み込み機能へのポインタ
    };

    //==========================================================================
    //
    // class  : SetXModel
    // Content: Xモデル登録クラス
    //
    //==========================================================================
    class SetXModel
    {
    public:
        SetXModel();
        virtual ~SetXModel();

        /**
        @brief モデルデータの登録
        @param data [in] モデルデータ
        */
        void SetXModelData(const XModelReference & data);

        /**
        @brief モデルデータの取得
        */
        XModelReference & GetXModelData();
    protected:
        XModelReference m_XModelData; // モデルデータ
    };
}

_MSLIB_END