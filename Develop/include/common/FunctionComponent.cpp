//==========================================================================
// FunctionComponent [FunctionComponent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "FunctionComponent.h"

_MSLIB_BEGIN

namespace function
{
    std::list<FunctionComponent*> FunctionComponent::m_list;

    FunctionComponent::FunctionComponent()
    {
        m_list.push_back(this);
    }

    FunctionComponent::~FunctionComponent()
    {
        auto itr = std::find(m_list.begin(), m_list.end(), this);
        if (itr != m_list.end())
            m_list.erase(itr);
    }

    void FunctionComponent::UpdateAll()
    {
        for (auto &itr : m_list)
        {
            if (!itr->m_Activity)continue;
            itr->Update();
        }
    }
}

_MSLIB_END