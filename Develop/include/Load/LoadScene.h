//==========================================================================
// ロードシーン[load.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "LoadScreen.h"
#include "Fade.h"

//==========================================================================
//
// class  : CLoadScene
// Content: ロードシーン
//
//==========================================================================
class LoadScene : public mslib::scene_manager::BaseScene
{
public:
    LoadScene();
    ~LoadScene();
    CFade * GetFade();
    CLoadScreen * GetLoadScreen();
private:
    CFade *m_fade; // フェード
    CLoadScreen *m_load; // ロード画面
};
