//==========================================================================
// フェード[Fade.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Fade.h"
#include "resource_list.h"

//==========================================================================
// 初期化
void CFade::Init(void)
{
    auto & WinSize = GetDevice()->GetWindowsSize();
    auto *sprite = Sprite()->Create();

    sprite->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));

    m_Param.m_Change = false;
    m_Param.m_Key = false;
    m_Param.m_Draw = true;
    m_Param.m_In = true;
    m_Param.m_a = 255;

    sprite->SetPolygonSize((float)WinSize.x, (float)WinSize.y);
    sprite->SetColor(0.0f, 0.0f, 0.0f, 1.0f);
    sprite->SetPriority(999999.0f);
}

//==========================================================================
// 更新
void CFade::Update(void)
{
    auto *sprite = Sprite()->Get(0);

    // 処理実行判定
    if (m_Param.m_Key)
    {
        sprite->SetActivity(true);

        // フェードin,aut切り替え
        if (m_Param.m_Change)
        {
            m_Param.m_a += 5;
            if (255 < m_Param.m_a)
            {
                m_Param.m_Key = false;
                m_Param.m_In = true;
                m_Param.m_a = 255;
            }
        }
        else
        {
            m_Param.m_a -= 5;
            if (m_Param.m_a < 0)
            {
                m_Param.m_Key = false;
                m_Param.m_Draw = false;
                m_Param.m_a = 0;
            }
        }
        sprite->SetColor(0.0f, 0.0f, 0.0f, m_Param.m_a / 255.0f);
    }
    else
    {
        sprite->SetActivity(false);
    }
}

//==========================================================================
// デバッグ
void CFade::Debug(void)
{
}

//==========================================================================
// フェードイン
void CFade::In(void)
{
    m_Param.m_Key = true;
    m_Param.m_Change = true;
    m_Param.m_Draw = true;
    m_Param.m_In = false;
    m_Param.m_a = 0;
}

//==========================================================================
// フェードアウト
void CFade::Out(void)
{
    // out用のパラメータの初期化
    m_Param.m_Key = true;
    m_Param.m_Change = false;
    m_Param.m_In = false;
    m_Param.m_a = 255;
}

//==========================================================================
// フェードイン終了判定
bool CFade::FeadInEnd(void)
{
    return m_Param.m_In; // 判定
}
