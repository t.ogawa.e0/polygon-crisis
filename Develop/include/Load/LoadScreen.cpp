//==========================================================================
// ロードスクリーン[LoadScreen.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LoadScreen.h"
#include "resource_list.h"

//==========================================================================
// 初期化
void CLoadScreen::Init(void)
{
    auto & WinSize = GetDevice()->GetWindowsSize();

    auto *background = Sprite()->Create();
    auto *ring = Sprite()->Create();
    auto *font = Sprite()->Create();

    //==========================================================================
    // 背景
    background->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));
    background->SetPolygonSize((float)WinSize.x, (float)WinSize.y);
    background->SetColor(0.0f, 0.0f, 0.0f, 1.0f);
    background->SetPriority(999999.1f);

    //==========================================================================
    // リング
    ring->SetTextureData(GetTextureLoader()->Load(RESOURCE_LoadTex_DDS));
    auto * pPolySize = ring->GetPolygonSize();
    ring->SetPolygonSize(*pPolySize*0.15f);
    ring->SetColor(1.0f, 1.0f, 0.0f, 1.0f);
    ring->SetPosition(WinSize.x - pPolySize->x, WinSize.y - pPolySize->y);
    ring->SetPriority(999999.2f);

    //==========================================================================
    // ロードフォント
    m_paramload.m_Change = false;
    m_paramload.m_a = 100;
    font->SetTextureData(GetTextureLoader()->Load(RESOURCE_NowLoading_DDS));
    font->SetPolygonSize(*font->GetPolygonSize()*0.15f);
    font->SetColor(1.0f, 1.0f, 0.0f, m_paramload.m_a / 255.0f);
    font->SetPosition(WinSize.x - font->GetPolygonSize()->x, WinSize.y - (pPolySize->y*0.6f));
    font->SetPriority(999999.3f);
}

//==========================================================================
// 更新
void CLoadScreen::Update(void)
{
    auto *ring = Sprite()->Get(1);
    auto *font = Sprite()->Get(2);

    // 現在時刻の取得
    m_NewTime = timeGetTime();

    // 前回との差を取得
    DWORD ___time = m_NewTime - m_OldTime;

    //時間渡し
    m_OldTime = m_NewTime;

    // 差を回転力に変換
    ring->AddRotation(0, 0, ___time*0.0025f);

    Change((int)(___time*0.5f));
    font->SetColor(1.0f, 1.0f, 0.0f, m_paramload.m_a / 255.0f);
}

//==========================================================================
// デバッグ
void CLoadScreen::Debug(void)
{
}

void CLoadScreen::SetActivity(bool activity)
{
    if (m_activity != activity)
    {
        for (int i = 0; i < Sprite()->Size(); i++)
        {
            Sprite()->Get(i)->SetActivity(activity);
        }
        m_activity = activity;
    }
}

//==========================================================================
// αチェンジ
void CLoadScreen::Change(int Speed)
{
    // チェンジフラグ
    if (m_paramload.m_Change)
    {
        m_paramload.m_a -= Speed;
        if (m_paramload.m_a <= 0)
        {
            m_paramload.m_a = 0;
            m_paramload.m_Change = false;
        }
    }
    else
    {
        m_paramload.m_a += Speed;
        if (255 <= m_paramload.m_a)
        {
            m_paramload.m_a = 255;
            m_paramload.m_Change = true;
        }
    }
}
