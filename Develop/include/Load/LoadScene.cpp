//==========================================================================
// ロードシーン[LoadScene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LoadScene.h"
#include "LoadScreen.h"
#include "Fade.h"

LoadScene::LoadScene() : BaseScene("LoadScene", "LoadScene")
{
    m_fade = AddComponent<CFade>();
    m_load = AddComponent<CLoadScreen>();
}

LoadScene::~LoadScene()
{
    m_fade = nullptr;
    m_load = nullptr;
}

CFade * LoadScene::GetFade()
{
    return m_fade;
}

CLoadScreen * LoadScene::GetLoadScreen()
{
    return m_load;
}
