//==========================================================================
// リザルトシーン [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Asset/ThankYouForPlaying.h"
#include "Asset/ControllerObject.h"
#include "Asset/BGMObject.h"
#include "Scene.h"
#include "config.h"
#include "resource_list.h"

namespace Result
{
    Scene::Scene() : BaseScene("Result", SceneLevel::__Result__)
    {
		AddComponent<common::ControllerObject>();
		AddComponent<common::ThankYouForPlaying>();
		AddComponent<common::BGMObject>(RESOURCE_title_bgm_wav);
    }

    Scene::~Scene()
    {
    }
}