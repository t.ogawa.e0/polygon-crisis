//==========================================================================
// ThankYouForPlaying [ThankYouForPlaying.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ThankYouForPlaying.h"
#include "Asset/ControllerObject.h"
#include "System/Screen.h"
#include "Title/Scene.h"
#include "resource_list.h"

namespace common
{
	constexpr float __color_value__ = 0.05f;

	ThankYouForPlaying::ThankYouForPlaying() : ObjectManager("ThankYouForPlaying")
	{
		m_frame_count = mslib::Seconds(3);
		m_PressAnyKey = nullptr;
		m_color_value = __color_value__;
		m_se_label = 0;
	}

	ThankYouForPlaying::~ThankYouForPlaying()
	{
	}
	void ThankYouForPlaying::Init()
	{
		auto & WinSize = GetDevice()->GetWindowsSize();

		auto obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_ThankYouForPlaying_DDS));
		obj->SetComponentName("ThankYouForPlayingSprite");
		auto size = *obj->GetPolygonSize() / 2;
		obj->SetPosition((WinSize.x / 2.0f) - size.x, (WinSize.y / 2.0f) - size.y);

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));
		obj->SetComponentName("ThankYouForPlaying_Background");
		obj->SetPolygonSize((float)WinSize.x, (float)WinSize.y);

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_PressAnyKey_DDS));
		obj->SetComponentName("PressAnyKeySprite");
		size = *obj->GetPolygonSize() / 2;
		obj->SetPosition((WinSize.x / 2.0f) - size.x, (WinSize.y / 2.0f) - size.y);
		m_PressAnyKey = obj;

		auto sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_select_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label);

		m_ControllerObject = GetParent()->GetComponent<common::ControllerObject>()->XInput_();

		m_XInputButton.push_back(mslib::xinput::XInputButton::X);
		m_XInputButton.push_back(mslib::xinput::XInputButton::Y);
		m_XInputButton.push_back(mslib::xinput::XInputButton::A);
		m_XInputButton.push_back(mslib::xinput::XInputButton::B);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_UP);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_DOWN);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_LEFT);
		m_XInputButton.push_back(mslib::xinput::XInputButton::DPAD_RIGHT);
		m_XInputButton.push_back(mslib::xinput::XInputButton::START);
		m_XInputButton.push_back(mslib::xinput::XInputButton::BACK);
		m_XInputButton.push_back(mslib::xinput::XInputButton::LEFT_THUMB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::RIGHT_THUMB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::LEFT_LB);
		m_XInputButton.push_back(mslib::xinput::XInputButton::RIGHT_RB);
	}
	void ThankYouForPlaying::Update()
	{
		m_frame_count--;

		// 有効時の処理
		if (m_frame_count < 0)
		{
			m_PressAnyKey->SetActivity(true);
			m_PressAnyKey->AddColor(0, 0, 0, m_color_value);
			auto color = m_PressAnyKey->GetColor();

			if (1.0f < color->a)
			{
				m_PressAnyKey->SetColor(color->r, color->g, color->b, 1);
				m_color_value = -__color_value__;
			}
			else if (color->a < 0.0f)
			{
				m_PressAnyKey->SetColor(color->r, color->g, color->b, 0);
				m_color_value = __color_value__;
			}
			m_frame_count = 0;
		}
		// 有効ではないので非表示
		else if (0 < m_frame_count)
		{
			auto color = m_PressAnyKey->GetColor();
			m_PressAnyKey->SetColor(color->r, color->g, color->b, 0);
			m_PressAnyKey->SetActivity(false);
		}

		constexpr int index = 0;

		if (!m_ControllerObject->Check(index))return;
		for (auto & itr : m_XInputButton)
		{
			if (m_ControllerObject->Trigger(itr, index))
			{
				XAudio()->Play(m_se_label);
				CScreen::ScreenChange(new Title::Scene);
			}
		}
	}
	void ThankYouForPlaying::Debug()
	{
	}
}