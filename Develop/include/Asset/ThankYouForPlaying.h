//==========================================================================
// ThankYouForPlaying [ThankYouForPlaying.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : ThankYouForPlaying
	// Content:ThankYouForPlaying
	//
	//==========================================================================
	class ThankYouForPlaying : public mslib::ObjectManager
	{
	public:
		ThankYouForPlaying();
		~ThankYouForPlaying();
		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	protected:
		int m_frame_count; // フレームカウンタ
		mslib::sprite::Sprite * m_PressAnyKey;
		float m_color_value;
		mslib::xinput::XInput * m_ControllerObject;
		std::list<mslib::xinput::XInputButton> m_XInputButton;
		int m_se_label;
	};
}