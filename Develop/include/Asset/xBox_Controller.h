//==========================================================================
// xBox Controller [xBox Controller.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : xBox_Controller
	// Content: xBox Controller
	//
	//==========================================================================
	class xBox_Controller : public mslib::ObjectManager
	{
	public:
		xBox_Controller();
		~xBox_Controller();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;

		void ActivityFlag(bool flag);
	private:
		mslib::sprite::Sprite * m_sprite;
	};
}