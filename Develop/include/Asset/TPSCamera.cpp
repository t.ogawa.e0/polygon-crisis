//==========================================================================
// TPSカメラ [TPSCamera.h]
// author: tatsuya ogawa
//==========================================================================
#include "TPSCamera.h"
#include "ControllerObject.h"

namespace common
{
    constexpr float MoveCamera = 0.05f;

    TPSCamera::TPSCamera() : ObjectManager("TPSCamera")
    {
        m_controller = nullptr;
		AddActivity();
    }

    TPSCamera::~TPSCamera()
    {
    }
    void TPSCamera::Init()
    {
		auto contr = GetParent()->GetComponent<common::ControllerObject>();
		if (contr != nullptr)
			m_controller = contr->XInput_();

        auto camera = Camera()->Create();
        camera->IsMainCamera();
        camera->Init(D3DXVECTOR3(0.0f, 2.0f, -2.5f), D3DXVECTOR3(0.0f, 2.0f, 0.0f));

        auto obj = Object()->Create();
        obj->SetMatrixType(mslib::object::MatrixType::World);
        obj->SetComponentName(mslib::text("%p", obj));

        obj = Object()->Create();
        obj->SetMatrixType(mslib::object::MatrixType::World);
        obj->SetComponentName(mslib::text("%p", obj));
    }
    void TPSCamera::Update()
    {
        auto * pObj1 = Object()->Get(0);
        auto * pObj2 = Object()->Get(1);
        auto * pCamera = Camera()->Get(0);

        // カメラ本来の機能
        pCamera->SetAt(*pObj1->GetPosition());
        pCamera->SetEye(*pObj1->GetPosition());

        pObj2->SetPosition(pCamera->GetLook1().eye + *pObj1->GetPosition());
        pObj2->LockOn(*pObj1);

        // カメラの操作処理
        if (mslib::nullptr_check(m_controller))
        {
            if (m_controller->Check(0))
            {
                D3DXVECTOR3 vec;
                if (m_controller->AnalogR(0, vec))
                {
                    auto camera = Camera()->Get(0);
                    vec *= MoveCamera;
                    vec.y = 0;

                    camera->AddViewRotation(vec.x, 0, 0);
                    camera->AddViewRotationY(vec.z, mslib::ToRadian(35.0f));
                }
            }
        }
    }
    void TPSCamera::Debug()
    {
    }
    void TPSCamera::SetObjectSmooth(mslib::transform::Transform * object)
    {
        auto * pObj1 = Object()->Get(0);

        // オブジェクトの複製
        auto obj1 = *(mslib::transform::Transform*)pObj1;
        auto obj2 = *object;

        obj2.AddPosition(0.1f, 0.1f, 0);
        obj1.SetPosition(obj1.GetPosition()->x, obj2.GetPosition()->y, obj1.GetPosition()->z);

        // 対象に追従
        for (int i = 0; i < 5; i++)
        {
            pObj1->AddRotationX(pObj1->ToSee(obj2), 1.0f);
        }

        pObj1->AddPosition(0, 0, mslib::collider::Distance(&obj1, &obj2)*0.07f);
        auto * pPos = pObj1->GetPosition();
        pObj1->SetPosition(pPos->x, object->GetPosition()->y, pPos->z);
    }
    void TPSCamera::SetObjectPerfection(mslib::transform::Transform * object)
    {
        auto * pObj1 = Object()->Get(0);
        pObj1->SetPosition(*object->GetPosition());
    }
}