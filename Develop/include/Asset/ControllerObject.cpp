//==========================================================================
// コントローラーオブジェクト [ControllerObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ControllerObject.h"

namespace common
{
    ControllerObject::ControllerObject() : ObjectManager("ControllerObject")
    {
        m_numXInput = 1;
    }

    ControllerObject::ControllerObject(int numXInput) : ObjectManager("ControllerObject")
    {
        m_numXInput = numXInput;
    }

    ControllerObject::~ControllerObject()
    {
    }

    void ControllerObject::Init()
    {
        XInput_()->Init(m_numXInput);
    }
    void ControllerObject::Update()
    {
		// 実体化されているコントローラー数実行
        for (int i = 0; i < m_numXInput; i++)
            if (XInput_()->Check(i))
                XInput_()->Update();
    }
    void ControllerObject::Debug()
    {
    }
}