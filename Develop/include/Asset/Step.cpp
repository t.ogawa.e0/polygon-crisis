//==========================================================================
// �X�e�b�v [Step.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Step.h"

namespace common
{
    using namespace mslib;
    Step::Step()
    {
        SetComponentName("Step");
        m_powor = 0.0f;
        m_one_frame_powor = 0.0f;
        m_flag = false;
        AddActivity();
    }

    Step::~Step()
    {
    }
    void Step::Update()
    {
        m_flag = false;

        auto obj = GetWarrantyParent<transform::Transform>();
        if (obj == nullptr)return;
        if (m_powor < 0.0f)return;

        obj->AddPosition(0, 0, m_powor);
        m_powor -= m_one_frame_powor;
        m_flag = true;
    }
    void Step::Play(float powor, float one_frame_powor)
    {
        m_powor = powor;
        m_one_frame_powor = one_frame_powor;
    }
    bool Step::Trigger()
    {
        return m_flag;
    }
}