//==========================================================================
// 攻撃オブジェクトの管理者 [AttackObjectAdministrator.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Player/LifeUI.h"
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : AttackObjectAdministrator
	// Content: 攻撃オブジェクトの管理者
	//
	//==========================================================================
	class AttackObjectAdministrator : public mslib::ObjectManager
    {
    public:
        AttackObjectAdministrator();
        ~AttackObjectAdministrator();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
	private:
		// 破棄
		void Delete();
		// ダメージ処理
		void PlayerDamage();
	private:
		player::LifeUI * m_LifeUI;
		std::unordered_map<void*, bool> m_sled; // sled
	};
}
