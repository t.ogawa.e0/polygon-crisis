//==========================================================================
// �s���͈� [ActionRange.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ActionRange.h"

namespace common
{
    std::list<mslib::transform::Transform*> ActionRange::m_AllActionRangeData;

    ActionRange::ActionRange() : ObjectManager(mslib::text("%p", this))
    {
        m_NumActionRange = 0;
    }

    ActionRange::ActionRange(const std::string & ObjectName, int NumActionRange) : ObjectManager(ObjectName)
    {
        m_NumActionRange = NumActionRange;
    }

    ActionRange::~ActionRange()
    {
        auto obj = Object()->Get(0);
        if (!mslib::nullptr_check(obj))return;

		// �q�����������
        for (auto & itr : obj->GetChild())
        {
            auto itr_ = std::find(m_AllActionRangeData.begin(), m_AllActionRangeData.end(), itr);
            if (itr_ == m_AllActionRangeData.end())continue;
            m_AllActionRangeData.erase(itr_);
        }
    }
    void ActionRange::Init()
    {
        if (m_NumActionRange == 0)return;

        auto obj = Object()->Create();
        
        obj->SetComponentName(GetComponentName() + "Object");

        for (int i = 0; i < m_NumActionRange; i++)
        {
            auto object = obj->AddComponent<mslib::object::Object>();

            object->SetComponentName(GetComponentName() + std::to_string(i));
            object->SetMatrixType(mslib::object::MatrixType::Local);
            m_AllActionRangeData.push_back(object);
            m_ActionRangeData.push_back(object);
        }
    }
    void ActionRange::Update()
    {
    }
    void ActionRange::Debug()
    {
    }
    std::list<mslib::transform::Transform*> ActionRange::GetActionRangeData()
    {
        return m_ActionRangeData;
    }
    const std::list<mslib::transform::Transform*>& ActionRange::GetAllActionRangeData()
    {
        return m_AllActionRangeData;
    }
}