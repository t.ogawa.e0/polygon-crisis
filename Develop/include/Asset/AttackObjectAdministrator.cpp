//==========================================================================
// 攻撃オブジェクトの管理者 [AttackObjectAdministrator.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "AttackObjectAdministrator.h"
#include "AttackCollision.h"
#include "Enemy/AttackCollision/EnemyAttackObject.h"
#include "Asset/Character.h"

namespace common
{
    AttackObjectAdministrator::AttackObjectAdministrator() : ObjectManager("AttackObjectAdministrator")
    {
		m_LifeUI = nullptr;
    }

    AttackObjectAdministrator::~AttackObjectAdministrator()
    {
    }
    void AttackObjectAdministrator::Init()
    {
		m_LifeUI = GetParent()->GetComponent<player::LifeUI>();
    }
    void AttackObjectAdministrator::Update()
    {
		PlayerDamage();
		Delete();
    }
    void AttackObjectAdministrator::Debug()
    {
    }
	void AttackObjectAdministrator::Delete()
	{
		// 破棄処理
		std::list<AttackCollision*> delete_list;

		// 破棄対象の格納
		for (auto & itr : AttackCollision::GetAllAttackCollision())
		{
			if (!itr->GetDeleteFlag())continue;
			delete_list.push_back(itr);
		}

		// 破棄処理
		for (auto & itr : delete_list)
		{
			AttackCollision::DestroyAttackCollision(itr);
		}
	}
	void AttackObjectAdministrator::PlayerDamage()
	{
		if (m_LifeUI != nullptr)
		{
			// 判定チェック
			for (auto & itr : common::PlayerCharacter::GetAllPlayerCharacter())
			{
				// 防御フラグ時は無効
				if (itr->GetDefenseCollider()->GetUpdateFlag())continue;

				// 既に判定のログが存在する
				if (m_sled.find(itr) != m_sled.end()) 
				{
					// 判定が出ているので終了
					if (itr->GetPlayerCollider()->Trigger())continue;

					// 出ていないので破棄
					m_sled.erase(m_sled.find(itr));
				}

				// 判定が出ていない
				if (!itr->GetPlayerCollider()->Trigger())continue;

				m_sled[itr] = true; // 当たり判定フラグを立てる

				for (int i = 0; i < 12; i++)
					m_LifeUI->AddDamage();
			}
		}
	}
}