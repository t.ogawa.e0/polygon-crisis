//==========================================================================
// Map [Map.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Map.h"
#include "Asset/Character.h"
#include "Asset/ShaderModel.h"
#include "Asset/LightObject.h"
#include "Asset/TPSCamera.h"
#include "Asset/FieldBox.h"
#include "Asset/FieldPlane.h"
#include "Asset/FieldHighGround.h"
#include "common/DefaultSystem.h"
#include "config.h"
#include "resource_list.h"

namespace common
{
    constexpr const int __FloorObject__ = 0;
    constexpr const int __WallObject__ = 1;
    constexpr const int __HighGroundObject__ = 2;
    constexpr const int __NormalObject__ = 3;

    Map::Map() : ObjectManager("Map")
    {
        m_LightObject = nullptr;
        m_TPSCamera = nullptr;
        m_data.Reserve(4);
        m_data.SetComponentName("MapDataParameter");
    }

    Map::~Map()
    {
        m_data.Release();
    }
    void Map::Init()
    {
        m_LightObject = GetParent()->GetComponent<common::LightObject>();
        m_TPSCamera = GetParent()->GetComponent<common::TPSCamera>();

        Object()->Create()->SetComponentName("FloorObject");
        Object()->Create()->SetComponentName("WallObject");
        Object()->Create()->SetComponentName("HighGroundObject");
        Object()->Create()->SetComponentName("NormalObject");

        auto floor = m_data.Create();
        auto wall = m_data.Create();
        auto high_ground = m_data.Create();
        auto normal = m_data.Create();

        // データの読み取り
        Load();

        // 初期化時に必要数のデータを反映
        for (int i = 0; i < floor->num; i++)
        {
            AddFloor(floor->list, i);
        }

        // 初期化時に必要数のデータを反映
        for (int i = 0; i < wall->num; i++)
        {
            AddWall(wall->list, i);
        }

        // 初期化時に必要数のデータを反映
        for (int i = 0; i < high_ground->num; i++)
        {
            AddHighGround(high_ground->list, i);
        }

        // 初期化時に必要数のデータを反映
        for (int i = 0; i < normal->num; i++)
        {
            AddNormalObject(normal->list, i);
        }
    }
    void Map::Update()
    {
    }
    void Map::Debug()
    {
		// オブジェクト追加機能群
        if (ImGui()->NewMenu("FloorObject"))
        {
            FloorSystem();
            ImGui()->EndMenu();
        }
        if (ImGui()->NewMenu("WallObject"))
        {
            WallSystem();
            ImGui()->EndMenu();
        }
        if (ImGui()->NewMenu("HighGroundObject"))
        {
            HighGroundSystem();
            ImGui()->EndMenu();
        }
        if (ImGui()->NewMenu("NormalObject"))
        {
            NormalObjectSystem();
            ImGui()->EndMenu();
        }
        if (ImGui()->MenuItem("Save"))
        {
            Save();
        }
        if (ImGui()->MenuItem("Load"))
        {
            Load();
        }
    }
    void Map::FloorSystem()
    {

        // 地面の追加
        if (ImGui()->MenuItem("Add Floor"))
        {
            auto data = m_data.Get(__FloorObject__);
            AddFloor(data->list, data->num);
            data->num++;
        }

        // 破棄機能
        RemoveSystem<ShaderModel::PerPixelLightingModel>(__FloorObject__, "Floor");
    }
    void Map::AddFloor(std::list<void*> & list, int id)
    {
        auto light = mslib::cast<common::LightObject>(m_LightObject)->Light()->Get(0);
        auto camera = mslib::cast<common::TPSCamera>(m_TPSCamera)->Camera();
        auto floor = Object()->Get(__FloorObject__);
        auto model = floor->AddComponent<ShaderModel::PerPixelLightingModel>();

		// データのセット
        model->SetXModelData(GetXModelLoader()->Load(RESOURCE_Floor_x));
        model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        model->SetCamera(camera->Get(0)->GetCamera());
        model->SetLight(light);
        model->SetComponentName("Floor" + std::to_string(id));
        model->SetMatrixType(mslib::object::MatrixType::Local);
        list.push_back(model);

        auto plane = model->AddComponent<common::FieldPlane>();
        plane->SetComponentName("GroundCollider");

		// 全てのキャラクターとリンク
        for (auto & itr : common::Character::GetAllCharacter())
        {
            if (mslib::nullptr_check(itr->GetVersusMap()))
                plane->AddCollider(itr->GetVersusMap());
        }
    }
    void Map::WallSystem()
    {
        // 地面の追加
        if (ImGui()->MenuItem("Add Wall"))
        {
            auto data = m_data.Get(__WallObject__);
            AddWall(data->list, data->num);
            data->num++;
        }

        // 破棄機能
        RemoveSystem<ShaderModel::PerPixelLightingModel>(__WallObject__, "Wall");
    }
    void Map::AddWall(std::list<void*> & list, int id)
    {
        auto light = mslib::cast<common::LightObject>(m_LightObject)->Light()->Get(0);
        auto camera = mslib::cast<common::TPSCamera>(m_TPSCamera)->Camera();
        auto wall = Object()->Get(__WallObject__);
        auto model = wall->AddComponent<ShaderModel::PerPixelLightingModel>();

		// データのセット
        model->SetXModelData(GetXModelLoader()->Load(RESOURCE_Box_x));
        model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        model->SetCamera(camera->Get(0)->GetCamera());
        model->SetLight(light);
        model->SetComponentName("Wall" + std::to_string(id));
        model->SetMatrixType(mslib::object::MatrixType::Local);
        list.push_back(model);

        auto plane = model->AddComponent<common::FieldBox>();
        plane->SetComponentName("BoxCollider");

		// 全てのキャラクターとリンク
        for (auto & itr : common::Character::GetAllCharacter())
        {
            if (mslib::nullptr_check(itr->GetVersusWallBox()))
                plane->AddCollider(itr->GetVersusWallBox());
        }
    }
    void Map::HighGroundSystem()
    {
        // 地面の追加
        if (ImGui()->MenuItem("Add HighGround"))
        {
            auto data = m_data.Get(__HighGroundObject__);
            AddHighGround(data->list, data->num);
            data->num++;
        }

        // 破棄機能
        RemoveSystem<ShaderModel::PerPixelLightingModel>(__HighGroundObject__, "HighGround");
    }
    void Map::AddHighGround(std::list<void*> & list, int id)
    {
        auto light = mslib::cast<common::LightObject>(m_LightObject)->Light()->Get(0);
        auto camera = mslib::cast<common::TPSCamera>(m_TPSCamera)->Camera();
        auto high_ground = Object()->Get(__HighGroundObject__);
        auto model = high_ground->AddComponent<ShaderModel::PerPixelLightingModel>();

		// データの登録
        model->SetXModelData(GetXModelLoader()->Load(RESOURCE_Box_x));
        model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        model->SetCamera(camera->Get(0)->GetCamera());
        model->SetLight(light);
        model->SetComponentName("HighGround" + std::to_string(id));
        model->SetMatrixType(mslib::object::MatrixType::Local);
        list.push_back(model);

		// 機能名
        auto pFieldHighGround = model->AddComponent<common::FieldHighGround>();
        pFieldHighGround->GetFieldBox()->SetComponentName("BoxCollider");
        pFieldHighGround->GetFieldPlane()->SetComponentName("PlaneCollider");
        pFieldHighGround->GetFieldUpPlane()->SetComponentName("PlaneCollider_By_Up");

		// 挙動を設定
        pFieldHighGround->GetFieldUpPlane()->UpCollider(true);
        pFieldHighGround->GetFieldUpPlane()->UnderCollider(false);
        pFieldHighGround->GetFieldUpPlane()->SetPositionInputMode(false);

		// 全てのキャラクターとリンク
        for (auto & itr : common::Character::GetAllCharacter())
        {
            if (mslib::nullptr_check(itr->GetVersusHighGroundBox()))
                pFieldHighGround->GetFieldBox()->AddCollider(itr->GetVersusHighGroundBox());

            if (mslib::nullptr_check(itr->GetVersusHighGroundUnder()))
                pFieldHighGround->GetFieldPlane()->AddCollider(itr->GetVersusHighGroundUnder());

            if (mslib::nullptr_check(itr->GetVersusHighGroundUp()))
                pFieldHighGround->GetFieldUpPlane()->AddCollider(itr->GetVersusHighGroundUp());
        }
    }
    void Map::NormalObjectSystem()
    {
        // 地面の追加
        if (ImGui()->MenuItem("Add NormalObject"))
        {
            auto data = m_data.Get(__NormalObject__);
            AddNormalObject(data->list, data->num);
            data->num++;
        }

        // 破棄機能
        RemoveSystem<ShaderModel::PerPixelLightingCube>(__NormalObject__, "NormalObject");
    }
    void Map::AddNormalObject(std::list<void*> & list, int id)
    {
        auto light = mslib::cast<common::LightObject>(m_LightObject)->Light()->Get(0);
        auto camera = mslib::cast<common::TPSCamera>(m_TPSCamera)->Camera();
        auto normal = Object()->Get(__NormalObject__);
        auto cube = normal->AddComponent<ShaderModel::PerPixelLightingCube>();

		// データの反映
        cube->SetCubeData(GetCreateCube()->Create());
        cube->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        cube->SetTextureData(GetTextureLoader()->Load(""));
        cube->SetMatrixType(mslib::object::MatrixType::Local);
        cube->SetCamera(camera->Get(0)->GetCamera());
        cube->SetLight(light);
        cube->SetComponentName("NormalObject" + std::to_string(id));
        list.push_back(cube);
    }
    void Map::Load()
    {
		auto scene = GetParent()->GetComponent<mslib::scene_manager::BaseScene>();
		if (scene == nullptr)return;

		auto & ifstream = mslib::DefaultSystem::BeginFstreamReadingBinary(scene->GetFileName() + "MapData");
        if (!ifstream.fail())
        {
            ifstream.clear();
            ifstream.seekg(0, ifstream.beg);

            // 読み取り
            ifstream.read((char *)&m_data.Get(__FloorObject__)->num, sizeof(m_data.Get(__FloorObject__)->num));
            ifstream.read((char *)&m_data.Get(__WallObject__)->num, sizeof(m_data.Get(__WallObject__)->num));
            ifstream.read((char *)&m_data.Get(__HighGroundObject__)->num, sizeof(m_data.Get(__HighGroundObject__)->num));
            ifstream.read((char *)&m_data.Get(__NormalObject__)->num, sizeof(m_data.Get(__NormalObject__)->num));
        }
        mslib::DefaultSystem::EndFstreamReadingBinary();
    }
    void Map::Save()
    {
		auto scene = GetParent()->GetComponent<mslib::scene_manager::BaseScene>();
		if (scene == nullptr)return;

        // 数データの保存
        auto & ofstream = mslib::DefaultSystem::BeginFstreamWritingBinary(scene->GetFileName() + "MapData");
        ofstream.write((char *)&m_data.Get(__FloorObject__)->num, sizeof(m_data.Get(__FloorObject__)->num));
        ofstream.write((char *)&m_data.Get(__WallObject__)->num, sizeof(m_data.Get(__WallObject__)->num));
        ofstream.write((char *)&m_data.Get(__HighGroundObject__)->num, sizeof(m_data.Get(__HighGroundObject__)->num));
        ofstream.write((char *)&m_data.Get(__NormalObject__)->num, sizeof(m_data.Get(__NormalObject__)->num));
        mslib::DefaultSystem::EndFstreamWritingBinary();
    }
}