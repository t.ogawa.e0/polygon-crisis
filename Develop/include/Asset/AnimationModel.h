//==========================================================================
// アニメーションモデル [AnimationModel.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <list>
#include <string>
#include <cstdio>
#include <algorithm>

#include "common\Object.h"
#include "common\Effect.h"
#include "common\Texture.h"
#include "common\Camera.h"
#include "common\Light.h"
#include "common/DefaultSystem.h"
#include "common/Transform.h"
#include "ShaderModel.h"

namespace animation_model
{
    constexpr const char * __none__ = "none";
    constexpr const char * __data__ = "_data";
    constexpr const char * __motion__ = "_motion";
    constexpr const char * __animation__ = "_animation"; 
    constexpr const char * __delimiter__ = "*"; // 混合キー合成ポイント

    //==========================================================================
    //
    // class  : AnimationModelTemplate
    // Content: アニメーションモデルのテンプレート
    //
    //==========================================================================
    class AnimationModelTemplate : public mslib::object::Object
    {
    public:
        using type_name = ShaderModel::PerPixelLightingModel;
        using pause_list = std::unordered_map<std::string, Transform>; // 各パーツ
        struct numbering // ナンバリング
        {
            int ID = 0; // ID
            pause_list pause; // パーツ
        };
        struct linear_data // 線形データ
        {
            std::string motion_name; // モーション名
            std::list<numbering> linear; // 線形
        };
    protected:
        struct animation_linear  // アニメーション用の線形
        {
            Transform * target = nullptr; // 対象ポインタ
            std::vector<mslib::transform::Parameter> linear; // 計算済み線形補間データ
        };
        class Interconnection // Data handled in linear interpolation are interconnected to generate one animation data.
        {
        public:
            Interconnection() {}
            Interconnection(numbering*_begin, numbering*_end) {
                begin = _begin;
                end = _end;
            }
            ~Interconnection() {}
            numbering * begin = nullptr;
            numbering * end = nullptr;
        };
        struct animation_parameter // アニメーション情報
        {
            void initializer() {
                animation_name.clear();
                parts.clear();
                one_motion_frame = 0;
                max_frame = 0;
                frame = 0;
                m_loop_frag = false;
                m_end_frag = false;
            }
            void initializer_connection() {
                m_connection_data.clear();
            }
            std::string animation_name; // アニメーション名
            std::list<animation_linear> parts; // パーツデータ
            std::list<int> m_connection_data; // アニメーションの接続データ
            int one_motion_frame = 0; // 1つのモーションにかけるフレーム
            int max_frame = 0; // 最大フレーム
            int frame = 0; // 現在のフレーム
            bool m_loop_frag = false; // ループフラグ
            bool m_end_frag = false; // 終了フラグ
        };
        class Mutual_t {
        public:
            Mutual_t() {}
            Mutual_t(int _begin, int _end) {
                begin = _begin;
                end = _end;
            }
            ~Mutual_t() {}
            int begin = 0;
            int end = 0;
        };
        using motion_data = std::unordered_map<std::string, linear_data>; // モーションデータ
        using animation_data = std::unordered_map<std::string, animation_parameter>; // アニメーションデータ
    public:
        AnimationModelTemplate() :m_ParenModel(nullptr), m_play_animation(nullptr), m_add_update_flame(1) { SetComponentName("AnimationModelTemplate"); }
        virtual ~AnimationModelTemplate() { m_ModelParts.clear(); }

        /**
        @brief 更新
        */
        void Update() override {
            // デフォルトの場合設定
            if (m_MatType == mslib::object::MatrixType::Default)
                m_MatType = mslib::object::MatrixType::World;

            UpdateAnimation();
            UpdateMatrix();
        }

        /**
        @brief アニメーション更新速度
        */
        void SetAnimationUpdateFlame(int flame) {
            m_add_update_flame = flame;
        }

        /**
        @brief アニメーション更新速度の取得
        */
        int GetAnimationUpdateFlame() {
            return m_add_update_flame;
        }

        //==========================================================================
        // パーツの追加
        //==========================================================================
        virtual type_name * AddParts(const std::string & parts_name, const std::string & paren_name = __none__) final {
            Component * obj = nullptr;
            if (paren_name == __none__) {
                obj = AddComponent<type_name>();
            }
            else if (paren_name != __none__) {
                Component * paren = NodeSearch<type_name>(paren_name);
                if (paren == nullptr)return nullptr;
                obj = paren->AddComponent<type_name>();
            }
            obj->SetComponentName(parts_name);
            m_ModelParts.push_back((type_name*)obj);
            return (type_name*)obj;
        }

        //==========================================================================
        // パーツリストの取得
        //==========================================================================
        std::list<type_name*> & GetPartsList() {
            return m_ModelParts;
        }

        //==========================================================================
        // 保存
        //==========================================================================
        void Save(const std::string & file_name) {
            SaveMotion(file_name);
            SavePause(file_name);
            SaveInterconnection(file_name);
        }

        //==========================================================================
        // 読み込み
        //==========================================================================
        void Load(const std::string & file_name) {
            m_motion_data.clear();
            LoadMotion(file_name);
            LoadPause(file_name);
            LoadInterconnection(file_name);
            GenerateAnimation(file_name);
        }

        //==========================================================================
        // ポーズデータの取得
        //==========================================================================
        motion_data & GetMotionData() {
            return m_motion_data;
        }

        //==========================================================================
        // ポーズデータの登録
        //==========================================================================
        void SetMotionData(const std::string & name) {
            int id = 0;
            // ポーズデータ用領域の生成
            auto &data = m_motion_data[name];

            // データが存在する場合ナンバリングを行う
            if (data.linear.size() != 0)
                id = (--data.linear.end())->ID;

            id++;

            // 新しいモーションの生成
            data.motion_name = name;
            data.linear.emplace_back();
            auto itr1 = --data.linear.end();
            itr1->ID = id;
            for (auto & itr2 : m_ModelParts) {
                auto obj = dynamic_cast<Transform*>(itr2);
                if (obj == nullptr)continue;
                itr1->pause[obj->GetComponentName()].SetComponentName(obj->GetComponentName());
            }

            // ID順にソートします
            data.linear.sort([&](numbering & a, numbering & b) {return a.ID < b.ID ? true : false; });

            // 生成したモーションに対して、描画用モデルの姿勢を反映します。
            for (auto & list : m_ModelParts) {
                auto parts = itr1->pause.find(list->GetComponentName());
                if (parts != itr1->pause.end()) {
                    parts->second.SetPosition(list->GetParameter()->position);
                    parts->second.SetRotation(list->GetParameter()->rotation);
                    parts->second.SetScale(list->GetParameter()->scale);
                }
            }
        }

        //==========================================================================
        // 再生中かどうかのチェック 再生中なら true
        //==========================================================================
        bool Check(const std::string & animation) {
            // アニメーションが反映されていない
            if (m_play_animation == nullptr)return false;
            // チェック情報が一致しない
            if (m_play_animation->animation_name != animation)return false;
            // 再生中ならtrue
            return m_play_animation->m_end_frag == false ? true : false;
        }

        // 再生
        void Play(const std::string & animation, bool loop = false) {
            auto itr = m_animation_data.find(animation);
            if (itr == m_animation_data.end())return;

            m_play_animation = &itr->second;
            m_play_animation->frame = 0;
            m_play_animation->m_end_frag = false;
            m_play_animation->m_loop_frag = loop;
        }

        // 停止
        void Stop(const std::string & animation) {
            if (Check(animation))
                m_play_animation = nullptr;
        }

        // 全停止
        void StopAll() {
            m_play_animation = nullptr;
        }

        // アニメーション接続データの取得
        std::list<int> & GetAnimationConnectionData(const std::string & animation) {
            return m_animation_data[animation].m_connection_data;
        }

        // Generate animation data
        void AnimationModelTemplate::GenerateAnimation(const std::string & label) {
            auto itr = m_motion_data.find(label);
            if (itr == m_motion_data.end())return;

            auto itr_inte = m_animation_data.find(label);
            if (itr_inte == m_animation_data.end())return;

            if ((int)itr_inte->second.m_connection_data.size() == 0)return;

            // モーションデータよりデータを摘出
            std::vector<Interconnection> interconnection_data; // 相互接続用
            std::vector<Mutual_t> mutual; // 接続用

            // 接続データの生成
            for (auto itr_begin = itr_inte->second.m_connection_data.begin(); itr_begin != itr_inte->second.m_connection_data.end(); ++itr_begin) {
                auto itr_end = itr_begin;
                ++itr_end;
                if (itr_end == itr_inte->second.m_connection_data.end())
                    break;

                // 接続データの設定
                mutual.emplace_back(*itr_begin, *itr_end);
            }

            if ((int)mutual.size() == 0)return;

            // 接続データを元にデータを接続
            for (auto &itr_mutual : mutual) {
                numbering * begin = nullptr;
                numbering * end = nullptr;

                // 開始データの検索
                for (auto & itr_linear : itr->second.linear) {
                    if (itr_linear.ID != itr_mutual.begin)continue;
                    begin = &itr_linear;
                }

                // 終了データの検索
                for (auto & itr_linear : itr->second.linear) {
                    if (itr_linear.ID != itr_mutual.end)continue;
                    end = &itr_linear;
                }

                // 登録
                interconnection_data.push_back(Interconnection(begin, end));
            }

            // モーション名を設定
            auto & animation = m_animation_data[itr->second.motion_name];

            // 初期化
            animation.initializer();

            // 名前反映
            animation.animation_name = itr->second.motion_name;

            // 1つのモーションにかけるフレーム
            animation.one_motion_frame = 60;

            // フレーム設定
            animation.max_frame = animation.one_motion_frame * (int)interconnection_data.size();

            // パーツの数だけ領域生成
            for (auto & parts_names : m_ModelParts) {
                // 保存領域生成
                animation.parts.emplace_back();
                auto itr_parts = --animation.parts.end();

                // 60フレーム分確保
                itr_parts->linear.reserve(animation.max_frame);
                itr_parts->target = parts_names;

                // 実体生成
                for (int i = 0; i < (int)itr_parts->linear.capacity(); i++)
                    itr_parts->linear.emplace_back();
            }

            int frame = 0;
            int frame_limit = animation.one_motion_frame;
            for (auto & itr_interconnection : interconnection_data) {
                // 各パーツに合わせたアニメーションデータを生成します。
                for (auto & linear_datas : animation.parts) {
                    auto itr_pause_begin = itr_interconnection.begin->pause.find(linear_datas.target->GetComponentName());
                    auto itr_pause_end = itr_interconnection.end->pause.find(linear_datas.target->GetComponentName());
                    auto * para_begin = itr_pause_begin->second.GetParameter();
                    auto * para_end = itr_pause_end->second.GetParameter();

                    // 差を計算
                    auto parameter = *para_begin;
                    auto position = para_end->position - para_begin->position;
                    auto rotation = para_end->rotation - para_begin->rotation;
                    auto scale = para_end->scale - para_begin->scale;

                    // 領域は確保されているので、領域の数分実行
                    for (int i = frame; i < frame_limit; i++) {
                        parameter.position = parameter.position + (position / (float)animation.one_motion_frame);
                        parameter.rotation = parameter.rotation + (rotation / (float)animation.one_motion_frame);
                        parameter.scale = parameter.scale + (scale / (float)animation.one_motion_frame);
                        linear_datas.linear[i](parameter);
                    }
                }
                frame_limit += animation.one_motion_frame;
                frame += animation.one_motion_frame;
            }
        }
    protected:
        // アニメーションの更新
        void UpdateAnimation() {
            if (m_play_animation == nullptr)return;

            // ループ再生が無効である時に、処理可能フレームを超えた際に実行
            if (!m_play_animation->m_loop_frag)
                if (m_play_animation->max_frame <= m_play_animation->frame) {
                    m_play_animation->m_end_frag = true;
                    return;
                }

            // パーツごとに現在のフレームに適したアニメーションデータにアクセスを行う
            for (auto & itr : m_play_animation->parts) {
                auto & linear_object = itr.linear[m_play_animation->frame];
                itr.target->SetPosition(linear_object.position);
                itr.target->SetRotation(linear_object.rotation);
                itr.target->SetScale(linear_object.scale);
            }

            m_play_animation->frame += m_add_update_flame; // フレームカウンタ

            // ループ再生が有効時に、最大フレームを超えた際に実行
            if (m_play_animation->m_loop_frag)
                if (m_play_animation->max_frame <= m_play_animation->frame)
                    m_play_animation->frame = 0;
        }

        // モーション構成情報の保存
        void SaveMotion(const std::string & file_name) {
            auto & ofstream = mslib::DefaultSystem::BeginFstreamWritingBinary(file_name + __motion__);
            for (auto & itr1 : m_motion_data) {
                for (auto & itr2 : itr1.second.linear) {
                    // モーション名+ナンバリングIDの混合キー
                    auto first = itr1.first + __delimiter__ + std::to_string(itr2.ID) + __delimiter__;
                    ofstream << first << std::endl;
                }
            }
            mslib::DefaultSystem::EndFstreamWritingBinary();
        }

        // モーション構成情報を取得
        void LoadMotion(const std::string & file_name) {
            auto & ifstream = mslib::DefaultSystem::BeginFstreamReadingBinary(file_name + __motion__);
            for (; !ifstream.eof(); ) {
                int count_delimiter = 0;
                linear_data * pMotionData = nullptr;
                std::string reading_line_buffer;
                // read by line
                std::getline(ifstream, reading_line_buffer);
                if (reading_line_buffer == "")break;

                // read by delimiter on reading "one" line
                const char delimiter = ' ';
                std::string separated_string_buffer;
                std::istringstream line_separater(reading_line_buffer);
                std::getline(line_separater, separated_string_buffer, delimiter);

                std::string str_data;
                // 構成情報の生成
                for (auto itr1 = separated_string_buffer.begin(); itr1 != separated_string_buffer.end(); ++itr1) {
                    // デミリタ出現まで続ける
                    std::string str_;
                    str_ = (*itr1);
                    if (str_ != std::string(__delimiter__)) {
                        str_data += str_;
                        continue;
                    }
                    count_delimiter++;
                    // モーション名
                    if (count_delimiter == 1) {
                        pMotionData = &m_motion_data[str_data];
                        pMotionData->motion_name = str_data;
                    }
                    // ナンバリング
                    if (count_delimiter == 2) {
                        pMotionData->linear.emplace_back();
                        auto itr3 = --pMotionData->linear.end();
                        itr3->ID = std::stoi(str_data);
                        // 内部に、パーツの構成データを登録
                        for (auto & itr2 : m_ModelParts) {
                            itr3->pause[itr2->GetComponentName()].SetComponentName(itr2->GetComponentName());
                        }
                        break;
                    }
                    str_data.clear();
                }
            }
            mslib::DefaultSystem::EndFstreamWritingBinary();
        }

        // アニメーション構成IDの保存
        void SaveInterconnection(const std::string & file_name) {
            std::list<std::string> animation_data_list; // アニメーション構成データのリスト
            for (auto & itr1 : m_animation_data) {
                std::string str_animation_data = itr1.second.animation_name; // アクセスキー
                // アニメーション接続データを保存用データに変換します。
                for (auto & itr2 : itr1.second.m_connection_data)
                    str_animation_data += (std::string(__delimiter__) + std::to_string(itr2));
                animation_data_list.push_back(str_animation_data);
            }

            // 書き込み
            auto & ofstream = mslib::DefaultSystem::BeginFstreamWritingBinary(file_name + __animation__);
            for (auto & itr : animation_data_list)
                ofstream << itr << std::endl;
            mslib::DefaultSystem::EndFstreamWritingBinary();
        }

        // アニメーション構成IDを取得
        void LoadInterconnection(const std::string & file_name) {
            auto & ifstream = mslib::DefaultSystem::BeginFstreamReadingBinary(file_name + __animation__);
            for (; !ifstream.eof(); ) {
                std::string reading_line_buffer;
                // read by line
                std::getline(ifstream, reading_line_buffer);
                if (reading_line_buffer == "")break;

                // read by delimiter on reading "one" line
                const char delimiter = ' ';
                std::string separated_string_buffer;
                std::istringstream line_separater(reading_line_buffer);
                std::getline(line_separater, separated_string_buffer, delimiter);

                int counter = 0; // これが接続IDの数になります

                // 特定の文字列カウント
                for (auto & itr : separated_string_buffer) {
                    std::string str_s;
                    str_s = itr;
                    if (str_s == __delimiter__)
                        counter++;
                }

                // アクセスキーを取り出します。
                std::string access_key = string_search(__delimiter__, separated_string_buffer); // access key

                auto &animation_data = m_animation_data[access_key];
                animation_data.initializer();
                animation_data.initializer_connection();
                animation_data.animation_name = access_key;

                // データを取り出します。
                std::string str_id;
                for (auto itr = separated_string_buffer.begin(); itr != separated_string_buffer.end(); ) {
                    std::string str_s;
                    str_s = (*itr);

                    // 格納判定のために取り出したら破棄します
                    itr = separated_string_buffer.erase(itr);

                    // デミリタの場合
                    if (str_s != __delimiter__) {
                        str_id += str_s;
                    }
                    // デミリタである、またはコンテナが終了
                    if (str_s == __delimiter__ || itr == separated_string_buffer.end()) {
                        if ((int)str_id.size() != 0)
                            animation_data.m_connection_data.push_back(std::stoi(str_id));
                        str_id.clear();
                    }
                }
            }
            mslib::DefaultSystem::EndFstreamWritingBinary();
        }

        // ポーズデータの保存
        void SavePause(const std::string & file_name) {
            int id = 0;
            auto & ofstream = mslib::DefaultSystem::BeginFstreamWritingBinary(file_name + __data__);
            // パーツ構成データ
            for (auto & itr : m_ModelParts) {
                mslib::DefaultSystem::SaverTemplate(ofstream, GetComponentName(), dynamic_cast<Transform*>(itr), id);
                id++;
            }

            // モーションデータ
            for (auto & itr1 : m_motion_data) {
                for (auto & itr2 : itr1.second.linear) {
                    id = 0;
                    for (auto & itr3 : itr2.pause) {
                        // モーション名+ナンバリングIDとパーツ名の混合キー
                        auto first = itr1.first + __delimiter__ + std::to_string(itr2.ID) + __delimiter__ + itr3.first;
                        mslib::DefaultSystem::SaverTemplate(ofstream, first, dynamic_cast<Transform*>(&itr3.second), id);
                        id++;
                    }
                }
            }
            mslib::DefaultSystem::EndFstreamWritingBinary();
        }

        // ポーズデータの取得
        void LoadPause(const std::string & file_name) {
            int id = 0;
            auto & ifstream = mslib::DefaultSystem::BeginFstreamReadingBinary(file_name + __data__);

            // パーツ構成データ
            for (auto & itr : m_ModelParts) {
                mslib::DefaultSystem::LoaderTemplate(ifstream, GetComponentName(), dynamic_cast<Transform*>(itr), id);
                id++;
            }
            // ポーズデータ
            for (auto & itr1 : m_motion_data) {
                for (auto & itr2 : itr1.second.linear) {
                    id = 0;
                    for (auto & itr3 : itr2.pause) {
                        // モーション名+ナンバリングIDとパーツ名の混合キー
                        auto first = itr1.first + __delimiter__ + std::to_string(itr2.ID) + __delimiter__ + itr3.first;
                        mslib::DefaultSystem::LoaderTemplate(ifstream, first, dynamic_cast<Transform*>(&itr3.second), id);
                        id++;
                    }
                }
            }
            mslib::DefaultSystem::EndFstreamReadingBinary();
        }
    private:
        // 文字列検索
        std::string string_search(const std::string & delimiter, std::string & data) {
            std::string str;
            for (auto itr = data.begin(); itr != data.end(); ) {
                std::string str_s;
                str_s = (*itr);
                if (str_s == delimiter) break;
                str += str_s;
                itr = data.erase(itr);
            }
            return str;
        }
    protected:
        type_name * m_ParenModel; // 全パーツの親オブジェクト
        std::list<type_name*> m_ModelParts; // パーツリスト
        motion_data m_motion_data; // ポーズデータ
        animation_data m_animation_data; // アニメーションデータ
        animation_parameter * m_play_animation; // 再生アニメーション
        int m_add_update_flame; // 更新
    };

    //==========================================================================
    //
    // class  : AnimationShaderModel
    // Content: アニメーションシェーダーモデル
    //
    //==========================================================================
    class AnimationShaderModel : public AnimationModelTemplate, public ShaderModel::SetEffectParameter
    {
    public:
        AnimationShaderModel();
        ~AnimationShaderModel();
        type_name * AddParts2(const std::string & parts_name, const mslib::xmodel::XModelReference & xmodel_data, const std::string & paren_name = __none__);
    };
}
