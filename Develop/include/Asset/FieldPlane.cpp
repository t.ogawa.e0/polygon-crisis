//==========================================================================
// ���� [FieldPlane.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "FieldPlane.h"

namespace common
{
    std::list<FieldPlane*> FieldPlane::m_FieldPlane;

    FieldPlane::FieldPlane()
    {
        m_FieldPlane.push_back(this);
    }

    FieldPlane::~FieldPlane()
    {
        auto itr = std::find(m_FieldPlane.begin(), m_FieldPlane.end(), this);
        if (itr == m_FieldPlane.end())return;
        m_FieldPlane.erase(itr);
    }
    std::list<FieldPlane*>& FieldPlane::GetAllFieldPlane()
    {
        return m_FieldPlane;
    }
}