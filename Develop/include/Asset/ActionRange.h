//==========================================================================
// 行動範囲 [ActionRange.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : ActionRange
	// Content: 行動範囲
	//
	//==========================================================================
	class ActionRange : public mslib::ObjectManager
    {
    public:
        ActionRange();
        ActionRange(const std::string & ObjectName, int NumActionRange);
        ~ActionRange();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;

		/**
		@brief 範囲データの取得
		*/
		std::list<mslib::transform::Transform*> GetActionRangeData();

		/**
		@brief 全体の範囲データの取得
		*/
		static const std::list<mslib::transform::Transform*> & GetAllActionRangeData();
    protected:
        int m_NumActionRange; // 行動範囲用オブジェクトの数
        static std::list<mslib::transform::Transform*>m_AllActionRangeData; // 行動範囲データ
        std::list<mslib::transform::Transform*>m_ActionRangeData; // 行動範囲データ
    };
}