//==========================================================================
// TPSカメラ [TPSCamera.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
    //==========================================================================
    //
    // class  : TPSCamera
    // Content: TPSカメラ
    //
    //==========================================================================
    class TPSCamera : public mslib::ObjectManager
    {
    public:
        TPSCamera();
        ~TPSCamera();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;

        void SetObjectSmooth(mslib::transform::Transform * object);
        void SetObjectPerfection(mslib::transform::Transform * object);
    private:
        mslib::xinput::XInput * m_controller; // コントローラー
    };
}