//==========================================================================
// フィールドのコリジョン [FieldCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : FieldCollider
	// Content: フィールドのコリジョン
	//
	//==========================================================================
	class FieldCollider
    {
    public:
        FieldCollider();
        ~FieldCollider();
		// 全てのフィールドコリジョンの取得
        static std::list<FieldCollider*> & GetAllFieldCollider();
    protected:
        static std::list<FieldCollider*> m_FieldCollider; // コリジョン
    };
}