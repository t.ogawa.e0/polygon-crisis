//==========================================================================
// FieldBox [FieldBox.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "FieldBox.h"

namespace common
{
    std::list<FieldBox*> FieldBox::m_FieldBox;

    FieldBox::FieldBox()
    {
        m_FieldBox.push_back(this);
    }

    FieldBox::~FieldBox()
    {
        auto itr = std::find(m_FieldBox.begin(), m_FieldBox.end(), this);
        if (itr == m_FieldBox.end())return;
        m_FieldBox.erase(itr);
    }
    std::list<FieldBox*>& FieldBox::GetAllFieldBox()
    {
        return m_FieldBox;
    }
}