//==========================================================================
// キャラクター [Character.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "common/FunctionComponent.h"
#include "common/Initializer.h"

namespace common
{
    //==========================================================================
    //
    // class  : Character
    // Content: キャラクター 
    //
    //==========================================================================
    class Character : public mslib::component::Component, public mslib::initializer::Initializer
    {
    public:
        Character();
        Character(bool set);
        ~Character();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief コリジョン更新
        */
        void Collider();

        /**
        @brief 対Box壁コリジョン
        */
        mslib::collider::Collider * GetVersusWallBox();

        /**
        @brief 対高台コリジョン
        */
        mslib::collider::Collider * GetVersusHighGroundUnder();

        /**
        @brief 対高台上向きコリジョン
        */
        mslib::collider::Collider * GetVersusHighGroundUp();

        /**
        @brief 対Box高台コリジョン
        */
        mslib::collider::Collider * GetVersusHighGroundBox();

        /**
        @brief 対マップコリジョン
        */
        mslib::collider::Collider * GetVersusMap();

        /**
        @brief 重力機能
        */
        mslib::gravity::Gravity * GetGravity();

        /**
        @brief パラメーター取得
        */
        mslib::transform::Parameter & GetParameter();

        /**
        @brief パラメーターセット
        */
        void SetParameter(mslib::transform::Transform * obj);

        /**
        @brief キャラクターの取得
        */
        static std::list<Character*> & GetAllCharacter();
    protected:
        mslib::collider::Collider * m_Map; // マップとの対抗コリジョン
        mslib::collider::Collider * m_WallBox; // 壁型との対抗コリジョン
        mslib::collider::Collider * m_HighGroundCollBox; // 高台との対抗コリジョン
        mslib::collider::Collider * m_HighGroundCollUnder; // 高台の下に落ちないための対抗コリジョン
        mslib::collider::Collider * m_HighGroundCollUp; // 高台の上にいるための対抗コリジョン
        mslib::gravity::Gravity * m_Gravity; // 重力
        mslib::transform::Parameter m_OldParameter; // 古い座標
    protected:
        static std::list<Character*> m_character; // 全キャラクター
    };

	//==========================================================================
	//
	// class  : PlayerCharacter
	// Content: プレイヤーキャラクター
	//
	//==========================================================================
	class PlayerCharacter : public Character
    {
    public:
        PlayerCharacter();
        PlayerCharacter(bool set);
        ~PlayerCharacter();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 防御用コリジョン
        */
        mslib::collider::BoxCollider * GetDefenseCollider();

        /**
        @brief プレイヤー用コリジョン
        */
        mslib::collider::BoxCollider * GetPlayerCollider();

        /**
        @brief ターゲット用オブジェクト
        */
        mslib::transform::Transform * GetTargetObject();

		// 全てのプレイヤーを取得
        static std::list<PlayerCharacter*> & GetAllPlayerCharacter();

		// プレイヤーを取得
        static PlayerCharacter * GetPlayerCharacter(mslib::transform::Transform*obj);
    protected:
        mslib::collider::BoxCollider * m_DefenseCollider; // 防御コリジョン
        mslib::collider::BoxCollider * m_PlayerCollider; // プレイヤーのコリジョン
        mslib::transform::Transform * m_TargetObject; // ターゲットとしたオブジェクト
        static std::list<PlayerCharacter*> m_PlayerCharacter; // プレイヤーのみ
    };

	//==========================================================================
	//
	// class  : EnemyCharacter
	// Content: エネミーキャラクター
	//
	//==========================================================================
	class EnemyCharacter : public Character
    {
    public:
        EnemyCharacter();
        EnemyCharacter(bool set);
        ~EnemyCharacter();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief ターゲット用オブジェクト
        */
        mslib::transform::Transform * GetTargetObject();

		// 全てのエネミーを取得
        static std::list<EnemyCharacter*> & GetAllEnemyCharacter();

		// エネミーの取得
        static EnemyCharacter * GetEnemyCharacter(mslib::transform::Transform*obj);
    protected:
        mslib::transform::Transform * m_TargetObject; // ターゲットとしたオブジェクト
        static std::list<EnemyCharacter*> m_EnemyCharacter; // エネミーのみ
    };
}