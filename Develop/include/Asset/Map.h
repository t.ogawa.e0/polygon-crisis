//==========================================================================
// Map [Map.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
    //==========================================================================
    //
    // class  : Map
    // Content: マップ
    //
    //==========================================================================
    class Map : public mslib::ObjectManager
    {
    protected:
        struct Data
        {
            int num = 0; // データ数
            std::list<void*> list; // データ
        };
    public:
        Map();
        ~Map();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    protected:
		// ImGui
        void FloorSystem();
		// 地面追加
        void AddFloor(std::list<void*> & list, int id);
		// ImGui
        void WallSystem();
		// 壁の追加
        void AddWall(std::list<void*> & list, int id);
		// ImGui
        void HighGroundSystem();
		// 高台の追加
        void AddHighGround(std::list<void*> & list, int id);
		// ImGui
        void NormalObjectSystem();
		// 判定無し描画オブジェクトの追加
        void AddNormalObject(std::list<void*> & list, int id);
		// 読み込み
        void Load();
		// 保存
        void Save();

		// 破棄機能のテンプレート
        template<typename _Ty>
        void RemoveSystem(int data_id, const std::string & menu_name) {
            // 地面の破棄
            if (ImGui()->NewMenu("Remove " + menu_name))
            {
                auto obj = Object()->Get(data_id);
                auto data = m_data.Get(data_id);

				// どちらかのデータが nullptr の場合終了
                if (obj == nullptr || data == nullptr) {
                    ImGui()->EndMenu();
                    return;
                }

                // 地面リスト
                for (auto itr = data->list.begin(); itr != data->list.end(); )
                {
                    auto item = mslib::cast<_Ty>((*itr));

                    // 選択されない限りコンテニュー
                    if (!ImGui()->MenuItem(item->GetComponentName()))
                    {
                        ++itr;
                        continue;
                    }

                    // オブジェクトを破棄
                    itr = data->list.erase(itr);
                    obj->DestroyComponent(item);
                    data->num--;

                    int id_count = 0;
                    for (auto & itr_list : data->list)
                    {
                        auto rename = mslib::cast<_Ty>(itr_list);

                        rename->SetComponentName(menu_name + std::to_string(id_count));
                        id_count++;
                    }
                    data->num = id_count;
                }
                ImGui()->EndMenu();
            }
        }
    protected:
        mslib::wrapper::vector<Data> m_data; // データ
        void * m_LightObject; // 光源のポインタ
        void * m_TPSCamera; // カメラのポインタ
    };
}
