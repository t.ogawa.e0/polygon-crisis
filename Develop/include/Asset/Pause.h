//==========================================================================
// ポーズ [Pause.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
    //==========================================================================
    //
    // class  : Pause
    // Content: ポーズ 
    //
    //==========================================================================
    class Pause : public mslib::ObjectManager
    {
    public:
        Pause();
        ~Pause();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
		// 停止
		void ActivityON();
		// 停止解除
		void ActivityOFF();
    protected:
		mslib::xinput::XInput * m_ControllerObject; // コントローラー
		mslib::sprite::Sprite * m_RestartFrame; // 2Dオブジェクト
		mslib::sprite::Sprite * m_EndFrame; // 2Dオブジェクト
		int m_index_id; // 選択ID
		bool m_init; // 初期化フラグ
        bool m_act_key; // ポーズキー
		int m_se_label; // SEのサウンドlabel
    };
}