//==========================================================================
// アニメーションモデル [AnimationModel.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "AnimationModel.h"

namespace animation_model
{
    AnimationShaderModel::AnimationShaderModel()
    {
        SetComponentName("AnimationShaderModel");
        m_camera = nullptr;
    }
    AnimationShaderModel::~AnimationShaderModel()
    {
    }
    AnimationShaderModel::type_name * AnimationShaderModel::AddParts2(const std::string & parts_name, const mslib::xmodel::XModelReference & xmodel_data, const std::string & paren_name)
    {
        auto obj = AddParts(parts_name, paren_name);
        obj->SetEffectData(m_EffectData);
        obj->SetXModelData(xmodel_data);
        obj->SetCamera(m_camera);
        obj->SetLight(m_light);
        return obj;
    }
}