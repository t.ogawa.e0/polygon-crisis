//==========================================================================
// ゲームの終了判定 [GameEnd.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : GameEnd
	// Content: ゲームの終了判定
	//
	//==========================================================================
	class GameEnd : public mslib::ObjectManager
	{
	public:
		GameEnd();
		~GameEnd();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;

		// ゲームオーバー
		void GameOver();

		// ゲームクリア
		void GameClear();
	protected:
		mslib::sprite::Sprite * m_GameOver; // ゲームオーバーオブジェクト
		mslib::sprite::Sprite * m_GameClear; // ゲームクリアオブジェクト
		bool m_init; // 初期化フラグ
		bool m_end_count; // 終了カウント
		int m_end_time; // 終了までの時間
	};
}
