//==========================================================================
// 高台コリジョン [FieldHighGround.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldCollider.h"
#include "FieldBox.h"
#include "FieldPlane.h"

namespace common
{
	//==========================================================================
	//
	// class  : FieldHighGround
	// Content: 高台コリジョン
	//
	//==========================================================================
	class FieldHighGround : public mslib::component::Component
    {
    public:
        FieldHighGround();
        ~FieldHighGround();
		// ボックスの取得
        common::FieldBox * GetFieldBox();
		// 下判定の平面
        common::FieldPlane * GetFieldPlane();
		// 上判定の平面
        common::FieldPlane * GetFieldUpPlane();
		// 全ての高台コリジョンの取得
        static std::list<FieldHighGround*> & GetAllFieldHighGround();
    protected:
        common::FieldBox * m_FieldBox; // Box
        common::FieldPlane * m_FieldPlane; // 平面
        common::FieldPlane * m_FieldUpPlane; // 平面
        static std::list<FieldHighGround*> m_FieldHighGround; // 全ての高台管理
    };
}