//==========================================================================
// ジャンプ [Jump.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : Jump
	// Content: ジャンプ
	//
	//==========================================================================
	class Jump : public mslib::function::FunctionComponent
    {
    public:
        Jump();
        ~Jump();
        // 更新
        void Update() override;
        // ジャンプ
        void Play(float powor, float one_frame_powor);
        // ジャンプ中かどうか
        bool Trigger();
    protected:
        float m_powor; // ジャンプ力
        float m_one_frame_powor; // 1フレームで上がる力
        bool m_flag; // 実行判定
    };
}