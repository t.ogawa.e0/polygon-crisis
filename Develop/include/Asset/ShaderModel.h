//==========================================================================
// シェーダーモデル [ShaderModel.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "common\Object.h"
#include "common\Effect.h"
#include "common\Texture.h"
#include "common\Camera.h"
#include "common\Light.h"

namespace ShaderModel
{
    //==========================================================================
    //
    // class  : SetEffectParameter
    // Content: シェーダーを利用した拡張データ
    //
    //==========================================================================
    class SetEffectParameter : public mslib::effect::SetEffect
    {
    public:
        SetEffectParameter();
        virtual ~SetEffectParameter();
        /**
        @brief カメラの登録
        */
        void SetCamera(mslib::camera::Camera * camera);

        /**
        @brief ライトの登録
        */
        void SetLight(mslib::object::Light * light);
    protected:
        /**
        @brief 更新可能か
        @return 更新可能な場合 true が返ります
        */
        bool CanIRenew();
    protected:
        mslib::camera::Camera * m_camera; // カメラ
        mslib::object::Light * m_light; // ライトオブジェクト
    };

    //==========================================================================
    //
    // class  : ShaderObject
    // Content: シェーダーオブジェクト
    //
    //==========================================================================
    class ShaderObject : public mslib::object::Object, public SetEffectParameter
    {
    public:
        ShaderObject();
        virtual ~ShaderObject();
    };

    //==========================================================================
    //
    // class  : ShaderXModel
    // Content: シェーダーXModel
    //
    //==========================================================================
    class ShaderXModel : public mslib::object::XModel, public SetEffectParameter
    {
    public:
        ShaderXModel();
        virtual ~ShaderXModel();

        /**
        @brief モデルデータの登録
        @param data [in] モデルデータ
        */
        void SetXModelData(const mslib::xmodel::XModelReference & data);
    private:
    };

    //==========================================================================
    //
    // class  : PerPixelLightingModel
    // Content: パーピクセル・ライティングモデル
    //
    //==========================================================================
    class PerPixelLightingModel : public ShaderXModel
    {
    public:
        PerPixelLightingModel();
        virtual ~PerPixelLightingModel();

        /**
        @brief 描画
        @param device [in] デバイス
        */
        void Draw(LPDIRECT3DDEVICE9 device) override;
    };

    //==========================================================================
    //
    // class  : ToonShaderModel
    // Content: トゥーンシェーダーモデル
    //
    //==========================================================================
    class ToonShaderModel : public ShaderXModel, public mslib::texture::SetTexture
    {
    public:
        ToonShaderModel();
        virtual ~ToonShaderModel();

        /**
        @brief 描画
        @param device [in] デバイス
        */
        void Draw(LPDIRECT3DDEVICE9 device) override;
    private:
        void DrawShaderModel(LPDIRECT3DDEVICE9 device);
    };

    //==========================================================================
    //
    // class  : PerPixelLightingCube
    // Content: パーピクセル・ライティングキューブ
    //
    //==========================================================================
    class PerPixelLightingCube : public mslib::object::Cube, public SetEffectParameter
    {
    public:
        PerPixelLightingCube();
        ~PerPixelLightingCube();

        /**
        @brief 描画
        @param device [in] デバイス
        */
        void Draw(LPDIRECT3DDEVICE9 device) override;
    };
}

