//==========================================================================
// コントローラーオブジェクト [ControllerObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
    //==========================================================================
    //
    // class  : ControllerObject
    // Content: コントローラーオブジェクト
    //
    //==========================================================================
    class ControllerObject : public mslib::ObjectManager
    {
    public:
        ControllerObject();
        ControllerObject(int numXInput);
        ~ControllerObject();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    private:
        int m_numXInput; // 数
    };
}
