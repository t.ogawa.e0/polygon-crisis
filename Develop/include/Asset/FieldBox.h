//==========================================================================
// フィールドの箱 [FieldBox.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldCollider.h"

namespace common
{
	//==========================================================================
	//
	// class  : FieldBox
	// Content: フィールドの箱
	//
	//==========================================================================
	class FieldBox : public FieldCollider, public mslib::collider::BoxCollider
    {
    public:
        FieldBox();
        ~FieldBox();
		// 全ての箱を取得
        static std::list<FieldBox*> & GetAllFieldBox();
    protected:
        static std::list<FieldBox*> m_FieldBox; // 箱
    };
}