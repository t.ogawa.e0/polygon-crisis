//==========================================================================
// �W�����v [Jump.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Jump.h"

namespace common
{
    using namespace mslib;

    Jump::Jump()
    {
        SetComponentName("Jump");
        m_powor = 0.0f;
        m_one_frame_powor = 0.0f;
        m_flag = false;
        AddActivity();
    }

    Jump::~Jump()
    {
    }
    void Jump::Update()
    {
        m_flag = false;

        auto obj = GetWarrantyParent<transform::Transform>();
        if (obj == nullptr)return;
        if (m_powor < 0.0f)return;

        obj->AddPosition(0, m_powor, 0);
        m_powor -= m_one_frame_powor;
        m_flag = true;
    }
    void Jump::Play(float powor, float one_frame_powor)
    {
        m_powor = powor;
        m_one_frame_powor = one_frame_powor;
    }
    bool Jump::Trigger()
    {
        return m_flag;
    }
}