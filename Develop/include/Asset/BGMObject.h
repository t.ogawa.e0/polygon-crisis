//==========================================================================
// BGMObject [BGMObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : BGMObject
	// Content: BGMObject
	//
	//==========================================================================
	class BGMObject : public mslib::ObjectManager
	{
	public:
		BGMObject();
		BGMObject(const std::string & bgm_file);
		~BGMObject();
		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;
	protected:
		std::string m_bgm_file; // ファイルパス
		int m_bgm_label; // ラベル
		bool m_flag; // 再生フラグ
	};
}