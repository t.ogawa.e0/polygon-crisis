//==========================================================================
// ステップ [Step.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : Step
	// Content: ステップ
	//
	//==========================================================================
	class Step : public mslib::function::FunctionComponent
    {
    public:
        Step();
        ~Step();
        // 更新
        void Update() override;
        // ステップ
        void Play(float powor, float one_frame_powor);
        // ステップ中かどうか
        bool Trigger();
    protected:
        float m_powor;
        float m_one_frame_powor; // フレームで減少する量
        bool m_flag;
    };
}