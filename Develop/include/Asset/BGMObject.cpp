//==========================================================================
// BGMObject [BGMObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BGMObject.h"
#include "resource_list.h"

namespace common
{
	BGMObject::BGMObject() : ObjectManager("BGMObject")
	{
		m_bgm_label = 0;
		m_flag = false;
	}

	BGMObject::BGMObject(const std::string & bgm_file) : ObjectManager("BGMObject"), m_bgm_file(bgm_file)
	{
		m_bgm_label = 0;
		m_flag = false;
	}

	BGMObject::~BGMObject()
	{
		XAudio()->Stop(m_bgm_label);
	}
	void BGMObject::Init()
	{
		auto sound = mslib::xaudio2::XAudio2SoundLabel(m_bgm_file, -1, 1.0f);
		XAudio()->Init(sound, m_bgm_label);
	}
	void BGMObject::Update()
	{
		if (m_flag == false)
		{
			XAudio()->Play(m_bgm_label);
			m_flag = true;
		}
	}
	void BGMObject::Debug()
	{
	}
}