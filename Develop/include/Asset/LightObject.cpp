//==========================================================================
// ���C�g [LightObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LightObject.h"

namespace common
{
    LightObject::LightObject() : ObjectManager("LightObject")
    {
    }

    LightObject::~LightObject()
    {
    }
    void LightObject::Init()
    {
        auto light = Light()->Create();
        light->Enable(0, true);
        light->SetType(D3DLIGHTTYPE::D3DLIGHT_DIRECTIONAL);

		auto effect = light->AddComponent<mslib::object::Effekseer>();
		effect->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/flare.efk", "flare"));
		effect->SetComponentName("Flare");
		effect->Play();
    }
    void LightObject::Update()
    {
    }
    void LightObject::Debug()
    {
    }
}