//==========================================================================
// スカイドーム [SkyDome.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SkyDome.h"
#include "resource_list.h"

namespace common
{
    SkyDome::SkyDome() : ObjectManager("SkyDome")
    {
    }

    SkyDome::~SkyDome()
    {
    }
    void SkyDome::Init()
    {
        auto obj = Sphere()->Create();

        obj->SetComponentName("SkyDomeObject");
        obj->SetSphereData(GetCreateSphere()->Create(20));
        obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_skydome_DDS));
    }
    void SkyDome::Update()
    {
    }
    void SkyDome::Debug()
    {
    }
}