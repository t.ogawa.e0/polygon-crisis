//==========================================================================
// ポーズ [Pause.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Pause.h"
#include "Asset/ControllerObject.h"
#include "System/Screen.h"
#include "Result/Scene.h"
#include "resource_list.h"

namespace common
{
    const std::string bool_text(bool flag) 
    {
        return flag == true ? "true" : "false";
    }

    Pause::Pause() : ObjectManager("Pause")
    {
        m_act_key = false;
		m_ControllerObject = nullptr;
		m_RestartFrame = nullptr;
		m_EndFrame = nullptr;
		m_init = false;
		m_index_id = 0;
		m_se_label = 0;
    }

    Pause::~Pause()
    {
    }
    void Pause::Init()
    {
		// 初期化
		auto & WinSize = GetDevice()->GetWindowsSize();
		m_ControllerObject = GetParent()->GetComponent<common::ControllerObject>()->XInput_();

		auto obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Pause_DDS));
		obj->SetComponentName("PauseSprite");

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Restart_DDS));
		obj->SetComponentName("RestartSprite");

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_RestartFrame_DDS));
		obj->SetComponentName("RestartFrameSprite");
		m_RestartFrame = obj;

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_End_DDS));
		obj->SetComponentName("EndSprite");

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_EndFrame_DDS));
		obj->SetComponentName("EndFrameSprite");
		m_EndFrame = obj;

		obj = Sprite()->Create();
		obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));
		obj->SetComponentName("Background");
		obj->SetPolygonSize((float)WinSize.x, (float)WinSize.y);

		auto sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_select_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label);
	}
    void Pause::Update()
    {
		constexpr int index = 0;

		// 初回のみ
		if (m_init == false)
		{
			ActivityOFF();
			m_init = true;
		}

		// コントローラーのチェック
		if (!m_ControllerObject->Check(index))return;

		// ポーズ切り替え
		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::START, index))
		{
			mslib::bool_change(m_act_key);
			XAudio()->Play(m_se_label);
			if (m_act_key)
				ActivityON();
			if (!m_act_key)
				ActivityOFF();
		}

		// ポーズが有効ではない場合終了
		if (!m_act_key)return;

		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::DPAD_UP, index))
		{
			m_index_id = 0;
			m_RestartFrame->SetActivity(true);
			m_EndFrame->SetActivity(false);
			XAudio()->Play(m_se_label);
		}
		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::DPAD_DOWN, index))
		{
			m_index_id = 1;
			m_RestartFrame->SetActivity(false);
			m_EndFrame->SetActivity(true);
			XAudio()->Play(m_se_label);
		}

		// 確定時、選択IDに合わせた処理を実行
		if (m_ControllerObject->Trigger(mslib::xinput::XInputButton::B, index))
		{
			XAudio()->Play(m_se_label);
			if (m_index_id == 0)
			{
				ActivityOFF();
				return;
			}
			else if (m_index_id == 1)
			{
				CScreen::ScreenChange(new Result::Scene);
				return;
			}
		}
	}
    void Pause::Debug()
    {
        if (ImGui()->Button("Pause Key [" + bool_text(m_act_key) + "]" + mslib::text("Num = %d", (int)GetAllActivity().size())))
        {
            mslib::bool_change(m_act_key);
			if (m_act_key)
				ActivityON();
			if (!m_act_key)
				ActivityOFF();
        }
    }
	void Pause::ActivityON()
	{
		m_index_id = 0;
		for (int i = 0; i < Sprite()->Size(); i++)
			Sprite()->Get(i)->SetActivity(true);
		m_RestartFrame->SetActivity(true);
		m_EndFrame->SetActivity(false);
		m_act_key = true;
		for (auto itr : GetAllActivity())
			itr->SetActivity(false);
	}
	void Pause::ActivityOFF()
	{
		m_index_id = 0;
		for (int i = 0; i < Sprite()->Size(); i++)
			Sprite()->Get(i)->SetActivity(false);
		m_RestartFrame->SetActivity(false);
		m_EndFrame->SetActivity(false);
		m_act_key = false;
		for (auto itr : GetAllActivity())
			itr->SetActivity(true);
	}
}