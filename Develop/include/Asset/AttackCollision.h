//==========================================================================
// 攻撃コリジョン [AttackCollision.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
	//==========================================================================
	//
	// class  : AttackCollision
	// Content: 攻撃コリジョン
	//
	//==========================================================================
	class AttackCollision : public mslib::object::Object
    {
    public:
        AttackCollision();
        ~AttackCollision();
		// コリジョン追加
        mslib::collider::Collider * AddCollider();
		// コリジョンの取得
        mslib::collider::Collider * GetCollider();
		// 破棄フラグの取得
        bool GetDeleteFlag();
		// 全ての攻撃コリジョンを取得
        static std::list<AttackCollision*> & GetAllAttackCollision();
		// 攻撃コリジョンこ破棄
        static void DestroyAttackCollision(AttackCollision *& obj);
    protected:
        static std::list<AttackCollision*>m_AttackCollision; // 全攻撃コリジョン
        mslib::collider::Collider * m_collider; // コリジョン
        bool m_delete_flag; // 破棄命令
    };
}
