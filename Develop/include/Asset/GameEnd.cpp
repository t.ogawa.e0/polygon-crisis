//==========================================================================
// ゲームの終了判定 [GameEnd.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameEnd.h"
#include "System/Screen.h"
#include "Result/Scene.h"
#include "Enemy/EnemyManager.h"
#include "resource_list.h"

namespace common
{
	// 終了時間の定義
	constexpr int __EndTime__ = mslib::Seconds(2);

	GameEnd::GameEnd() :ObjectManager("GameEnd")
	{
		m_end_count = false;
		m_init = false;
		m_end_time = __EndTime__;
	}

	GameEnd::~GameEnd()
	{
	}
	void GameEnd::Init()
	{
		m_GameOver = Sprite()->Create();
		m_GameOver->SetTextureData(GetTextureLoader()->Load(RESOURCE_GameOver_DDS));
		m_GameOver->SetComponentName("GameOverUI");
		m_GameOver->SetActivity(false);

		m_GameClear = Sprite()->Create();
		m_GameClear->SetTextureData(GetTextureLoader()->Load(RESOURCE_GameClear_DDS));
		m_GameClear->SetComponentName("GameClearUI");
		m_GameClear->SetActivity(false);
	}
	void GameEnd::Update()
	{
		// 初回のみ
		if (m_init == false)
		{
			m_GameOver->SetActivity(false);
			m_GameClear->SetActivity(false);
			m_init = true;
		}

		// 終了カウント発生していない際の制御
		if (!m_end_count)
		{
			// 生成数と、サイズが一致しないときに実行。エディタモード時の速終了回避
			if (enemy::EnemyManager::GetGeneratedSize() != enemy::EnemyManager::GetSize())
			{
				// エネミーが0の時にクリア判定を出す
				if (enemy::EnemyManager::GetSize() == 0)
				{
					GameClear();
				}
			}
		}

		// 終了判定が有効
		if (m_end_count)
		{
			m_end_time--;
			if (m_end_time < 0)
			{
				CScreen::ScreenChange(new Result::Scene);
				m_end_count = false;
			}
		}
	}
	void GameEnd::Debug()
	{
		if (ImGui()->Button("GameClear"))
			GameClear();
		if (ImGui()->Button("GameOver"))
			GameOver();
	}
	void GameEnd::GameOver()
	{
		m_end_count = true;
		m_GameOver->SetActivity(true);
	}
	void GameEnd::GameClear()
	{
		m_end_count = true;
		m_GameClear->SetActivity(true);
	}
}