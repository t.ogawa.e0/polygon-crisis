//==========================================================================
// シェーダーモデル [ShaderModel.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ShaderModel.h"

namespace ShaderModel
{
    //==========================================================================
    //
    // class  : SetEffectParameter
    // Content: シェーダーを利用した拡張データ
    //
    //==========================================================================
    SetEffectParameter::SetEffectParameter()
    {
        m_camera = nullptr;
        m_light = nullptr;
    }
    SetEffectParameter::~SetEffectParameter()
    {
    }
    void SetEffectParameter::SetCamera(mslib::camera::Camera * camera)
    {
        m_camera = camera;
    }

    void SetEffectParameter::SetLight(mslib::object::Light * light)
    {
        m_light = light;
    }

    bool SetEffectParameter::CanIRenew()
    {
        return m_camera != nullptr&&m_light != nullptr ? true : false;
    }

    //==========================================================================
    //
    // class  : ShaderObject
    // Content: シェーダーオブジェクト
    //
    //==========================================================================
    ShaderObject::ShaderObject()
    {
        SetComponentName("ShaderObject");
        m_camera = nullptr;
        m_light = nullptr;
    }

    ShaderObject::~ShaderObject() {}

    //==========================================================================
    //
    // class  : ShaderXModel
    // Content: シェーダーXModel
    //
    //==========================================================================
    ShaderXModel::ShaderXModel()
    {
        SetComponentName("ShaderXModel");
    }
    ShaderXModel::~ShaderXModel()
    {
    }
    void ShaderXModel::SetXModelData(const mslib::xmodel::XModelReference & data)
    {
        m_XModelData = data;

        for (auto & itr : m_XModelData.MatD3D())
        {
            if (itr.Diffuse.a == 0.0f)
                itr.Diffuse.a = 1.0f;

            if (itr.Power == 0.0f)
                itr.Power = 1.0f;

            itr.Ambient = itr.Emissive = itr.Specular = itr.Diffuse;
        }
    }

    //==========================================================================
    //
    // class  : PerPixelLightingModel
    // Content: パーピクセル・ライティングモデル
    //
    //==========================================================================
    PerPixelLightingModel::PerPixelLightingModel()
    {
        SetComponentName("PerPixelLightingModel");
    }

    PerPixelLightingModel::~PerPixelLightingModel() {}

    void PerPixelLightingModel::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_XModelData.Existence() && m_EffectData.Existence() && CanIRenew())
        {
            // シェーダ開始
            auto &diffuse = GetD3DMaterial9().Diffuse;
            auto &ambient = GetD3DMaterial9().Ambient;
            auto &specular = GetD3DMaterial9().Specular;
            auto &emmisive = GetD3DMaterial9().Emissive;

			auto &light_diffuse = m_light->GetD3DMaterial9().Diffuse;
			auto &light_ambient = m_light->GetD3DMaterial9().Ambient;
			auto &light_specular = m_light->GetD3DMaterial9().Specular;
			auto &light_emmisive = m_light->GetD3DMaterial9().Emissive;

            //平行光源の位置ベクトルから方向ベクトルを計算する
            D3DXVECTOR4 LightDir;
            m_light->GetDirection(*this, LightDir);

            auto pCamera = m_camera;
            if (pCamera->GetCamera() != nullptr)
                pCamera = pCamera->GetCamera();

            UINT numPass = 0;
            D3DXVECTOR3 eye = pCamera->GetLook1().eye + pCamera->GetLook2().eye;
            D3DXVECTOR4 v4diffuse1(diffuse.r, diffuse.g, diffuse.b, diffuse.a); // 平行光源の色
            D3DXVECTOR4 v4ambient1(ambient.r, ambient.g, ambient.b, ambient.a); // 環境光
            D3DXVECTOR4 v4specular1(specular.r, specular.g, specular.b, specular.a); // スペキュラ
            D3DXVECTOR4 v4emmisive1(emmisive.r, emmisive.g, emmisive.b, emmisive.a); // エミッシブ
			D3DXVECTOR4 v4diffuse2(light_diffuse.r, light_diffuse.g, light_diffuse.b, light_diffuse.a); // 平行光源の色
			D3DXVECTOR4 v4ambient2(light_ambient.r, light_ambient.g, light_ambient.b, light_ambient.a); // 環境光
			D3DXVECTOR4 v4specular2(light_specular.r, light_specular.g, light_specular.b, light_specular.a); // スペキュラ
			D3DXVECTOR4 v4emmisive2(light_emmisive.r, light_emmisive.g, light_emmisive.b, light_emmisive.a); // エミッシブ
			D3DXVECTOR4 v4camerapos(eye.x, eye.y, eye.z, 0.0f);
            D3DXVECTOR4 tempcolor;
            D3DXMATRIX ViewMatrix;
            D3DXMATRIX ProjectionMatrix;

            device->GetTransform(D3DTS_VIEW, &ViewMatrix);
            device->GetTransform(D3DTS_PROJECTION, &ProjectionMatrix);

            m_EffectData->SetTechnique("PerPixelLighting");
            m_EffectData->Begin(&numPass, 0);
            m_EffectData->BeginPass(0);

            // 定数をセット(頂点シェーダー)
            m_EffectData->SetMatrix("World", &m_WorldMatrix);
            m_EffectData->SetMatrix("View", &ViewMatrix);
            m_EffectData->SetMatrix("Projection", &ProjectionMatrix);

            // 環境光
            m_EffectData->SetVector("Diffuse", &v4diffuse2);
            m_EffectData->SetVector("Ambient", &v4ambient2);
            m_EffectData->SetVector("Specular", &v4specular2);
            m_EffectData->SetVector("Emmisive", &v4emmisive2);

            // 平行光源
            m_EffectData->SetVector("LightDir", &LightDir);

            // カメラ位置
            m_EffectData->SetVector("CameraPos", &v4camerapos);

            // マテリアル自体の色反映
            for (int i = 0; i < (int)m_XModelData.NumMesh(); i++)
            {
                if (!m_XModelData.Texture()[i].Existence())continue;

				tempcolor.x = m_XModelData.MatD3D()[i].Diffuse.r*v4diffuse1.x;
				tempcolor.y = m_XModelData.MatD3D()[i].Diffuse.g*v4diffuse1.y;
				tempcolor.z = m_XModelData.MatD3D()[i].Diffuse.b*v4diffuse1.z;
				tempcolor.w = m_XModelData.MatD3D()[i].Diffuse.a*v4diffuse1.w;
				m_EffectData->SetVector("DiffuseMatrix", &tempcolor);

				tempcolor.x = m_XModelData.MatD3D()[i].Ambient.r*v4ambient1.x;
				tempcolor.y = m_XModelData.MatD3D()[i].Ambient.g*v4ambient1.y;
				tempcolor.z = m_XModelData.MatD3D()[i].Ambient.b*v4ambient1.z;
				tempcolor.w = m_XModelData.MatD3D()[i].Ambient.a*v4ambient1.w;
                m_EffectData->SetVector("AmbientMatrix", &tempcolor);

                tempcolor.x = m_XModelData.MatD3D()[i].Specular.r*v4specular1.x;
				tempcolor.y = m_XModelData.MatD3D()[i].Specular.g*v4specular1.y;
				tempcolor.z = m_XModelData.MatD3D()[i].Specular.b*v4specular1.z;
				tempcolor.w = m_XModelData.MatD3D()[i].Specular.a*v4specular1.w;
                m_EffectData->SetVector("SpecularMatrix", &tempcolor);

                tempcolor.x = m_XModelData.MatD3D()[i].Emissive.r*v4emmisive1.x;
                tempcolor.y = m_XModelData.MatD3D()[i].Emissive.g*v4emmisive1.y;
                tempcolor.z = m_XModelData.MatD3D()[i].Emissive.b*v4emmisive1.z;
                tempcolor.w = m_XModelData.MatD3D()[i].Emissive.a*v4emmisive1.w;
                m_EffectData->SetVector("EmmisiveMatrix", &tempcolor);

				m_EffectData->SetFloat("Power", m_XModelData.MatD3D()[i].Power*m_light->GetD3DMaterial9().Power);

                // テクスチャを使用するか否かをセット
                if ((&m_XModelData.Texture()[i]) != nullptr)
                {
                    m_EffectData->SetBool("TexFlag", true);
                }
                else
                {
                    m_EffectData->SetBool("TexFlag", false);
                }
                m_EffectData->SetTexture("Texture", &m_XModelData.Texture()[i]);
                m_EffectData->CommitChanges();
                device->SetTexture(0, &m_XModelData.Texture()[i]);
                m_XModelData->DrawSubset(i);
                device->SetTexture(0, nullptr);
            }

            m_EffectData->EndPass();
            m_EffectData->End();
        }
    }

    //==========================================================================
    //
    // class  : ToonShaderModel
    // Content: トゥーンシェーダーモデル
    //
    //==========================================================================
    ToonShaderModel::ToonShaderModel()
    {
        SetComponentName("ToonShaderModel");
    }
    ToonShaderModel::~ToonShaderModel() {}

    void ToonShaderModel::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (m_XModelData.Existence() && m_EffectData.Existence() && m_TextureData.Existence() && CanIRenew())
        {
            // シェーダ開始
            auto &diffuse = GetD3DMaterial9().Diffuse;
            auto &ambient = GetD3DMaterial9().Ambient;
            auto &specular = GetD3DMaterial9().Specular;
            auto &emmisive = GetD3DMaterial9().Emissive;

            //平行光源の位置ベクトルから方向ベクトルを計算する
            D3DXVECTOR4 LightDir;
            m_light->GetDirection(*this, LightDir);

            auto pCamera = m_camera;
            if (pCamera->GetCamera() != nullptr)
                pCamera = pCamera->GetCamera();

            UINT numPass = 0;
            D3DXVECTOR3 eye = pCamera->GetLook1().eye + pCamera->GetLook2().eye;
            D3DXVECTOR4 v4diffuse(diffuse.r, diffuse.g, diffuse.b, diffuse.a); // 平行光源の色
            D3DXVECTOR4 v4ambient(ambient.r, ambient.g, ambient.b, ambient.a); // 環境光
            D3DXVECTOR4 v4specular(specular.r, specular.g, specular.b, specular.a); // スペキュラ
            D3DXVECTOR4 v4emmisive(emmisive.r, emmisive.g, emmisive.b, emmisive.a); // エミッシブ
            D3DXVECTOR4 v4camerapos(eye.x, eye.y, eye.z, 0.0f);
            D3DXMATRIX ViewMatrix;
            D3DXMATRIX ProjectionMatrix;

            device->GetTransform(D3DTS_VIEW, &ViewMatrix);
            device->GetTransform(D3DTS_PROJECTION, &ProjectionMatrix);

            m_EffectData->SetTechnique("ToonShader");
            m_EffectData->Begin(&numPass, 0);

            //--------------------- pass1 ---------------------
            m_EffectData->BeginPass(1);

            // 定数をセット(頂点シェーダー)
            m_EffectData->SetMatrix("World", &m_WorldMatrix);
            m_EffectData->SetMatrix("View", &ViewMatrix);
            m_EffectData->SetMatrix("Projection", &ProjectionMatrix);

            // 裏面描画
            device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW); //※RenderStateを裏面設定に

            DrawShaderModel(device);

            m_EffectData->EndPass();

            //--------------------- pass0 ---------------------
            m_EffectData->BeginPass(0);
            // 光
            // 定数をセット(頂点シェーダー)
            m_EffectData->SetMatrix("World", &m_WorldMatrix);
            m_EffectData->SetMatrix("View", &ViewMatrix);
            m_EffectData->SetMatrix("Projection", &ProjectionMatrix);

            m_EffectData->SetVector("Diffuse", &v4diffuse);
            m_EffectData->SetVector("Ambient", &v4ambient);
            m_EffectData->SetVector("Specular", &v4specular);
            m_EffectData->SetVector("Emmisive", &v4emmisive);

            // 平行光源
            m_EffectData->SetVector("LightDir", &LightDir);

            // カメラ位置
            m_EffectData->SetVector("CameraPos", &v4camerapos);

            // 表面描画
            device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
            DrawShaderModel(device);

            m_EffectData->EndPass();

            m_EffectData->End();
        }
    }

    void ToonShaderModel::DrawShaderModel(LPDIRECT3DDEVICE9 device)
    {
        D3DXVECTOR4  tempcolor;

        // マテリアル自体の色反映
        for (int i = 0; i < (int)m_XModelData.NumMesh(); i++)
        {
            if (!m_XModelData.Texture()[i].Existence())continue;

            tempcolor.x = m_XModelData.MatD3D()[i].Diffuse.r;
            tempcolor.y = m_XModelData.MatD3D()[i].Diffuse.g;
            tempcolor.z = m_XModelData.MatD3D()[i].Diffuse.b;
            tempcolor.w = m_XModelData.MatD3D()[i].Diffuse.a;
            m_EffectData->SetVector("DiffuseMatrix", &tempcolor);

            tempcolor.x = m_XModelData.MatD3D()[i].Ambient.r;
            tempcolor.y = m_XModelData.MatD3D()[i].Ambient.g;
            tempcolor.z = m_XModelData.MatD3D()[i].Ambient.b;
            tempcolor.w = m_XModelData.MatD3D()[i].Ambient.a;
            m_EffectData->SetVector("AmbientMatrix", &tempcolor);

            tempcolor.x = m_XModelData.MatD3D()[i].Specular.r;
            tempcolor.y = m_XModelData.MatD3D()[i].Specular.g;
            tempcolor.z = m_XModelData.MatD3D()[i].Specular.b;
            tempcolor.w = m_XModelData.MatD3D()[i].Specular.a;
            m_EffectData->SetVector("SpecularMatrix", &tempcolor);

            tempcolor.x = m_XModelData.MatD3D()[i].Emissive.r;
            tempcolor.y = m_XModelData.MatD3D()[i].Emissive.g;
            tempcolor.z = m_XModelData.MatD3D()[i].Emissive.b;
            tempcolor.w = m_XModelData.MatD3D()[i].Emissive.a;
            m_EffectData->SetVector("EmmisiveMatrix", &tempcolor);

            m_EffectData->SetFloat("Power", m_XModelData.MatD3D()[i].Power*GetD3DMaterial9().Power);

            // テクスチャを使用するか否かをセット
            if ((&m_XModelData.Texture()[i]) != nullptr)
            {
                m_EffectData->SetBool("TexFlag", true);
            }
            else
            {
                m_EffectData->SetBool("TexFlag", false);
            }

            m_EffectData->SetTexture("Texture", &m_XModelData.Texture()[i]);
            m_EffectData->SetTexture("ToonTexture", &m_TextureData);

            m_EffectData->CommitChanges();
            device->SetTexture(0, &m_XModelData.Texture()[i]);
            m_XModelData->DrawSubset(i);
            device->SetTexture(0, nullptr);
        }
    }
    PerPixelLightingCube::PerPixelLightingCube()
    {
        SetComponentName("PerPixelLightingCube");
    }
    PerPixelLightingCube::~PerPixelLightingCube()
    {
    }
    void PerPixelLightingCube::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 呼ばれたら更新の無効化
        LockUpdate();

        if (!m_CubeData.Existence() && !m_EffectData.Existence() && !CanIRenew())return;

        // シェーダ開始
		auto &diffuse = GetD3DMaterial9().Diffuse;
		auto &ambient = GetD3DMaterial9().Ambient;
		auto &specular = GetD3DMaterial9().Specular;
		auto &emmisive = GetD3DMaterial9().Emissive;

		auto &light_diffuse = m_light->GetD3DMaterial9().Diffuse;
		auto &light_ambient = m_light->GetD3DMaterial9().Ambient;
		auto &light_specular = m_light->GetD3DMaterial9().Specular;
		auto &light_emmisive = m_light->GetD3DMaterial9().Emissive;

        //平行光源の位置ベクトルから方向ベクトルを計算する
        D3DXVECTOR4 LightDir;
        m_light->GetDirection(*this, LightDir);

        auto pCamera = m_camera;
        if (pCamera->GetCamera() != nullptr)
            pCamera = pCamera->GetCamera();

        UINT numPass = 0;
        D3DXVECTOR3 eye = pCamera->GetLook1().eye + pCamera->GetLook2().eye;
		D3DXVECTOR4 v4diffuse1(diffuse.r, diffuse.g, diffuse.b, diffuse.a); // 平行光源の色
		D3DXVECTOR4 v4ambient1(ambient.r, ambient.g, ambient.b, ambient.a); // 環境光
		D3DXVECTOR4 v4specular1(specular.r, specular.g, specular.b, specular.a); // スペキュラ
		D3DXVECTOR4 v4emmisive1(emmisive.r, emmisive.g, emmisive.b, emmisive.a); // エミッシブ
		D3DXVECTOR4 v4diffuse2(light_diffuse.r, light_diffuse.g, light_diffuse.b, light_diffuse.a); // 平行光源の色
		D3DXVECTOR4 v4ambient2(light_ambient.r, light_ambient.g, light_ambient.b, light_ambient.a); // 環境光
		D3DXVECTOR4 v4specular2(light_specular.r, light_specular.g, light_specular.b, light_specular.a); // スペキュラ
		D3DXVECTOR4 v4emmisive2(light_emmisive.r, light_emmisive.g, light_emmisive.b, light_emmisive.a); // エミッシブ
        D3DXVECTOR4 v4camerapos(eye.x, eye.y, eye.z, 0.0f);
        D3DXMATRIX ViewMatrix;
        D3DXMATRIX ProjectionMatrix;

        device->GetTransform(D3DTS_VIEW, &ViewMatrix);
        device->GetTransform(D3DTS_PROJECTION, &ProjectionMatrix);

        m_EffectData->SetTechnique("PerPixelLighting");
        m_EffectData->Begin(&numPass, 0);
        m_EffectData->BeginPass(0);

        // 定数をセット(頂点シェーダー)
        m_EffectData->SetMatrix("World", &m_WorldMatrix);
        m_EffectData->SetMatrix("View", &ViewMatrix);
        m_EffectData->SetMatrix("Projection", &ProjectionMatrix);

        // 環境光
        m_EffectData->SetVector("Diffuse", &v4diffuse2);
        m_EffectData->SetVector("Ambient", &v4ambient2);
        m_EffectData->SetVector("Specular", &v4specular2);
        m_EffectData->SetVector("Emmisive", &v4emmisive2);

		// マテリアル
        m_EffectData->SetVector("DiffuseMatrix", &v4diffuse1);
        m_EffectData->SetVector("AmbientMatrix", &v4ambient1);
        m_EffectData->SetVector("SpecularMatrix", &v4specular1);
        m_EffectData->SetVector("EmmisiveMatrix", &v4emmisive1);

		m_EffectData->SetFloat("Power", m_light->GetD3DMaterial9().Power);

        // 平行光源
        m_EffectData->SetVector("LightDir", &LightDir);

        // カメラ位置
        m_EffectData->SetVector("CameraPos", &v4camerapos);

        // テクスチャを使用するか否かをセット
        if (m_TextureData.Existence())
        {
            if ((&m_TextureData) != nullptr)
            {
                m_EffectData->SetBool("TexFlag", true);
            }
            else
            {
                m_EffectData->SetBool("TexFlag", false);
            }
            m_EffectData->SetTexture("Texture", &m_TextureData);
            device->SetTexture(0, &m_TextureData);
        }
        else
        {
            m_EffectData->SetBool("TexFlag", false);
            m_EffectData->SetTexture("Texture", nullptr);
            device->SetTexture(0, nullptr);
        }

        m_EffectData->CommitChanges();
        
        device->SetStreamSource(0, (&m_CubeData).VertexBuffer, 0, sizeof(VERTEX_4));
        device->SetIndices((&m_CubeData).IndexBuffer);
        device->SetFVF(FVF_VERTEX_4);
        device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_CubeData.Info().NumVertex, 0, m_CubeData.Info().NumTriangle);
        device->SetTexture(0, nullptr);

        m_EffectData->EndPass();
        m_EffectData->End();
    }
}