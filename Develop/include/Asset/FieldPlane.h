//==========================================================================
// 平面 [FieldPlane.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldCollider.h"

namespace common
{
	//==========================================================================
	//
	// class  : FieldPlane
	// Content: 平面
	//
	//==========================================================================
	class FieldPlane : public FieldCollider, public mslib::collider::PlaneCollider
    {
    public:
        FieldPlane();
        ~FieldPlane();
		// 全ての平面コリジョンを取得
        static std::list<FieldPlane*> & GetAllFieldPlane();
    protected:
        static std::list<FieldPlane*> m_FieldPlane; // 平面の管理
    };
}