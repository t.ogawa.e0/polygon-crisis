//==========================================================================
// ライト [LightObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace common
{
    //==========================================================================
    //
    // class  : LightObject
    // Content: ライト 
    //
    //==========================================================================
    class LightObject : public mslib::ObjectManager
    {
    public:
        LightObject();
        ~LightObject();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    };
}