//==========================================================================
// 攻撃コリジョン [AttackCollision.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "AttackCollision.h"

namespace common
{
    std::list<AttackCollision*> AttackCollision::m_AttackCollision;

    AttackCollision::AttackCollision()
    {
        SetComponentName("AttackCollision");
        m_AttackCollision.push_back(this);
        m_collider = nullptr;
        AddActivity();
        SetMatrixType(mslib::object::MatrixType::Local);
        m_delete_flag = false;
    }

    AttackCollision::~AttackCollision()
    {
        auto itr = std::find(m_AttackCollision.begin(), m_AttackCollision.end(), this);
        if (itr != m_AttackCollision.end())
            m_AttackCollision.erase(itr);
    }
    mslib::collider::Collider * AttackCollision::AddCollider()
    {
		// 一回のみ有効
        if (m_collider == nullptr)
            m_collider = AddComponent<mslib::collider::Collider>();
        return m_collider;
    }
    mslib::collider::Collider * AttackCollision::GetCollider()
    {
        return m_collider != nullptr ? m_collider : AddCollider();
    }
    bool AttackCollision::GetDeleteFlag()
    {
        return m_delete_flag;
    }
    std::list<AttackCollision*> & AttackCollision::GetAllAttackCollision()
    {
        return m_AttackCollision;
    }
    void AttackCollision::DestroyAttackCollision(AttackCollision *& obj)
    {
        if (obj == nullptr)return;

		// 破棄対象の親にアクセスし、その親から破棄を行う
        auto parent = obj->GetParent();
        if (parent->DestroyComponent(obj))
            obj = nullptr;
    }
}