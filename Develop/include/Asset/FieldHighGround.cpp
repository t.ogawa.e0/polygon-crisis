//==========================================================================
// FieldHighGround [FieldHighGround.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "FieldHighGround.h"

namespace common
{
    std::list<FieldHighGround*> FieldHighGround::m_FieldHighGround;

    FieldHighGround::FieldHighGround()
    {
        SetComponentName("FieldHighGround");
        m_FieldBox = nullptr;
        m_FieldPlane = nullptr;
        m_FieldUpPlane = nullptr;
        m_FieldHighGround.push_back(this);
    }

    FieldHighGround::~FieldHighGround()
    {
        auto itr = std::find(m_FieldHighGround.begin(), m_FieldHighGround.end(), this);
        if (itr == m_FieldHighGround.end())return;
        m_FieldHighGround.erase(itr);
    }
    common::FieldBox * FieldHighGround::GetFieldBox()
    {
        if (m_FieldBox == nullptr)
            m_FieldBox = GetParent()->AddComponent<common::FieldBox>();
        return m_FieldBox;
    }
    common::FieldPlane * FieldHighGround::GetFieldPlane()
    {
        if (m_FieldPlane == nullptr)
            m_FieldPlane = GetParent()->AddComponent<common::FieldPlane>();
        return m_FieldPlane;
    }
    common::FieldPlane * FieldHighGround::GetFieldUpPlane()
    {
        if (m_FieldUpPlane == nullptr)
            m_FieldUpPlane = GetParent()->AddComponent<common::FieldPlane>();
        return m_FieldUpPlane;
    }
    std::list<FieldHighGround*>& FieldHighGround::GetAllFieldHighGround()
    {
        return m_FieldHighGround;
    }
}