//==========================================================================
// xBox Controller [xBox Controller.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "xBox_Controller.h"
#include "resource_list.h"

namespace common
{
	xBox_Controller::xBox_Controller() : ObjectManager("xBox_Controller")
	{
		m_sprite = nullptr;
	}

	xBox_Controller::~xBox_Controller()
	{
	}
	void xBox_Controller::Init()
	{
		m_sprite = Sprite()->Create();
		m_sprite->SetTextureData(GetTextureLoader()->Load(RESOURCE_xBox_Controller_DDS));
		m_sprite->SetComponentName("xBox_Controller");
	}

	void xBox_Controller::Update()
	{
	}

	void xBox_Controller::Debug()
	{
	}
	void xBox_Controller::ActivityFlag(bool flag)
	{
		m_sprite->SetActivity(flag);
		SetActivity(flag);
	}
}