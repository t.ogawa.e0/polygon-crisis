//==========================================================================
// フィールドのコリジョン [FieldCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "FieldCollider.h"

namespace common
{
    std::list<FieldCollider*> FieldCollider::m_FieldCollider;

    FieldCollider::FieldCollider()
    {
        m_FieldCollider.push_back(this);
    }

    FieldCollider::~FieldCollider()
    {
        auto itr = std::find(m_FieldCollider.begin(), m_FieldCollider.end(), this);
        if (itr == m_FieldCollider.end())return;
        m_FieldCollider.erase(itr);
    }
    std::list<FieldCollider*> & FieldCollider::GetAllFieldCollider()
    {
        return m_FieldCollider;
    }
}