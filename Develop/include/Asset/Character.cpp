//==========================================================================
// キャラクター [Character.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Character.h"

namespace common
{
    std::list<Character*> Character::m_character; // 全キャラクター
    std::list<PlayerCharacter*> PlayerCharacter::m_PlayerCharacter; // プレイヤーのみ
    std::list<EnemyCharacter*> EnemyCharacter::m_EnemyCharacter; // エネミーのみ

    Character::Character()
    {
        SetComponentName("CharacterFunction");
        m_WallBox = nullptr;
        m_HighGroundCollUnder = nullptr;
        m_HighGroundCollUp = nullptr;
        m_HighGroundCollBox = nullptr;
        m_Map = nullptr;
        m_Gravity = nullptr;
        m_OldParameter.Identity();
        m_character.push_back(this);
    }
    Character::Character(bool set)
    {
        SetComponentName("CharacterFunction");
        m_WallBox = nullptr;
        m_HighGroundCollUnder = nullptr;
        m_HighGroundCollUp = nullptr;
        m_HighGroundCollBox = nullptr;
        m_Map = nullptr;
        m_Gravity = nullptr;
        m_OldParameter.Identity();
        if (set)
        {
            m_character.push_back(this);
        }
    }
    Character::~Character()
    {
        auto itr = std::find(m_character.begin(), m_character.end(), this);
        if (itr != m_character.end())
            m_character.erase(itr);
    }
    void Character::Init()
    {
        auto obj = GetWarrantyParent<mslib::transform::Transform>();
        if (obj == nullptr)return;

		// キャラクター共通の機能
        m_WallBox = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollUnder = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollBox = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollUp = obj->AddComponent<mslib::collider::Collider>();
        m_Map = obj->AddComponent<mslib::collider::Collider>();
        m_Gravity = obj->AddComponent<mslib::gravity::Gravity>();

		// 名前を登録
        m_WallBox->SetComponentName("VersusWallBox");
        m_HighGroundCollUnder->SetComponentName("VersusHighGroundUnder");
        m_HighGroundCollUp->SetComponentName("VersusHighGroundUp");
        m_HighGroundCollBox->SetComponentName("VersusHighGroundBox");
        m_Map->SetComponentName("VersusMap");
    }
    void Character::Collider()
    {
        auto obj = GetWarrantyParent<mslib::transform::Transform>();
        if (obj == nullptr)return;

        // 無効化
        m_HighGroundCollUnder->LockUpdate();
        m_HighGroundCollBox->UnlockUpdate();

        // 壁のBoxに接触時
        if (m_WallBox->Trigger()) {
            obj->SetPosition(m_OldParameter.position.x, obj->GetPosition()->y, m_OldParameter.position.z);
        }

        // 高台のBoxに接触時
        if (m_HighGroundCollBox->Trigger() && !m_HighGroundCollUp->Trigger()) {
            obj->SetPosition(m_OldParameter.position.x, obj->GetPosition()->y, m_OldParameter.position.z);
        }

        // 高台の上にいるとき
        if (m_HighGroundCollUp->Trigger()) {
            m_HighGroundCollBox->LockUpdate();
            m_HighGroundCollUnder->UnlockUpdate();
        }
    }
    mslib::collider::Collider * Character::GetVersusWallBox()
    {
        return m_WallBox;
    }
    mslib::collider::Collider * Character::GetVersusHighGroundUnder()
    {
        return m_HighGroundCollUnder;
    }
    mslib::collider::Collider * Character::GetVersusHighGroundUp()
    {
        return m_HighGroundCollUp;
    }
    mslib::collider::Collider * Character::GetVersusHighGroundBox()
    {
        return m_HighGroundCollBox;
    }
    mslib::collider::Collider * Character::GetVersusMap()
    {
        return m_Map;
    }
    mslib::gravity::Gravity * Character::GetGravity()
    {
        return m_Gravity;
    }
    mslib::transform::Parameter & Character::GetParameter()
    {
        return m_OldParameter;
    }
    void Character::SetParameter(mslib::transform::Transform * obj)
    {
        if (obj == nullptr)return;
        m_OldParameter = *obj->GetParameter();
    }
    std::list<Character*>& Character::GetAllCharacter()
    {
        return m_character;
    }
    PlayerCharacter::PlayerCharacter()
    {
        m_DefenseCollider = nullptr;
        m_PlayerCollider = nullptr;
        m_TargetObject = nullptr;
        m_PlayerCharacter.push_back(this);
    }
    PlayerCharacter::PlayerCharacter(bool set) : Character(set)
    {
        m_DefenseCollider = nullptr;
        m_PlayerCollider = nullptr;
        m_TargetObject = nullptr;
        m_PlayerCharacter.push_back(this);
    }
    PlayerCharacter::~PlayerCharacter()
    {
        auto itr = std::find(m_PlayerCharacter.begin(), m_PlayerCharacter.end(), this);
        if (itr != m_PlayerCharacter.end())
            m_PlayerCharacter.erase(itr);
    }
    void PlayerCharacter::Init()
    {
        auto obj = GetWarrantyParent<mslib::transform::Transform>();
        if (obj == nullptr)return;

		// プレイヤー共通の機能
        m_WallBox = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollUnder = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollBox = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollUp = obj->AddComponent<mslib::collider::Collider>();
        m_Map = obj->AddComponent<mslib::collider::Collider>();
        m_Gravity = obj->AddComponent<mslib::gravity::Gravity>();
        m_PlayerCollider = obj->AddComponent<mslib::collider::BoxCollider>();
        m_DefenseCollider = obj->AddComponent<mslib::collider::BoxCollider>();
        m_TargetObject = obj->AddComponent<mslib::object::Object>();

		// 機能名を登録
        m_PlayerCollider->SetComponentName("PlayerCollider");
        m_DefenseCollider->SetComponentName("DefenseCollider");
        m_WallBox->SetComponentName("VersusWallBox");
        m_HighGroundCollUnder->SetComponentName("VersusHighGroundUnder");
        m_HighGroundCollUp->SetComponentName("VersusHighGroundUp");
        m_HighGroundCollBox->SetComponentName("VersusHighGroundBox");
        m_Map->SetComponentName("VersusMap");
        m_TargetObject->SetComponentName("TargetObject");

		// 停止オブジェクトい反映
        m_PlayerCollider->AddActivity();
        m_DefenseCollider->AddActivity();
    }
    mslib::collider::BoxCollider * PlayerCharacter::GetDefenseCollider()
    {
        return m_DefenseCollider;
    }
    mslib::collider::BoxCollider * PlayerCharacter::GetPlayerCollider()
    {
        return m_PlayerCollider;
    }
    mslib::transform::Transform * PlayerCharacter::GetTargetObject()
    {
        return m_TargetObject;
    }
    std::list<PlayerCharacter*>& PlayerCharacter::GetAllPlayerCharacter()
    {
        return m_PlayerCharacter;
    }
    PlayerCharacter * PlayerCharacter::GetPlayerCharacter(mslib::transform::Transform * obj)
    {
        for (auto & itr : m_PlayerCharacter)
            if (itr->GetWarrantyParent<mslib::transform::Transform>() == obj)return itr;
        return nullptr;
    }
    EnemyCharacter::EnemyCharacter()
    {
        m_TargetObject = nullptr;
        m_EnemyCharacter.push_back(this);
    }
    EnemyCharacter::EnemyCharacter(bool set) : Character(set)
    {
        m_TargetObject = nullptr;
        m_EnemyCharacter.push_back(this);
    }
    EnemyCharacter::~EnemyCharacter()
    {
        auto itr = std::find(m_EnemyCharacter.begin(), m_EnemyCharacter.end(), this);
        if (itr != m_EnemyCharacter.end())
            m_EnemyCharacter.erase(itr);
    }
    void EnemyCharacter::Init()
    {
        auto obj = GetWarrantyParent<mslib::transform::Transform>();
        if (obj == nullptr)return;

		// エネミーの共通機能
        m_WallBox = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollUnder = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollBox = obj->AddComponent<mslib::collider::Collider>();
        m_HighGroundCollUp = obj->AddComponent<mslib::collider::Collider>();
        m_Map = obj->AddComponent<mslib::collider::Collider>();
        m_Gravity = obj->AddComponent<mslib::gravity::Gravity>();
        m_TargetObject = obj->AddComponent<mslib::object::Object>();

		// 機能名の追加
        m_WallBox->SetComponentName("VersusWallBox");
        m_HighGroundCollUnder->SetComponentName("VersusHighGroundUnder");
        m_HighGroundCollUp->SetComponentName("VersusHighGroundUp");
        m_HighGroundCollBox->SetComponentName("VersusHighGroundBox");
        m_Map->SetComponentName("VersusMap");
        m_TargetObject->SetComponentName("TargetObject");
    }
    mslib::transform::Transform * EnemyCharacter::GetTargetObject()
    {
        return m_TargetObject;
    }
    std::list<EnemyCharacter*>& EnemyCharacter::GetAllEnemyCharacter()
    {
        return m_EnemyCharacter;
    }
    EnemyCharacter * EnemyCharacter::GetEnemyCharacter(mslib::transform::Transform * obj)
    {
        for (auto & itr : m_EnemyCharacter)
            if (itr->GetWarrantyParent<mslib::transform::Transform>() == obj)return itr;
        return nullptr;
    }
}