//==========================================================================
// DirectXライブラリ[dxlib.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <d3d9.h>
#include <d3dx9.h>
#include <XInput.h>
#include <XAudio2.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include <dinput.h>
#include <tchar.h>

//==========================================================================
// 独自lib include
//==========================================================================
#include <common\mslib.hpp>
#include <common\WindowsAPI.h>
#include <common\AspectRatio.h>
#include <common\ObjectManager.h> // 継承用
#include <common\SceneManager.h>
#include <common\dx9math.h>

/*
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // デバッグ
    void Debug(void)override;
*/
