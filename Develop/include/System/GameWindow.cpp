//==========================================================================
// ゲームウィンドウ[GameWindow.cpp]
// author: tatuya ogawa
//==========================================================================
#include "GameWindow.h"
#include "Screen.h"
#include "resource.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr DWORD WINDOW_STYLE = (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));
constexpr int __fps = 60;
constexpr long long __time_per_frame = (long long)1000 / __fps;

//==========================================================================
// 関数
//==========================================================================
// FPS固定
static const bool FpsFixed(void);

CGameWindow::CGameWindow()
{
    AddComponent<mslib::WindowsAPI>("Default", "Default");
    SetComponentName("CGameWindow");
}

CGameWindow::CGameWindow(const std::string & class_name, const std::string & window_name)
{
    AddComponent<mslib::WindowsAPI>(class_name, window_name);
    SetComponentName("CGameWindow");
}

CGameWindow::~CGameWindow()
{
}

//==========================================================================
// ウィンドウ生成
int CGameWindow::Window(HINSTANCE hInstance, const mslib::int2 & data, bool Mode, int nCmdShow)
{
    RECT wr, dr;
    auto * pWindowsAPI = GetComponent<mslib::WindowsAPI>();
    
    pWindowsAPI->MyClass(CS_VREDRAW | CS_HREDRAW, WndProc, (LPCSTR)IDI_ICON1, (LPCSTR)IDI_ICON1, nullptr, hInstance);

    // マウスカーソル表示設定
    ShowCursor(true);

    // Direct3Dオブジェクトの作成 に失敗時終了 
    auto * pdevice = mslib::manager::Manager::GetDevice();
    pdevice->Init();

    // ウィンドウRECT
    wr = pWindowsAPI->GetWindowRECT(WINDOW_STYLE, data.x, data.y, false);

    //デスクトップサイズ習得
    GetWindowRect(GetDesktopWindow(), &dr);

    // 初期位置修正
    wr.left = dr.right / 2;
    wr.top = dr.bottom / 2;
    wr.left -= wr.right / 2;
    wr.top -= wr.bottom / 2;

    // ウィンドウの生成
    pWindowsAPI->Create(WINDOW_STYLE, wr, nullptr, nullptr, nullptr, nCmdShow);

    // 色々失敗した時終了
    if (pdevice->CreateWindowMode(data, Mode))
    {
        return -1;
    }

    // デバイスにウィンドウハンドルを教える
    pdevice->SetHwnd(pWindowsAPI->GetHWND());

    // ゲームループ
    return GameLoop(hInstance, pWindowsAPI->GetHWND());
}

//==========================================================================
// ウィンドウプロシージャ
LRESULT CALLBACK CGameWindow::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    auto * pdevice = mslib::manager::Manager::GetDevice();
    mslib::MsImGui imgui;

	if (pdevice->GetHwnd() != nullptr)
	{
		hWnd = pdevice->GetHwnd();
	}

	if (imgui.ImGui_WndProcHandler(hWnd, uMsg, wParam, lParam))
	{
		return true;
	}

	// メッセージの種類に応じて処理を分岐します。
	switch (uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:	// キー入力
		switch (wParam)
		{
		case VK_ESCAPE:
			// [ESC]キーが押されたら
			if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				DestroyWindow(hWnd);	// ウィンドウを破棄
			}
			else
			{
				return 0;	// いいえの時
			}
		}
		break;
	case WM_LBUTTONDOWN:
		SetFocus(hWnd);
		break;
	case WM_CLOSE:	// ×ボタン押した時
		if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			DestroyWindow(hWnd);	// ウィンドウを破棄
		}
		else
		{
			return 0;	// いいえの時
		}
		break;
	default:
		break;
	}

	//デフォルトの処理
	return imgui.SetMenu(hWnd, uMsg, wParam, lParam, pdevice->GetD3DDevice(), &pdevice->Getd3dpp());
}

//==========================================================================
// ゲームループ
int CGameWindow::GameLoop(HINSTANCE hInstance, HWND hWnd)
{
    auto * pWindowsAPI = GetComponent<mslib::WindowsAPI>();
    auto * pScreen = AddComponent<CScreen>();
    MSG & msg = pWindowsAPI->GetMSG(); // メッセージ構造体

	if (pScreen->Init(hInstance, hWnd))
	{
        pWindowsAPI->ErrorMessage("初期化に失敗しました");
		return-1;
	}

	// メッセージループ
	for (;;)
	{
		// 何があってもスルー
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				//メッセージ処理
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (FpsFixed())
			{
				if (pScreen->Update())
				{
                    pWindowsAPI->ErrorMessage("初期化に失敗しました");
					return-1;
				}

				//描画
                pScreen->Draw();
			}
		}
	}

	return (int)msg.wParam;
}

//==========================================================================
// FPS固定
const bool FpsFixed(void)
{
    static auto start = std::chrono::system_clock::now();

    while (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() < __time_per_frame);

    start = std::chrono::system_clock::now();

    return true;
}
