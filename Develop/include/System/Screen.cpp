//==========================================================================
// スクリーン[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Screen.h"
#include "Title\Scene.h"
#include "Result\Scene.h"
#include "Opening\Scene.h"
#include "Stage1/Scene.h"
#include "Editor/Motion/Scene.h"
#include "CheckCollider/Scene.h"

//==========================================================================
// 実体
//==========================================================================
mslib::scene_manager::BaseScene *CScreen::m_next_scene = nullptr; // name

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __ImGuiWindowY = 26;

CScreen::CScreen()
{
    SetComponentName("Screen");
    m_change = false;
    m_count = 0;
    m_end_count = 0;
    m_start = false;
    m_initializer = false;
#if defined(_MSLIB_DEBUG)
    m_system.Get("Fill Mode");
    m_system.Get("Fittering");
    m_system.Get("System Window") = true;
#endif
}

CScreen::~CScreen()
{
    Uninit();
}

//==========================================================================
// 初期化
bool CScreen::Init(HINSTANCE hInstance, HWND hWnd)
{
	srand((unsigned)time(nullptr));
    mslib::manager::Manager manager; // マネージャー

    if (manager.GetDevice()->CreateDevice()) { return true; }
    if (manager.GetDInputKeyboard()->Init(hInstance, hWnd)) { return true; }
    if (manager.GetDInputController()->Init(hInstance, hWnd)) { return true; }
    if (manager.GetDInputMouse()->Init(hInstance, hWnd)) { return true; }
    if (manager.CreateXAudio2Device()->Init(hWnd)) { return true; }
    auto pDevice = manager.GetDevice()->GetD3DDevice();	//デバイス渡し	
    mslib::sprite::Sprite::CreateRenderer(pDevice);

    m_change = false;
	m_count = 0;
	m_start = false;
	m_next_scene = nullptr;

    m_scene = AddComponent<mslib::scene_manager::SceneManager>();
    m_LoadScene = m_scene->AddComponent<LoadScene>();
   
    m_ImGui.Init(hWnd, pDevice);

    // ImGui 新しいフレーム
    m_ImGui.NewFrame();
    for (;m_LoadScene->Init() != true;);
    m_ImGui.EndFrame();

    return false;
}

//==========================================================================
// 解放
void CScreen::Uninit(void)
{
    startdatarelease();
    m_ImGui.Uninit();
    m_scene->Uninit();

    mslib::sprite::Sprite::DeleteRenderer();

    if (m_next_scene != nullptr)
    {
        delete m_next_scene;
        m_next_scene = nullptr;
    }
}

//==========================================================================
// 更新処理
bool CScreen::Update(void)
{
    // ImGui 新しいフレーム
	m_ImGui.NewFrame();

    // システムウィンドウ
    ImGuiSystemMenuBar();

    // 次のウィンドウの表示位置変更
    ImGuiNextWindow();

#if defined(_MSLIB_DEBUG)
    auto *pdevice = mslib::manager::Manager::GetDevice();
    m_ImGui.FixedWindow("Operation window", m_system.Get("System Window"), { 300,(float)pdevice->GetWindowsSize()->y - __ImGuiWindowY });
#endif

    if (initializer()) { return true; }

    mslib::manager::Manager::DInputUpdate();
    m_LoadScene->GetFade()->Update();
    fade();

    // ロードが終わっているとき
    if (m_change == true)
    {
        m_LoadScene->GetLoadScreen()->SetActivity(false);
    }
    else
    {
        m_LoadScene->GetLoadScreen()->SetActivity(true);
        m_LoadScene->GetLoadScreen()->Update();
    }
    m_scene->Update();
    mslib::manager::Manager::UpdateAll();

    m_ImGui.EndWindow();
	m_ImGui.EndFrame();

	return false;
}

//==========================================================================
// 描画処理
void CScreen::Draw(void)
{
    auto *pdevice = mslib::manager::Manager::GetDevice();

    Fillmode(pdevice->GetD3DDevice());
    Fittering(pdevice->GetD3DDevice());
    auto flag = mslib::manager::Manager::DrawAll();
    EndFillmode(pdevice->GetD3DDevice());

    if (flag)
    {
        m_ImGui.Draw(pdevice->GetD3DDevice());
    }

    m_ImGui.DeviceReset(pdevice->DrawEnd(), pdevice->GetD3DDevice(), &pdevice->Getd3dpp());
}

//==========================================================================
// シーン変更キー
void CScreen::ScreenChange(mslib::scene_manager::BaseScene * scene)
{
    if (m_next_scene != nullptr)
    {
        delete scene;
    }
    else if (m_next_scene == nullptr)
    {
        m_next_scene = scene;
    }
}

//==========================================================================
// スクリーンの切り替え
void CScreen::Change(mslib::scene_manager::BaseScene * scene)
{
	// すべての初期化
    m_scene->ChangeScene(scene);
}

//==========================================================================
// 最初の初期化データの破棄
void CScreen::startdatarelease(void)
{
}

//==========================================================================
// スクリーンの初期化
bool CScreen::initializer(void)
{
	//==========================================================================
	// 最初の画面
#if defined(_MSLIB_DEBUG)
	if (m_count == 30 && m_change == false && m_start == false)
	{
		m_start = true;
		Change(nullptr);
		m_next_scene = nullptr;
		m_initializer = false;
	}
#else
	if (m_count == 30 && m_change == false && m_start == false)
	{
		m_start = true;
		ScreenChange(new Opening::Scene);
		m_initializer = false;
	}
#endif

    //==========================================================================
	// シーン変更
    if (m_count == 30 && m_change == false && m_start == true && m_next_scene != nullptr)
    {
        Change(m_next_scene);
        m_next_scene = nullptr;
        m_initializer = false;
    }

    //==========================================================================
    // 初期化中
    if (30 <= m_count && m_change == false && m_initializer != true)
    {
        // 全インスタンスの初期化終了判定
        m_initializer = m_scene->Init();
        // 全てのインスタンスの初期化が終了した際に、ロードを終えるための情報を与える
        if (m_initializer == true)
        {
            m_end_count = m_count;
            m_end_count += 30;
        }
    }

	//==========================================================================
	// フェード終了判定
    if (m_count == m_end_count && m_change == false && m_initializer == true)
    {
        m_LoadScene->GetFade()->Out();
		m_change = true;
	}

    // ロードカウンタ
	if (m_change == false)
	{
        m_count++;
	}

	return false;
}

//==========================================================================
// フェード
void CScreen::fade(void)
{
	// フェードイン
    if (m_next_scene != nullptr)
    {
		if (!m_LoadScene->GetFade()->GetDraw())
		{
            m_LoadScene->GetFade()->In();
		}
	}

    // フェードが終わりスクリーンがロード画面ではない場合
    if (m_LoadScene->GetFade()->FeadInEnd() && m_change == true)
    {
        m_change = false;
        m_count = 0;
    }
}

//==========================================================================
// 描画モード
void CScreen::Fillmode(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_MSLIB_DEBUG)
    if (m_system.Get("Fill Mode"))
    {
        SetRenderWIREFRAME(pDevice);
    }
    else
    {
        SetRenderSOLID(pDevice);
    }
#else
    pDevice;
#endif
}

//==========================================================================
// 描画モード
void CScreen::EndFillmode(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_MSLIB_DEBUG)
    if (m_system.Get("Fill Mode"))
    {
        SetRenderSOLID(pDevice);
    }
#else
    pDevice;
#endif
}

//==========================================================================
// フィルタリング
void CScreen::Fittering(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_MSLIB_DEBUG)
    if (m_system.Get("Fittering"))
	{
		SamplerFitteringGraphical(pDevice);
	}
	else
	{
		SamplerFitteringLINEAR(pDevice);
	}
#else
    pDevice;
#endif
}

//==========================================================================
// デバッグ用シーンchange
void CScreen::DebugSceneChange(void)
{
#if defined(_MSLIB_DEBUG)
    if (m_ImGui.NewMenu("Scene Manager", true))
    {
        m_ImGui._Bullet();
        if (m_ImGui.MenuItem("Title"))
        {
            ScreenChange(new Title::Scene);
        }
        m_ImGui._Bullet();
        if (m_ImGui.MenuItem("Result"))
        {
            ScreenChange(new Result::Scene);
        }
		m_ImGui._Bullet();
		if (m_ImGui.MenuItem("Stage1"))
		{
			ScreenChange(new Stage1::Scene);
		}
		m_ImGui._Bullet();
		if (m_ImGui.MenuItem("Opening"))
		{
			ScreenChange(new Opening::Scene);
		}
        m_ImGui._Bullet();
        if (m_ImGui.MenuItem("Motion"))
        {
            ScreenChange(new Motion::Scene);
        }
        m_ImGui._Bullet();
        if (m_ImGui.MenuItem("CheckCollider"))
        {
            ScreenChange(new CheckCollider::Scene);
        }
        m_ImGui.EndMenu();
    }
#endif
}

//==========================================================================
// SystemWindow
void CScreen::ImGuiSystemMenuBar(void)
{
#if defined(_MSLIB_DEBUG)
    // ImGui System Bar
    auto *pdevice = mslib::manager::Manager::GetDevice();
    m_ImGui._BeginMainMenuBar({ 0,(float)pdevice->GetWindowsSize()->y - __ImGuiWindowY });
    if (m_ImGui.NewMenu("System"))
    {
        // ワイヤーフレームに切り替える
        m_ImGui.Checkbox("Switch to wire frame", m_system.Get("Fill Mode"));
        // フィルタリングのセット
        m_ImGui.Checkbox("Change filtering", m_system.Get("Fittering"));
        m_ImGui.Checkbox("System Open", m_system.Get("System Window"));
        m_ImGui._Bullet();
        if (m_ImGui.MenuItem("End"))
        {
            DestroyWindow(pdevice->GetHwnd());
        }
        m_ImGui.EndMenu();
    }
    // ImGui ウィンドウ情報
    m_ImGui.WindowInfo();

    // ImGui シーンボタン
    DebugSceneChange();
    m_ImGui._EndMainMenuBar();
#endif
}

//==========================================================================
// NextWindow
void CScreen::ImGuiNextWindow(void)
{
#if defined(_MSLIB_DEBUG)
    // ImGui Operation window
    if (m_system.Get("System Window"))
    {
        m_ImGui.SetNextWindowPos({ 0,0 });
    }
    else
    {
        m_ImGui.SetNextWindowPos({ -300,0 });
    }
#endif
}
