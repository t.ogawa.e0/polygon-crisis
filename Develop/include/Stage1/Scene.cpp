//==========================================================================
// チュートリアル [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "Player/Weapon.h"
#include "Player/LifeUI.h"
#include "Player/Player.h"
#include "Enemy/Nucleus/Nucleus.h"
#include "Enemy/Uaz469/Uaz469.h"
#include "Enemy/Torchka/Torchka.h"
#include "Asset/ControllerObject.h"
#include "Asset/LightObject.h"
#include "Asset/TPSCamera.h"
#include "Asset/Map.h"
#include "Asset/SkyDome.h"
#include "Asset/ActionRange.h"
#include "Asset/Pause.h"
#include "Asset/AttackObjectAdministrator.h"
#include "Asset/BGMObject.h"
#include "Asset/Pause.h"
#include "Asset/GameEnd.h"
#include "Asset/xBox_Controller.h"
#include "config.h"
#include "resource_list.h"

#include "common/dx9math.h"

namespace Stage1
{
	class InitObject : public mslib::ObjectManager
	{
	public:
		InitObject() :ObjectManager("InitObject") {}
		~InitObject() {
			for (auto & itr : m_MsEffekseer)
			{
				itr.SetHandle(-1);
			}
			m_MsEffekseer.clear();
		}
		virtual void Init() {
			auto effe = GetEffekseerLoader();
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/Defense.efk", "Defense"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/Jump.efk", "Jump"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/Sword.efk", "Sword"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/DamageEffect.efk", "DamageEffect"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/GoldenEffect.efk", "GoldenEffect"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/Trajectory.efk", "Trajectory"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/smoke.efk", "smoke"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/TorchkaAttack1.efk", "TorchkaAttack"));
			m_MsEffekseer.push_back(effe->Load((const EFK_CHAR*)L"resource/Effekseer/NucleusAttack1.efk", "NucleusAttack1"));
		}
		virtual void Update() {}
		virtual void Debug() {}
	private:
		std::list<mslib::MsEffekseer::EffekseerReference> m_MsEffekseer;
	};

    Scene::Scene() : BaseScene("Stage1", SceneLevel::__Stage1__)
    {
		AddComponent<InitObject>();
        AddComponent<common::LightObject>();
        AddComponent<common::ControllerObject>();
        AddComponent<Editor::EditorCamera>();
        AddComponent<common::TPSCamera>();
        AddComponent<common::ActionRange>("AnUrbanArea", 142);
		AddComponent<enemy::Nucleus>(2, 2);
		AddComponent<player::LifeUI>();
        AddComponent<player::Player>();
        AddComponent<enemy::Uaz469>(2, 2);
        AddComponent<enemy::Torchka>(2, 2);
        AddComponent<common::Map>();
        AddComponent<weapon::Weapon>();
        AddComponent<common::SkyDome>();
        AddComponent<common::AttackObjectAdministrator>();
		AddComponent<common::BGMObject>(RESOURCE_battle_bgm_wav);
		AddComponent<common::Pause>();
		AddComponent<common::GameEnd>();
		AddComponent<common::xBox_Controller>();
    }

    Scene::~Scene()
    {
    }

}