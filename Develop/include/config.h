//==========================================================================
// コンフィグ [config.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

#include <string>

namespace FolderStructure {
    const std::string __slash__ = std::string("/");
    const std::string __resource__ = std::string("resource") + __slash__;
    const std::string __level__ = __resource__ + std::string("level") + __slash__;
    const std::string __motion__ = __resource__ + std::string("motion") + __slash__;
}

namespace SceneLevel {
    const std::string __Title__ = FolderStructure::__level__ + std::string("level1");
    const std::string __Game__ = FolderStructure::__level__ + std::string("level2");
    const std::string __Result__ = FolderStructure::__level__ + std::string("level3");
    const std::string __Tutorial__ = FolderStructure::__level__ + std::string("level4");
    const std::string __Stage1__ = FolderStructure::__level__ + std::string("level5");
    const std::string __MotionEditor__ = FolderStructure::__level__ + std::string("level100");
    const std::string __CheckCollider__ = FolderStructure::__level__ + std::string("level101");
    const std::string __Effekseer__ = FolderStructure::__level__ + std::string("level102");
}

