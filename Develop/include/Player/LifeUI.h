//==========================================================================
// LifeUI [LifeUI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "Asset/GameEnd.h"
#include "dxlib.h"

namespace player
{
	//==========================================================================
	//
	// class  : LifeUI
	// Content: LifeUI
	//
	//==========================================================================
	class LifeUI : public mslib::ObjectManager
	{
	public:
		LifeUI();
		~LifeUI();

		/**
		@brief 初期化
		*/
		void Init() override;

		/**
		@brief 更新
		*/
		void Update() override;

		/**
		@brief デバッグ
		*/
		void Debug() override;

		// 一回の呼び出しで1%のダメージが加算されます
		void AddDamage();
	protected:
		void Input(int value);
	protected:
		mslib::math::Vector3 m_main_position; // ゲージの基本座標
		mslib::math::Vector3 m_end_position; // ゲージの消失地点
		std::vector<mslib::sprite::SpriteAnimation*> m_NumberBox;
		common::GameEnd * m_GameEnd;
		float m_one_percent; // 1%
		float m_damage; // ダメージ
		int m_call_count; // 呼び出しカウント
		bool m_init;
		bool m_change;
	};
}
