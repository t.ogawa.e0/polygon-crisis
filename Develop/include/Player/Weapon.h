//==========================================================================
// 武器オブジェクト [Weapon.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Asset/AttackCollision.h"

namespace weapon
{
    class Weapon : public mslib::ObjectManager
    {
    public:
        Weapon();
        ~Weapon();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;

        mslib::transform::Transform * GetWeaponModel();
    private:
        mslib::transform::Transform * m_weapon;
        bool m_init_flag;
    };
}
