//==========================================================================
// プレイヤー [Player.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Player.h"
#include "Asset/ControllerObject.h"
#include "Asset/ShaderModel.h"
#include "Asset/TPSCamera.h"
#include "Asset/Jump.h"
#include "Asset/Step.h"
#include "Asset/LightObject.h"
#include "Asset/Character.h"
#include "AttackObject/AttackBox.h"
#include "AttackObject/AttackEffect.h"
#include "Editor/Motion/MotionEditor.h"
#include "SetAnimation.h"
#include "Weapon.h"
#include "PlayerManager.h"
#include "common/DefaultSystem.h"
#include "config.h"
#include "resource_list.h"

namespace player
{
    constexpr float move_buf = 0.1f;
    constexpr int virtual_object = 0;
    constexpr int virtual_controller = 1;
    constexpr int virtual_controller_camera = 2;

    constexpr int effe_Defense = 0;
	constexpr int effe_Jump = 1;
	constexpr int effe_Sword = 2;

    Player::Player() : ObjectManager("Player")
    {
        m_controller = nullptr;
        m_player = nullptr;
        m_jump = nullptr;
        m_step = nullptr;
        m_HandObject = nullptr;
        m_PlayerAnimation = nullptr;
        m_WeaponManager = nullptr;
        m_input_param = D3DXVECTOR3(0, 0, 0);
		m_jump_caunt = 0;
		m_AttackBox = nullptr;
		m_AttackEffect = nullptr;
        m_set_count = 0;
        m_function = nullptr;
        AddActivity();
		m_se_label_attack = 0;
		m_se_label_move = 0;
		m_se_label_landing = 0;
		m_move_se_count = 0;
    }

    Player::~Player()
    {
    }
    void Player::Init()
    {
        // 仮想オブジェクト
        auto obj1 = Object()->Create();
        auto obj2 = Object()->Create();

        // 機能を追加
        auto pPlayerCharacter = obj1->AddComponent<common::PlayerCharacter>();
        m_function = pPlayerCharacter;

		// アニメーション
        m_PlayerAnimation = obj1->AddComponent<PlayerAnimation::SetAnimation>();

		// マネージャーに登録
        obj1->AddComponent<PlayerManager>();

		// 攻撃範囲
		m_AttackBox = obj1->AddComponent<AttackBox>();
		mslib::cast<AttackBox>(m_AttackBox)->InitAll();

		// 外部機能を取得
		m_CameraObj = GetParent()->GetComponent<common::TPSCamera>();
		m_controller = GetParent()->GetComponent<common::ControllerObject>()->XInput_();

        // シェーダーを使用したモデル
        Setting();

        obj1->SetComponentName("VirtualObject");
        obj2->SetComponentName("VirtualController");

        // エフェクトオブジェクト
        auto obj = obj1->AddComponent<mslib::object::Effekseer>();
        obj->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/Defense.efk", "Defense"));
        obj->SetComponentName("DefenseEffect");
        m_effe.push_back(obj);

		// エフェクト取得
		obj = Effekseer()->Create();
		obj->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/Jump.efk", "Jump"));
		obj->SetComponentName(mslib::text("%p", obj));
		m_effe.push_back(obj);

		// エフェクト取得
		obj = m_HandObject->AddComponent<mslib::object::Effekseer>();
		obj->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/Sword.efk", "Sword"));
		obj->SetComponentName(mslib::text("%p", obj));
		m_effe.push_back(obj);

		// 攻撃エッフェクト
		m_AttackEffect = obj1->AddComponent<AttackEffect>();
		mslib::cast<AttackEffect>(m_AttackEffect)->InitAll();
		mslib::cast<AttackEffect>(m_AttackEffect)->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/DamageEffect.efk", "DamageEffect"));

		// 黄金エフェクト
		obj = m_HandObject->AddComponent<mslib::object::Effekseer>();
		obj->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/GoldenEffect.efk", "GoldenEffect"));
		obj->SetComponentName(mslib::text("%p", obj));
		obj->SetScale(0.5f, 0.5f, 0.5f);
		obj->Play(obj);

		// SE設定
		auto sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_battle_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label_attack);

		sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_footstep02_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label_move);

		sound = mslib::xaudio2::XAudio2SoundLabel(RESOURCE_footstep01_se_wav, 0, 1.0f);
		XAudio()->Init(sound, m_se_label_landing);
    }
    void Player::Update()
    {
		// 敵との当たり判定が出現した回数だけエフェクトを生成
		for (int i = 0; i < mslib::cast<AttackBox>(m_AttackBox)->GetHitCount(); i++)
		{
			mslib::cast<AttackEffect>(m_AttackEffect)->Create();
		}

        // コリジョンの処理
        Collider();

        // 入力処理
        InputProcessing();

		// 武器
        WeaponInput();
    }
    void Player::Debug()
    {
		// デバッグ
		if (ImGui()->Button("Create Attack Effect"))
			mslib::cast<AttackEffect>(m_AttackEffect)->Create();
	}
	mslib::transform::Transform * Player::GetHandObject()
    {
        return m_HandObject;
    }
    void Player::InputProcessing()
    {
		// モデルに反映
        m_input_param = D3DXVECTOR3(0, 0, 0);
        auto pPlayerCharacter = mslib::cast<common::PlayerCharacter>(m_function);
        pPlayerCharacter->SetParameter(Object()->Get(virtual_object));

        Keyboard();
        Controller();
        Jump();
        Defense();
        Step();
        Attack();
        ModelController();
    }
    void Player::Keyboard()
    {
        if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckLandingEnd() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckStepEnd() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckAttack() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckAttackEnd()
            )return;

        if (GetDInputKeyboard()->Press(KeyboardButton::KEY_A))
        {
            m_input_param.x = -0.05f;
        }
        if (GetDInputKeyboard()->Press(KeyboardButton::KEY_D))
        {
            m_input_param.x = 0.05f;
        }
        if (GetDInputKeyboard()->Press(KeyboardButton::KEY_W))
        {
            m_input_param.z = 0.05f;
        }
        if (GetDInputKeyboard()->Press(KeyboardButton::KEY_S))
        {
            m_input_param.z = -0.05f;
        }
    }
    void Player::Controller()
    {
		// コントローラーの有無
        if (!mslib::nullptr_check(m_controller))return;
        if (!m_controller->Check(0))return;

		// 入力
        D3DXVECTOR3 vinput;
        if (m_controller->AnalogL(0, vinput))
        {
            m_input_param = vinput;
            m_input_param *= move_buf;
            m_input_param.y = 0;
        }
    }
    void Player::Collider()
    {
        auto obj1 = Object()->Get(virtual_object);
        auto obj2 = Object()->Get(virtual_controller);
        
        // 仮想コントローラーに反映
        obj2->SetPosition(*obj1->GetPosition());

        auto pPlayerCharacter = mslib::cast<common::PlayerCharacter>(m_function);
        pPlayerCharacter->Collider();

		if (m_jump_caunt == 0)return;

        // 地面に接触時
		if (pPlayerCharacter->GetVersusMap()->Trigger() || pPlayerCharacter->GetVersusHighGroundUnder()->Trigger()) {
			m_jump_caunt = 0;
			if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->LandingEnd())
				XAudio()->Play(m_se_label_landing);
		}
        else
        {
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->Landing();
        }
    }
    void Player::ModelController()
    {
        auto obj1 = Object()->Get(virtual_object);
        auto obj2 = Object()->Get(virtual_controller);
        auto pCameraObj = mslib::cast<common::TPSCamera>(m_CameraObj);
        bool bmove = true;
        bool bdefense = mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckDefense();

        if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckLandingEnd() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckStepEnd() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckAttack() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckAttackEnd() ||
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckDefenseEnd()
            ) {
            bmove = false;
        }

        // 入力データがある場合反映
        if (m_input_param != D3DXVECTOR3(0, 0, 0))
        {
            D3DXVECTOR3 front;
            if (bmove || bdefense)
            {
                // 仮想コントローラーの向きを更新
                front = pCameraObj->Camera()->Get(0)->GetCamera()->GetVector().front;
                obj2->AddRotationX(front, 1.0f);
                obj2->AddPosition(m_input_param);
            }

            if (bmove && !bdefense)
            {
                // 仮想オブジェクトの向きを更新
                for (int i = 0; i < 10; i++)
                    obj1->AddRotationX(front, 1.0f);
                obj1->AddPosition(m_input_param);
                mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->Move();
				if (27 < m_move_se_count)
				{
					XAudio()->Play(m_se_label_move);
					m_move_se_count = -1;
				}
				m_move_se_count++;
            }
        }
        else
        {
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->MoveEnd();
        }

        // 描画オブジェクトの向きを更新し、座標を更新
        auto axis = m_player->ToSee(*obj2);
        m_player->AddRotationX(axis, 1.0f);
        m_player->SetPosition(*obj1->GetPosition());
        pCameraObj->SetObjectSmooth(m_player);
        for (int i = 0; i < 10; i++)
            obj1->AddRotationX(axis, 1.0f);
    }
    void Player::Jump()
    {
        if (!mslib::nullptr_check(m_controller))return;
        if (!m_controller->Check(0))return;

        // ジャンプの処理が存在するとき実行
        if (mslib::nullptr_check(m_jump))
        {
            // 終了判定が出た場合処理の破棄
            auto jm = (common::Jump*)m_jump;
            if (!jm->Trigger())
            {
                auto obj = Object()->Get(virtual_object);
                obj->DestroyComponent(jm);
                m_jump = nullptr;
            }
        }

        if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckDefense())return;

        // ジャンプ処理を実行
        if (m_controller->Trigger(mslib::xinput::XInputButton::A, 0))
        {
            if (mslib::nullptr_check(m_jump))return;
            if (2 == m_jump_caunt)return;
            auto obj = Object()->Get(virtual_object);
            auto jm = obj->AddComponent<common::Jump>();
            jm->Play(1, 0.1f);
            m_jump = jm;
            m_jump_caunt++;
            if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->Jump())
                m_effe[effe_Jump]->Play(m_player);
        }
    }
    void Player::Step()
    {
        if (!mslib::nullptr_check(m_controller))return;
        if (!m_controller->Check(0))return;

        // ステップの処理が存在するとき実行
        if (mslib::nullptr_check(m_step))
        {
            // 終了判定が出た場合処理の破棄
            auto st = (common::Step*)m_step;
            if (!st->Trigger())
            {
                auto obj = Object()->Get(virtual_object);
                obj->DestroyComponent(st);
                m_step = nullptr;
                mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->StepEnd();
            }
        }

        // ステップ実行可能条件
        if (m_input_param == D3DXVECTOR3(0, 0, 0) &&
            !mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckLanding() &&
            !mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckAttack() &&
            !mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckStepEnd()
            )return;

        // ステップ処理を実行
        if (m_controller->Trigger(mslib::xinput::XInputButton::B, 0) && !m_controller->Press(mslib::xinput::XInputButton::RIGHT_RB, 0))
        {
            if (mslib::nullptr_check(m_step))return;
            auto obj = Object()->Get(virtual_object);
            auto st = obj->AddComponent<common::Step>();
            st->Play(1, 0.1f);
            m_step = st;
            mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->Step();
        }
    }

    void Player::Setting()
    {
        // 外部からオブジェクトを取得
        auto light = GetParent()->GetComponent<common::LightObject>()->Light()->Get(0);
        auto camera = mslib::cast<common::TPSCamera>(m_CameraObj)->Camera();
        auto model = AddComponent<animation_model::AnimationShaderModel>();
        model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        model->SetCamera(camera->Get(0)->GetCamera());
        model->SetLight(light);
        model->SetComponentName("HumanMotion");

        model->AddParts2("Body", GetXModelLoader()->Load(PlayerAnimation::PartsName::Body));
        model->AddParts2("Head", GetXModelLoader()->Load(PlayerAnimation::PartsName::Head), "Body");
        model->AddParts2("Thigh_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Thigh_L), "Body");
        model->AddParts2("Calf_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Calf_L), "Thigh_L");
        model->AddParts2("Foot_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Foot_L), "Calf_L");
        model->AddParts2("Thigh_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Thigh_R), "Body");
        model->AddParts2("Calf_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Calf_R), "Thigh_R");
        model->AddParts2("Foot_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Foot_R), "Calf_R");
        model->AddParts2("Arms_Parent_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Parent_L), "Body");
        model->AddParts2("Arms_Child_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Child_L), "Arms_Parent_L");
        model->AddParts2("Hand_L", GetXModelLoader()->Load(PlayerAnimation::PartsName::Hand_L), "Arms_Child_L");
        model->AddParts2("Arms_Parent_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Parent_R), "Body");
        model->AddParts2("Arms_Child_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Arms_Child_R), "Arms_Parent_R");
        m_HandObject = model->AddParts2("Hand_R", GetXModelLoader()->Load(PlayerAnimation::PartsName::Hand_R), "Arms_Child_R");

        model->Load(FolderStructure::__motion__ + model->GetComponentName());
        model->GenerateAnimation(PlayerAnimation::MotionName::Attack1);
        model->GenerateAnimation(PlayerAnimation::MotionName::Attack2);
        model->GenerateAnimation(PlayerAnimation::MotionName::Attack3);
        model->GenerateAnimation(PlayerAnimation::MotionName::Attack4);
        model->GenerateAnimation(PlayerAnimation::MotionName::Attack5);
        model->GenerateAnimation(PlayerAnimation::MotionName::AttackEnd1);
        model->GenerateAnimation(PlayerAnimation::MotionName::AttackEnd2);
        model->GenerateAnimation(PlayerAnimation::MotionName::AttackEnd3);
        model->GenerateAnimation(PlayerAnimation::MotionName::AttackEnd4);
        model->GenerateAnimation(PlayerAnimation::MotionName::AttackEnd5);
        model->GenerateAnimation(PlayerAnimation::MotionName::Step);
        model->GenerateAnimation(PlayerAnimation::MotionName::StepEnd);
        model->GenerateAnimation(PlayerAnimation::MotionName::Landing);
        model->GenerateAnimation(PlayerAnimation::MotionName::LandingEnd);
        model->GenerateAnimation(PlayerAnimation::MotionName::Defense);
        model->GenerateAnimation(PlayerAnimation::MotionName::DefenseEnd);
        model->GenerateAnimation(PlayerAnimation::MotionName::Taiki);
        model->GenerateAnimation(PlayerAnimation::MotionName::Move);
        model->GenerateAnimation(PlayerAnimation::MotionName::Jump);

        mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->SetAnimationModel(model);

        m_player = model;
    }
    void Player::Attack()
    {
        if (!mslib::nullptr_check(m_controller))return;
        if (!m_controller->Check(0))return;

        if (m_controller->Trigger(mslib::xinput::XInputButton::X, 0))
        {
            if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->Attack())
            {
				XAudio()->Play(m_se_label_attack);
				mslib::cast<AttackBox>(m_AttackBox)->Attack();
				m_effe[effe_Sword]->Play();
            }
        }
    }
    void Player::Defense()
    {
        if (!mslib::nullptr_check(m_controller))return;
        if (!m_controller->Check(0))return;

        if (m_controller->Press(mslib::xinput::XInputButton::B, 0) && m_controller->Press(mslib::xinput::XInputButton::RIGHT_RB, 0))
        {
            auto pPlayerCharacter = mslib::cast<common::PlayerCharacter>(m_function);
            if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->Defense())
            {
				XAudio()->Play(m_se_label_attack);
                pPlayerCharacter->GetDefenseCollider()->UnlockUpdate();
                pPlayerCharacter->GetPlayerCollider()->LockUpdate();
                m_effe[effe_Defense]->Play();
            }
        }
        else
        {
            auto pPlayerCharacter = mslib::cast<common::PlayerCharacter>(m_function);
            pPlayerCharacter->GetDefenseCollider()->LockUpdate();
            pPlayerCharacter->GetPlayerCollider()->UnlockUpdate();
            if (mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->CheckDefense())
            {
                mslib::cast<PlayerAnimation::SetAnimation>(m_PlayerAnimation)->DefenseEnd();
            }
        }
    }
    void Player::WeaponInput()
    {
        if (m_set_count == 29)
        {
            if (!mslib::nullptr_check(m_WeaponManager))
            {
				m_WeaponManager = GetParent()->GetComponent<weapon::Weapon>();
				if (m_WeaponManager != nullptr&&m_HandObject != nullptr)
				{
					auto obj = mslib::cast<weapon::Weapon>(m_WeaponManager)->GetWeaponModel();
					m_HandObject->SetComponent(obj);
				}
			}
        }
        if (m_set_count != 30)
            m_set_count++;
    }
}