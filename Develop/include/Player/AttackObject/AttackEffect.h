//==========================================================================
// AttackEffect [AttackEffect.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace player
{
	class AttackEffect : public mslib::function::FunctionComponent, public mslib::initializer::Initializer, public mslib::MsEffekseer::SetEffekseer
	{
		struct Effect
		{
			Effect(mslib::object::Effekseer * obj) {
				effect = obj;
			}
			mslib::object::Effekseer * effect = nullptr;
			bool init = false;
		};
	public:
		AttackEffect();
		~AttackEffect();
		virtual void Init()override;
		virtual void Update() override;
		void Create();
	protected:
		std::vector<mslib::object::Effekseer*> m_effect_object; // テンプレート座標
		std::list<Effect> m_effect_list; // 生成エフェクトリスト
		std::mt19937 m_mt; // メルセンヌ・ツイスタの32ビット版、引数は初期シード値
		mslib::rand_int m_effect_rand; // エフェクト座標選択
		mslib::rand_float m_radian_rand; // ラジアン各
		int m_effect_count; // 出現したエフェクトの総数
	};
}