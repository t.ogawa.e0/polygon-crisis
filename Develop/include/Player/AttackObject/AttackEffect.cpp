//==========================================================================
// AttackEffect [AttackEffect.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "AttackEffect.h"

namespace player
{
	constexpr int __NumEffectCount__ = 5;
	AttackEffect::AttackEffect()
	{
		SetComponentName("AttackEffect");
		m_effect_object.reserve(__NumEffectCount__);
		m_effect_count = 0;
	}

	AttackEffect::~AttackEffect()
	{
	}
	void AttackEffect::Init()
	{
		std::random_device rnd; // 非決定的な乱数生成器を生成
		std::mt19937 mt(rnd()); // メルセンヌ・ツイスタの32ビット版、引数は初期シード値

		auto obj = GetParent()->AddComponent<mslib::object::Object>();
		obj->SetComponentName("AttackEffectBox");

		for (int i = 0; i < __NumEffectCount__; i++)
		{
			auto effe = obj->AddComponent<mslib::object::Effekseer>();
			effe->SetComponentName("AttackEffect" + std::to_string(i));
			m_effect_object.push_back(effe);
		}

		// 初期シード値の保存
		m_mt = mt;
		m_effect_rand = mslib::rand_int(0, __NumEffectCount__ - 1);
		m_radian_rand = mslib::rand_float(mslib::ToRadian(0.0f), mslib::ToRadian(360.0f));
	}
	void AttackEffect::Update()
	{
		for (auto itr = m_effect_list.begin(); itr != m_effect_list.end(); )
		{
			// 初期化されていない場合実行
			if (!(*itr).init)
			{
				(*itr).effect->AddRotation(m_radian_rand(m_mt), m_radian_rand(m_mt), m_radian_rand(m_mt));
				(*itr).init = true;
			}

			// 再生されていない場合終了
			if (!(*itr).effect->Check())
			{
				// コンポーネントを破棄し、管理を終了する
				DestroyComponent((*itr).effect);
				itr = m_effect_list.erase(itr);
				continue;
			}

			// 何事もなければ更新
			++itr;
		}
	}
	void AttackEffect::Create()
	{
		int serect = 0;

		// 箱の最大値の場合再選択
		for (;;)
		{
			serect = m_effect_rand(m_mt);

			// 最大値ではないので終了
			if ((int)m_effect_object.size() != serect)break;
		}

		// 各種データの取得
		auto obj = m_effect_object[serect];
		auto mat = obj->GetWorldMatrix();
		auto effect = AddComponent<mslib::object::Effekseer>();

		m_effect_count++;

		// ナンバリング
		effect->SetComponentName("Effect" + std::to_string(m_effect_count));
		effect->SetEffekseerData(GetEffekseerData());
		effect->SetMatrixType(mslib::object::MatrixType::Local);
		effect->SetPosition(mat->_41, mat->_42, mat->_43);
		effect->CreateWorldMatrix();
		effect->Play(effect);
		m_effect_list.push_back(Effect(effect));
	}
}