//==========================================================================
// AttackBox [AttackBox.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "AttackBox.h"
#include "common/Collider.h"
#include "Asset/FieldBox.h"
#include "Asset/FieldPlane.h"
#include "Asset/FieldHighGround.h"
#include "Enemy/EnemyManager.h"

namespace player
{
	AttackBox::AttackBox()
    {
        SetComponentName("AttackBox");
		m_BoxCollider = nullptr;
		m_NumHitCount = 0;
    }

	AttackBox::~AttackBox()
    {
    }
	void AttackBox::Init()
	{
		m_BoxCollider = GetParent()->AddComponent<mslib::collider::BoxCollider>();
		m_BoxCollider->SetComponentName("AttackBox_by_BoxCollider");
	}
	void AttackBox::Update()
	{
		m_NumHitCount = 0;

		// チェックtask
		for (; m_task.begin() != m_task.end(); )
		{
			auto itr = m_task.begin();
			if ((*itr)->m_collider->Trigger())
			{
				m_NumHitCount++;
				(*itr)->m_HitCount--;
			}
			m_task.erase(itr);
		}

		// 登録されたEnemyチェック
		for (auto itr = m_enemy.begin(); itr != m_enemy.end(); )
		{
			// 上限に達していない
			if (itr->second.m_HitCount != 0)
			{
				++itr;
				continue;
			}

			// 相手を消す処理
			itr->second.m_Parent->DestroyComponent(itr->second.m_this);
			itr = m_enemy.erase(itr);
		}
	}
	void AttackBox::Attack()
	{
		for (auto & itr : enemy::EnemyManager::GetAllEnemy())
		{
			// 存在しないので生成
			if (m_enemy.find(itr->GetEnemy()) == m_enemy.end())
			{
				auto & enemy = m_enemy[itr->GetEnemy()];
				enemy.m_HitCount = 5;
				enemy.m_Parent = itr->GetEnemy()->GetParent();
				enemy.m_this = itr->GetEnemy();
				enemy.m_collider = enemy.m_this->AddComponent<mslib::collider::Collider>();
				m_BoxCollider->AddCollider(enemy.m_collider);
			}

			auto itr2 = m_enemy.find(itr->GetEnemy());
			if (itr2 == m_enemy.end())continue;

			// タスクに登録
			m_task.push_back(&itr2->second);
		}
	}
	int AttackBox::GetHitCount()
	{
		return m_NumHitCount;
	}
}