//==========================================================================
// AttackBox [AttackBox.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace player
{
	class AttackBox : public mslib::function::FunctionComponent, public mslib::initializer::Initializer
	{
	protected:
		struct Attack_t {
			int m_HitCount = 0;
			Component * m_Parent = nullptr;
			mslib::transform::Transform * m_this = nullptr;
			mslib::collider::Collider * m_collider = nullptr;
		};
	public:
		AttackBox();
        ~AttackBox();
		virtual void Init()override;
		virtual void Update() override;
		void Attack();
		int GetHitCount();
    protected:
		std::unordered_map<mslib::transform::Transform*, Attack_t> m_enemy;
		mslib::collider::BoxCollider * m_BoxCollider;
		std::list<Attack_t*> m_task; // �U��task
		int m_NumHitCount;
    };
}