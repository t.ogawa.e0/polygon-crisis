//==========================================================================
// アニメーション反映のためだけの処理 [SetAnimation.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once
#include "dxlib.h"
#include "common/FunctionComponent.h"
#include "Editor/Motion/MotionEditor.h"
#include "resource_list.h"

namespace PlayerAnimation
{
    namespace PartsName {
        constexpr const char* Arms_Child_L = RESOURCE_Human_Arms_Child_L_x;
        constexpr const char* Arms_Child_R = RESOURCE_Human_Arms_Child_R_x;
        constexpr const char* Arms_Parent_L = RESOURCE_Human_Arms_Parent_L_x;
        constexpr const char* Arms_Parent_R = RESOURCE_Human_Arms_Parent_R_x;
        constexpr const char* Body = RESOURCE_Human_Body_x;
        constexpr const char* Calf_L = RESOURCE_Human_Calf_L_x;
        constexpr const char* Calf_R = RESOURCE_Human_Calf_R_x;
        constexpr const char* Foot_L = RESOURCE_Human_Foot_L_x;
        constexpr const char* Foot_R = RESOURCE_Human_Foot_R_x;
        constexpr const char* Hand_L = RESOURCE_Human_Hand_L_x;
        constexpr const char* Hand_R = RESOURCE_Human_Hand_R_x;
        constexpr const char* Head = RESOURCE_Human_Head_x;
        constexpr const char* Thigh_L = RESOURCE_Human_Thigh_L_x;
        constexpr const char* Thigh_R = RESOURCE_Human_Thigh_R_x;
    };
    namespace MotionName {

        // 以下対となるアニメーション
        constexpr const char* Step = "Step"; // ステップ
        constexpr const char* StepEnd = "StepEnd"; // ステップ終了

        constexpr const char* Landing = "Landing"; // 空中
        constexpr const char* LandingEnd = "LandingEnd"; // 着地

        constexpr const char* Defense = "Defense"; // 防御
        constexpr const char* DefenseEnd = "DefenseEnd"; // 防御終了

        // 独立アニメーション
        constexpr const char* Taiki = "Taiki"; // 待機

        constexpr const char* Move = "Move"; // 移動

        constexpr const char* Jump = "Jump"; // ジャンプ

        // 繋ぎ
        constexpr const char* Attack1 = "Attack1"; // 攻撃
        constexpr const char* Attack2 = "Attack2"; // 攻撃
        constexpr const char* Attack3 = "Attack3"; // 攻撃
        constexpr const char* Attack4 = "Attack4"; // 攻撃
        constexpr const char* Attack5 = "Attack5"; // 攻撃

        constexpr const char* AttackEnd1 = "AttackEnd1"; // 攻撃終了
        constexpr const char* AttackEnd2 = "AttackEnd2"; // 攻撃終了
        constexpr const char* AttackEnd3 = "AttackEnd3"; // 攻撃終了
        constexpr const char* AttackEnd4 = "AttackEnd4"; // 攻撃終了
        constexpr const char* AttackEnd5 = "AttackEnd5"; // 攻撃終了

    }

    class SetAnimation : public mslib::function::FunctionComponent
    {
    public:
        SetAnimation();
        ~SetAnimation();
        void Update() override;
        void SetAnimationModel(animation_model::AnimationShaderModel*obj);

        // 移動アニメーション
        bool Move();
        bool CheckMove();
        // 移動停止アニメーション
        void MoveEnd();

        // 待機アニメーション
        bool Taiki();
        bool CheckTaiki();

        // ジャンプアニメーション
        bool Jump();
        bool CheckJump();

        // 空中アニメーション
        bool Landing();
        bool CheckLanding();
        // 着地アニメーション
        bool LandingEnd();
        bool CheckLandingEnd();

        // 防御アニメーション
        bool Defense();
        bool CheckDefense();
        // 防御終了アニメーション
        bool DefenseEnd();
        bool CheckDefenseEnd();

        // ステップアニメーション
        bool Step();
        bool CheckStep();
        // ステップ終了アニメーション
        bool StepEnd();
        bool CheckStepEnd();

        // 攻撃アニメーション
        bool Attack();
        bool CheckAttack();
        bool CheckAttackEnd();
    protected:
        animation_model::AnimationShaderModel * m_model;
        int m_AttackCount;
        bool m_Landing;
        bool m_Step;
        bool m_AttackEnd;
    };
}
