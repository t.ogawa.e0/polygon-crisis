//==========================================================================
// LifeUI [LifeUI.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LifeUI.h"
#include "resource_list.h"

namespace player
{
	constexpr float __OneHundredPercent__ = 0.01f; // 100%
	constexpr int __InitCallValue__ = mslib::Seconds(10); // 最大値
	constexpr int __NumberBoxSize__ = 3; // 箱の最大
	constexpr int __PusyuScore__ = 10; // 繰り上げ定数

	LifeUI::LifeUI() : ObjectManager("LifeUI")
	{
		m_init = false;
		m_change = false;
		m_damage = 0.0f;
		m_call_count = 0;
		m_GameEnd = nullptr;
	}

	LifeUI::~LifeUI()
	{
	}
	void LifeUI::Init()
	{
		auto obj1 = Sprite()->Create();
		obj1->SetTextureData(GetTextureLoader()->Load(RESOURCE_LifeUI_DDS));
		obj1->SetComponentName("LifeUI");

		auto obj2 = Sprite()->Create();
		obj2->SetTextureData(GetTextureLoader()->Load(RESOURCE_LifeGauge_DDS));
		obj2->SetPolygonSize((mslib::math::Vector2(obj2->GetPolygonSize()->x, 20)*GetDevice()->ScreenBuffScale())*0.9f);
		obj2->SetComponentName("LifeGauge");

		auto obj3 = Sprite()->Create();
		obj3->SetTextureData(GetTextureLoader()->Load(RESOURCE_White1Pixel_DDS));
		obj3->SetPolygonSize(*obj2->GetPolygonSize());
		obj3->SetComponentName("LifeWhite1Pixel");

		m_NumberBox.reserve(__NumberBoxSize__);
		for (int i = 0; i < __NumberBoxSize__; i++)
		{
			auto obj = SpriteAnimation()->Create();
			obj->SetTextureData(GetTextureLoader()->Load(RESOURCE_Number_DDS));
			obj->SetComponentName("Number" + std::to_string(i));
			obj->SetAnimationData(1, 10, 10);
			m_NumberBox.push_back(obj);
		}

		m_GameEnd = GetParent()->GetComponent<common::GameEnd>();
	}
	void LifeUI::Update()
	{
		auto obj = Sprite()->Get(1);

		// 初期化
		if (m_init == false)
		{
			m_main_position = *obj->GetPosition();
			m_end_position = m_main_position;
			m_end_position.x -= obj->GetPolygonSize()->x;
			m_one_percent = obj->GetPolygonSize()->x * __OneHundredPercent__;
			m_init = true;
		}

		// ダメージ分処理を実行する
		auto damage = m_damage * __OneHundredPercent__;
		obj->AddPosition(-damage, 0, 0);
		m_damage -= damage;

		// ダメージが-値にならないために必要
		if (m_damage < 0)
			m_damage = 0;

		// ゲージ回復まで
		m_call_count--;

		// 回復が有効
		if (m_call_count <= 0)
		{
			// 割合で回復処理
			m_call_count = 0;
			obj->AddPosition(m_one_percent, 0, 0);
			if (m_main_position.x < obj->GetPosition()->x)
				obj->SetPosition(m_main_position);
		}

		Input((int)((obj->GetPosition()->x - m_end_position.x) / m_one_percent));
	}
	void LifeUI::Debug()
	{
		auto obj = Sprite()->Get(1);
		int DamageCount = 0;

		if (ImGui()->_InputInt("Add Damage Count", &DamageCount, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			for (int i = 0; i < DamageCount; i++)
			{
				AddDamage();
			}
		}

		int value = (int)((obj->GetPosition()->x - m_end_position.x) / m_one_percent);
		ImGui()->Text(mslib::text("%d ％", value));
		ImGui()->Text(mslib::text("value : %d : %d", value%__PusyuScore__, value));
		value /= __PusyuScore__;
		ImGui()->Text(mslib::text("value : %d : %d", value%__PusyuScore__, value));
		value /= __PusyuScore__;
		ImGui()->Text(mslib::text("value : %d : %d", value%__PusyuScore__, value));
		value /= __PusyuScore__;
	}
	void LifeUI::AddDamage()
	{
		if (!GetActivity())return;

		// ダメージ加算
		m_damage += m_one_percent;
		m_call_count = __InitCallValue__;
	}
	void LifeUI::Input(int value)
	{
		for (auto & itr : m_NumberBox)
		{
			itr->SetActivity(false);
			itr->PlayAnimation(false);
		}

		if (value < 0)
		{
			value = 0;
			auto obj_itr = m_NumberBox[2];
			obj_itr->SetActivity(true);
			obj_itr->PlayAnimation(true);
			obj_itr->SetAnimationCounter(value);
			SetActivity(false);
			if (m_GameEnd != nullptr)
				m_GameEnd->GameOver();
			return;
		}

		for (int i = __NumberBoxSize__; i != 0; i--)
		{
			auto obj_itr = m_NumberBox[i - 1];

			obj_itr->SetActivity(true);
			obj_itr->PlayAnimation(true);
			obj_itr->SetAnimationCounter(value%__PusyuScore__);
			value /= __PusyuScore__;
			if (value == 0) break;
		}
	}
}