//==========================================================================
// プレイヤーマネージャー [PlayerManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlayerManager.h"

namespace player
{
    std::list<PlayerManager*> PlayerManager::m_player; // プレイヤー

    PlayerManager::PlayerManager()
    {
        SetComponentName("PlayerManager" + mslib::text("%p", this));
        m_player.push_back(this);
        m_obj = nullptr;
    }

    PlayerManager::~PlayerManager()
    {
        auto itr = std::find(m_player.begin(), m_player.end(), this);
        if (itr != m_player.end())
            m_player.erase(itr);
    }
    void PlayerManager::Init()
    {
        m_obj = GetWarrantyParent<mslib::transform::Transform>();
    }
    std::list<PlayerManager*>& PlayerManager::GetAllPlayer()
    {
        return m_player;
    }
    mslib::transform::Transform * PlayerManager::GetPlayer()
    {
        return m_obj;
    }
}