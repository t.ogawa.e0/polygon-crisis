//==========================================================================
// 武器オブジェクト [Weapon.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Weapon.h"
#include "Asset/ShaderModel.h"
#include "Asset/LightObject.h"
#include "Asset/TPSCamera.h"
#include "resource_list.h"

namespace weapon
{
    Weapon::Weapon() : ObjectManager("Weapon")
    {
        m_weapon = nullptr;
        m_init_flag = false;
    }

    Weapon::~Weapon()
    {
    }
    void Weapon::Init()
    {
        auto light = GetParent()->GetComponent<common::LightObject>()->Light()->Get(0);
        auto camera = GetParent()->GetComponent<common::TPSCamera>()->Camera();

        auto obj = Object()->Create();
        obj->SetComponentName("WeaponObject");

        m_weapon = obj->AddComponent<mslib::object::Object>();

        m_weapon->SetComponentName("Connection");

        auto model = m_weapon->AddComponent<ShaderModel::PerPixelLightingModel>();
        model->SetXModelData(GetXModelLoader()->Load(RESOURCE_Weapon_x));
        model->SetEffectData(GetEffectLoader()->Load(RESOURCE_PerPixelLighting_fx));
        model->SetCamera(camera->Get(0)->GetCamera());
        model->SetLight(light);
        model->SetComponentName("Weapon");

		// エフェクトオブジェクト
		auto effect = model->AddComponent<mslib::object::Effekseer>();
		effect->SetEffekseerData(GetEffekseerLoader()->Load((const EFK_CHAR*)L"resource/Effekseer/Trajectory.efk", "Trajectory"));
		effect->SetComponentName("Trajectory");
		effect->Play(effect);
		effect->SetPosition(0, 2.41f, -0.02f);
    }
    void Weapon::Update()
    {
    }
    void Weapon::Debug()
    {
    }
    mslib::transform::Transform * Weapon::GetWeaponModel()
    {
        return mslib::cast<ShaderModel::PerPixelLightingModel>(m_weapon);
    }
}