//==========================================================================
// アニメーション反映のためだけの処理 [SetAnimation.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SetAnimation.h"

namespace PlayerAnimation
{
    constexpr const int __AttackCountMax__ = 5;

    SetAnimation::SetAnimation()
    {
        SetComponentName("SetAnimation");
        m_model = nullptr;
        m_AttackCount = 0;
        m_Landing = false;
        m_Step = false;
        m_AttackEnd = false;
        AddActivity();
    }

    SetAnimation::~SetAnimation()
    {
    }
    void SetAnimation::Update()
    {
        /*
        以下、自動処理
        */

        // 1撃目
        if (m_AttackCount == 1 && !m_model->Check(MotionName::Attack1) && !m_model->Check(MotionName::AttackEnd1) && !m_AttackEnd)
        {
            m_model->Play(MotionName::AttackEnd1);
            m_model->SetAnimationUpdateFlame(2);
            m_AttackEnd = true;
        }

        // 2撃目
        if (m_AttackCount == 2 && !m_model->Check(MotionName::Attack2) && !m_model->Check(MotionName::AttackEnd2) && !m_AttackEnd)
        {
            m_model->Play(MotionName::AttackEnd2);
            m_model->SetAnimationUpdateFlame(2);
            m_AttackEnd = true;
        }

        // 3撃目
        if (m_AttackCount == 3 && !m_model->Check(MotionName::Attack3) && !m_model->Check(MotionName::AttackEnd3) && !m_AttackEnd)
        {
            m_model->Play(MotionName::AttackEnd3);
            m_model->SetAnimationUpdateFlame(2);
            m_AttackEnd = true;
        }

        // 4撃目
        if (m_AttackCount == 4 && !m_model->Check(MotionName::Attack4) && !m_model->Check(MotionName::AttackEnd4) && !m_AttackEnd)
        {
            m_model->Play(MotionName::AttackEnd4);
            m_model->SetAnimationUpdateFlame(2);
            m_AttackEnd = true;
        }

        // 5撃目
        if (m_AttackCount == 5 && !m_model->Check(MotionName::Attack5) && !m_model->Check(MotionName::AttackEnd5) && !m_AttackEnd)
        {
            m_model->Play(MotionName::AttackEnd5);
            m_model->SetAnimationUpdateFlame(2);
            m_AttackEnd = true;
        }

        // 攻撃関連のアニメーションが実行中ではない場合
        if (
            !m_model->Check(MotionName::Attack1) &&
            !m_model->Check(MotionName::Attack2) &&
            !m_model->Check(MotionName::Attack3) &&
            !m_model->Check(MotionName::Attack4) &&
            !m_model->Check(MotionName::Attack5) &&
            !m_model->Check(MotionName::AttackEnd1) &&
            !m_model->Check(MotionName::AttackEnd2) &&
            !m_model->Check(MotionName::AttackEnd3) &&
            !m_model->Check(MotionName::AttackEnd4) &&
            !m_model->Check(MotionName::AttackEnd5)
            )
        {
            m_AttackEnd = false;
            m_AttackCount = 0;
        }

        if (!m_model->Check(MotionName::Step) && !m_model->Check(MotionName::StepEnd))
        {
            m_Step = false;
        }

        //if (!m_model->Check(MotionName::Landing) && !m_model->Check(MotionName::LandingEnd))
        //{
        //    m_Landing = false;
        //}

        // 自動的に待機モーションへ移行
        if (!m_Step && !m_Landing)
            Taiki();
    }
    void SetAnimation::SetAnimationModel(animation_model::AnimationShaderModel * obj)
    {
        m_model = obj;
    }
    bool SetAnimation::Move()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        待機モーション以外が実行中の場合処理不可能
        */

        if (
            !m_model->Check(MotionName::Move) &&
            !m_model->Check(MotionName::Jump) &&
            !m_model->Check(MotionName::Attack1) &&
            !m_model->Check(MotionName::Attack2) &&
            !m_model->Check(MotionName::Attack3) &&
            !m_model->Check(MotionName::Attack4) &&
            !m_model->Check(MotionName::Attack5) &&
            !m_model->Check(MotionName::AttackEnd1) &&
            !m_model->Check(MotionName::AttackEnd2) &&
            !m_model->Check(MotionName::AttackEnd3) &&
            !m_model->Check(MotionName::AttackEnd4) &&
            !m_model->Check(MotionName::AttackEnd5) &&
            !m_model->Check(MotionName::Step) &&
            !m_model->Check(MotionName::StepEnd) &&
            !m_model->Check(MotionName::Landing) &&
            !m_model->Check(MotionName::LandingEnd) &&
            !m_model->Check(MotionName::Defense) &&
            !m_model->Check(MotionName::DefenseEnd) 
            )
        {
            m_model->Play(MotionName::Move, true);
            m_model->SetAnimationUpdateFlame(9);
            return true;
        }
        return false;
    }
    bool SetAnimation::CheckMove()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Move);
    }
    void SetAnimation::MoveEnd()
    {
        if (m_model == nullptr)return;

        // 実行中である
        if (m_model->Check(MotionName::Move))
        {
            // 停止
            m_model->Stop(MotionName::Move);
        }
    }
    bool SetAnimation::Taiki()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        全ての処理が実行中ではない
        */

        if (
            !m_model->Check(MotionName::Move) &&
            !m_model->Check(MotionName::Taiki) &&
            !m_model->Check(MotionName::Jump) &&
            !m_model->Check(MotionName::Attack1) &&
            !m_model->Check(MotionName::Attack2) &&
            !m_model->Check(MotionName::Attack3) &&
            !m_model->Check(MotionName::Attack4) &&
            !m_model->Check(MotionName::Attack5) &&
            !m_model->Check(MotionName::AttackEnd1) &&
            !m_model->Check(MotionName::AttackEnd2) &&
            !m_model->Check(MotionName::AttackEnd3) &&
            !m_model->Check(MotionName::AttackEnd4) &&
            !m_model->Check(MotionName::AttackEnd5) &&
            !m_model->Check(MotionName::Step) &&
            !m_model->Check(MotionName::StepEnd) &&
            !m_model->Check(MotionName::Landing) &&
            !m_model->Check(MotionName::LandingEnd) &&
            !m_model->Check(MotionName::Defense) &&
            !m_model->Check(MotionName::DefenseEnd)
            ) 
        {
            m_model->Play(MotionName::Taiki, true);
            m_model->SetAnimationUpdateFlame(2);
            return true;
        }
        return false;
    }
    bool SetAnimation::CheckTaiki()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Taiki);
    }
    bool SetAnimation::Jump()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        ジャンプ処理が実行中ではない
        攻撃処理が実行中ではない
        ガード中ではない
        */

        if (
            !m_model->Check(MotionName::Jump) &&
            //!m_model->Check(MotionName::Attack1) &&
            //!m_model->Check(MotionName::Attack2) &&
            //!m_model->Check(MotionName::Attack3) &&
            //!m_model->Check(MotionName::Attack4) &&
            //!m_model->Check(MotionName::Attack5) &&
            !m_model->Check(MotionName::Defense)
            )
        {
            m_model->Play(MotionName::Jump);
            m_model->SetAnimationUpdateFlame(3);
            m_Landing = false;
            m_AttackEnd = false;
            m_AttackCount = 0;
            return true;
        }

        return false;
    }
    bool SetAnimation::CheckJump()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Jump);
    }
    bool SetAnimation::Landing()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        空中処理が実行されていない
        空中処理が実行後ではない
        */

        if (!m_Landing && !m_model->Check(MotionName::Landing))
        {
            m_model->Play(MotionName::Landing);
            m_model->SetAnimationUpdateFlame(2);
            m_Landing = true;
            return true;
        }
        return false;
    }
    bool SetAnimation::CheckLanding()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Landing);
    }
    bool SetAnimation::LandingEnd()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        着地処理が実行されていない
        空中処理の実行後である
        */

        if (m_Landing && !m_model->Check(MotionName::LandingEnd))
        {
            m_model->Play(MotionName::LandingEnd);
            m_model->SetAnimationUpdateFlame(8);
            m_Landing = false;
            return true;
        }

        return false;
    }
    bool SetAnimation::CheckLandingEnd()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::LandingEnd);
    }
    bool SetAnimation::Defense()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        防御中ではない
        防御終了中ではない
        */

        if (!m_model->Check(MotionName::Defense) && !m_model->Check(MotionName::DefenseEnd))
        {
            m_model->Play(MotionName::Defense, true);
            m_model->SetAnimationUpdateFlame(1);
            return true;
        }

        return false;
    }
    bool SetAnimation::CheckDefense()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Defense);
    }
    bool SetAnimation::DefenseEnd()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        防御中のみ発動
        防御終了中は発動
        */

        if (!m_model->Check(MotionName::DefenseEnd) && m_model->Check(MotionName::Defense))
        {
            m_model->Play(MotionName::DefenseEnd);
            m_model->SetAnimationUpdateFlame(8);
            return true;
        }

        return false;
    }
    bool SetAnimation::CheckDefenseEnd()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::DefenseEnd);
    }
    bool SetAnimation::Step()
    {
        if (m_model == nullptr)return false;
        /*
        条件

        待機中は実行不可能
        攻撃中は実行不可能
        ステップ時は不可能
        ステップ終了時も実行不可能
        防御中は実行不可能
        */

        if (
            !m_model->Check(MotionName::Taiki) &&
            //!m_model->Check(MotionName::Attack1) &&
            //!m_model->Check(MotionName::Attack2) &&
            //!m_model->Check(MotionName::Attack3) &&
            //!m_model->Check(MotionName::Attack4) &&
            //!m_model->Check(MotionName::Attack5) &&
            //!m_model->Check(MotionName::Attack5) &&
            !m_model->Check(MotionName::StepEnd) &&
            !m_model->Check(MotionName::Step) &&
            !m_Step &&
            !m_model->Check(MotionName::Defense)
            )
        {
            m_model->Play(MotionName::Step);
            m_model->SetAnimationUpdateFlame(2);
            m_Step = true;
            m_Landing = false;
            m_AttackEnd = false;
            m_AttackCount = 0;
            return true;
        }

        return false;
    }
    bool SetAnimation::CheckStep()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::Step);
    }
    bool SetAnimation::StepEnd()
    {
        if (m_model == nullptr)return false;

        /*
        条件

        ステップは終了中ではない
        ステップが実行後であった
        */

        if (m_Step && !m_model->Check(MotionName::StepEnd))
        {
            m_model->Play(MotionName::StepEnd);
            m_model->SetAnimationUpdateFlame(8);
            m_Step = false;
            return true;
        }
        return false;
    }
    bool SetAnimation::CheckStepEnd()
    {
        if (m_model == nullptr)return false;
        return m_model->Check(MotionName::StepEnd);
    }
    bool SetAnimation::Attack()
    {
        if (m_model == nullptr)return false;

        /*
        条件1

        以下の処理が実行中の場合終了

        ステップ時
        空中にいる
        防御中
        */

        if (m_model->Check(MotionName::Step) ||
            m_model->Check(MotionName::Landing) ||
            m_model->Check(MotionName::Defense))return false;

        /*
        条件7

        攻撃アニメーションが実行中である場合終了
        */
        if (
            m_model->Check(MotionName::Attack1) ||
            m_model->Check(MotionName::Attack2) ||
            m_model->Check(MotionName::Attack3) ||
            m_model->Check(MotionName::Attack4) ||
            m_model->Check(MotionName::Attack5) 
            )
        {
            return false;
        }

        /*
        条件2

        初撃判定時
        */
        if (m_AttackCount == 0 && !m_model->Check(MotionName::Attack1))
        {
            m_model->Play(MotionName::Attack1);
            m_model->SetAnimationUpdateFlame(4);
            m_AttackCount++;
            m_AttackEnd = false;
            return true;
        }

        /*
        条件3

        2撃目
        */
        if (m_AttackCount == 1 && !m_model->Check(MotionName::Attack2))
        {
            m_model->Play(MotionName::Attack2);
            m_model->SetAnimationUpdateFlame(4);
            m_AttackCount++;
            m_AttackEnd = false;
            return true;
        }

        /*
        条件4

        3撃目
        */
        if (m_AttackCount == 2 && !m_model->Check(MotionName::Attack3))
        {
            m_model->Play(MotionName::Attack3);
            m_model->SetAnimationUpdateFlame(4);
            m_AttackCount++;
            m_AttackEnd = false;
            return true;
        }

        /*
        条件5

        4撃目
        */
        if (m_AttackCount == 3 && !m_model->Check(MotionName::Attack4))
        {
            m_model->Play(MotionName::Attack4);
            m_model->SetAnimationUpdateFlame(4);
            m_AttackCount++;
            m_AttackEnd = false;
            return true;
        }

        /*
        条件6

        5撃目
        */
        if (m_AttackCount == 4 && !m_model->Check(MotionName::Attack5))
        {
            m_model->Play(MotionName::Attack5);
            m_model->SetAnimationUpdateFlame(4);
            m_AttackCount++;
            m_AttackEnd = false;
            return true;
        }

        // 攻撃モーションが全てスルーされた
        return false;
    }
    bool SetAnimation::CheckAttack()
    {
        if (m_model == nullptr)return false;
        return
            m_model->Check(MotionName::Attack1) ||
            m_model->Check(MotionName::Attack2) ||
            m_model->Check(MotionName::Attack3) ||
            m_model->Check(MotionName::Attack4) ||
            m_model->Check(MotionName::Attack5)
            ? true : false;
    }
    bool SetAnimation::CheckAttackEnd()
    {
        if (m_model == nullptr)return false;
        return
            m_model->Check(MotionName::AttackEnd1) ||
            m_model->Check(MotionName::AttackEnd2) ||
            m_model->Check(MotionName::AttackEnd3) ||
            m_model->Check(MotionName::AttackEnd4) ||
            m_model->Check(MotionName::AttackEnd5)
            ? true : false;
    }
}