//==========================================================================
// プレイヤー [Player.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace player
{
    //==========================================================================
    //
    // class  : Player
    // Content: プレイヤー 
    //
    //==========================================================================
    class Player : public mslib::ObjectManager
    {
    public:
        Player();
        ~Player();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;

        /**
        @brief 手オブジェクトの取得
        */
        mslib::transform::Transform * GetHandObject();
    private:
        /**
        @brief 入力処理
        */
        void InputProcessing();
        /**
        @brief キーボード
        */
        void Keyboard();
        /**
        @brief コントローラー
        */
        void Controller();
        /**
        @brief コリジョンの処理
        */
        void Collider();
        /**
        @brief モデルコントローラー
        */
        void ModelController();

        void Jump();

        void Step();

        void Setting();

        void Attack();

        void Defense();

        void WeaponInput();
    private:
        mslib::object::Object * m_player; // モデル
        mslib::xinput::XInput * m_controller; // コントローラー
        D3DXVECTOR3 m_input_param; // 入力値の保持
        void * m_jump; // ジャンプ
        void * m_step; // ステップ
        void * m_CameraObj; // カメラ
		void * m_AttackBox; // 攻撃有効範囲
		void * m_AttackEffect; // 攻撃エフェクト
        int m_jump_caunt; // ジャンプカウント
        void * m_PlayerAnimation; // プレイヤーのアニメーション
        std::vector<mslib::object::Effekseer*> m_effe;
        mslib::transform::Transform * m_HandObject; // 手のオブジェクト
        mslib::ObjectManager * m_WeaponManager; // 武器オブジェクト
        int m_set_count; // 登録までのカウント
        void * m_function;
		int m_se_label_attack;
		int m_se_label_move;
		int m_se_label_landing;
		int m_move_se_count;
    };
}