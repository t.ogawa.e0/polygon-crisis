//==========================================================================
// プレイヤーマネージャー [PlayerManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace player
{
    class PlayerManager : public mslib::component::Component, public mslib::initializer::Initializer
    {
    public:
        PlayerManager();
        ~PlayerManager();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief プレイヤーの取得
        */
        static std::list<PlayerManager*> & GetAllPlayer();

        mslib::transform::Transform * GetPlayer();
    protected:
        static std::list<PlayerManager*> m_player; // プレイヤー
        mslib::transform::Transform * m_obj; // プレイヤー
    };
}