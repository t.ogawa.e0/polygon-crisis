//=============================================================================
// resource file path
// 2019/03/06
// 01:03:04
//=============================================================================
#pragma once

//=============================================================================
// filename extension [.png]
//=============================================================================

#ifndef RESOURCE_aura3_type2_png
#define RESOURCE_aura3_type2_png "resource/Effekseer/Texture/aura3_type2.png"
#endif // !RESOURCE_aura3_type2_png
#ifndef RESOURCE_LaserMain02_png
#define RESOURCE_LaserMain02_png "resource/Effekseer/Texture/LaserMain02.png"
#endif // !RESOURCE_LaserMain02_png
#ifndef RESOURCE_Line01_png
#define RESOURCE_Line01_png "resource/Effekseer/Texture/Line01.png"
#endif // !RESOURCE_Line01_png
#ifndef RESOURCE_Particle01_png
#define RESOURCE_Particle01_png "resource/Effekseer/Texture/Particle01.png"
#endif // !RESOURCE_Particle01_png
#ifndef RESOURCE_Particle02_png
#define RESOURCE_Particle02_png "resource/Effekseer/Texture/Particle02.png"
#endif // !RESOURCE_Particle02_png
#ifndef RESOURCE_Particle_Soft_png
#define RESOURCE_Particle_Soft_png "resource/Effekseer/Texture/Particle_Soft.png"
#endif // !RESOURCE_Particle_Soft_png

//=============================================================================
// filename extension [.DDS]
//=============================================================================

#ifndef RESOURCE_End_DDS
#define RESOURCE_End_DDS "resource/texture/UI/End.DDS"
#endif // !RESOURCE_End_DDS
#ifndef RESOURCE_EndFrame_DDS
#define RESOURCE_EndFrame_DDS "resource/texture/UI/EndFrame.DDS"
#endif // !RESOURCE_EndFrame_DDS
#ifndef RESOURCE_Exit_DDS
#define RESOURCE_Exit_DDS "resource/texture/UI/Exit.DDS"
#endif // !RESOURCE_Exit_DDS
#ifndef RESOURCE_ExitFrame_DDS
#define RESOURCE_ExitFrame_DDS "resource/texture/UI/ExitFrame.DDS"
#endif // !RESOURCE_ExitFrame_DDS
#ifndef RESOURCE_GameClear_DDS
#define RESOURCE_GameClear_DDS "resource/texture/UI/GameClear.DDS"
#endif // !RESOURCE_GameClear_DDS
#ifndef RESOURCE_GameOver_DDS
#define RESOURCE_GameOver_DDS "resource/texture/UI/GameOver.DDS"
#endif // !RESOURCE_GameOver_DDS
#ifndef RESOURCE_LifeGauge_DDS
#define RESOURCE_LifeGauge_DDS "resource/texture/UI/LifeGauge.DDS"
#endif // !RESOURCE_LifeGauge_DDS
#ifndef RESOURCE_LifeUI_DDS
#define RESOURCE_LifeUI_DDS "resource/texture/UI/LifeUI.DDS"
#endif // !RESOURCE_LifeUI_DDS
#ifndef RESOURCE_LoadTex_DDS
#define RESOURCE_LoadTex_DDS "resource/texture/UI/LoadTex.DDS"
#endif // !RESOURCE_LoadTex_DDS
#ifndef RESOURCE_NowLoading_DDS
#define RESOURCE_NowLoading_DDS "resource/texture/UI/NowLoading.DDS"
#endif // !RESOURCE_NowLoading_DDS
#ifndef RESOURCE_Number_DDS
#define RESOURCE_Number_DDS "resource/texture/UI/Number.DDS"
#endif // !RESOURCE_Number_DDS
#ifndef RESOURCE_Pause_DDS
#define RESOURCE_Pause_DDS "resource/texture/UI/Pause.DDS"
#endif // !RESOURCE_Pause_DDS
#ifndef RESOURCE_PauseBotton_DDS
#define RESOURCE_PauseBotton_DDS "resource/texture/UI/PauseBotton.DDS"
#endif // !RESOURCE_PauseBotton_DDS
#ifndef RESOURCE_Play_DDS
#define RESOURCE_Play_DDS "resource/texture/UI/Play.DDS"
#endif // !RESOURCE_Play_DDS
#ifndef RESOURCE_PlayFrame_DDS
#define RESOURCE_PlayFrame_DDS "resource/texture/UI/PlayFrame.DDS"
#endif // !RESOURCE_PlayFrame_DDS
#ifndef RESOURCE_PressAnyKey_DDS
#define RESOURCE_PressAnyKey_DDS "resource/texture/UI/PressAnyKey.DDS"
#endif // !RESOURCE_PressAnyKey_DDS
#ifndef RESOURCE_Restart_DDS
#define RESOURCE_Restart_DDS "resource/texture/UI/Restart.DDS"
#endif // !RESOURCE_Restart_DDS
#ifndef RESOURCE_RestartFrame_DDS
#define RESOURCE_RestartFrame_DDS "resource/texture/UI/RestartFrame.DDS"
#endif // !RESOURCE_RestartFrame_DDS
#ifndef RESOURCE_ThankYouForPlaying_DDS
#define RESOURCE_ThankYouForPlaying_DDS "resource/texture/UI/ThankYouForPlaying.DDS"
#endif // !RESOURCE_ThankYouForPlaying_DDS
#ifndef RESOURCE_title_DDS
#define RESOURCE_title_DDS "resource/texture/UI/title.DDS"
#endif // !RESOURCE_title_DDS
#ifndef RESOURCE_White1Pixel_DDS
#define RESOURCE_White1Pixel_DDS "resource/texture/UI/White1Pixel.DDS"
#endif // !RESOURCE_White1Pixel_DDS
#ifndef RESOURCE_xBox_Controller_DDS
#define RESOURCE_xBox_Controller_DDS "resource/texture/UI/xBox_Controller.DDS"
#endif // !RESOURCE_xBox_Controller_DDS
#ifndef RESOURCE_Opening1_DDS
#define RESOURCE_Opening1_DDS "resource/texture/Opening/Opening1.DDS"
#endif // !RESOURCE_Opening1_DDS
#ifndef RESOURCE_Opening2_DDS
#define RESOURCE_Opening2_DDS "resource/texture/Opening/Opening2.DDS"
#endif // !RESOURCE_Opening2_DDS
#ifndef RESOURCE_Opening3_DDS
#define RESOURCE_Opening3_DDS "resource/texture/Opening/Opening3.DDS"
#endif // !RESOURCE_Opening3_DDS
#ifndef RESOURCE_skydome_DDS
#define RESOURCE_skydome_DDS "resource/texture/Game/skydome.DDS"
#endif // !RESOURCE_skydome_DDS

//=============================================================================
// filename extension [.x]
//=============================================================================

#ifndef RESOURCE_Weapon_x
#define RESOURCE_Weapon_x "resource/Model/Weapon/Weapon.x"
#endif // !RESOURCE_Weapon_x
#ifndef RESOURCE_UAZ469_Body_x
#define RESOURCE_UAZ469_Body_x "resource/Model/UAZ469/UAZ469_Body.x"
#endif // !RESOURCE_UAZ469_Body_x
#ifndef RESOURCE_UAZ469_ChildLeg_x
#define RESOURCE_UAZ469_ChildLeg_x "resource/Model/UAZ469/UAZ469_ChildLeg.x"
#endif // !RESOURCE_UAZ469_ChildLeg_x
#ifndef RESOURCE_UAZ469_ParentLeg_x
#define RESOURCE_UAZ469_ParentLeg_x "resource/Model/UAZ469/UAZ469_ParentLeg.x"
#endif // !RESOURCE_UAZ469_ParentLeg_x
#ifndef RESOURCE_Torchka_x
#define RESOURCE_Torchka_x "resource/Model/Torchka/Torchka.x"
#endif // !RESOURCE_Torchka_x
#ifndef RESOURCE_Nucleus1_x
#define RESOURCE_Nucleus1_x "resource/Model/Nucleus/Nucleus1.x"
#endif // !RESOURCE_Nucleus1_x
#ifndef RESOURCE_Nucleus2_x
#define RESOURCE_Nucleus2_x "resource/Model/Nucleus/Nucleus2.x"
#endif // !RESOURCE_Nucleus2_x
#ifndef RESOURCE_Human_Arms_Child_L_x
#define RESOURCE_Human_Arms_Child_L_x "resource/Model/Human/Human_Arms_Child_L.x"
#endif // !RESOURCE_Human_Arms_Child_L_x
#ifndef RESOURCE_Human_Arms_Child_R_x
#define RESOURCE_Human_Arms_Child_R_x "resource/Model/Human/Human_Arms_Child_R.x"
#endif // !RESOURCE_Human_Arms_Child_R_x
#ifndef RESOURCE_Human_Arms_Parent_L_x
#define RESOURCE_Human_Arms_Parent_L_x "resource/Model/Human/Human_Arms_Parent_L.x"
#endif // !RESOURCE_Human_Arms_Parent_L_x
#ifndef RESOURCE_Human_Arms_Parent_R_x
#define RESOURCE_Human_Arms_Parent_R_x "resource/Model/Human/Human_Arms_Parent_R.x"
#endif // !RESOURCE_Human_Arms_Parent_R_x
#ifndef RESOURCE_Human_Body_x
#define RESOURCE_Human_Body_x "resource/Model/Human/Human_Body.x"
#endif // !RESOURCE_Human_Body_x
#ifndef RESOURCE_Human_Calf_L_x
#define RESOURCE_Human_Calf_L_x "resource/Model/Human/Human_Calf_L.x"
#endif // !RESOURCE_Human_Calf_L_x
#ifndef RESOURCE_Human_Calf_R_x
#define RESOURCE_Human_Calf_R_x "resource/Model/Human/Human_Calf_R.x"
#endif // !RESOURCE_Human_Calf_R_x
#ifndef RESOURCE_Human_Foot_L_x
#define RESOURCE_Human_Foot_L_x "resource/Model/Human/Human_Foot_L.x"
#endif // !RESOURCE_Human_Foot_L_x
#ifndef RESOURCE_Human_Foot_R_x
#define RESOURCE_Human_Foot_R_x "resource/Model/Human/Human_Foot_R.x"
#endif // !RESOURCE_Human_Foot_R_x
#ifndef RESOURCE_Human_Hand_L_x
#define RESOURCE_Human_Hand_L_x "resource/Model/Human/Human_Hand_L.x"
#endif // !RESOURCE_Human_Hand_L_x
#ifndef RESOURCE_Human_Hand_R_x
#define RESOURCE_Human_Hand_R_x "resource/Model/Human/Human_Hand_R.x"
#endif // !RESOURCE_Human_Hand_R_x
#ifndef RESOURCE_Human_Head_x
#define RESOURCE_Human_Head_x "resource/Model/Human/Human_Head.x"
#endif // !RESOURCE_Human_Head_x
#ifndef RESOURCE_Human_Thigh_L_x
#define RESOURCE_Human_Thigh_L_x "resource/Model/Human/Human_Thigh_L.x"
#endif // !RESOURCE_Human_Thigh_L_x
#ifndef RESOURCE_Human_Thigh_R_x
#define RESOURCE_Human_Thigh_R_x "resource/Model/Human/Human_Thigh_R.x"
#endif // !RESOURCE_Human_Thigh_R_x
#ifndef RESOURCE_Floor_x
#define RESOURCE_Floor_x "resource/Model/Floor/Floor.x"
#endif // !RESOURCE_Floor_x
#ifndef RESOURCE_Box_x
#define RESOURCE_Box_x "resource/Model/Box/Box.x"
#endif // !RESOURCE_Box_x

//=============================================================================
// filename extension [.wav]
//=============================================================================

#ifndef RESOURCE_battle_se_wav
#define RESOURCE_battle_se_wav "resource/sound/se/battle_se.wav"
#endif // !RESOURCE_battle_se_wav
#ifndef RESOURCE_footstep01_se_wav
#define RESOURCE_footstep01_se_wav "resource/sound/se/footstep01_se.wav"
#endif // !RESOURCE_footstep01_se_wav
#ifndef RESOURCE_footstep02_se_wav
#define RESOURCE_footstep02_se_wav "resource/sound/se/footstep02_se.wav"
#endif // !RESOURCE_footstep02_se_wav
#ifndef RESOURCE_select_se_wav
#define RESOURCE_select_se_wav "resource/sound/se/select_se.wav"
#endif // !RESOURCE_select_se_wav
#ifndef RESOURCE_battle_bgm_wav
#define RESOURCE_battle_bgm_wav "resource/sound/bgm/battle_bgm.wav"
#endif // !RESOURCE_battle_bgm_wav
#ifndef RESOURCE_title_bgm_wav
#define RESOURCE_title_bgm_wav "resource/sound/bgm/title_bgm.wav"
#endif // !RESOURCE_title_bgm_wav

//=============================================================================
// filename extension [.bmp]
//=============================================================================

#ifndef RESOURCE_Header_bmp
#define RESOURCE_Header_bmp "resource/texture/Header/Header.bmp"
#endif // !RESOURCE_Header_bmp

//=============================================================================
// filename extension [.fx]
//=============================================================================

#ifndef RESOURCE_PerPixelLighting_fx
#define RESOURCE_PerPixelLighting_fx "resource/Shader/PerPixelLighting.fx"
#endif // !RESOURCE_PerPixelLighting_fx
#ifndef RESOURCE_Shadow_fx
#define RESOURCE_Shadow_fx "resource/Shader/Shadow.fx"
#endif // !RESOURCE_Shadow_fx
#ifndef RESOURCE_Sprite2D_fx
#define RESOURCE_Sprite2D_fx "resource/Shader/Sprite2D.fx"
#endif // !RESOURCE_Sprite2D_fx
#ifndef RESOURCE_ToonShader_fx
#define RESOURCE_ToonShader_fx "resource/Shader/ToonShader.fx"
#endif // !RESOURCE_ToonShader_fx
#ifndef RESOURCE_Wave_fx
#define RESOURCE_Wave_fx "resource/Shader/Wave.fx"
#endif // !RESOURCE_Wave_fx

//=============================================================================
// filename extension [.efk]
//=============================================================================

#ifndef RESOURCE_DamageEffect_efk
#define RESOURCE_DamageEffect_efk "resource/Effekseer/DamageEffect.efk"
#endif // !RESOURCE_DamageEffect_efk
#ifndef RESOURCE_Defense_efk
#define RESOURCE_Defense_efk "resource/Effekseer/Defense.efk"
#endif // !RESOURCE_Defense_efk
#ifndef RESOURCE_flare_efk
#define RESOURCE_flare_efk "resource/Effekseer/flare.efk"
#endif // !RESOURCE_flare_efk
#ifndef RESOURCE_GoldenEffect_efk
#define RESOURCE_GoldenEffect_efk "resource/Effekseer/GoldenEffect.efk"
#endif // !RESOURCE_GoldenEffect_efk
#ifndef RESOURCE_Jump_efk
#define RESOURCE_Jump_efk "resource/Effekseer/Jump.efk"
#endif // !RESOURCE_Jump_efk
#ifndef RESOURCE_NucleusAttack1_efk
#define RESOURCE_NucleusAttack1_efk "resource/Effekseer/NucleusAttack1.efk"
#endif // !RESOURCE_NucleusAttack1_efk
#ifndef RESOURCE_smoke_efk
#define RESOURCE_smoke_efk "resource/Effekseer/smoke.efk"
#endif // !RESOURCE_smoke_efk
#ifndef RESOURCE_Sword_efk
#define RESOURCE_Sword_efk "resource/Effekseer/Sword.efk"
#endif // !RESOURCE_Sword_efk
#ifndef RESOURCE_TorchkaAttack1_efk
#define RESOURCE_TorchkaAttack1_efk "resource/Effekseer/TorchkaAttack1.efk"
#endif // !RESOURCE_TorchkaAttack1_efk
#ifndef RESOURCE_Trajectory_efk
#define RESOURCE_Trajectory_efk "resource/Effekseer/Trajectory.efk"
#endif // !RESOURCE_Trajectory_efk
