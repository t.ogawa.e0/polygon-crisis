**Polygon Crisis**
====

**Overview**
====

*2019/2月に完成した作品です。<br>制作期間はフレームワークの制作を含めて4ヶ月です。*

**[内容物]-----------------------------------------------------------------------------<br>**
*  [Application Framework](Application%20Framework) - Polygon Crisisを制作時に作成したフレームワークです。
*  [Execution Data](Execution%20Data) - Polygon Crisisの実行データです。
*  [Develop](Develop) - Polygon Crisisの開発データです。
*  [DirectX SDK Installer](DirectX%20SDK%20Installer) - DirectX SDK の インストールプログラムです。

**[[Execution Data](Execution%20Data)]-----------------------------------------------------------------------------<br>**
*<br>フォルダ内部には以下のデータが含まれています。*
* [Polygon Crisis.exe](Execution Data/Polygon Crisis.exe) - ゲームの実行データです。
* [Polygon Crisis MapEditor Mode.exe](Execution Data/Polygon Crisis MapEditor Mode.exe) - ゲーム内のステージを設計したエディタモードです。
* [Polygon Crisis MotionEditor Mode.exe](Execution Data/Polygon Crisis MotionEditor Mode.exe) - プレイヤーのモーションを作成したモーションエディタモードです。
* [Polygon Crisis Debug Mode.exe](Execution Data/Polygon Crisis Debug Mode.exe) - デバッグ機能を残したゲームの実行データです。
* [resource](Execution Data/resource) - ゲームの実行に必要なデータが含まれています。

**[[Develop](Develop)]-----------------------------------------------------------------------------<br>**
*<br>このゲームの開発データです。<br>*
*統合開発環境は [Visual Studio 2017](https://my.visualstudio.com/Downloads?q=Visual%20Studio%202017&pgroup=) です。<br>*
*コンパイルには [DirectX 9.0c](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) が必要です。<br>*
*<br>この開発データには以下の外部ライブラリが含まれています。<br>*
*  [Effekseer](https://effekseer.github.io/)
*  [ImGui](https://github.com/ocornut/imgui)

**[[Application Framework](Application%20Framework)]-----------------------------------------------------------------------------<br>**
*<br>このゲームの開発で使用したフレームワークです。<br>*
*統合開発環境は [Visual Studio 2017](https://my.visualstudio.com/Downloads?q=Visual%20Studio%202017&pgroup=) です。<br>*
*コンパイルには [DirectX 9.0c](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) が必要です。<br>*
*<br>このフレームワークには以下の外部ライブラリが含まれています。<br>*
*  [Effekseer](https://effekseer.github.io/)
*  [ImGui](https://github.com/ocornut/imgui)

**[[DirectX SDK Installer](DirectX%20SDK%20Installer)]-----------------------------------------------------------------------------<br>**
*<br>ゲームの実行に失敗した際に、このインストーラーを使用してください。<br>*
*実行に必要最低限なデータをインストールします。<br>*


**Requirement**
====

*  [IDE (Integrated Development Environment)](https://my.visualstudio.com/Downloads?q=Visual%20Studio%202017&pgroup=) - Visual Studio 2017
*  [End-User Runtime](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) - DirectX 9.0c End-User Runtime
*  [Effekseer](https://effekseer.github.io/) - Effekseer
*  [ImGui](https://github.com/ocornut/imgui) - ImGui

##

![VisualStudio2017](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/VisualStudio2017.png)
![c++](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/C%EF%BC%8B%EF%BC%8B.png)
![DirectX9SDK](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/DirectX9SDK.png)
![Effekseer](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/Effekseer.png)